<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/module/luceed_sync_dash.twig */
class __TwigTemplate_fed1455faf817a4f88d868a127df72a706451ecbef0e5924696fb96a8e10bfef extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
    <div class=\"page-header\">
        <div class=\"container-fluid\">
            <div class=\"pull-right\">
            </div>
            <h1>";
        // line 7
        echo ($context["title"] ?? null);
        echo "</h1>
            <ul class=\"breadcrumb\">
                ";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 10
            echo "                    <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 10);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 10);
            echo "</a></li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "            </ul>
        </div>
    </div>
    <div class=\"container-fluid\">
        <div id=\"alert_placeholder\"></div>
        ";
        // line 17
        if (($context["error_warning"] ?? null)) {
            // line 18
            echo "            <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo ($context["error_warning"] ?? null);
            echo "
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
            </div>
        ";
        }
        // line 22
        echo "        ";
        if (($context["success"] ?? null)) {
            // line 23
            echo "            <div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ";
            echo ($context["success"] ?? null);
            echo "
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
            </div>
        ";
        }
        // line 27
        echo "        <div class=\"row\">
            <div class=\"col-sm-12 col-md-7\">
                <div class=\"panel panel-default\">
                    <div class=\"panel-heading\">
                        <h3 class=\"panel-title\"><i class=\"fa fa-list\"></i> ";
        // line 31
        echo ($context["text_products_import_panel"] ?? null);
        echo "</h3>
                    </div>
                    <div class=\"panel-body form-horizontal\">
                        <div class=\"form-group\">
                            <div class=\"col-sm-4\">
                                <a href=\"javascript:void(0)\" id=\"import-new-products\" data-toggle=\"tooltip\" title=\"";
        // line 36
        echo ($context["help_products_import"] ?? null);
        echo "\" class=\"btn btn-default\">";
        echo ($context["btn_products_import"] ?? null);
        echo "</a>
                            </div>
                            <p class=\"col-sm-8\" style=\"margin-top: 9px;\">";
        // line 38
        echo ($context["help_products_import"] ?? null);
        echo "</p>
                        </div>

                        <div class=\"form-group\">
                            <div class=\"col-sm-4\">
                                <a href=\"javascript:void(0)\" id=\"check-active-min\" data-toggle=\"tooltip\" title=\"";
        // line 43
        echo ($context["help_products_active"] ?? null);
        echo "\" class=\"btn btn-default\" style=\"margin-top: 9px;\">";
        echo ($context["btn_products_active"] ?? null);
        echo "</a>
                            </div>
                            <p class=\"col-sm-8\" style=\"margin-top: 9px;\">";
        // line 45
        echo ($context["help_products_active"] ?? null);
        echo "</p>
                        </div>

                        <div class=\"form-group\">
                            <div class=\"col-sm-4\">
                                <a href=\"javascript:void(0)\" id=\"update-prices-quantities\" data-toggle=\"tooltip\" title=\"";
        // line 50
        echo ($context["help_products_update_pq"] ?? null);
        echo "\" class=\"btn btn-default\">";
        echo ($context["btn_products_update_pq"] ?? null);
        echo "</a>
                            </div>
                            <p class=\"col-sm-8\" style=\"margin-top: 9px;\">";
        // line 52
        echo ($context["help_products_update_pq"] ?? null);
        echo "</p>
                        </div>

                        <div class=\"form-group\">
                            <div class=\"col-sm-4\">
                                <a href=\"javascript:void(0)\" id=\"update-prices\" data-toggle=\"tooltip\" title=\"";
        // line 57
        echo ($context["help_products_update_p"] ?? null);
        echo "\" class=\"btn btn-default\">";
        echo ($context["btn_products_update_p"] ?? null);
        echo "</a>
                            </div>
                            <p class=\"col-sm-8\" style=\"margin-top: 9px;\">";
        // line 59
        echo ($context["help_products_update_p"] ?? null);
        echo "</p>
                        </div>

                        <div class=\"form-group\">
                            <div class=\"col-sm-4\">
                                <a href=\"javascript:void(0)\" id=\"update-quantities\" data-toggle=\"tooltip\" title=\"";
        // line 64
        echo ($context["help_products_update_q"] ?? null);
        echo "\" class=\"btn btn-default\">";
        echo ($context["btn_products_update_q"] ?? null);
        echo "</a>
                            </div>
                            <p class=\"col-sm-8\" style=\"margin-top: 9px;\">";
        // line 66
        echo ($context["help_products_update_q"] ?? null);
        echo "</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class=\"col-sm-12 col-md-5\">
                <div class=\"panel panel-default\">
                    <div class=\"panel-heading\">
                        <h3 class=\"panel-title\"><i class=\"fa fa-list\"></i> ";
        // line 75
        echo ($context["text_categories_import_panel"] ?? null);
        echo "</h3>
                    </div>
                    <div class=\"panel-body form-horizontal\">
                        <div class=\"form-group\">
                            <div class=\"col-sm-5\">
                                <a href=\"javascript:void(0)\" id=\"import-new-categories\" data-toggle=\"tooltip\" title=\"";
        // line 80
        echo ($context["help_categories_import"] ?? null);
        echo "\" class=\"btn btn-default\">";
        echo ($context["btn_categories_import"] ?? null);
        echo "</a>
                            </div>
                            <p class=\"col-sm-7\">";
        // line 82
        echo ($context["help_categories_import"] ?? null);
        echo "</p>
                        </div>
                    </div>
                </div>

                <div class=\"panel panel-default\">
                    <div class=\"panel-heading\">
                        <h3 class=\"panel-title\"><i class=\"fa fa-list\"></i> ";
        // line 89
        echo ($context["text_manufacturer_import_panel"] ?? null);
        echo "</h3>
                    </div>
                    <div class=\"panel-body form-horizontal\">
                        <div class=\"form-group\">
                            <div class=\"col-sm-5\">
                                <a href=\"javascript:void(0)\" id=\"import-new-manufacturer\" data-toggle=\"tooltip\" title=\"";
        // line 94
        echo ($context["help_manufacturer_import"] ?? null);
        echo "\" class=\"btn btn-default\">";
        echo ($context["btn_manufacturer_import"] ?? null);
        echo "</a>
                            </div>
                            <p class=\"col-sm-7\">";
        // line 96
        echo ($context["help_manufacturer_import"] ?? null);
        echo "</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>

    </style>

    <script type=\"text/javascript\">
        \$(() => {
            \$('#import-new-categories')     .on('click', e => { callApi(e, 'importCategories'); });
            \$('#import-new-manufacturer')   .on('click', e => { callApi(e, 'importManufacturers'); });
            \$('#import-new-products')       .on('click', e => { callApi(e, 'importProducts'); });
            \$('#check-active-min')          .on('click', e => { callApi(e, 'checkMinQty'); });
            \$('#update-prices-quantities')  .on('click', e => { callApi(e, 'updatePricesAndQuantities'); });
            \$('#update-prices')             .on('click', e => { callApi(e, 'updatePrices'); });
            \$('#update-quantities')         .on('click', e => { callApi(e, 'updateQuantities'); });
        });


        function callApi(e, path) {
            console.log(e);

            \$('#' + e.currentTarget.id).removeClass('btn-default');
            \$('#' + e.currentTarget.id).addClass('btn-warning');
            \$('#' + e.currentTarget.id).text('Provjeravanje Stanja..')

            \$.ajax({
                url: 'index.php?route=extension/module/luceed_sync/' + path + '&user_token=";
        // line 127
        echo ($context["user_token"] ?? null);
        echo "',
                dataType: 'json',
                success: function(json) {
                    checkResult(json);
                }
            });
        }


        function checkResult(result) {
            if (result.status == 200) {
                AG_alert.success(result.message);
            }
            if (result.status == 300) {
                AG_alert.warning(result.message);
            }
        }


        function hideAlert() {
            setTimeout(() => { \$('#alert_placeholder').html(''); }, 6300);
        }


        AG_alert = () => {}
        AG_alert.success = (message) => {
            \$('#alert_placeholder').html('<div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ' + message + '<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
            hideAlert();
        }
        AG_alert.warning = (message) => {
            \$('#alert_placeholder').html('<div class=\"alert alert-warning alert-dismissible\"><i class=\"fa fa-warning\"></i> ' + message + '<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>')
            hideAlert();
        }

    </script>
</div>
";
        // line 163
        echo ($context["footer"] ?? null);
        echo "
";
    }

    public function getTemplateName()
    {
        return "extension/module/luceed_sync_dash.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  301 => 163,  262 => 127,  228 => 96,  221 => 94,  213 => 89,  203 => 82,  196 => 80,  188 => 75,  176 => 66,  169 => 64,  161 => 59,  154 => 57,  146 => 52,  139 => 50,  131 => 45,  124 => 43,  116 => 38,  109 => 36,  101 => 31,  95 => 27,  87 => 23,  84 => 22,  76 => 18,  74 => 17,  67 => 12,  56 => 10,  52 => 9,  47 => 7,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "extension/module/luceed_sync_dash.twig", "");
    }
}
