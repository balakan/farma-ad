<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/module/luceed_sync_dash.twig */
class __TwigTemplate_d4dc078f317088599050f5c6ed6a41fa98b9fd4f50f5dd3dad439d6244fe5abb extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<style>
    .darker-bg {
        color: white !important;
        background-color: #929292 !important;
    }
</style>
";
        // line 7
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
    <div class=\"page-header\">
        <div class=\"container-fluid\">
            <div class=\"pull-right\">
            </div>
            <h1>";
        // line 13
        echo ($context["title"] ?? null);
        echo "</h1>
            <ul class=\"breadcrumb\">
                ";
        // line 15
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 16
            echo "                    <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 16);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 16);
            echo "</a></li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "            </ul>
        </div>
    </div>
    <div class=\"container-fluid\">
        <div id=\"alert_placeholder\"></div>
        ";
        // line 23
        if (($context["error_warning"] ?? null)) {
            // line 24
            echo "            <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo ($context["error_warning"] ?? null);
            echo "
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
            </div>
        ";
        }
        // line 28
        echo "        ";
        if (($context["success"] ?? null)) {
            // line 29
            echo "            <div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ";
            echo ($context["success"] ?? null);
            echo "
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
            </div>
        ";
        }
        // line 33
        echo "        <div class=\"row\">
            <div class=\"col-sm-12 col-md-7\">
                <div class=\"panel panel-default\">
                    <div class=\"panel-heading darker-bg\">
                        <h3 class=\"panel-title\"><i class=\"fa fa-list\"></i> ";
        // line 37
        echo ($context["text_products_import_panel"] ?? null);
        echo "</h3>
                    </div>
                    <div class=\"panel-body form-horizontal\">
                        <div class=\"form-group\">
                            <div class=\"col-sm-4\">
                                <a href=\"javascript:void(0)\" id=\"import-new-products\" data-toggle=\"tooltip\" title=\"";
        // line 42
        echo ($context["btn_products_import"] ?? null);
        echo "\" class=\"btn btn-info\">";
        echo ($context["btn_products_import"] ?? null);
        echo "</a>
                            </div>
                            <p class=\"col-sm-8\" style=\"margin-top: 9px;\">";
        // line 44
        echo ($context["help_products_import"] ?? null);
        echo "</p>
                        </div>

                        <div class=\"form-group\">
                            <div class=\"col-sm-4\">
                                <a href=\"javascript:void(0)\" id=\"check-active-min\" data-toggle=\"tooltip\" title=\"";
        // line 49
        echo ($context["btn_products_active"] ?? null);
        echo "\" class=\"btn btn-info\" style=\"margin-top: 9px;\">";
        echo ($context["btn_products_active"] ?? null);
        echo "</a>
                            </div>
                            <p class=\"col-sm-8\" style=\"margin-top: 9px;\">";
        // line 51
        echo ($context["help_products_active"] ?? null);
        echo "</p>
                        </div>

                        <div class=\"form-group\">
                            <div class=\"col-sm-4\">
                                <a href=\"javascript:void(0)\" id=\"update-prices-quantities\" data-toggle=\"tooltip\" title=\"";
        // line 56
        echo ($context["btn_products_update_pq"] ?? null);
        echo "\" class=\"btn btn-info\">";
        echo ($context["btn_products_update_pq"] ?? null);
        echo "</a>
                            </div>
                            <p class=\"col-sm-8\" style=\"margin-top: 9px;\">";
        // line 58
        echo ($context["help_products_update_pq"] ?? null);
        echo "</p>
                        </div>

                        <div class=\"form-group\">
                            <div class=\"col-sm-4\">
                                <a href=\"javascript:void(0)\" id=\"update-prices\" data-toggle=\"tooltip\" title=\"";
        // line 63
        echo ($context["btn_products_update_p"] ?? null);
        echo "\" class=\"btn btn-info\">";
        echo ($context["btn_products_update_p"] ?? null);
        echo "</a>
                            </div>
                            <p class=\"col-sm-8\" style=\"margin-top: 9px;\">";
        // line 65
        echo ($context["help_products_update_p"] ?? null);
        echo "</p>
                        </div>

                        <div class=\"form-group\">
                            <div class=\"col-sm-4\">
                                <a href=\"javascript:void(0)\" id=\"update-quantities\" data-toggle=\"tooltip\" title=\"";
        // line 70
        echo ($context["btn_products_update_q"] ?? null);
        echo "\" class=\"btn btn-info\">";
        echo ($context["btn_products_update_q"] ?? null);
        echo "</a>
                            </div>
                            <p class=\"col-sm-8\" style=\"margin-top: 9px;\">";
        // line 72
        echo ($context["help_products_update_q"] ?? null);
        echo "</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class=\"col-sm-12 col-md-5\">
                <div class=\"panel panel-default\">
                    <div class=\"panel-heading darker-bg\">
                        <h3 class=\"panel-title\"><i class=\"fa fa-list\"></i> ";
        // line 81
        echo ($context["text_categories_import_panel"] ?? null);
        echo "</h3>
                    </div>
                    <div class=\"panel-body form-horizontal\">
                        <div class=\"form-group\">
                            <div class=\"col-sm-5\">
                                <a href=\"javascript:void(0)\" id=\"import-new-categories\" data-toggle=\"tooltip\" title=\"";
        // line 86
        echo ($context["btn_categories_import"] ?? null);
        echo "\" class=\"btn btn-info\">";
        echo ($context["btn_categories_import"] ?? null);
        echo "</a>
                            </div>
                            <p class=\"col-sm-7\">";
        // line 88
        echo ($context["help_categories_import"] ?? null);
        echo "</p>
                        </div>
                    </div>
                </div>

                <div class=\"panel panel-default\">
                    <div class=\"panel-heading darker-bg\">
                        <h3 class=\"panel-title\"><i class=\"fa fa-list\"></i> ";
        // line 95
        echo ($context["text_manufacturer_import_panel"] ?? null);
        echo "</h3>
                    </div>
                    <div class=\"panel-body form-horizontal\">
                        <div class=\"form-group\">
                            <div class=\"col-sm-5\">
                                <a href=\"javascript:void(0)\" id=\"import-new-manufacturer\" data-toggle=\"tooltip\" title=\"";
        // line 100
        echo ($context["btn_manufacturer_import"] ?? null);
        echo "\" class=\"btn btn-info\">";
        echo ($context["btn_manufacturer_import"] ?? null);
        echo "</a>
                            </div>
                            <p class=\"col-sm-7\">";
        // line 102
        echo ($context["help_manufacturer_import"] ?? null);
        echo "</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type=\"text/javascript\">
        \$(() => {
            \$('#import-new-categories')     .on('click', e => { callApi(e, 'importCategories'); });
            \$('#import-new-manufacturer')   .on('click', e => { callApi(e, 'importManufacturers'); });
            \$('#import-new-products')       .on('click', e => { callApi(e, 'importProducts'); });
            \$('#check-active-min')          .on('click', e => { callApi(e, 'checkMinQty'); });
            \$('#update-prices-quantities')  .on('click', e => { callApi(e, 'updatePricesAndQuantities'); });
            \$('#update-prices')             .on('click', e => { callApi(e, 'updatePrices'); });
            \$('#update-quantities')         .on('click', e => { callApi(e, 'updateQuantities'); });
        });


        /**
         *
         */
        function callApi(e, path) {
            let target = e.currentTarget;
            let text = target.text;

            lockTarget(path.charAt(0), \$('#' + target.id));

            \$.ajax({
                url: 'index.php?route=extension/module/luceed_sync/' + path + '&user_token=";
        // line 132
        echo ($context["user_token"] ?? null);
        echo "',
                dataType: 'json',
                success: function(json) {
                    checkResult(json);
                    releseTarget(text, \$('#' + target.id));
                }
            });
        }

        /**
         *
         */
        function lockTarget(target, btn) {
            btn.removeClass('btn-info');
            btn.addClass('btn-warning');

            if (target == 'i') {
                btn.html('";
        // line 149
        echo ($context["btn_importing"] ?? null);
        echo "');
            }
            if (target == 'u') {
                btn.html('";
        // line 152
        echo ($context["btn_updating"] ?? null);
        echo "');
            }
            if (target == 'c') {
                btn.html('";
        // line 155
        echo ($context["btn_checking"] ?? null);
        echo "');
            }
        }

        /**
         *
         */
        function releseTarget(text, btn) {
            btn.removeClass('btn-warning');
            btn.addClass('btn-info');
            btn.text(text);
        }

        /**
         *
         */
        function checkResult(result) {
            if (result.status == 200) {
                AG_alert.success(result.message);
            }
            if (result.status == 300) {
                AG_alert.warning(result.message);
            }
        }

        /**
         *
         */
        function hideAlert() {
            setTimeout(() => { \$('#alert_placeholder').html(''); }, 6300);
        }


        AG_alert = () => {}
        /**
         * Success alert.
         * @param message
         */
        AG_alert.success = (message) => {
            \$('#alert_placeholder').html('<div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ' + message + '<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
            hideAlert();
        }
        /**
         * Warning or error alert.
         * @param message
         */
        AG_alert.warning = (message) => {
            \$('#alert_placeholder').html('<div class=\"alert alert-warning alert-dismissible\"><i class=\"fa fa-warning\"></i> ' + message + '<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>')
            hideAlert();
        }

    </script>
</div>
";
        // line 208
        echo ($context["footer"] ?? null);
        echo "
";
    }

    public function getTemplateName()
    {
        return "extension/module/luceed_sync_dash.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  357 => 208,  301 => 155,  295 => 152,  289 => 149,  269 => 132,  236 => 102,  229 => 100,  221 => 95,  211 => 88,  204 => 86,  196 => 81,  184 => 72,  177 => 70,  169 => 65,  162 => 63,  154 => 58,  147 => 56,  139 => 51,  132 => 49,  124 => 44,  117 => 42,  109 => 37,  103 => 33,  95 => 29,  92 => 28,  84 => 24,  82 => 23,  75 => 18,  64 => 16,  60 => 15,  55 => 13,  45 => 7,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "extension/module/luceed_sync_dash.twig", "");
    }
}
