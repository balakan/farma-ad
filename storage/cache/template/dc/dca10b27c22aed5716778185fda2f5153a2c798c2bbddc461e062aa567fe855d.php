<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* basel/template/product/single_product.twig */
class __TwigTemplate_02864a00db77114db278beb83c50f19bcf7b3368f5d22fb6c5d4bcc6440172d6 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"item single-product\">
    <div class=\"image\" ";
        // line 2
        if ((($context["columns"] ?? null) == "list")) {
            echo "style=\"width:";
            echo ($context["img_width"] ?? null);
            echo "px\"";
        }
        echo ">
        <a href=\"";
        // line 3
        echo twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "href", [], "any", false, false, false, 3);
        echo "\">
            <img src=\"";
        // line 4
        echo twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "thumb", [], "any", false, false, false, 4);
        echo "\" alt=\"";
        echo twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "name", [], "any", false, false, false, 4);
        echo "\" title=\"";
        echo twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "name", [], "any", false, false, false, 4);
        echo "\" loading=\"lazy\" />
            ";
        // line 5
        if (twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "thumb2", [], "any", false, false, false, 5)) {
            // line 6
            echo "                <img class=\"thumb2\" src=\"";
            echo twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "thumb2", [], "any", false, false, false, 6);
            echo "\" alt=\"";
            echo twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "name", [], "any", false, false, false, 6);
            echo "\" loading=\"lazy\" title=\"";
            echo twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "name", [], "any", false, false, false, 6);
            echo "\" />
            ";
        }
        // line 8
        echo "        </a>
        ";
        // line 9
        if (((twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "price", [], "any", false, false, false, 9) && twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "special", [], "any", false, false, false, 9)) && ($context["salebadge_status"] ?? null))) {
            // line 10
            echo "            <div class=\"sale-counter id";
            echo twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "product_id", [], "any", false, false, false, 10);
            echo "\"></div>
            <span class=\"badge sale_badge\"><i>";
            // line 11
            echo twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "sale_badge", [], "any", false, false, false, 11);
            echo "</i></span>
        ";
        }
        // line 13
        echo "        ";
        if (twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "new_label", [], "any", false, false, false, 13)) {
            // line 14
            echo "            <span class=\"badge new_badge\"><i>";
            echo ($context["basel_text_new"] ?? null);
            echo "</i></span>
        ";
        }
        // line 16
        echo "        ";
        if (((twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "quantity", [], "any", false, false, false, 16) < 1) && ($context["stock_badge_status"] ?? null))) {
            // line 17
            echo "            <span class=\"badge out_of_stock_badge\"><i>";
            echo ($context["basel_text_out_of_stock"] ?? null);
            echo "</i></span>
            ";
            // line 18
            $context["button_cart"] = ($context["basel_text_out_of_stock"] ?? null);
            // line 19
            echo "        ";
        } else {
            // line 20
            echo "            ";
            $context["button_cart"] = ($context["default_button_cart"] ?? null);
            // line 21
            echo "        ";
        }
        // line 22
        echo "        <a class=\"img-overlay\" href=\"";
        echo twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "href", [], "any", false, false, false, 22);
        echo "\"></a>
        <div class=\"btn-center catalog_hide\"><a class=\"btn btn-light-outline btn-thin\" onclick=\"cart.add('";
        // line 23
        echo twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "product_id", [], "any", false, false, false, 23);
        echo "', '";
        echo twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "minimum", [], "any", false, false, false, 23);
        echo "');\">";
        echo ($context["button_cart"] ?? null);
        echo "</a></div>
        <div class=\"icons-wrapper\">
            <a class=\"icon is_wishlist\" data-toggle=\"tooltip\" data-placement=\"";
        // line 25
        echo ($context["tooltip_align"] ?? null);
        echo "\"  data-title=\"";
        echo ($context["button_wishlist"] ?? null);
        echo "\" onclick=\"wishlist.add('";
        echo twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "product_id", [], "any", false, false, false, 25);
        echo "');\"><span class=\"icon-heart\"></span></a>
            <a class=\"icon is_compare\" onclick=\"compare.add('";
        // line 26
        echo twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "product_id", [], "any", false, false, false, 26);
        echo "');\" data-toggle=\"tooltip\" data-placement=\"";
        echo ($context["tooltip_align"] ?? null);
        echo "\" data-title=\"";
        echo ($context["button_compare"] ?? null);
        echo "\"><span class=\"icon-refresh\"></span></a>
            <a class=\"icon is_quickview hidden-xs\" onclick=\"quickview('";
        // line 27
        echo twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "product_id", [], "any", false, false, false, 27);
        echo "');\" data-toggle=\"tooltip\" data-placement=\"";
        echo ($context["tooltip_align"] ?? null);
        echo "\" data-title=\"";
        echo ($context["basel_button_quickview"] ?? null);
        echo "\"><span class=\"icon-magnifier-add\"></span></a>
        </div> <!-- .icons-wrapper -->
    </div><!-- .image ends -->
    <div class=\"caption\">
        <a class=\"product-name\" href=\"";
        // line 31
        echo twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "href", [], "any", false, false, false, 31);
        echo "\">";
        echo twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "name", [], "any", false, false, false, 31);
        echo "</a>
        ";
        // line 32
        if (twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "rating", [], "any", false, false, false, 32)) {
            // line 33
            echo "            <div class=\"rating\">
                <span class=\"rating_stars rating r";
            // line 34
            echo twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "rating", [], "any", false, false, false, 34);
            echo "\">
                    <i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>
                </span>
            </div>
        ";
        }
        // line 39
        echo "        <div class=\"price-wrapper\">
            <div class=\"row\">
                <div class=\"col-lg-4\">
                    ";
        // line 42
        if (twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "price", [], "any", false, false, false, 42)) {
            // line 43
            echo "
                        <div class=\"price\">

                            ";
            // line 46
            if (twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "special", [], "any", false, false, false, 46)) {
                // line 47
                echo "                                <span class=\"price-old price\">";
                echo twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "price", [], "any", false, false, false, 47);
                echo "</span>
                                <span class=\"price-new\">";
                // line 48
                echo twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "special", [], "any", false, false, false, 48);
                echo "</span>
                            ";
            } else {
                // line 50
                echo "                                <span class=\"price\">Cijena</span>
                                <span>";
                // line 51
                echo twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "price", [], "any", false, false, false, 51);
                echo "</span>
                            ";
            }
            // line 53
            echo "                            ";
            if (twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "tax", [], "any", false, false, false, 53)) {
                // line 54
                echo "                                <span class=\"price-tax\">";
                echo ($context["text_tax"] ?? null);
                echo " ";
                echo twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "tax", [], "any", false, false, false, 54);
                echo "</span>
                            ";
            }
            // line 56
            echo "                        </div><!-- .price -->
                    ";
        }
        // line 58
        echo "                </div>
                <p class=\"description\">";
        // line 59
        if (twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "description", [], "any", false, false, false, 59)) {
            echo twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "description", [], "any", false, false, false, 59);
        }
        echo "</p>
                <div class=\"col-lg-8\">

                    <div class=\"input-group\">
                        ";
        // line 63
        if (twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "options", [], "any", false, false, false, 63)) {
            // line 64
            echo "                            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "options", [], "any", false, false, false, 64));
            foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
                // line 65
                echo "                                ";
                if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 65) == "select")) {
                    // line 66
                    echo "
                                    <select name=\"option[";
                    // line 67
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 67);
                    echo "]\" id=\"input-option";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 67);
                    echo "\" class=\"form-control num";
                    echo twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "product_id", [], "any", false, false, false, 67);
                    echo "\">
                                        ";
                    // line 68
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["option"], "product_option_value", [], "any", false, false, false, 68));
                    foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                        // line 69
                        echo "                                            <option value=\"";
                        echo twig_get_attribute($this->env, $this->source, $context["option_value"], "product_option_value_id", [], "any", false, false, false, 69);
                        echo "\">";
                        echo twig_get_attribute($this->env, $this->source, $context["option_value"], "name", [], "any", false, false, false, 69);
                        echo "</option>
                                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 71
                    echo "                                        <option value=\"+\">+</option>
                                    </select>

                                    <script>
                                        \$('[name=\"option[";
                    // line 75
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 75);
                    echo "]\"]').otherDropdown({classes:'form-control', value:'+', placeholder:'Upiši', max: '";
                    echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "scale", [], "any", false, false, false, 75), "max", [], "any", false, false, false, 75);
                    echo "' })
                                    </script>

                                    ";
                    // line 78
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["option"], "product_option_value", [], "any", false, false, false, 78));
                    foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                        // line 79
                        echo "                                        ";
                        if ((twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 79) == 0)) {
                            // line 80
                            echo "                                            <input type=\"hidden\" id=\"input-default";
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 80);
                            echo "\" value=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "product_option_value_id", [], "any", false, false, false, 80);
                            echo "\">
                                        ";
                        }
                        // line 82
                        echo "                                    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 83
                    echo "
                                    <span class=\"input-group-btn\">
                                        <button class=\"btn btn-neutral btn-green\" onclick=\"javascript:void(0); kosarica_dodaj(";
                    // line 85
                    echo twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "product_id", [], "any", false, false, false, 85);
                    echo ", '";
                    echo twig_escape_filter($this->env, json_encode(twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "scale", [], "any", false, false, false, 85)));
                    echo "', '";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 85);
                    echo "');\">
                                            <span class=\"global-cart\"></span>
                                        </button>
                                    </span>

                                ";
                }
                // line 91
                echo "                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo " <!-- foreach option -->
                        ";
        }
        // line 93
        echo "                    </div><!-- /input-group -->

                </div>
            </div>

        </div><!-- .price-wrapper -->
        <div class=\"plain-links\">
            <a class=\"icon is_wishlist link-hover-color\" onclick=\"wishlist.add('";
        // line 100
        echo twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "product_id", [], "any", false, false, false, 100);
        echo "');\"><span class=\"icon-heart\"></span> ";
        echo ($context["button_wishlist"] ?? null);
        echo "</a>
            <a class=\"icon is_compare link-hover-color\" onclick=\"compare.add('";
        // line 101
        echo twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "product_id", [], "any", false, false, false, 101);
        echo "');\"><span class=\"icon-refresh\"></span> ";
        echo ($context["button_compare"] ?? null);
        echo "</a>
            <a class=\"icon is_quickview link-hover-color\" onclick=\"quickview('";
        // line 102
        echo twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "product_id", [], "any", false, false, false, 102);
        echo "');\"><span class=\"icon-magnifier-add\"></span> ";
        echo ($context["basel_button_quickview"] ?? null);
        echo "</a>
        </div><!-- .plain-links-->
    </div><!-- .caption-->
    ";
        // line 105
        if ((twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "sale_end_date", [], "any", false, false, false, 105) && ($context["countdown_status"] ?? null))) {
            // line 106
            echo "        <script>
            \$(function() {
                \$(\".sale-counter.id";
            // line 108
            echo twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "product_id", [], "any", false, false, false, 108);
            echo "\").countdown(\"";
            echo twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "sale_end_date", [], "any", false, false, false, 108);
            echo "\").on('update.countdown', function(event) {
                    var \$this = \$(this).html(event.strftime(''
                        + '<div>'
                        + '%D<i>";
            // line 111
            echo ($context["basel_text_days"] ?? null);
            echo "</i></div><div>'
                        + '%H <i>";
            // line 112
            echo ($context["basel_text_hours"] ?? null);
            echo "</i></div><div>'
                        + '%M <i>";
            // line 113
            echo ($context["basel_text_mins"] ?? null);
            echo "</i></div><div>'
                        + '%S <i>";
            // line 114
            echo ($context["basel_text_secs"] ?? null);
            echo "</i></div></div>'));
                });
            });
        </script>
    ";
        }
        // line 119
        echo "
    <script type=\"application/javascript\">
        function kosarica_dodaj(product_id, scale, select) {
            let selected = \$('#input-option' + select)[0];
            let default_id = \$('#input-default' + select)[0].value;

            if (selected.selectedIndex > JSON.parse(scale).items.length) {
                cart.add(product_id, Number(selected.value), 'option[' + product_id + ']=' + default_id);
            } else {
                cart.add(product_id, 1, 'option[' + product_id + ']=' + selected.value);
            }
        }
    </script>
</div><!-- .single-product ends -->";
    }

    public function getTemplateName()
    {
        return "basel/template/product/single_product.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  394 => 119,  386 => 114,  382 => 113,  378 => 112,  374 => 111,  366 => 108,  362 => 106,  360 => 105,  352 => 102,  346 => 101,  340 => 100,  331 => 93,  322 => 91,  309 => 85,  305 => 83,  299 => 82,  291 => 80,  288 => 79,  284 => 78,  276 => 75,  270 => 71,  259 => 69,  255 => 68,  247 => 67,  244 => 66,  241 => 65,  236 => 64,  234 => 63,  225 => 59,  222 => 58,  218 => 56,  210 => 54,  207 => 53,  202 => 51,  199 => 50,  194 => 48,  189 => 47,  187 => 46,  182 => 43,  180 => 42,  175 => 39,  167 => 34,  164 => 33,  162 => 32,  156 => 31,  145 => 27,  137 => 26,  129 => 25,  120 => 23,  115 => 22,  112 => 21,  109 => 20,  106 => 19,  104 => 18,  99 => 17,  96 => 16,  90 => 14,  87 => 13,  82 => 11,  77 => 10,  75 => 9,  72 => 8,  62 => 6,  60 => 5,  52 => 4,  48 => 3,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "basel/template/product/single_product.twig", "/Users/alive/Sites/Agmedia/Live/kaonekad/upload/catalog/view/theme/basel/template/product/single_product.twig");
    }
}
