<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* basel/template/extension/quickcheckout/shipping_method.twig */
class __TwigTemplate_1f904e28c82c7416d68cc61bdb61423486d7e5cb0d023d7ef444d577e4721cf5 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["error_warning"] ?? null)) {
            // line 2
            echo "    <div class=\"alert alert-danger\">";
            echo ($context["error_warning"] ?? null);
            echo "</div>
";
        }
        // line 4
        if (($context["shipping_methods"] ?? null)) {
            // line 5
            echo "    <p>";
            echo ($context["text_shipping_method"] ?? null);
            echo "</p>
    ";
            // line 6
            if (($context["shipping"] ?? null)) {
                // line 7
                echo "        <table class=\"table\">
            ";
                // line 8
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["shipping_methods"] ?? null));
                foreach ($context['_seq'] as $context["key"] => $context["shipping_method"]) {
                    // line 9
                    echo "                ";
                    if ( !twig_get_attribute($this->env, $this->source, $context["shipping_method"], "error", [], "any", false, false, false, 9)) {
                        // line 10
                        echo "                    ";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["shipping_method"], "quote", [], "any", false, false, false, 10));
                        foreach ($context['_seq'] as $context["_key"] => $context["quote"]) {
                            // line 11
                            echo "                        <tr>
                            <td>";
                            // line 12
                            if ((twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 12) == ($context["code"] ?? null))) {
                                // line 13
                                echo "                                    <input type=\"radio\" name=\"shipping_method\" value=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 13);
                                echo "\" id=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 13);
                                echo "\" checked=\"checked\" />
                                ";
                            } else {
                                // line 15
                                echo "                                    <input type=\"radio\" name=\"shipping_method\" value=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 15);
                                echo "\" id=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 15);
                                echo "\" />
                                ";
                            }
                            // line 16
                            echo "</td>
                            <td style=\"width:100%;padding-left:10px;\">
                                <label for=\"";
                            // line 18
                            echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 18);
                            echo "\">
                                    ";
                            // line 19
                            if ((($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["shipping_logo"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4[$context["key"]] ?? null) : null)) {
                                // line 20
                                echo "                                        <img src=\"";
                                echo (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = ($context["shipping_logo"] ?? null)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144[$context["key"]] ?? null) : null);
                                echo "\" alt=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["shipping_method"], "title", [], "any", false, false, false, 20);
                                echo "\" title=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["shipping_method"], "title", [], "any", false, false, false, 20);
                                echo "\" />
                                    ";
                            }
                            // line 22
                            echo "                                    ";
                            echo twig_get_attribute($this->env, $this->source, $context["quote"], "title", [], "any", false, false, false, 22);
                            echo "</label></td>
                            <td style=\"text-align: right;\"><label for=\"";
                            // line 23
                            echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 23);
                            echo "\">";
                            echo twig_get_attribute($this->env, $this->source, $context["quote"], "text", [], "any", false, false, false, 23);
                            echo "</label></td>
                        </tr>
                    ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['quote'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 26
                        echo "                ";
                    } else {
                        // line 27
                        echo "                    <tr>
                        <td colspan=\"3\"><div class=\"error\">";
                        // line 28
                        echo twig_get_attribute($this->env, $this->source, $context["shipping_method"], "error", [], "any", false, false, false, 28);
                        echo "</div></td>
                    </tr>
                ";
                    }
                    // line 31
                    echo "            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['shipping_method'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 32
                echo "        </table>
    ";
            } else {
                // line 34
                echo "        <select class=\"form-control\" name=\"shipping_method\">
            ";
                // line 35
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["shipping_methods"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["shipping_method"]) {
                    // line 36
                    echo "                ";
                    if ( !twig_get_attribute($this->env, $this->source, $context["shipping_method"], "error", [], "any", false, false, false, 36)) {
                        // line 37
                        echo "                    ";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["shipping_method"], "quote", [], "any", false, false, false, 37));
                        foreach ($context['_seq'] as $context["_key"] => $context["quote"]) {
                            // line 38
                            echo "                        ";
                            if ((twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 38) == ($context["code"] ?? null))) {
                                // line 39
                                echo "                            ";
                                $context["code"] = twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 39);
                                // line 40
                                echo "                            ";
                                $context["exists"] = true;
                                // line 41
                                echo "                        <option value=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 41);
                                echo "\" selected=\"selected\">
                        ";
                            } else {
                                // line 43
                                echo "                            <option value=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 43);
                                echo "\">
                        ";
                            }
                            // line 45
                            echo "                        ";
                            echo twig_get_attribute($this->env, $this->source, $context["quote"], "title", [], "any", false, false, false, 45);
                            echo "&nbsp;&nbsp;(";
                            echo twig_get_attribute($this->env, $this->source, $context["quote"], "text", [], "any", false, false, false, 45);
                            echo ")</option>
                    ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['quote'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 47
                        echo "                ";
                    }
                    // line 48
                    echo "            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['shipping_method'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 49
                echo "        </select><br />
    ";
            }
            // line 51
            echo "
    <select class=\"form-control\" name=\"shipping_method_select\">
        <option value=\"1\" selected=\"selected\">18/21h</option>
    </select><br />

    <br />
";
        }
        // line 58
        if ((($context["delivery"] ?? null) && (( !($context["delivery_delivery_time"] ?? null) || (($context["delivery_delivery_time"] ?? null) == "1")) || (($context["delivery_delivery_time"] ?? null) == "3")))) {
            // line 59
            echo "    <div";
            echo ((($context["delivery_required"] ?? null)) ? (" class=\"required\"") : (""));
            echo ">
        <label class=\"control-label\"><strong>";
            // line 60
            echo ($context["text_delivery"] ?? null);
            echo "</strong></label>
        ";
            // line 61
            if ((($context["delivery_delivery_time"] ?? null) == "1")) {
                // line 62
                echo "            <input type=\"text\" name=\"delivery_date\" value=\"";
                echo ($context["delivery_date"] ?? null);
                echo "\" class=\"form-control date\" readonly=\"true\" style=\"background:#ffffff;\" />
        ";
            } else {
                // line 64
                echo "            <input type=\"text\" name=\"delivery_date\" value=\"";
                echo ($context["delivery_date"] ?? null);
                echo "\" class=\"form-control date\" readonly=\"true\" style=\"background:#ffffff;\" />
        ";
            }
            // line 66
            echo "        ";
            if ((($context["delivery_delivery_time"] ?? null) == "3")) {
                echo "<br />
            <select name=\"delivery_time\" class=\"form-control\">";
                // line 67
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["delivery_times"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["quickcheckout_delivery_time"]) {
                    // line 68
                    echo "                    ";
                    if ((($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = $context["quickcheckout_delivery_time"]) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b[($context["language_id"] ?? null)] ?? null) : null)) {
                        // line 69
                        echo "                        ";
                        if ((($context["delivery_time"] ?? null) == (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = $context["quickcheckout_delivery_time"]) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002[($context["language_id"] ?? null)] ?? null) : null))) {
                            // line 70
                            echo "                            <option value=\"";
                            echo (($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = $context["quickcheckout_delivery_time"]) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4[($context["language_id"] ?? null)] ?? null) : null);
                            echo "\" selected=\"selected\">";
                            echo (($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = $context["quickcheckout_delivery_time"]) && is_array($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) || $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 instanceof ArrayAccess ? ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666[($context["language_id"] ?? null)] ?? null) : null);
                            echo "</option>
                        ";
                        } else {
                            // line 72
                            echo "                            <option value=\"";
                            echo (($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = $context["quickcheckout_delivery_time"]) && is_array($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e) || $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e instanceof ArrayAccess ? ($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e[($context["language_id"] ?? null)] ?? null) : null);
                            echo "\">";
                            echo (($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 = $context["quickcheckout_delivery_time"]) && is_array($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52) || $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 instanceof ArrayAccess ? ($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52[($context["language_id"] ?? null)] ?? null) : null);
                            echo "</option>
                        ";
                        }
                        // line 74
                        echo "                    ";
                    }
                    // line 75
                    echo "                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['quickcheckout_delivery_time'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                echo "</select>
        ";
            }
            // line 77
            echo "    </div>
";
        } elseif ((        // line 78
($context["delivery_delivery_time"] ?? null) && (($context["delivery_delivery_time"] ?? null) == "2"))) {
            // line 79
            echo "    <input type=\"text\" name=\"delivery_date\" value=\"\" class=\"hide\" />
    <select name=\"delivery_time\" class=\"hide\"><option value=\"\"></option></select>
    <strong>";
            // line 81
            echo ($context["text_estimated_delivery"] ?? null);
            echo "</strong><br />
    ";
            // line 82
            echo ($context["estimated_delivery"] ?? null);
            echo "<br />
    ";
            // line 83
            echo ($context["estimated_delivery_time"] ?? null);
            echo "
";
        } else {
            // line 85
            echo "    <input type=\"text\" name=\"delivery_date\" value=\"\" class=\"hide\" />
    <select name=\"delivery_time\" class=\"hide\"><option value=\"\"></option></select>
";
        }
        // line 88
        echo "
<script type=\"text/javascript\"><!--
  \$('#shipping-method input[name=\\'shipping_method\\'], #shipping-method select[name=\\'shipping_method\\']').on('change', function() {
      ";
        // line 91
        if ( !($context["logged"] ?? null)) {
            // line 92
            echo "    if (\$('#payment-address input[name=\\'shipping_address\\']:checked').val()) {
      var post_data = \$('#payment-address input[type=\\'text\\'], #payment-address input[type=\\'checkbox\\']:checked, #payment-address input[type=\\'radio\\']:checked, #payment-address input[type=\\'hidden\\'], #payment-address select, #shipping-method input[type=\\'text\\'], #shipping-method input[type=\\'checkbox\\']:checked, #shipping-method input[type=\\'radio\\']:checked, #shipping-method input[type=\\'hidden\\'], #shipping-method select, #shipping-method textarea');
    } else {
      var post_data = \$('#shipping-address input[type=\\'text\\'], #shipping-address input[type=\\'checkbox\\']:checked, #shipping-address input[type=\\'radio\\']:checked, #shipping-address input[type=\\'hidden\\'], #shipping-address select, #shipping-method input[type=\\'text\\'], #shipping-method input[type=\\'checkbox\\']:checked, #shipping-method input[type=\\'radio\\']:checked, #shipping-method input[type=\\'hidden\\'], #shipping-method select, #shipping-method textarea');
    }

    \$.ajax({
      url: 'index.php?route=extension/quickcheckout/shipping_method/set',
      type: 'post',
      data: post_data,
      dataType: 'html',
      cache: false,
      success: function(html) {
          ";
            // line 105
            if (($context["cart"] ?? null)) {
                // line 106
                echo "        loadCart();
          ";
            }
            // line 108
            echo "
          ";
            // line 109
            if (($context["shipping_reload"] ?? null)) {
                // line 110
                echo "        reloadPaymentMethod();
          ";
            }
            // line 112
            echo "      },
        ";
            // line 113
            if (($context["debug"] ?? null)) {
                // line 114
                echo "      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
      }
        ";
            }
            // line 118
            echo "    });
      ";
        } else {
            // line 120
            echo "    if (\$('#shipping-address input[name=\\'shipping_address\\']:checked').val() == 'new') {
      var url = 'index.php?route=extension/quickcheckout/shipping_method/set';
      var post_data = \$('#shipping-address input[type=\\'text\\'], #shipping-address input[type=\\'checkbox\\']:checked, #shipping-address input[type=\\'radio\\']:checked, #shipping-address input[type=\\'hidden\\'], #shipping-address select, #shipping-method input[type=\\'text\\'], #shipping-method input[type=\\'checkbox\\']:checked, #shipping-method input[type=\\'radio\\']:checked, #shipping-method input[type=\\'hidden\\'], #shipping-method select, #shipping-method textarea');
    } else {
      var url = 'index.php?route=extension/quickcheckout/shipping_method/set&address_id=' + \$('#shipping-address select[name=\\'address_id\\']').val();
      var post_data = \$('#shipping-method input[type=\\'text\\'], #shipping-method input[type=\\'checkbox\\']:checked, #shipping-method input[type=\\'radio\\']:checked, #shipping-method input[type=\\'hidden\\'], #shipping-method select, #shipping-method textarea');
    }

    \$.ajax({
      url: url,
      type: 'post',
      data: post_data,
      dataType: 'html',
      cache: false,
      success: function(html) {
          ";
            // line 135
            if (($context["cart"] ?? null)) {
                // line 136
                echo "        loadCart();
          ";
            }
            // line 138
            echo "
          ";
            // line 139
            if (($context["shipping_reload"] ?? null)) {
                // line 140
                echo "        if (\$('#payment-address input[name=\\'payment_address\\']').val() == 'new') {
          reloadPaymentMethod();
        } else {
          reloadPaymentMethodById(\$('#payment-address select[name=\\'address_id\\']').val());
        }
          ";
            }
            // line 146
            echo "      },
        ";
            // line 147
            if (($context["debug"] ?? null)) {
                // line 148
                echo "      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
      }
        ";
            }
            // line 152
            echo "    });
      ";
        }
        // line 154
        echo "  });

  \$(document).ready(function() {
    \$('#shipping-method input[name=\\'shipping_method\\']:checked, #shipping-method select[name=\\'shipping_method\\']').trigger('change');
  });

    ";
        // line 160
        if ((($context["delivery"] ?? null) && (($context["delivery_delivery_time"] ?? null) == "1"))) {
            // line 161
            echo "  \$(document).ready(function() {
    \$('input[name=\\'delivery_date\\']').datetimepicker({
      format: 'YYYY-MM-DD HH:mm',
      minDate: '";
            // line 164
            echo ($context["delivery_min"] ?? null);
            echo "',
      maxDate: '";
            // line 165
            echo ($context["delivery_max"] ?? null);
            echo "',
      disabledDates: [";
            // line 166
            echo ($context["delivery_unavailable"] ?? null);
            echo "],
      enabledHours: [";
            // line 167
            echo ($context["hours"] ?? null);
            echo "],
      ignoreReadonly: true,
        ";
            // line 169
            if ((($context["delivery_days_of_week"] ?? null) != "")) {
                // line 170
                echo "      daysOfWeekDisabled: [";
                echo ($context["delivery_days_of_week"] ?? null);
                echo "]
        ";
            }
            // line 172
            echo "    });
  });
    ";
        } elseif ((        // line 174
($context["delivery"] ?? null) && ((($context["delivery_delivery_time"] ?? null) == "3") || (($context["delivery_delivery_time"] ?? null) == "0")))) {
            // line 175
            echo "  \$('input[name=\\'delivery_date\\']').datetimepicker({
    format: 'YYYY-MM-DD',
    minDate: '";
            // line 177
            echo ($context["delivery_min"] ?? null);
            echo "',
    maxDate: '";
            // line 178
            echo ($context["delivery_max"] ?? null);
            echo "',
    disabledDates: [";
            // line 179
            echo ($context["delivery_unavailable"] ?? null);
            echo "],
    ignoreReadonly: true,
      ";
            // line 181
            if ((($context["delivery_days_of_week"] ?? null) != "")) {
                // line 182
                echo "    daysOfWeekDisabled: [";
                echo ($context["delivery_days_of_week"] ?? null);
                echo "]
      ";
            }
            // line 184
            echo "  });
    ";
        }
        // line 186
        echo "  //--></script>";
    }

    public function getTemplateName()
    {
        return "basel/template/extension/quickcheckout/shipping_method.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  490 => 186,  486 => 184,  480 => 182,  478 => 181,  473 => 179,  469 => 178,  465 => 177,  461 => 175,  459 => 174,  455 => 172,  449 => 170,  447 => 169,  442 => 167,  438 => 166,  434 => 165,  430 => 164,  425 => 161,  423 => 160,  415 => 154,  411 => 152,  405 => 148,  403 => 147,  400 => 146,  392 => 140,  390 => 139,  387 => 138,  383 => 136,  381 => 135,  364 => 120,  360 => 118,  354 => 114,  352 => 113,  349 => 112,  345 => 110,  343 => 109,  340 => 108,  336 => 106,  334 => 105,  319 => 92,  317 => 91,  312 => 88,  307 => 85,  302 => 83,  298 => 82,  294 => 81,  290 => 79,  288 => 78,  285 => 77,  276 => 75,  273 => 74,  265 => 72,  257 => 70,  254 => 69,  251 => 68,  247 => 67,  242 => 66,  236 => 64,  230 => 62,  228 => 61,  224 => 60,  219 => 59,  217 => 58,  208 => 51,  204 => 49,  198 => 48,  195 => 47,  184 => 45,  178 => 43,  172 => 41,  169 => 40,  166 => 39,  163 => 38,  158 => 37,  155 => 36,  151 => 35,  148 => 34,  144 => 32,  138 => 31,  132 => 28,  129 => 27,  126 => 26,  115 => 23,  110 => 22,  100 => 20,  98 => 19,  94 => 18,  90 => 16,  82 => 15,  74 => 13,  72 => 12,  69 => 11,  64 => 10,  61 => 9,  57 => 8,  54 => 7,  52 => 6,  47 => 5,  45 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "basel/template/extension/quickcheckout/shipping_method.twig", "");
    }
}
