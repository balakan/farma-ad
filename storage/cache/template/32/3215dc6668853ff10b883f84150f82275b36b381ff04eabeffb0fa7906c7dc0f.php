<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/module/shipping_collector_form.twig */
class __TwigTemplate_0aa6389bdc802cfc84c4fb8da959947fe059ccfb4bf0bd83b47f853cb877a580 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <button type=\"submit\" form=\"form-module\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo ($context["button_save"] ?? null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
        <a href=\"";
        // line 7
        echo ($context["cancel"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_cancel"] ?? null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1>";
        // line 8
        echo ($context["heading_title"] ?? null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 11
            echo "        <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 11);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 11);
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">
    ";
        // line 17
        if (($context["error_warning"] ?? null)) {
            // line 18
            echo "    <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo ($context["error_warning"] ?? null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 22
        echo "    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i> ";
        // line 24
        echo ($context["text_form"] ?? null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
        <form action=\"";
        // line 27
        echo ($context["action"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-module\" class=\"form-horizontal\">
            <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-date\"><span data-toggle=\"tooltip\" title=\"";
        // line 29
        echo ($context["help_collect_date"] ?? null);
        echo "\">";
        echo ($context["entry_date"] ?? null);
        echo "</span></label>
                <div class=\"col-sm-3\">
                    <div class=\"input-group date\">
                        <input type=\"text\" name=\"collect_date\" value=\"";
        // line 32
        echo ($context["collect_date"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_date"] ?? null);
        echo "\" data-date-format=\"YYYY-MM-DD\" id=\"input-date\" class=\"form-control\" required />
                        <span class=\"input-group-btn\">
                    <button type=\"button\" class=\"btn btn-default\"><i class=\"fa fa-calendar\"></i></button>
                    </span></div>
                    ";
        // line 36
        if (($context["error_collect_date"] ?? null)) {
            // line 37
            echo "                        <p class=\"text-danger\" style=\"font-size: 10px;\">";
            echo ($context["error_collect_date"] ?? null);
            echo "</p>
                    ";
        }
        // line 39
        echo "                </div>
            </div>

            <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-time\"><span data-toggle=\"tooltip\" title=\"";
        // line 43
        echo ($context["help_collect_time"] ?? null);
        echo "\">";
        echo ($context["entry_time"] ?? null);
        echo "</span></label>
                <div class=\"col-sm-3 col-md-2 col-lg-1\">
                    <button type=\"button\" data-toggle=\"tooltip\" title=\"";
        // line 45
        echo ($context["button_save"] ?? null);
        echo "\" class=\"btn btn-default btn-block\"><i class=\"fa fa-save\"></i> 11-16h</button>
                </div>
                <div class=\"col-sm-3 col-md-2 col-lg-1\">
                    <button type=\"button\" data-toggle=\"tooltip\" title=\"";
        // line 48
        echo ($context["button_save"] ?? null);
        echo "\" class=\"btn btn-default btn-block\"><i class=\"fa fa-save\"></i> 18-21h</button>
                </div>
                <div class=\"col-sm-4 col-md-6 col-lg-8\">
                    <input type=\"text\" name=\"collect_time\" value=\"";
        // line 51
        echo ($context["collect_time"] ?? null);
        echo "\" placeholder=\"\" id=\"input-time\" class=\"form-control\" />
                </div>
            </div>

            <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-max\"><span data-toggle=\"tooltip\" title=\"";
        // line 56
        echo ($context["help_collect_max"] ?? null);
        echo "\">";
        echo ($context["entry_max"] ?? null);
        echo "</span></label>
                <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"collect_max\" value=\"";
        // line 58
        echo ($context["collect_max"] ?? null);
        echo "\" placeholder=\"\" id=\"input-max\" class=\"form-control\" />
                </div>
            </div>

            <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-collected\"><span data-toggle=\"tooltip\" title=\"";
        // line 63
        echo ($context["help_collected"] ?? null);
        echo "\">";
        echo ($context["entry_collected"] ?? null);
        echo "</span></label>
                <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"collected\" value=\"";
        // line 65
        echo ($context["collected"] ?? null);
        echo "\" placeholder=\"\" id=\"input-collected\" class=\"form-control\" />
                </div>
            </div>

            <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-status\">";
        // line 70
        echo ($context["entry_status"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                    <select name=\"status\" id=\"input-status\" class=\"form-control\">
                        ";
        // line 73
        if (($context["status"] ?? null)) {
            // line 74
            echo "                            <option value=\"1\" selected=\"selected\">";
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                            <option value=\"0\">";
            // line 75
            echo ($context["text_disabled"] ?? null);
            echo "</option>
                        ";
        } else {
            // line 77
            echo "                            <option value=\"1\">";
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                            <option value=\"0\" selected=\"selected\">";
            // line 78
            echo ($context["text_disabled"] ?? null);
            echo "</option>
                        ";
        }
        // line 80
        echo "                    </select>
                </div>
            </div>

        </form>
      </div>
    </div>
  </div>
</div>
<script type=\"text/javascript\"><!--
  \$('.date').datetimepicker({
    language: '";
        // line 91
        echo ($context["datepicker"] ?? null);
        echo "',
    pickTime: false
  });
  //--></script></div>
";
        // line 95
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "extension/module/shipping_collector_form.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  243 => 95,  236 => 91,  223 => 80,  218 => 78,  213 => 77,  208 => 75,  203 => 74,  201 => 73,  195 => 70,  187 => 65,  180 => 63,  172 => 58,  165 => 56,  157 => 51,  151 => 48,  145 => 45,  138 => 43,  132 => 39,  126 => 37,  124 => 36,  115 => 32,  107 => 29,  102 => 27,  96 => 24,  92 => 22,  84 => 18,  82 => 17,  76 => 13,  65 => 11,  61 => 10,  56 => 8,  50 => 7,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "extension/module/shipping_collector_form.twig", "");
    }
}
