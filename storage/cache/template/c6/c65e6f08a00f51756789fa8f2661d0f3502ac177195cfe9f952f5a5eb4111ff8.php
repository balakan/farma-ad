<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* basel/template/extension/quickcheckout/shipping_method.twig */
class __TwigTemplate_a7a4d9611e81ef31471cb86fb62c13b2d80f0c792b8ddd932ad6adeb618de5b3 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["error_warning"] ?? null)) {
            // line 2
            echo "    <div class=\"alert alert-danger\">";
            echo ($context["error_warning"] ?? null);
            echo "</div>
";
        }
        // line 4
        if (($context["shipping_methods"] ?? null)) {
            // line 5
            echo "    <p>";
            echo ($context["text_shipping_method"] ?? null);
            echo "</p>
    ";
            // line 6
            if (($context["shipping"] ?? null)) {
                // line 7
                echo "        <table class=\"table\">
            ";
                // line 8
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["shipping_methods"] ?? null));
                foreach ($context['_seq'] as $context["key"] => $context["shipping_method"]) {
                    // line 9
                    echo "                ";
                    if ( !twig_get_attribute($this->env, $this->source, $context["shipping_method"], "error", [], "any", false, false, false, 9)) {
                        // line 10
                        echo "                    ";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["shipping_method"], "quote", [], "any", false, false, false, 10));
                        foreach ($context['_seq'] as $context["_key"] => $context["quote"]) {
                            // line 11
                            echo "                        <tr>
                            <td>";
                            // line 12
                            if ((twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 12) == ($context["code"] ?? null))) {
                                // line 13
                                echo "                                    <input type=\"radio\" name=\"shipping_method\" value=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 13);
                                echo "\" id=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 13);
                                echo "\" checked=\"checked\" />
                                ";
                            } else {
                                // line 15
                                echo "                                    <input type=\"radio\" name=\"shipping_method\" value=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 15);
                                echo "\" id=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 15);
                                echo "\" />
                                ";
                            }
                            // line 16
                            echo "</td>
                            <td style=\"width:100%;padding-left:10px;\">
                                <label for=\"";
                            // line 18
                            echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 18);
                            echo "\">
                                    ";
                            // line 19
                            if ((($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["shipping_logo"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4[$context["key"]] ?? null) : null)) {
                                // line 20
                                echo "                                        <img src=\"";
                                echo (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = ($context["shipping_logo"] ?? null)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144[$context["key"]] ?? null) : null);
                                echo "\" alt=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["shipping_method"], "title", [], "any", false, false, false, 20);
                                echo "\" title=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["shipping_method"], "title", [], "any", false, false, false, 20);
                                echo "\" />
                                    ";
                            }
                            // line 22
                            echo "                                    ";
                            echo twig_get_attribute($this->env, $this->source, $context["quote"], "title", [], "any", false, false, false, 22);
                            echo "</label></td>
                            <td style=\"text-align: right;\"><label for=\"";
                            // line 23
                            echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 23);
                            echo "\">";
                            echo twig_get_attribute($this->env, $this->source, $context["quote"], "text", [], "any", false, false, false, 23);
                            echo "</label></td>
                        </tr>
                    ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['quote'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 26
                        echo "
                    <tr id=\"collector-select\" class=\"hide\">
                        <td colspan=\"3\">
                            <select name=\"collector_pick\" class=\"form-control\" id=\"input-collector-pick\">
                                <option value=\"NULL\">Test select...</option>
                                ";
                        // line 38
                        echo "                            </select>
                        </td>
                    </tr>

                ";
                    } else {
                        // line 43
                        echo "                    <tr>
                        <td colspan=\"3\"><div class=\"error\">";
                        // line 44
                        echo twig_get_attribute($this->env, $this->source, $context["shipping_method"], "error", [], "any", false, false, false, 44);
                        echo "</div></td>
                    </tr>
                ";
                    }
                    // line 47
                    echo "            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['shipping_method'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 48
                echo "        </table>
    ";
            } else {
                // line 50
                echo "        <select class=\"form-control\" name=\"shipping_method\">
            ";
                // line 51
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["shipping_methods"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["shipping_method"]) {
                    // line 52
                    echo "                ";
                    if ( !twig_get_attribute($this->env, $this->source, $context["shipping_method"], "error", [], "any", false, false, false, 52)) {
                        // line 53
                        echo "                    ";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["shipping_method"], "quote", [], "any", false, false, false, 53));
                        foreach ($context['_seq'] as $context["_key"] => $context["quote"]) {
                            // line 54
                            echo "                        ";
                            if ((twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 54) == ($context["code"] ?? null))) {
                                // line 55
                                echo "                            ";
                                $context["code"] = twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 55);
                                // line 56
                                echo "                            ";
                                $context["exists"] = true;
                                // line 57
                                echo "                        <option value=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 57);
                                echo "\" selected=\"selected\">
                        ";
                            } else {
                                // line 59
                                echo "                            <option value=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 59);
                                echo "\">
                        ";
                            }
                            // line 61
                            echo "                        ";
                            echo twig_get_attribute($this->env, $this->source, $context["quote"], "title", [], "any", false, false, false, 61);
                            echo "&nbsp;&nbsp;(";
                            echo twig_get_attribute($this->env, $this->source, $context["quote"], "text", [], "any", false, false, false, 61);
                            echo ")</option>
                    ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['quote'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 63
                        echo "                ";
                    }
                    // line 64
                    echo "            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['shipping_method'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 65
                echo "        </select><br />
    ";
            }
            // line 67
            echo "    <br />
";
        }
        // line 69
        if ((($context["delivery"] ?? null) && (( !($context["delivery_delivery_time"] ?? null) || (($context["delivery_delivery_time"] ?? null) == "1")) || (($context["delivery_delivery_time"] ?? null) == "3")))) {
            // line 70
            echo "    <div";
            echo ((($context["delivery_required"] ?? null)) ? (" class=\"required\"") : (""));
            echo ">
        <label class=\"control-label\"><strong>";
            // line 71
            echo ($context["text_delivery"] ?? null);
            echo "</strong></label>
        ";
            // line 72
            if ((($context["delivery_delivery_time"] ?? null) == "1")) {
                // line 73
                echo "            <input type=\"text\" name=\"delivery_date\" value=\"";
                echo ($context["delivery_date"] ?? null);
                echo "\" class=\"form-control date\" readonly=\"true\" style=\"background:#ffffff;\" />
        ";
            } else {
                // line 75
                echo "            <input type=\"text\" name=\"delivery_date\" value=\"";
                echo ($context["delivery_date"] ?? null);
                echo "\" class=\"form-control date\" readonly=\"true\" style=\"background:#ffffff;\" />
        ";
            }
            // line 77
            echo "        ";
            if ((($context["delivery_delivery_time"] ?? null) == "3")) {
                echo "<br />
            <select name=\"delivery_time\" class=\"form-control\">";
                // line 78
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["delivery_times"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["quickcheckout_delivery_time"]) {
                    // line 79
                    echo "                    ";
                    if ((($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = $context["quickcheckout_delivery_time"]) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b[($context["language_id"] ?? null)] ?? null) : null)) {
                        // line 80
                        echo "                        ";
                        if ((($context["delivery_time"] ?? null) == (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = $context["quickcheckout_delivery_time"]) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002[($context["language_id"] ?? null)] ?? null) : null))) {
                            // line 81
                            echo "                            <option value=\"";
                            echo (($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = $context["quickcheckout_delivery_time"]) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4[($context["language_id"] ?? null)] ?? null) : null);
                            echo "\" selected=\"selected\">";
                            echo (($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = $context["quickcheckout_delivery_time"]) && is_array($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) || $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 instanceof ArrayAccess ? ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666[($context["language_id"] ?? null)] ?? null) : null);
                            echo "</option>
                        ";
                        } else {
                            // line 83
                            echo "                            <option value=\"";
                            echo (($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = $context["quickcheckout_delivery_time"]) && is_array($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e) || $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e instanceof ArrayAccess ? ($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e[($context["language_id"] ?? null)] ?? null) : null);
                            echo "\">";
                            echo (($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 = $context["quickcheckout_delivery_time"]) && is_array($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52) || $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 instanceof ArrayAccess ? ($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52[($context["language_id"] ?? null)] ?? null) : null);
                            echo "</option>
                        ";
                        }
                        // line 85
                        echo "                    ";
                    }
                    // line 86
                    echo "                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['quickcheckout_delivery_time'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                echo "</select>
        ";
            }
            // line 88
            echo "    </div>
";
        } elseif ((        // line 89
($context["delivery_delivery_time"] ?? null) && (($context["delivery_delivery_time"] ?? null) == "2"))) {
            // line 90
            echo "    <input type=\"text\" name=\"delivery_date\" value=\"\" class=\"hide\" />
    <select name=\"delivery_time\" class=\"hide\"><option value=\"\"></option></select>
    <strong>";
            // line 92
            echo ($context["text_estimated_delivery"] ?? null);
            echo "</strong><br />
    ";
            // line 93
            echo ($context["estimated_delivery"] ?? null);
            echo "<br />
    ";
            // line 94
            echo ($context["estimated_delivery_time"] ?? null);
            echo "
";
        } else {
            // line 96
            echo "    <input type=\"text\" name=\"delivery_date\" value=\"\" class=\"hide\" />
    <select name=\"delivery_time\" class=\"hide\"><option value=\"\"></option></select>
";
        }
        // line 99
        echo "
<script type=\"text/javascript\"><!--
    \$('#shipping-method input[name=\\'shipping_method\\'], #shipping-method select[name=\\'shipping_method\\']').on('change', function() {

        // fj.agmedia.hr
        let checked = \$('#shipping-method input[name=\\'shipping_method\\']:checked').val();
        console.log(checked)
        if (checked == 'collector.collector') {
            \$('#collector-select').removeClass('hide');
        } else {
            \$('#collector-select').addClass('hide');
        }


        ";
        // line 113
        if ( !($context["logged"] ?? null)) {
            // line 114
            echo "        if (\$('#payment-address input[name=\\'shipping_address\\']:checked').val()) {
            var post_data = \$('#payment-address input[type=\\'text\\'], #payment-address input[type=\\'checkbox\\']:checked, #payment-address input[type=\\'radio\\']:checked, #payment-address input[type=\\'hidden\\'], #payment-address select, #shipping-method input[type=\\'text\\'], #shipping-method input[type=\\'checkbox\\']:checked, #shipping-method input[type=\\'radio\\']:checked, #shipping-method input[type=\\'hidden\\'], #shipping-method select, #shipping-method textarea');
        } else {
            var post_data = \$('#shipping-address input[type=\\'text\\'], #shipping-address input[type=\\'checkbox\\']:checked, #shipping-address input[type=\\'radio\\']:checked, #shipping-address input[type=\\'hidden\\'], #shipping-address select, #shipping-method input[type=\\'text\\'], #shipping-method input[type=\\'checkbox\\']:checked, #shipping-method input[type=\\'radio\\']:checked, #shipping-method input[type=\\'hidden\\'], #shipping-method select, #shipping-method textarea');
        }

        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/shipping_method/set',
            type: 'post',
            data: post_data,
            dataType: 'html',
            cache: false,
            success: function(html) {
                ";
            // line 127
            if (($context["cart"] ?? null)) {
                // line 128
                echo "                loadCart();
                ";
            }
            // line 130
            echo "
                ";
            // line 131
            if (($context["shipping_reload"] ?? null)) {
                // line 132
                echo "                reloadPaymentMethod();
                ";
            }
            // line 134
            echo "            },
            ";
            // line 135
            if (($context["debug"] ?? null)) {
                // line 136
                echo "            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
            }
            // line 140
            echo "        });
        ";
        } else {
            // line 142
            echo "        if (\$('#shipping-address input[name=\\'shipping_address\\']:checked').val() == 'new') {
            var url = 'index.php?route=extension/quickcheckout/shipping_method/set';
            var post_data = \$('#shipping-address input[type=\\'text\\'], #shipping-address input[type=\\'checkbox\\']:checked, #shipping-address input[type=\\'radio\\']:checked, #shipping-address input[type=\\'hidden\\'], #shipping-address select, #shipping-method input[type=\\'text\\'], #shipping-method input[type=\\'checkbox\\']:checked, #shipping-method input[type=\\'radio\\']:checked, #shipping-method input[type=\\'hidden\\'], #shipping-method select, #shipping-method textarea');
        } else {
            var url = 'index.php?route=extension/quickcheckout/shipping_method/set&address_id=' + \$('#shipping-address select[name=\\'address_id\\']').val();
            var post_data = \$('#shipping-method input[type=\\'text\\'], #shipping-method input[type=\\'checkbox\\']:checked, #shipping-method input[type=\\'radio\\']:checked, #shipping-method input[type=\\'hidden\\'], #shipping-method select, #shipping-method textarea');
        }

        \$.ajax({
            url: url,
            type: 'post',
            data: post_data,
            dataType: 'html',
            cache: false,
            success: function(html) {
                ";
            // line 157
            if (($context["cart"] ?? null)) {
                // line 158
                echo "                loadCart();
                ";
            }
            // line 160
            echo "
                ";
            // line 161
            if (($context["shipping_reload"] ?? null)) {
                // line 162
                echo "                if (\$('#payment-address input[name=\\'payment_address\\']').val() == 'new') {
                    reloadPaymentMethod();
                } else {
                    reloadPaymentMethodById(\$('#payment-address select[name=\\'address_id\\']').val());
                }
                ";
            }
            // line 168
            echo "            },
            ";
            // line 169
            if (($context["debug"] ?? null)) {
                // line 170
                echo "            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
            }
            // line 174
            echo "        });
        ";
        }
        // line 176
        echo "    });

    \$(document).ready(function() {
        \$('#shipping-method input[name=\\'shipping_method\\']:checked, #shipping-method select[name=\\'shipping_method\\']').trigger('change');
    });

    ";
        // line 182
        if ((($context["delivery"] ?? null) && (($context["delivery_delivery_time"] ?? null) == "1"))) {
            // line 183
            echo "    \$(document).ready(function() {
        \$('input[name=\\'delivery_date\\']').datetimepicker({
            format: 'YYYY-MM-DD HH:mm',
            minDate: '";
            // line 186
            echo ($context["delivery_min"] ?? null);
            echo "',
            maxDate: '";
            // line 187
            echo ($context["delivery_max"] ?? null);
            echo "',
            disabledDates: [";
            // line 188
            echo ($context["delivery_unavailable"] ?? null);
            echo "],
            enabledHours: [";
            // line 189
            echo ($context["hours"] ?? null);
            echo "],
            ignoreReadonly: true,
            ";
            // line 191
            if ((($context["delivery_days_of_week"] ?? null) != "")) {
                // line 192
                echo "            daysOfWeekDisabled: [";
                echo ($context["delivery_days_of_week"] ?? null);
                echo "]
            ";
            }
            // line 194
            echo "        });
    });
    ";
        } elseif ((        // line 196
($context["delivery"] ?? null) && ((($context["delivery_delivery_time"] ?? null) == "3") || (($context["delivery_delivery_time"] ?? null) == "0")))) {
            // line 197
            echo "    \$('input[name=\\'delivery_date\\']').datetimepicker({
        format: 'YYYY-MM-DD',
        minDate: '";
            // line 199
            echo ($context["delivery_min"] ?? null);
            echo "',
        maxDate: '";
            // line 200
            echo ($context["delivery_max"] ?? null);
            echo "',
        disabledDates: [";
            // line 201
            echo ($context["delivery_unavailable"] ?? null);
            echo "],
        ignoreReadonly: true,
        ";
            // line 203
            if ((($context["delivery_days_of_week"] ?? null) != "")) {
                // line 204
                echo "        daysOfWeekDisabled: [";
                echo ($context["delivery_days_of_week"] ?? null);
                echo "]
        ";
            }
            // line 206
            echo "    });
    ";
        }
        // line 208
        echo "    //--></script>";
    }

    public function getTemplateName()
    {
        return "basel/template/extension/quickcheckout/shipping_method.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  507 => 208,  503 => 206,  497 => 204,  495 => 203,  490 => 201,  486 => 200,  482 => 199,  478 => 197,  476 => 196,  472 => 194,  466 => 192,  464 => 191,  459 => 189,  455 => 188,  451 => 187,  447 => 186,  442 => 183,  440 => 182,  432 => 176,  428 => 174,  422 => 170,  420 => 169,  417 => 168,  409 => 162,  407 => 161,  404 => 160,  400 => 158,  398 => 157,  381 => 142,  377 => 140,  371 => 136,  369 => 135,  366 => 134,  362 => 132,  360 => 131,  357 => 130,  353 => 128,  351 => 127,  336 => 114,  334 => 113,  318 => 99,  313 => 96,  308 => 94,  304 => 93,  300 => 92,  296 => 90,  294 => 89,  291 => 88,  282 => 86,  279 => 85,  271 => 83,  263 => 81,  260 => 80,  257 => 79,  253 => 78,  248 => 77,  242 => 75,  236 => 73,  234 => 72,  230 => 71,  225 => 70,  223 => 69,  219 => 67,  215 => 65,  209 => 64,  206 => 63,  195 => 61,  189 => 59,  183 => 57,  180 => 56,  177 => 55,  174 => 54,  169 => 53,  166 => 52,  162 => 51,  159 => 50,  155 => 48,  149 => 47,  143 => 44,  140 => 43,  133 => 38,  126 => 26,  115 => 23,  110 => 22,  100 => 20,  98 => 19,  94 => 18,  90 => 16,  82 => 15,  74 => 13,  72 => 12,  69 => 11,  64 => 10,  61 => 9,  57 => 8,  54 => 7,  52 => 6,  47 => 5,  45 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "basel/template/extension/quickcheckout/shipping_method.twig", "");
    }
}
