<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/module/shipping_collector_list.twig */
class __TwigTemplate_d6a348f89ed38a835e4281d32a57ae535fbb44fb86491769b1922358cdad862e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
    <div class=\"page-header\">
        <div class=\"container-fluid\">
            <div class=\"pull-right\">
                <a href=\"";
        // line 6
        echo ($context["advance"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["help_advance"] ?? null);
        echo "\" class=\"btn btn-default\">";
        echo ($context["text_advance"] ?? null);
        echo "</a>
                <a href=\"";
        // line 7
        echo ($context["add"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_add"] ?? null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-plus\"></i></a>
                <button type=\"button\" data-toggle=\"tooltip\" title=\"";
        // line 8
        echo ($context["button_delete"] ?? null);
        echo "\" class=\"btn btn-danger\" onclick=\"confirm('";
        echo ($context["text_confirm"] ?? null);
        echo "') ? \$('#form-attribute').submit() : false;\"><i class=\"fa fa-trash-o\"></i></button>
            </div>
            <h1>";
        // line 10
        echo ($context["heading_title"] ?? null);
        echo "</h1>
            <ul class=\"breadcrumb\">
                ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 13
            echo "                    <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 13);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 13);
            echo "</a></li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 15
        echo "            </ul>
        </div>
    </div>
    <div class=\"container-fluid\">
        ";
        // line 19
        if (($context["error_warning"] ?? null)) {
            // line 20
            echo "            <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo ($context["error_warning"] ?? null);
            echo "
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
            </div>
        ";
        }
        // line 24
        echo "        ";
        if (($context["success"] ?? null)) {
            // line 25
            echo "            <div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ";
            echo ($context["success"] ?? null);
            echo "
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
            </div>
        ";
        }
        // line 29
        echo "        <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
                <h3 class=\"panel-title\"><i class=\"fa fa-list\"></i> ";
        // line 31
        echo ($context["text_list"] ?? null);
        echo "</h3>
            </div>
            <div class=\"panel-body\">
                <form action=\"";
        // line 34
        echo ($context["delete"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-attribute\">
                    <div class=\"table-responsive\">
                        <table class=\"table table-bordered table-hover\">
                            <thead>
                            <tr>
                                <td style=\"width: 1px;\" class=\"text-center\"><input type=\"checkbox\" onclick=\"\$('input[name*=\\'selected\\']').prop('checked', this.checked);\" /></td>
                                <td class=\"text-left\">";
        // line 40
        if ((($context["sort"] ?? null) == "sc.column_date")) {
            // line 41
            echo "                                        <a href=\"";
            echo ($context["sort_date"] ?? null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, ($context["order"] ?? null));
            echo "\">";
            echo ($context["column_date"] ?? null);
            echo "</a>
                                    ";
        } else {
            // line 43
            echo "                                        <a href=\"";
            echo ($context["sort_date"] ?? null);
            echo "\">";
            echo ($context["column_date"] ?? null);
            echo "</a>
                                    ";
        }
        // line 44
        echo "</td>
                                <td class=\"text-center\">";
        // line 45
        echo ($context["column_collected"] ?? null);
        echo "</td>
                                <td class=\"text-center\" width=\"10%\">";
        // line 46
        echo ($context["column_time"] ?? null);
        echo "</td>
                                <td class=\"text-center\" width=\"10%\">";
        // line 47
        echo ($context["column_max"] ?? null);
        echo "</td>
                                <td class=\"text-center\" width=\"10%\">";
        // line 48
        echo ($context["column_status"] ?? null);
        echo "</td>
                                <td class=\"text-right\" width=\"10%\">";
        // line 49
        echo ($context["column_action"] ?? null);
        echo "</td>
                            </tr>
                            </thead>
                            <tbody>
                            ";
        // line 53
        if (($context["shipping_collectors"] ?? null)) {
            // line 54
            echo "                                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["shipping_collectors"] ?? null));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["shipping_collector"]) {
                // line 55
                echo "                                    <tr>
                                        <td class=\"text-center\">";
                // line 56
                if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["shipping_collector"], "shipping_collector_id", [], "any", false, false, false, 56), ($context["selected"] ?? null))) {
                    // line 57
                    echo "                                                <input type=\"checkbox\" name=\"selected[]\" value=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["shipping_collector"], "shipping_collector_id", [], "any", false, false, false, 57);
                    echo "\" checked=\"checked\" />
                                            ";
                } else {
                    // line 59
                    echo "                                                <input type=\"checkbox\" name=\"selected[]\" value=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["shipping_collector"], "shipping_collector_id", [], "any", false, false, false, 59);
                    echo "\" />
                                            ";
                }
                // line 60
                echo "</td>
                                        <td class=\"text-left\">";
                // line 61
                echo twig_get_attribute($this->env, $this->source, $context["shipping_collector"], "collect_date", [], "any", false, false, false, 61);
                echo "</td>
                                        <td class=\"text-center\">";
                // line 62
                echo twig_get_attribute($this->env, $this->source, $context["shipping_collector"], "collected", [], "any", false, false, false, 62);
                echo " <span><div id=\"collected-bar";
                echo twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 62);
                echo "\" rel=\"collected_bar\" data-percent=\"";
                echo twig_get_attribute($this->env, $this->source, $context["shipping_collector"], "collected", [], "any", false, false, false, 62);
                echo "\"></div></span></td>
                                        <td class=\"text-center\">";
                // line 63
                echo twig_get_attribute($this->env, $this->source, $context["shipping_collector"], "collect_time", [], "any", false, false, false, 63);
                echo "</td>
                                        <td class=\"text-center\">";
                // line 64
                echo twig_get_attribute($this->env, $this->source, $context["shipping_collector"], "collect_max", [], "any", false, false, false, 64);
                echo "</td>
                                        <td class=\"text-center\">";
                // line 65
                echo twig_get_attribute($this->env, $this->source, $context["shipping_collector"], "status", [], "any", false, false, false, 65);
                echo "</td>
                                        <td class=\"text-right\"><a href=\"";
                // line 66
                echo twig_get_attribute($this->env, $this->source, $context["shipping_collector"], "edit", [], "any", false, false, false, 66);
                echo "\" data-toggle=\"tooltip\" title=\"";
                echo ($context["button_edit"] ?? null);
                echo "\" class=\"btn btn-primary\"><i class=\"fa fa-pencil\"></i></a></td>
                                    </tr>
                                ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['shipping_collector'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 69
            echo "                            ";
        } else {
            // line 70
            echo "                                <tr>
                                    <td class=\"text-center\" colspan=\"7\">";
            // line 71
            echo ($context["text_no_results"] ?? null);
            echo "</td>
                                </tr>
                            ";
        }
        // line 74
        echo "                            </tbody>
                        </table>
                    </div>
                </form>
                <div class=\"row\">
                    <div class=\"col-sm-6 text-left\">";
        // line 79
        echo ($context["pagination"] ?? null);
        echo "</div>
                    <div class=\"col-sm-6 text-right\">";
        // line 80
        echo ($context["results"] ?? null);
        echo "</div>
                </div>
            </div>
        </div>
    </div>
    <style>
        /* (A) PROGRESS BAR WRAPPER */
        .progress {
            z-index: 3;
            position: relative;
            border: 1px solid #acb2d8;
            width: 100%;
            max-width: 400px; /* OPTIONAL */
        }

        /* (B) SHARED */
        .progress, .indicator, .bar {
            box-sizing: border-box;
            min-height: 20px;
        }
        .indicator, .bar {
            position: absolute;
            top: 0; left: 0;
        }

        /* (C) PERCENTAGE INDICATOR */
        .indicator {
            z-index: 2;
            width: 100%;
            text-align: center;
        }

        /* (D) BAR */
        .bar {
            z-index: 1;
            width: 0;
            background: #ced6ff;
            transition: width 0.5s;
        }
    </style>

    <script type=\"text/javascript\">
      \$(document).ready(() => {
        let defaultBars = document.querySelectorAll('div[rel^=\"collected_bar\"]');

        console.log(defaultBars);

        for (var i = 0; i < defaultBars.length; i++) {
          console.log(defaultBars[i].getAttribute('id'))
        }

      });

      /**
       *
       * @param wrap
       * @param indicate
       * @returns object
       */
      function bar (wrap, indicate) {
        // (A) CONTAINER ITSELF
        wrap = document.getElementById(wrap);
        wrap.classList.add(\"progress\");

        // (B) CREATE PROGRESS BAR
        let bar = document.createElement(\"div\");
        bar.classList.add(\"bar\");
        wrap.appendChild(bar);

        // (C) PERCENTAGE INDICATOR
        let indicator = null;
        if (indicate) {
          indicator = document.createElement(\"div\");
          indicator.classList.add(\"indicator\");
          indicator.innerHTML = \"0%\";
          wrap.appendChild(indicator);
        }

        // (D) RETURN FULLY FORMED PROGRESS BAR
        return {
          wrap: wrap,
          bar: bar,
          indicator: indicator,
          set : function (percent) {
            this.bar.style.width = percent + \"%\";
            if (this.indicator) { this.indicator.innerHTML = percent + \"%\"; }
          }
        }
      }
    </script>
</div>
";
        // line 171
        echo ($context["footer"] ?? null);
        echo "
";
    }

    public function getTemplateName()
    {
        return "extension/module/shipping_collector_list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  379 => 171,  285 => 80,  281 => 79,  274 => 74,  268 => 71,  265 => 70,  262 => 69,  243 => 66,  239 => 65,  235 => 64,  231 => 63,  223 => 62,  219 => 61,  216 => 60,  210 => 59,  204 => 57,  202 => 56,  199 => 55,  181 => 54,  179 => 53,  172 => 49,  168 => 48,  164 => 47,  160 => 46,  156 => 45,  153 => 44,  145 => 43,  135 => 41,  133 => 40,  124 => 34,  118 => 31,  114 => 29,  106 => 25,  103 => 24,  95 => 20,  93 => 19,  87 => 15,  76 => 13,  72 => 12,  67 => 10,  60 => 8,  54 => 7,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "extension/module/shipping_collector_list.twig", "");
    }
}
