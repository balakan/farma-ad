<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* basel/template/common/mobile-nav.twig */
class __TwigTemplate_955f025481b70d8d336a56684519d9ea7f2383421021ce99ec21b5d20d7a7aec extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"main-menu-wrapper hidden-md hidden-lg\">


    <div class=\"closemenu\">
        <div class=\"icon-element mnu\">

            <a class=\"shortcut-wrapper menu-closer hidden-md hidden-lg\">
                <i class=\"icon-line-cross icon\"></i>
            </a>

        </div>
    </div>
<ul class=\"mobile-top\">
    <li class=\"mobile-lang-curr\"></li>
    ";
        // line 15
        if (($context["header_search"] ?? null)) {
            // line 16
            echo "    <li class=\"search\">
        <div class=\"search-holder-mobile\">
        <input type=\"text\" name=\"search-mobile\" value=\"\" placeholder=\"\" class=\"form-control\" /><a class=\"fa fa-search\"></a>
        </div>
    </li>
    ";
        }
        // line 22
        echo "</ul>
";
        // line 23
        if (($context["primary_menu"] ?? null)) {
            // line 24
            echo "<ul class=\"categories\">
";
            // line 25
            if ((($context["primary_menu"] ?? null) == "oc")) {
                // line 26
                echo "<!-- Default menu -->
";
                // line 27
                echo ($context["default_menu"] ?? null);
                echo "
";
            } elseif (            // line 28
array_key_exists("primary_menu", $context)) {
                // line 29
                echo "<!-- Mega menu -->
";
                // line 30
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["primary_menu_mobile"] ?? null));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["key"] => $context["row"]) {
                    // line 31
                    $this->loadTemplate("basel/template/common/menus/mega_menu.twig", "basel/template/common/mobile-nav.twig", 31)->display($context);
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['row'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
            }
            // line 34
            echo "</ul>
";
        }
        // line 36
        if (($context["secondary_menu"] ?? null)) {
            // line 37
            echo "<ul class=\"categories\">
    ";
            // line 38
            if ((($context["secondary_menu"] ?? null) == "oc")) {
                // line 39
                echo "        <!-- Default menu -->
        ";
                // line 40
                echo ($context["default_menu"] ?? null);
                echo "
    ";
            } elseif (            // line 41
array_key_exists("secondary_menu", $context)) {
                // line 42
                echo "        <!-- Mega menu -->
        ";
                // line 43
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["secondary_menu_mobile"] ?? null));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["key"] => $context["row"]) {
                    // line 44
                    echo "        \t";
                    $this->loadTemplate("basel/template/common/menus/mega_menu.twig", "basel/template/common/mobile-nav.twig", 44)->display($context);
                    // line 45
                    echo "        ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['row'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 46
                echo "    ";
            }
            // line 47
            echo "</ul>
";
        }
        // line 49
        echo "<ul class=\"categories\">
    ";
        // line 50
        $this->loadTemplate("basel/template/common/static_links.twig", "basel/template/common/mobile-nav.twig", 50)->display($context);
        // line 51
        echo "</ul>
</div>
<span class=\"body-cover menu-closer\"></span>";
    }

    public function getTemplateName()
    {
        return "basel/template/common/mobile-nav.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  186 => 51,  184 => 50,  181 => 49,  177 => 47,  174 => 46,  160 => 45,  157 => 44,  140 => 43,  137 => 42,  135 => 41,  131 => 40,  128 => 39,  126 => 38,  123 => 37,  121 => 36,  117 => 34,  102 => 31,  85 => 30,  82 => 29,  80 => 28,  76 => 27,  73 => 26,  71 => 25,  68 => 24,  66 => 23,  63 => 22,  55 => 16,  53 => 15,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "basel/template/common/mobile-nav.twig", "/Users/alive/Sites/Agmedia/Live/kaonekad/upload/catalog/view/theme/basel/template/common/mobile-nav.twig");
    }
}
