<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/module/luceed_sync_dash.twig */
class __TwigTemplate_27b0ab461f81c80a19994755e66362d38b2896bbe65e4f088f9502404e3e3836 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
    <div class=\"page-header\">
        <div class=\"container-fluid\">
            <div class=\"pull-right\">
            </div>
            <h1>";
        // line 7
        echo ($context["title"] ?? null);
        echo "</h1>
            <ul class=\"breadcrumb\">
                ";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 10
            echo "                    <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 10);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 10);
            echo "</a></li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "            </ul>
        </div>
    </div>
    <div class=\"container-fluid\">
        ";
        // line 16
        if (($context["error_warning"] ?? null)) {
            // line 17
            echo "            <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo ($context["error_warning"] ?? null);
            echo "
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
            </div>
        ";
        }
        // line 21
        echo "        ";
        if (($context["success"] ?? null)) {
            // line 22
            echo "            <div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ";
            echo ($context["success"] ?? null);
            echo "
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
            </div>
        ";
        }
        // line 26
        echo "        <div class=\"row\">
            <div class=\"col-sm-12 col-md-7\">
                <div class=\"panel panel-default\">
                    <div class=\"panel-heading\">
                        <h3 class=\"panel-title\"><i class=\"fa fa-list\"></i> ";
        // line 30
        echo ($context["text_products"] ?? null);
        echo "</h3>
                    </div>
                    <div class=\"panel-body form-horizontal\">
                        <div class=\"form-group\">
                            <div class=\"col-sm-4\">
                                <a href=\"javascript:void(e);\" data-toggle=\"tooltip\" title=\"";
        // line 35
        echo ($context["help_products"] ?? null);
        echo "\" class=\"btn btn-default\">Import New Products</a>
                            </div>
                            <p class=\"col-sm-8\" style=\"margin-top: 9px;\">";
        // line 37
        echo ($context["help_products"] ?? null);
        echo "</p>
                        </div>

                        <div class=\"row\">

                        </div>
                    </div>
                </div>
            </div>

            <div class=\"col-sm-12 col-md-5\">
                <div class=\"panel panel-default\">
                    <div class=\"panel-heading\">
                        <h3 class=\"panel-title\"><i class=\"fa fa-list\"></i> ";
        // line 50
        echo ($context["text_categories"] ?? null);
        echo "</h3>
                    </div>
                    <div class=\"panel-body form-horizontal\">
                        <div class=\"form-group\">
                            <div class=\"col-sm-5\">
                                <a href=\"javascript:void(e);\" id=\"import-new-categories\" data-toggle=\"tooltip\" title=\"";
        // line 55
        echo ($context["help_categories"] ?? null);
        echo "\" class=\"btn btn-default\">Import New Categories</a>
                            </div>
                            <p class=\"col-sm-7\">";
        // line 57
        echo ($context["help_categories"] ?? null);
        echo "</p>
                        </div>

                        <div class=\"row\">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>

    </style>

    <script type=\"text/javascript\">
      \$(document).ready(() => {
        \$('#import-new-categories').on('click', (e) => {
          \$.ajax({
            url: 'index.php?route=extension/module/luceed_sync/importProducts&user_token=";
        // line 76
        echo ($context["user_token"] ?? null);
        echo "&filter_name=',
            dataType: 'json',
            success: function(json) {
              console.log(json)
            }
          });
        });
      });
    </script>
</div>
";
        // line 86
        echo ($context["footer"] ?? null);
        echo "
";
    }

    public function getTemplateName()
    {
        return "extension/module/luceed_sync_dash.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  177 => 86,  164 => 76,  142 => 57,  137 => 55,  129 => 50,  113 => 37,  108 => 35,  100 => 30,  94 => 26,  86 => 22,  83 => 21,  75 => 17,  73 => 16,  67 => 12,  56 => 10,  52 => 9,  47 => 7,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "extension/module/luceed_sync_dash.twig", "");
    }
}
