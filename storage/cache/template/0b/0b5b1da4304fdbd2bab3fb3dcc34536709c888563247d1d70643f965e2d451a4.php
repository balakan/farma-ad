<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/module/shipping_collector_form.twig */
class __TwigTemplate_a3b0bdec7f2f37ec6758fb8004c772e261286efef70f195d3624b8eb58a4a269 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <button type=\"submit\" form=\"form-module\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo ($context["button_save"] ?? null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
        <a href=\"";
        // line 7
        echo ($context["cancel"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_cancel"] ?? null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1>";
        // line 8
        echo ($context["heading_title"] ?? null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 11
            echo "        <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 11);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 11);
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">
    ";
        // line 17
        if (($context["error_warning"] ?? null)) {
            // line 18
            echo "    <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo ($context["error_warning"] ?? null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 22
        echo "    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i> ";
        // line 24
        echo ($context["text_edit"] ?? null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
        <form action=\"";
        // line 27
        echo ($context["action"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-module\" class=\"form-horizontal\">
            <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-date\">";
        // line 29
        echo ($context["entry_date"] ?? null);
        echo "</label><br><span class=\"small\">";
        echo ($context["entry_date_help"] ?? null);
        echo "</span>
                <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"collect_date\" value=\"";
        // line 31
        echo ($context["collect_date"] ?? null);
        echo "\" placeholder=\"\" id=\"input-date\" class=\"form-control\" />
                </div>
            </div>

            <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-time\">";
        // line 36
        echo ($context["entry_time"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"collect_time\" value=\"";
        // line 38
        echo ($context["collect_time"] ?? null);
        echo "\" placeholder=\"\" id=\"input-time\" class=\"form-control\" />
                </div>
            </div>

            <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-max\">";
        // line 43
        echo ($context["entry_max"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"collect_max\" value=\"";
        // line 45
        echo ($context["collect_max"] ?? null);
        echo "\" placeholder=\"\" id=\"input-max\" class=\"form-control\" />
                </div>
            </div>

            <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-max\">";
        // line 50
        echo ($context["entry_max"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"collect_max\" value=\"";
        // line 52
        echo ($context["collect_max"] ?? null);
        echo "\" placeholder=\"\" id=\"input-max\" class=\"form-control\" />
                </div>
            </div>

            <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-status\">";
        // line 57
        echo ($context["entry_status"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                    <select name=\"status\" id=\"input-status\" class=\"form-control\">
                        ";
        // line 60
        if (($context["status"] ?? null)) {
            // line 61
            echo "                            <option value=\"1\" selected=\"selected\">";
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                            <option value=\"0\">";
            // line 62
            echo ($context["text_disabled"] ?? null);
            echo "</option>
                        ";
        } else {
            // line 64
            echo "                            <option value=\"1\">";
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                            <option value=\"0\" selected=\"selected\">";
            // line 65
            echo ($context["text_disabled"] ?? null);
            echo "</option>
                        ";
        }
        // line 67
        echo "                    </select>
                </div>
            </div>

        </form>
      </div>
    </div>
  </div>
</div>
";
        // line 76
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "extension/module/shipping_collector_form.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  200 => 76,  189 => 67,  184 => 65,  179 => 64,  174 => 62,  169 => 61,  167 => 60,  161 => 57,  153 => 52,  148 => 50,  140 => 45,  135 => 43,  127 => 38,  122 => 36,  114 => 31,  107 => 29,  102 => 27,  96 => 24,  92 => 22,  84 => 18,  82 => 17,  76 => 13,  65 => 11,  61 => 10,  56 => 8,  50 => 7,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "extension/module/shipping_collector_form.twig", "");
    }
}
