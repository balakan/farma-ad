<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* basel/template/product/product.twig */
class __TwigTemplate_f7618abe9fd95e2ad3d1e3f9eca1ec6a29a6a7186bea462b697cfe11aaa937aa extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo "

";
        // line 3
        if ((($context["product_layout"] ?? null) != "full-width")) {
            // line 4
            echo "    <style>
        .product-page .image-area {
        ";
            // line 6
            if (((($context["product_layout"] ?? null) == "images-left") && ($context["images"] ?? null))) {
                // line 7
                echo "            width: ";
                echo ((($context["img_w"] ?? null) + ($context["img_a_w"] ?? null)) + 20);
                echo "px;
        ";
            } else {
                // line 9
                echo "            width: ";
                echo ($context["img_w"] ?? null);
                echo "px;
        ";
            }
            // line 11
            echo "        }
        .product-page .main-image {
            width:";
            // line 13
            echo ($context["img_w"] ?? null);
            echo "px;
        }
        .product-page .image-additional {
        ";
            // line 16
            if ((($context["product_layout"] ?? null) == "images-left")) {
                // line 17
                echo "            width: ";
                echo ($context["img_a_w"] ?? null);
                echo "px;
            height: ";
                // line 18
                echo ($context["img_h"] ?? null);
                echo "px;
        ";
            } else {
                // line 20
                echo "            width: ";
                echo ($context["img_w"] ?? null);
                echo "px;
        ";
            }
            // line 22
            echo "        }
        .product-page .image-additional.has-arrows {
        ";
            // line 24
            if ((($context["product_layout"] ?? null) == "images-left")) {
                // line 25
                echo "            height: ";
                echo (($context["img_h"] ?? null) - 40);
                echo "px;
        ";
            }
            // line 27
            echo "        }
        @media (min-width: 992px) and (max-width: 1199px) {
            .product-page .image-area {
            ";
            // line 30
            if ((($context["product_layout"] ?? null) == "images-left")) {
                // line 31
                echo "                width: ";
                echo (((($context["img_w"] ?? null) + ($context["img_a_w"] ?? null)) / 1.25) + 20);
                echo "px;
            ";
            } else {
                // line 33
                echo "                width: ";
                echo (($context["img_w"] ?? null) / 1.25);
                echo "px;
            ";
            }
            // line 35
            echo "            }
            .product-page .main-image {
                width:";
            // line 37
            echo (($context["img_w"] ?? null) / 1.25);
            echo "px;
            }
            .product-page .image-additional {
            ";
            // line 40
            if ((($context["product_layout"] ?? null) == "images-left")) {
                // line 41
                echo "                width: ";
                echo (($context["img_a_w"] ?? null) / 1.25);
                echo "px;
                height: ";
                // line 42
                echo (($context["img_h"] ?? null) / 1.25);
                echo "px;
            ";
            } else {
                // line 44
                echo "                width: ";
                echo (($context["img_w"] ?? null) / 1.25);
                echo "px;
            ";
            }
            // line 46
            echo "            }
        }
    </style>
";
        }
        // line 50
        echo "
<ul class=\"breadcrumb\">
    ";
        // line 52
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 53
            echo "        <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 53);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 53);
            echo "</a></li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "</ul>

<div class=\"container product-layout ";
        // line 57
        echo ($context["product_layout"] ?? null);
        echo "\">

    <div class=\"row\">";
        // line 59
        echo ($context["column_left"] ?? null);
        echo "
        ";
        // line 60
        if ((($context["column_left"] ?? null) && ($context["column_right"] ?? null))) {
            // line 61
            echo "            ";
            $context["class"] = "col-sm-6";
            // line 62
            echo "        ";
        } elseif ((($context["column_left"] ?? null) || ($context["column_right"] ?? null))) {
            // line 63
            echo "            ";
            $context["class"] = "col-md-9 col-sm-8";
            // line 64
            echo "        ";
        } else {
            // line 65
            echo "            ";
            $context["class"] = "col-sm-12";
            // line 66
            echo "        ";
        }
        // line 67
        echo "        <div id=\"content\" class=\"product-main no-min-height ";
        echo ($context["class"] ?? null);
        echo "\">
            ";
        // line 68
        echo ($context["content_top"] ?? null);
        echo "

            <div class=\"table product-info product-page\">

                <div class=\"table-cell left\">

                    ";
        // line 74
        if ((($context["thumb"] ?? null) || ($context["images"] ?? null))) {
            // line 75
            echo "                    <div class=\"image-area ";
            if ( !($context["hover_zoom"] ?? null)) {
                echo "hover-zoom-disabled";
            }
            echo "\" id=\"gallery\">

                        ";
            // line 77
            if (($context["thumb"] ?? null)) {
                // line 78
                echo "                            <div class=\"main-image\">

                                ";
                // line 80
                if (((($context["price"] ?? null) && ($context["special"] ?? null)) && ($context["sale_badge"] ?? null))) {
                    // line 81
                    echo "                                    <span class=\"badge sale_badge\"><i>";
                    echo ($context["sale_badge"] ?? null);
                    echo "</i></span>
                                ";
                }
                // line 83
                echo "
                                ";
                // line 84
                if (($context["is_new"] ?? null)) {
                    // line 85
                    echo "                                    <span class=\"badge new_badge\"><i>";
                    echo ($context["basel_text_new"] ?? null);
                    echo "</i></span>
                                ";
                }
                // line 87
                echo "
                                ";
                // line 88
                if (((($context["qty"] ?? null) < 1) && ($context["stock_badge_status"] ?? null))) {
                    // line 89
                    echo "                                    <span class=\"badge out_of_stock_badge\"><i>";
                    echo ($context["basel_text_out_of_stock"] ?? null);
                    echo "</i></span>
                                ";
                }
                // line 91
                echo "
                                <a class=\"";
                // line 92
                if ( !($context["images"] ?? null)) {
                    echo "link cloud-zoom";
                }
                echo " ";
                if ((($context["product_layout"] ?? null) == "full-width")) {
                    echo "link";
                } else {
                    echo "cloud-zoom";
                }
                echo "\" id=\"main-image\" href=\"";
                echo ($context["popup"] ?? null);
                echo "\" rel=\"position:'inside', showTitle: false\"><img src=\"";
                echo ($context["thumb"] ?? null);
                echo "\" title=\"";
                echo ($context["heading_title"] ?? null);
                echo "\" alt=\"";
                echo ($context["heading_title"] ?? null);
                echo "\" /></a>
                            </div>
                        ";
            }
            // line 95
            echo "
                        ";
            // line 96
            if (($context["images"] ?? null)) {
                // line 97
                echo "                            <ul class=\"image-additional\">
                                ";
                // line 98
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["images"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                    // line 99
                    echo "                                    <li>
                                        <a class=\"link ";
                    // line 100
                    if ((($context["product_layout"] ?? null) != "full-width")) {
                        echo "cloud-zoom-gallery locked";
                    }
                    echo "\" href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["image"], "popup", [], "any", false, false, false, 100);
                    echo "\" rel=\"useZoom: 'main-image', smallImage: '";
                    echo twig_get_attribute($this->env, $this->source, $context["image"], "thumb_lg", [], "any", false, false, false, 100);
                    echo "'\"><img src=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["image"], "thumb", [], "any", false, false, false, 100);
                    echo "\" title=\"";
                    echo ($context["heading_title"] ?? null);
                    echo "\" alt=\"";
                    echo ($context["heading_title"] ?? null);
                    echo "\" /></a>
                                    </li>
                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 103
                echo "                                ";
                if ((($context["thumb"] ?? null) && (($context["product_layout"] ?? null) != "full-width"))) {
                    // line 104
                    echo "                                    <li><a class=\"link cloud-zoom-gallery locked active\" href=\"";
                    echo ($context["popup"] ?? null);
                    echo "\" rel=\"useZoom: 'main-image', smallImage: '";
                    echo ($context["thumb"] ?? null);
                    echo "'\"><img src=\"";
                    echo ($context["thumb_sm"] ?? null);
                    echo "\" title=\"";
                    echo ($context["heading_title"] ?? null);
                    echo "\" alt=\"";
                    echo ($context["heading_title"] ?? null);
                    echo "\" /></a></li>
                                ";
                }
                // line 106
                echo "                            </ul>
                        ";
            }
            // line 108
            echo "
                        <div class=\"shipnotice\">
                            <div class=\"alert alert-shipping\">
                                <i class=\"fa fa-truck\"></i> Brza zagrebačka dostava <span>Između 11h i 21h istoga dana
                 </span>
                            </div>
                        </div>

                    </div> <!-- .table-cell.left ends -->

                </div> <!-- .image-area ends -->
                ";
        }
        // line 120
        echo "
                <div class=\"table-cell w100 right\">
                    <div class=\"inner\">

                        <div class=\"product-h1\">
                            <h1 id=\"page-title\">";
        // line 125
        echo ($context["heading_title"] ?? null);
        echo "</h1>
                        </div>

                        ";
        // line 128
        if ((($context["review_status"] ?? null) && (($context["review_qty"] ?? null) > 0))) {
            // line 129
            echo "                            <div class=\"rating\">
    <span class=\"rating_stars rating r";
            // line 130
            echo ($context["rating"] ?? null);
            echo "\">
    <i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>
    </span>
                            </div>
                            <span class=\"review_link\">(<a class=\"hover_uline to_tabs\" onclick=\"\$('a[href=\\'#tab-review\\']').trigger('click'); return false;\">";
            // line 134
            echo ($context["reviews"] ?? null);
            echo "</a>)</span>
                        ";
        }
        // line 136
        echo "
                        ";
        // line 137
        if (($context["price"] ?? null)) {
            // line 138
            echo "                            <ul class=\"list-unstyled price\">
                                ";
            // line 139
            if ( !($context["special"] ?? null)) {
                // line 140
                echo "                                    <li><span class=\"live-price\">";
                echo ($context["price"] ?? null);
                echo "<span></li>
                                ";
            } else {
                // line 142
                echo "                                    <li><span class=\"price-old\">";
                echo ($context["price"] ?? null);
                echo "</span><span class=\"live-price-new\">";
                echo ($context["special"] ?? null);
                echo "<span></li>
                                    <span id=\"special_countdown\"></span>
                                ";
            }
            // line 145
            echo "                            </ul>

                            ";
            // line 147
            if (($context["discounts"] ?? null)) {
                // line 148
                echo "                                <p class=\"discount\">
                                    ";
                // line 149
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["discounts"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["discount"]) {
                    // line 150
                    echo "                                        <span>";
                    echo twig_get_attribute($this->env, $this->source, $context["discount"], "quantity", [], "any", false, false, false, 150);
                    echo ($context["text_discount"] ?? null);
                    echo "<i class=\"price\">";
                    echo twig_get_attribute($this->env, $this->source, $context["discount"], "price", [], "any", false, false, false, 150);
                    echo "</i></span>
                                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['discount'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 152
                echo "                                </p>
                            ";
            }
            // line 154
            echo "
                        ";
        }
        // line 155
        echo " <!-- if price ends -->
                        ";
        // line 156
        if ((($context["price"] ?? null) && ($context["tax"] ?? null))) {
            // line 157
            echo "                            <p class=\"info p-tax\"><b>";
            echo ($context["text_tax"] ?? null);
            echo "</b> <span class=\"live-price-tax\">";
            echo ($context["tax"] ?? null);
            echo "</span></p>
                        ";
        }
        // line 159
        echo "
                        ";
        // line 160
        if ((($context["meta_description_status"] ?? null) && ($context["meta_description"] ?? null))) {
            // line 161
            echo "                            <p class=\"meta_description\">";
            echo ($context["meta_description"] ?? null);
            echo "</p>
                        ";
        }
        // line 163
        echo "

                        <div id=\"product\">

                            ";
        // line 167
        if (($context["options"] ?? null)) {
            // line 168
            echo "                                <div class=\"options \">
                                    ";
            // line 169
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["options"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
                // line 170
                echo "
                                        ";
                // line 171
                if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 171) == "select")) {
                    // line 172
                    echo "                                            <div class=\"form-group";
                    if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 172)) {
                        echo " required";
                    }
                    echo " row nbmargin\">
                                                <div class=\" name col-md-12\">
                                                    <label class=\"control-label\" for=\"input-option";
                    // line 174
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 174);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 174);
                    echo "</label>
                                                </div>
                                                <div class=\" col-md-12\">
                                                    <select name=\"option[";
                    // line 177
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 177);
                    echo "]\" id=\"input-option";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 177);
                    echo "\" class=\"form-control kolicina\">
                                                        <option value=\"\">";
                    // line 178
                    echo ($context["text_select"] ?? null);
                    echo "</option>
                                                        ";
                    // line 179
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["option"], "product_option_value", [], "any", false, false, false, 179));
                    foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                        // line 180
                        echo "                                                            <option value=\"";
                        echo twig_get_attribute($this->env, $this->source, $context["option_value"], "product_option_value_id", [], "any", false, false, false, 180);
                        echo "\">";
                        echo twig_get_attribute($this->env, $this->source, $context["option_value"], "name", [], "any", false, false, false, 180);
                        echo "
                                                                ";
                        // line 181
                        if (twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 181)) {
                            // line 182
                            echo "                                                                    (";
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price_prefix", [], "any", false, false, false, 182);
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 182);
                            echo ")
                                                                ";
                        }
                        // line 184
                        echo "                                                            </option>
                                                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 186
                    echo "                                                        <option value=\"+\">+</option>
                                                    </select>

                                                    <script>
                                                        \$('[name=\"option[";
                    // line 190
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 190);
                    echo "]\"]').otherDropdown({classes:'form-control', value:'+', placeholder:'Upiši', max: '";
                    echo twig_get_attribute($this->env, $this->source, ($context["scale"] ?? null), "max", [], "any", false, false, false, 190);
                    echo "' })
                                                    </script>

                                                    ";
                    // line 193
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["option"], "product_option_value", [], "any", false, false, false, 193));
                    foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                        // line 194
                        echo "                                                        ";
                        if ((twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 194) == 0)) {
                            // line 195
                            echo "                                                            <input type=\"hidden\" id=\"input-default";
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 195);
                            echo "\" value=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "product_option_value_id", [], "any", false, false, false, 195);
                            echo "\">
                                                        ";
                        }
                        // line 197
                        echo "                                                    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 198
                    echo "
                                                </div>
                                            </div>
                                        ";
                }
                // line 202
                echo "


                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 205
            echo " <!-- foreach option -->
                                </div>
                            ";
        }
        // line 208
        echo "
                            ";
        // line 209
        if (($context["recurrings"] ?? null)) {
            // line 210
            echo "                                <hr>
                                <h3>";
            // line 211
            echo ($context["text_payment_recurring"] ?? null);
            echo "</h3>
                                <div class=\"form-group required\">
                                    <select name=\"recurring_id\" class=\"form-control\">
                                        <option value=\"\">";
            // line 214
            echo ($context["text_select"] ?? null);
            echo "</option>
                                        ";
            // line 215
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["recurrings"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["recurring"]) {
                // line 216
                echo "                                            <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["recurring"], "recurring_id", [], "any", false, false, false, 216);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["recurring"], "name", [], "any", false, false, false, 216);
                echo "</option>
                                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['recurring'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 218
            echo "                                    </select>
                                    <div class=\"help-block\" id=\"recurring-description\"></div>
                                </div>
                            ";
        }
        // line 222
        echo "
                            <div class=\"form-group buy catalog_hide \">

                                <!-- <input type=\"number\" step=\"1\" min=\"";
        // line 225
        echo ($context["minimum"] ?? null);
        echo "\" name=\"quantity\" value=\"";
        echo ($context["minimum"] ?? null);
        echo "\" id=\"input-quantity\" class=\"form-control input-quantity\" /> -->
                                <input type=\"hidden\" name=\"product_id\" value=\"";
        // line 226
        echo ($context["product_id"] ?? null);
        echo "\" />
                                ";
        // line 228
        echo "                                <button type=\"button\" data-loading-text=\"";
        echo ($context["text_loading"] ?? null);
        echo "\" onclick=\"javascript:void(0); kosarica_dodaj(";
        echo ($context["product_id"] ?? null);
        echo ", '";
        echo twig_escape_filter($this->env, json_encode(($context["scale"] ?? null)));
        echo "', '";
        echo twig_get_attribute($this->env, $this->source, ($context["option"] ?? null), "product_option_id", [], "any", false, false, false, 228);
        echo "');\"
                                        class=\"btn btn-primary\">";
        // line 229
        if (((($context["qty"] ?? null) < 1) && ($context["stock_badge_status"] ?? null))) {
            echo ($context["basel_text_out_of_stock"] ?? null);
        } else {
            echo "<i class=\"global-cart icon\"></i> ";
            echo ($context["button_cart"] ?? null);
        }
        echo "</button>
                            </div>
                            ";
        // line 231
        if ((($context["minimum"] ?? null) > 1)) {
            // line 232
            echo "                                <div class=\"alert alert-sm alert-info\"><i class=\"fa fa-info-circle\"></i> ";
            echo ($context["text_minimum"] ?? null);
            echo "</div>
                            ";
        }
        // line 234
        echo "
                        </div> <!-- #product ends -->


                        <p class=\"info is_wishlist\"><a onclick=\"wishlist.add('";
        // line 238
        echo ($context["product_id"] ?? null);
        echo "');\"><i class=\"icon-heart\"></i> ";
        echo ($context["button_wishlist"] ?? null);
        echo "</a></p>
                        <p class=\"info is_compare\"><a onclick=\"compare.add('";
        // line 239
        echo ($context["product_id"] ?? null);
        echo "');\"><i class=\"icon-refresh\"></i> ";
        echo ($context["button_compare"] ?? null);
        echo "</a></p>
                        ";
        // line 240
        if (($context["question_status"] ?? null)) {
            // line 241
            echo "                            <p class=\"info is_ask\"><a class=\"to_tabs\" onclick=\"\$('a[href=\\'#tab-questions\\']').trigger('click'); return false;\"><i class=\"icon-question\"></i> ";
            echo ($context["basel_button_ask"] ?? null);
            echo "</a></p>
                        ";
        }
        // line 243
        echo "
                        <div class=\"clearfix\"></div>

                        <div class=\"info-holder\">



                            ";
        // line 250
        if ((($context["price"] ?? null) && ($context["points"] ?? null))) {
            // line 251
            echo "                                <p class=\"info\"><b>";
            echo ($context["text_points"] ?? null);
            echo "</b> ";
            echo ($context["points"] ?? null);
            echo "</p>
                            ";
        }
        // line 253
        echo "
                            <p class=\"info ";
        // line 254
        if ((($context["qty"] ?? null) > 0)) {
            echo "in_stock";
        }
        echo "\"><b>";
        echo ($context["text_stock"] ?? null);
        echo "</b> ";
        echo ($context["stock"] ?? null);
        echo "</p>

                            ";
        // line 256
        if (($context["manufacturer"] ?? null)) {
            // line 257
            echo "                                <p class=\"info\"><b>";
            echo ($context["text_manufacturer"] ?? null);
            echo "</b> <a class=\"hover_uline\" href=\"";
            echo ($context["manufacturers"] ?? null);
            echo "\">";
            echo ($context["manufacturer"] ?? null);
            echo "</a></p>
                            ";
        }
        // line 259
        echo "
                            <p class=\"info\"><b>";
        // line 260
        echo ($context["text_model"] ?? null);
        echo "</b> ";
        echo ($context["model"] ?? null);
        echo "</p>

                            ";
        // line 262
        if (($context["reward"] ?? null)) {
            // line 263
            echo "                                <p class=\"info\"><b>";
            echo ($context["text_reward"] ?? null);
            echo "</b> ";
            echo ($context["reward"] ?? null);
            echo "</p>
                            ";
        }
        // line 265
        echo "
                            ";
        // line 266
        if (($context["tags"] ?? null)) {
            // line 267
            echo "                                <p class=\"info tags\"><b>";
            echo ($context["text_tags"] ?? null);
            echo "</b> &nbsp;<span>";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["tags"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["tag"]) {
                echo "<a class=\"hover_uline\" href=\"";
                echo twig_get_attribute($this->env, $this->source, $context["tag"], "href", [], "any", false, false, false, 267);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["tag"], "tag", [], "any", false, false, false, 267);
                echo "</a>";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tag'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo "</span></p>
                            ";
        }
        // line 269
        echo "
                            ";
        // line 270
        if (($context["basel_share_btn"] ?? null)) {
            // line 271
            echo "                                ";
            if ((($context["basel_sharing_style"] ?? null) == "large")) {
                // line 272
                echo "                                    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5f1aa4948aa12700134b0c47&product=inline-share-buttons' async='async'></script>
                                    <!-- ShareThis BEGIN --><div class=\"sharethis-inline-share-buttons\"></div><!-- ShareThis END -->
                                ";
            } else {
                // line 275
                echo "                                <hr>
                                    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5f1aa4948aa12700134b0c47&product=inline-share-buttons' async='async'></script>
                                    <!-- ShareThis BEGIN --><div class=\"sharethis-inline-share-buttons\"></div><!-- ShareThis END -->
                                ";
            }
            // line 279
            echo "                            ";
        }
        // line 280
        echo "
                        </div> <!-- .info-holder ends -->

                    </div> <!-- .inner ends -->


                    ";
        // line 286
        if (($context["full_width_tabs"] ?? null)) {
            // line 287
            echo "                </div> <!-- main column ends -->
                ";
            // line 288
            echo ($context["column_right"] ?? null);
            echo "
            </div> <!-- .row ends -->
        </div> <!-- .container ends -->
        ";
        }
        // line 292
        echo "
        ";
        // line 293
        if (($context["full_width_tabs"] ?? null)) {
            // line 294
            echo "        <div class=\"outer-container product-tabs-wrapper\">
            <div class=\"container\">
                ";
        } else {
            // line 297
            echo "                <div class=\"inline-tabs\">
                    ";
        }
        // line 299
        echo "
                    <!-- Tabs area start -->
                    <div class=\"row\">
                        <div class=\"col-sm-12\">

                            <ul class=\"nav nav-tabs ";
        // line 304
        echo ($context["product_tabs_style"] ?? null);
        echo " main_tabs\">
                                <li class=\"active\"><a href=\"#tab-description\" data-toggle=\"tab\">";
        // line 305
        echo ($context["tab_description"] ?? null);
        echo "</a></li>
                                ";
        // line 306
        if (($context["product_tabs"] ?? null)) {
            // line 307
            echo "                                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["product_tabs"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["tab"]) {
                // line 308
                echo "                                        <li><a href=\"#custom-tab-";
                echo twig_get_attribute($this->env, $this->source, $context["tab"], "tab_id", [], "any", false, false, false, 308);
                echo "\" data-toggle=\"tab\">";
                echo twig_get_attribute($this->env, $this->source, $context["tab"], "name", [], "any", false, false, false, 308);
                echo "</a></li>
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tab'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 310
            echo "                                ";
        }
        // line 311
        echo "                                ";
        if (($context["attribute_groups"] ?? null)) {
            // line 312
            echo "                                    <li><a href=\"#tab-specification\" data-toggle=\"tab\">";
            echo ($context["tab_attribute"] ?? null);
            echo "</a></li>
                                ";
        }
        // line 314
        echo "                                ";
        if (($context["review_status"] ?? null)) {
            // line 315
            echo "                                    <li><a href=\"#tab-review\" data-toggle=\"tab\">";
            echo ($context["tab_review"] ?? null);
            echo "</a></li>
                                ";
        }
        // line 317
        echo "                                ";
        if (($context["question_status"] ?? null)) {
            // line 318
            echo "                                    <li><a href=\"#tab-questions\" data-toggle=\"tab\">";
            echo ($context["basel_tab_questions"] ?? null);
            echo " (";
            echo ($context["questions_total"] ?? null);
            echo ")</a></li>
                                ";
        }
        // line 320
        echo "                            </ul>

                            <div class=\"tab-content\">

                                <div class=\"tab-pane active\" id=\"tab-description\">
                                    ";
        // line 325
        echo ($context["description"] ?? null);
        echo "
                                </div>

                                ";
        // line 328
        if (($context["product_tabs"] ?? null)) {
            // line 329
            echo "                                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["product_tabs"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["tab"]) {
                // line 330
                echo "                                        <div class=\"tab-pane\" id=\"custom-tab-";
                echo twig_get_attribute($this->env, $this->source, $context["tab"], "tab_id", [], "any", false, false, false, 330);
                echo "\">
                                            ";
                // line 331
                echo twig_get_attribute($this->env, $this->source, $context["tab"], "description", [], "any", false, false, false, 331);
                echo "
                                        </div>
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tab'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 334
            echo "                                ";
        }
        // line 335
        echo "
                                ";
        // line 336
        if (($context["attribute_groups"] ?? null)) {
            // line 337
            echo "                                    <div class=\"tab-pane\" id=\"tab-specification\">
                                        <table class=\"table specification\">
                                            ";
            // line 339
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["attribute_groups"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["attribute_group"]) {
                // line 340
                echo "                                                <thead>
                                                <tr>
                                                    <td colspan=\"2\">";
                // line 342
                echo twig_get_attribute($this->env, $this->source, $context["attribute_group"], "name", [], "any", false, false, false, 342);
                echo "</td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                ";
                // line 346
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["attribute_group"], "attribute", [], "any", false, false, false, 346));
                foreach ($context['_seq'] as $context["_key"] => $context["attribute"]) {
                    // line 347
                    echo "                                                    <tr>
                                                        <td class=\"text-left\"><b>";
                    // line 348
                    echo twig_get_attribute($this->env, $this->source, $context["attribute"], "name", [], "any", false, false, false, 348);
                    echo "</b></td>
                                                        <td class=\"text-right\">";
                    // line 349
                    echo twig_get_attribute($this->env, $this->source, $context["attribute"], "text", [], "any", false, false, false, 349);
                    echo "</td>
                                                    </tr>
                                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attribute'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 352
                echo "                                                </tbody>
                                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attribute_group'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 354
            echo "                                        </table>
                                    </div>
                                ";
        }
        // line 357
        echo "
                                ";
        // line 358
        if (($context["question_status"] ?? null)) {
            // line 359
            echo "                                    <div class=\"tab-pane\" id=\"tab-questions\">
                                        ";
            // line 360
            echo ($context["product_questions"] ?? null);
            echo "
                                    </div>
                                ";
        }
        // line 363
        echo "
                                ";
        // line 364
        if (($context["review_status"] ?? null)) {
            // line 365
            echo "                                <div class=\"tab-pane\" id=\"tab-review\">
                                    <div class=\"row\">
                                        <div class=\"col-sm-6\">
                                            <h4><b>";
            // line 368
            echo ($context["button_reviews"] ?? null);
            echo "</b></h4>

                                            <div id=\"review\">
                                                ";
            // line 371
            if (($context["seo_reviews"] ?? null)) {
                // line 372
                echo "                                                    ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["seo_reviews"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["review"]) {
                    // line 373
                    echo "                                                        <div class=\"table\">
                                                            <div class=\"table-cell\"><i class=\"fa fa-user\"></i></div>
                                                            <div class=\"table-cell right\">
                                                                <p class=\"author\"><b>";
                    // line 376
                    echo twig_get_attribute($this->env, $this->source, $context["review"], "author", [], "any", false, false, false, 376);
                    echo "</b>  -  ";
                    echo twig_get_attribute($this->env, $this->source, $context["review"], "date_added", [], "any", false, false, false, 376);
                    echo "
                                                                    <span class=\"rating\">
\t\t<span class=\"rating_stars rating r";
                    // line 378
                    echo twig_get_attribute($this->env, $this->source, $context["review"], "rating", [], "any", false, false, false, 378);
                    echo "\">
\t\t<i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>
\t\t</span>
\t\t</span>
                                                                </p>
                                                                ";
                    // line 383
                    echo twig_get_attribute($this->env, $this->source, $context["review"], "text", [], "any", false, false, false, 383);
                    echo "
                                                            </div>
                                                        </div>
                                                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['review'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 387
                echo "                                                    ";
                if (($context["pagination"] ?? null)) {
                    // line 388
                    echo "                                                        <div class=\"pagination-holder\">";
                    echo ($context["pagination"] ?? null);
                    echo "</div>
                                                    ";
                }
                // line 390
                echo "                                                ";
            } else {
                // line 391
                echo "                                                    <p>";
                echo ($context["text_no_reviews"] ?? null);
                echo "</p>
                                                ";
            }
            // line 393
            echo "                                            </div>

                                        </div>
                                        <div class=\"col-sm-6 right\">
                                            <form class=\"form-horizontal\" id=\"form-review\">

                                                <h4 id=\"review-notification\"><b>";
            // line 399
            echo ($context["text_write"] ?? null);
            echo "</b></h4>
                                                ";
            // line 400
            if (($context["review_guest"] ?? null)) {
                // line 401
                echo "
                                                    <div class=\"form-group required\">
                                                        <div class=\"col-sm-12 rating-stars\">
                                                            <label class=\"control-label\">";
                // line 404
                echo ($context["entry_rating"] ?? null);
                echo "</label>

                                                            <input type=\"radio\" value=\"1\" name=\"rating\" id=\"rating1\" />
                                                            <label for=\"rating1\"><i class=\"fa fa-star-o\"></i></label>

                                                            <input type=\"radio\" value=\"2\" name=\"rating\" id=\"rating2\" />
                                                            <label for=\"rating2\"><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i></label>

                                                            <input type=\"radio\" value=\"3\" name=\"rating\" id=\"rating3\" />
                                                            <label for=\"rating3\"><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i></label>

                                                            <input type=\"radio\" value=\"4\" name=\"rating\" id=\"rating4\" />
                                                            <label for=\"rating4\"><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i></label>

                                                            <input type=\"radio\" value=\"5\" name=\"rating\" id=\"rating5\" />
                                                            <label for=\"rating5\"><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i></label>
                                                        </div>
                                                    </div>

                                                    <div class=\"form-group required\">
                                                        <div class=\"col-sm-12\">
                                                            <label class=\"control-label\" for=\"input-name\">";
                // line 425
                echo ($context["entry_name"] ?? null);
                echo "</label>
                                                            <input type=\"text\" name=\"name\" value=\"";
                // line 426
                echo ($context["customer_name"] ?? null);
                echo "\" id=\"input-name\" class=\"form-control grey\" />
                                                        </div>
                                                    </div>
                                                    <div class=\"form-group required\">
                                                        <div class=\"col-sm-12\">
                                                            <label class=\"control-label\" for=\"input-review\">";
                // line 431
                echo ($context["entry_review"] ?? null);
                echo "</label>
                                                            <textarea name=\"text\" rows=\"5\" id=\"input-review\" class=\"form-control grey\"></textarea>
                                                            <small>";
                // line 433
                echo ($context["text_note"] ?? null);
                echo "</small>
                                                        </div>
                                                    </div>

                                                    <div class=\"form-group required\">
                                                        <div class=\"col-sm-12\">
                                                            ";
                // line 439
                echo ($context["captcha"] ?? null);
                echo "
                                                        </div>
                                                    </div>

                                                    <div class=\"buttons clearfix\">
                                                        <div class=\"text-right\">
                                                            <button type=\"button\" id=\"button-review\" data-loading-text=\"";
                // line 445
                echo ($context["text_loading"] ?? null);
                echo "\" class=\"btn btn-outline\">";
                echo ($context["button_continue"] ?? null);
                echo "</button>
                                                        </div>
                                                    </div>
                                                ";
            } else {
                // line 449
                echo "                                                    ";
                echo ($context["text_login"] ?? null);
                echo "
                                                ";
            }
            // line 451
            echo "                                            </form>
                                        </div>
                                    </div>
                                </div>
                                ";
        }
        // line 455
        echo "<!-- if review-status ends -->

                            </div> <!-- .tab-content ends -->
                        </div> <!-- .col-sm-12 ends -->
                    </div> <!-- .row ends -->
                    <!-- Tabs area ends -->

                    ";
        // line 462
        if (($context["full_width_tabs"] ?? null)) {
            // line 463
            echo "                </div>
                ";
        }
        // line 465
        echo "            </div>


        </div> <!-- .table-cell.right ends -->

    </div> <!-- .product-info ends -->

           <!-- Related Products -->

    ";
        // line 474
        if (($context["full_width_tabs"] ?? null)) {
            // line 475
            echo "    <div class=\"container c10padd\">
        ";
        }
        // line 477
        echo "
        ";
        // line 478
        if (($context["products"] ?? null)) {
            // line 479
            echo "            <div class=\"widget widget-related\">

                <div class=\"widget-title\">
                    <p class=\"main-title\"><span>";
            // line 482
            echo ($context["text_related"] ?? null);
            echo "</span></p>
                    <p class=\"widget-title-separator\"><i class=\"icon-line-cross\"></i></p>
                </div>

                <div class=\"grid grid-holder related carousel grid";
            // line 486
            echo ($context["basel_rel_prod_grid"] ?? null);
            echo "\">
                    ";
            // line 487
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 488
                echo "                        ";
                $this->loadTemplate("basel/template/product/single_product.twig", "basel/template/product/product.twig", 488)->display($context);
                // line 489
                echo "                    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 490
            echo "                </div>
            </div>
        ";
        }
        // line 493
        echo "
        ";
        // line 494
        echo ($context["content_bottom"] ?? null);
        echo "

        ";
        // line 496
        if (($context["full_width_tabs"] ?? null)) {
            // line 497
            echo "    </div>
    ";
        }
        // line 499
        echo "

    ";
        // line 501
        if ( !($context["full_width_tabs"] ?? null)) {
            // line 502
            echo "</div> <!-- main column ends -->
";
            // line 503
            echo ($context["column_right"] ?? null);
            echo "
    </div> <!-- .row ends -->
    </div> <!-- .container ends -->
";
        }
        // line 507
        echo "
<script src=\"catalog/view/theme/basel/js/lightgallery/js/lightgallery.min.js\"></script>
<script src=\"catalog/view/theme/basel/js/lightgallery/js/lg-zoom.min.js\"></script>
<script src=\"catalog/view/theme/basel/js/cloudzoom/cloud-zoom.1.0.2.min.js\"></script>
";
        // line 511
        if (($context["basel_price_update"] ?? null)) {
            // line 512
            echo "    <script src=\"index.php?route=extension/basel/live_options/js&product_id=";
            echo ($context["product_id"] ?? null);
            echo "\"></script>
";
        }
        // line 514
        echo "
";
        // line 515
        if (($context["products"] ?? null)) {
            // line 516
            echo "    <script><!--
        \$('.grid-holder.related').slick({
            prevArrow: \"<a class=\\\"arrow-left icon-arrow-left\\\"></a>\",
            nextArrow: \"<a class=\\\"arrow-right icon-arrow-right\\\"></a>\",
            dots:true,
            ";
            // line 521
            if ((($context["direction"] ?? null) == "rtl")) {
                // line 522
                echo "            rtl: true,
            ";
            }
            // line 524
            echo "            respondTo:'min',
            ";
            // line 525
            if ((($context["basel_rel_prod_grid"] ?? null) == "5")) {
                // line 526
                echo "            slidesToShow:5,slidesToScroll:5,responsive:[{breakpoint:1100,settings:{slidesToShow:4,slidesToScroll:4}},{breakpoint:960,settings:{slidesToShow:3,slidesToScroll:3}},{breakpoint:600,settings:{slidesToShow:2,slidesToScroll:2}},
                ";
            } elseif ((            // line 527
($context["basel_rel_prod_grid"] ?? null) == "4")) {
                // line 528
                echo "                slidesToShow:4,slidesToScroll:4,responsive:[{breakpoint:960,settings:{slidesToShow:3,slidesToScroll:3}},{breakpoint:600,settings:{slidesToShow:2,slidesToScroll:2}},
            ";
            } elseif ((            // line 529
($context["basel_rel_prod_grid"] ?? null) == "3")) {
                // line 530
                echo "            slidesToShow:3,slidesToScroll:3,responsive:[{breakpoint:600,settings:{slidesToShow:2,slidesToScroll:2}},
            ";
            } elseif ((            // line 531
($context["basel_rel_prod_grid"] ?? null) == "2")) {
                // line 532
                echo "            slidesToShow:2,slidesToScroll:2,responsive:[
            ";
            }
            // line 534
            echo "            ";
            if (($context["items_mobile_fw"] ?? null)) {
                // line 535
                echo "            {breakpoint:320,settings:{slidesToShow:1,slidesToScroll:1}}
            ";
            }
            // line 537
            echo "        ]
        });
        \$('.product-style2 .single-product .icon').attr('data-placement', 'top');
        \$('[data-toggle=\\'tooltip\\']').tooltip({container: 'body'});
        //--></script>
";
        }
        // line 543
        echo "
";
        // line 544
        if ((($context["sale_end_date"] ?? null) && ($context["product_page_countdown"] ?? null))) {
            // line 545
            echo "    <script>
        \$(function() {
            \$('#special_countdown').countdown('";
            // line 547
            echo ($context["sale_end_date"] ?? null);
            echo "').on('update.countdown', function(event) {
                var \$this = \$(this).html(event.strftime(''
                    + '<div class=\\\"special_countdown\\\"></span><p><span class=\\\"icon-clock\\\"></span> ";
            // line 549
            echo ($context["basel_text_offer_ends"] ?? null);
            echo "</p><div>'
                    + '%D<i>";
            // line 550
            echo ($context["basel_text_days"] ?? null);
            echo "</i></div><div>'
                    + '%H <i>";
            // line 551
            echo ($context["basel_text_hours"] ?? null);
            echo "</i></div><div>'
                    + '%M <i>";
            // line 552
            echo ($context["basel_text_mins"] ?? null);
            echo "</i></div><div>'
                    + '%S <i>";
            // line 553
            echo ($context["basel_text_secs"] ?? null);
            echo "</i></div></div>'));
            });
        });
    </script>
";
        }
        // line 558
        echo "
<script><!--
    \$('select[name=\\'recurring_id\\'], input[name=\"quantity\"]').change(function(){
        \$.ajax({
            url: 'index.php?route=product/product/getRecurringDescription',
            type: 'post',
            data: \$('input[name=\\'product_id\\'], input[name=\\'quantity\\'], select[name=\\'recurring_id\\']'),
            dataType: 'json',
            beforeSend: function() {
                \$('#recurring-description').html('');
            },
            success: function(json) {
                \$('.alert-dismissible, .text-danger').remove();

                if (json['success']) {
                    \$('#recurring-description').html(json['success']);
                }
            }
        });
    });
    //--></script>


<script type=\"application/javascript\">
    function kosarica_dodaj(product_id, scale, select) {
        let selected = \$('#input-option' + select)[0];
        let default_id = \$('#input-default' + select)[0].value;

        if (selected.selectedIndex > JSON.parse(scale).items.length) {
            cart.add(product_id, Number(selected.value), 'option[' + product_id + ']=' + default_id);
        } else {
            cart.add(product_id, 1, 'option[' + product_id + ']=' + selected.value);
        }
    }
</script>


<script><!--
    \$('#button-cart').on('click', function() {

        /*\$.ajax({
            url: 'index.php?route=extension/basel/basel_features/add_to_cart',
            type: 'post',
            data: \$('#product input[type=\\'text\\'], #product input[type=\\'hidden\\'], #product input[type=\\'number\\'], #product input[type=\\'radio\\']:checked, #product input[type=\\'checkbox\\']:checked, #product select, #product textarea'),
            dataType: 'json',
            beforeSend: function(json) {
                \$('body').append('<span class=\"basel-spinner ajax-call\"></span>');
            },

            success: function(json) {
                \$('.alert, .text-danger').remove();
                \$('.table-cell').removeClass('has-error');

                if (json.error) {
                    \$('.basel-spinner.ajax-call').remove();
                    if (json.error.option) {
                        for (i in json.error.option) {
                            var element = \$('#input-option' + i.replace('_', '-'));

                            if (element.parent().hasClass('input-group')) {
                                element.parent().after('<div class=\"text-danger\">' + json.error.option[i] + '</div>');
                            } else {
                                element.after('<div class=\"text-danger\">' + json.error.option[i] + '</div>');
                            }
                        }
                    }

                    if (json.error.recurring) {
                        \$('select[name=\\'recurring_id\\']').after('<div class=\"text-danger\">' + json['error']['recurring'] + '</div>');
                    }

                    // Highlight any found errors
                    \$('.text-danger').parent().addClass('has-error');
                }

                if (json.success_redirect) {

                    location = json.success_redirect;

                } else if (json.success) {

                    \$('.table-cell').removeClass('has-error');
                    \$('.alert, .popup-note, .basel-spinner.ajax-call, .text-danger').remove();

                    html = '<div class=\"popup-note\">';
                    html += '<div class=\"inner\">';
                    html += '<a class=\"popup-note-close\" onclick=\"\$(this).parent().parent().remove()\">&times;</a>';
                    html += '<div class=\"table\">';
                    html += '<div class=\"table-cell v-top img\"><img src=\"' + json.image + '\" /></div>';
                    html += '<div class=\"table-cell v-top\">' + json.success + '</div>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    \$('body').append(html);
                    setTimeout(function() {\$('.popup-note').hide();}, 8100);
                    // Need to set timeout otherwise it wont update the total
                    setTimeout(function () {
                        \$('.cart-total-items').html( json.total_items );
                        \$('.cart-total-amount').html( json.total_amount );
                    }, 100);

                    \$('#cart-content').load('index.php?route=common/cart/info #cart-content > *');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
        });*/
    });
    //--></script>
<script><!--
    \$('.date').datetimepicker({
        pickTime: false
    });

    \$('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });

    \$('.time').datetimepicker({
        pickDate: false
    });

    \$('button[id^=\\'button-upload\\']').on('click', function() {
        var node = this;

        \$('#form-upload').remove();

        \$('body').prepend('<form enctype=\"multipart/form-data\" id=\"form-upload\" style=\"display: none;\"><input type=\"file\" name=\"file\" /></form>');

        \$('#form-upload input[name=\\'file\\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function() {
            if (\$('#form-upload input[name=\\'file\\']').val() != '') {
                clearInterval(timer);

                \$.ajax({
                    url: 'index.php?route=tool/upload',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData(\$('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        \$(node).button('loading');
                    },
                    complete: function() {
                        \$(node).button('reset');
                    },
                    success: function(json) {
                        \$('.text-danger').remove();

                        if (json['error']) {
                            \$(node).parent().find('input').after('<div class=\"text-danger\">' + json['error'] + '</div>');
                        }

                        if (json['success']) {
                            alert(json['success']);

                            \$(node).parent().find('input').val(json['code']);
                        }
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                    }
                });
            }
        }, 500);
    });
    //--></script>
<script><!--
    \$('#review').delegate('.pagination a', 'click', function(e) {
        e.preventDefault();
        \$(\"html,body\").animate({scrollTop:((\$(\"#review\").offset().top)-50)},500);
        \$('#review').fadeOut(50);

        \$('#review').load(this.href);

        \$('#review').fadeIn(500);

    });


    \$('#button-review').on('click', function() {
        \$.ajax({
            url: 'index.php?route=product/product/write&product_id=";
        // line 749
        echo ($context["product_id"] ?? null);
        echo "',
            type: 'post',
            dataType: 'json',
            data: \$(\"#form-review\").serialize(),
            beforeSend: function() {
                \$('#button-review').button('loading');
            },
            complete: function() {
                \$('#button-review').button('reset');
            },
            success: function(json) {
                \$('.alert-success, .alert-danger').remove();

                if (json.error) {
                    \$('#review-notification').after('<div class=\"alert alert-sm alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ' + json.error + '</div>');
                }

                if (json.success) {
                    \$('#review-notification').after('<div class=\"alert alert-sm alert-success\"><i class=\"fa fa-check-circle\"></i> ' + json.success + '</div>');

                    \$('input[name=\\'name\\']').val('');
                    \$('textarea[name=\\'text\\']').val('');
                    \$('input[name=\\'rating\\']:checked').prop('checked', false);
                }
            }
        });
    });

    \$(document).ready(function() {
        ";
        // line 778
        if ((($context["product_layout"] ?? null) == "full-width")) {
            // line 779
            echo "// Sticky information
        \$('.table-cell.right .inner').theiaStickySidebar({containerSelector:'.product-info'});
        ";
        }
        // line 782
        echo "
// Reviews/Question scroll link
        \$(\".to_tabs\").click(function() {
            \$('html, body').animate({
                scrollTop: (\$(\".main_tabs\").offset().top - 100)
            }, 1000);
        });

// Sharing buttons
        ";
        // line 791
        if (($context["basel_share_btn"] ?? null)) {
            // line 792
            echo "        var share_url = encodeURIComponent(window.location.href);
        var page_title = '";
            // line 793
            echo ($context["heading_title"] ?? null);
            echo "';
        ";
            // line 794
            if (($context["thumb"] ?? null)) {
                // line 795
                echo "        var thumb = '";
                echo ($context["thumb"] ?? null);
                echo "';
        ";
            }
            // line 797
            echo "        \$('.fb_share').attr(\"href\", 'https://www.facebook.com/sharer/sharer.php?u=' + share_url + '');
        \$('.twitter_share').attr(\"href\", 'https://twitter.com/intent/tweet?source=' + share_url + '&text=' + page_title + ': ' + share_url + '');
        \$('.google_share').attr(\"href\", 'https://plus.google.com/share?url=' + share_url + '');
        \$('.pinterest_share').attr(\"href\", 'http://pinterest.com/pin/create/button/?url=' + share_url + '&media=' + thumb + '&description=' + page_title + '');
        \$('.vk_share').attr(\"href\", 'http://vkontakte.ru/share.php?url=' + share_url + '');
        ";
        }
        // line 803
        echo "    });
    //--></script>

";
        // line 806
        if ((($context["product_layout"] ?? null) != "full-width")) {
            // line 807
            echo "    <script>
        \$(document).ready(function() {
            \$('.image-additional a.link').click(function (e) {
                if (\$(this).hasClass(\"locked\")) {
                    e.stopImmediatePropagation();
                }
                \$('.image-additional a.link.active').removeClass('active');
                \$(this).addClass('active')
            });

            ";
            // line 817
            if (($context["images"] ?? null)) {
                // line 818
                echo "            \$('.cloud-zoom-wrap').click(function (e) {
                e.preventDefault();
                \$('.image-additional a.link.active').removeClass('locked').trigger('click').addClass('locked');
            });
            ";
            } else {
                // line 823
                echo "            \$('.cloud-zoom-wrap').click(function (e) {
                e.preventDefault();
                \$('#main-image').trigger('click');
            });
            ";
            }
            // line 828
            echo "
            \$('.image-additional').slick({
                prevArrow: \"<a class=\\\"icon-arrow-left\\\"></a>\",
                nextArrow: \"<a class=\\\"icon-arrow-right\\\"></a>\",
                appendArrows: '.image-additional .slick-list',
                arrows:true,
                ";
            // line 834
            if ((($context["direction"] ?? null) == "rtl")) {
                // line 835
                echo "                rtl: true,
                ";
            }
            // line 837
            echo "                infinite:false,
                ";
            // line 838
            if ((($context["product_layout"] ?? null) == "images-left")) {
                // line 839
                echo "                slidesToShow: ";
                echo twig_round((($context["img_h"] ?? null) / ($context["img_a_h"] ?? null)), 0, "floor");
                echo ",
                vertical:true,
                verticalSwiping:true,
                ";
            } else {
                // line 843
                echo "                slidesToShow: ";
                echo twig_round((($context["img_w"] ?? null) / ($context["img_a_w"] ?? null)));
                echo ",
                ";
            }
            // line 845
            echo "                responsive: [
                    {
                        breakpoint: 992,
                        settings: {
                            vertical:false,
                            verticalSwiping:false
                        }
                    }]
            });

        });
        //--></script>
";
        }
        // line 858
        echo "<script>
    \$(document).ready(function() {
// Image Gallery
        \$(\"#gallery\").lightGallery({
            selector: '.link',
            download:false,
            hideBarsDelay:99999
        });
    });
    //--></script>
<script type=\"application/ld+json\">
{
\"@context\": \"http://schema.org\",
\"@type\": \"Product\",
\"image\": [
";
        // line 873
        if (($context["thumb"] ?? null)) {
            // line 874
            echo "\"";
            echo ($context["thumb"] ?? null);
            echo "\"
";
        }
        // line 876
        echo "],
\"description\": \"";
        // line 877
        echo ($context["meta_description"] ?? null);
        echo "\",
";
        // line 878
        if (($context["review_qty"] ?? null)) {
            // line 879
            echo "\"aggregateRating\": {
\"@type\": \"AggregateRating\",
\"ratingValue\": \"";
            // line 881
            echo ($context["rating"] ?? null);
            echo "\",
\"reviewCount\": \"";
            // line 882
            echo ($context["review_qty"] ?? null);
            echo "\"},
";
        }
        // line 884
        echo "\"name\": \"";
        echo ($context["heading_title"] ?? null);
        echo "\",
\"sku\": \"";
        // line 885
        echo ($context["model"] ?? null);
        echo "\",
";
        // line 886
        if (($context["manufacturer"] ?? null)) {
            // line 887
            echo "\"brand\": \"";
            echo ($context["manufacturer"] ?? null);
            echo "\",
";
        }
        // line 889
        echo "\"offers\": {
\"@type\": \"Offer\",
";
        // line 891
        if ((($context["qty"] ?? null) > 0)) {
            // line 892
            echo "\"availability\": \"http://schema.org/InStock\",
";
        } else {
            // line 894
            echo "\"availability\": \"http://schema.org/OutOfStock\",
";
        }
        // line 896
        if (($context["price"] ?? null)) {
            // line 897
            echo "    ";
            if (($context["special"] ?? null)) {
                // line 898
                echo "\"price\": \"";
                echo ($context["special_snippet"] ?? null);
                echo "\",
";
            } else {
                // line 900
                echo "\"price\": \"";
                echo ($context["price_snippet"] ?? null);
                echo "\",
";
            }
            // line 902
            echo "\"priceCurrency\": \"";
            echo ($context["currency_code"] ?? null);
            echo "\"
";
        }
        // line 904
        echo "}
}
</script>
";
        // line 907
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "basel/template/product/product.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1888 => 907,  1883 => 904,  1877 => 902,  1871 => 900,  1865 => 898,  1862 => 897,  1860 => 896,  1856 => 894,  1852 => 892,  1850 => 891,  1846 => 889,  1840 => 887,  1838 => 886,  1834 => 885,  1829 => 884,  1824 => 882,  1820 => 881,  1816 => 879,  1814 => 878,  1810 => 877,  1807 => 876,  1801 => 874,  1799 => 873,  1782 => 858,  1767 => 845,  1761 => 843,  1753 => 839,  1751 => 838,  1748 => 837,  1744 => 835,  1742 => 834,  1734 => 828,  1727 => 823,  1720 => 818,  1718 => 817,  1706 => 807,  1704 => 806,  1699 => 803,  1691 => 797,  1685 => 795,  1683 => 794,  1679 => 793,  1676 => 792,  1674 => 791,  1663 => 782,  1658 => 779,  1656 => 778,  1624 => 749,  1431 => 558,  1423 => 553,  1419 => 552,  1415 => 551,  1411 => 550,  1407 => 549,  1402 => 547,  1398 => 545,  1396 => 544,  1393 => 543,  1385 => 537,  1381 => 535,  1378 => 534,  1374 => 532,  1372 => 531,  1369 => 530,  1367 => 529,  1364 => 528,  1362 => 527,  1359 => 526,  1357 => 525,  1354 => 524,  1350 => 522,  1348 => 521,  1341 => 516,  1339 => 515,  1336 => 514,  1330 => 512,  1328 => 511,  1322 => 507,  1315 => 503,  1312 => 502,  1310 => 501,  1306 => 499,  1302 => 497,  1300 => 496,  1295 => 494,  1292 => 493,  1287 => 490,  1273 => 489,  1270 => 488,  1253 => 487,  1249 => 486,  1242 => 482,  1237 => 479,  1235 => 478,  1232 => 477,  1228 => 475,  1226 => 474,  1215 => 465,  1211 => 463,  1209 => 462,  1200 => 455,  1193 => 451,  1187 => 449,  1178 => 445,  1169 => 439,  1160 => 433,  1155 => 431,  1147 => 426,  1143 => 425,  1119 => 404,  1114 => 401,  1112 => 400,  1108 => 399,  1100 => 393,  1094 => 391,  1091 => 390,  1085 => 388,  1082 => 387,  1072 => 383,  1064 => 378,  1057 => 376,  1052 => 373,  1047 => 372,  1045 => 371,  1039 => 368,  1034 => 365,  1032 => 364,  1029 => 363,  1023 => 360,  1020 => 359,  1018 => 358,  1015 => 357,  1010 => 354,  1003 => 352,  994 => 349,  990 => 348,  987 => 347,  983 => 346,  976 => 342,  972 => 340,  968 => 339,  964 => 337,  962 => 336,  959 => 335,  956 => 334,  947 => 331,  942 => 330,  937 => 329,  935 => 328,  929 => 325,  922 => 320,  914 => 318,  911 => 317,  905 => 315,  902 => 314,  896 => 312,  893 => 311,  890 => 310,  879 => 308,  874 => 307,  872 => 306,  868 => 305,  864 => 304,  857 => 299,  853 => 297,  848 => 294,  846 => 293,  843 => 292,  836 => 288,  833 => 287,  831 => 286,  823 => 280,  820 => 279,  814 => 275,  809 => 272,  806 => 271,  804 => 270,  801 => 269,  782 => 267,  780 => 266,  777 => 265,  769 => 263,  767 => 262,  760 => 260,  757 => 259,  747 => 257,  745 => 256,  734 => 254,  731 => 253,  723 => 251,  721 => 250,  712 => 243,  706 => 241,  704 => 240,  698 => 239,  692 => 238,  686 => 234,  680 => 232,  678 => 231,  668 => 229,  657 => 228,  653 => 226,  647 => 225,  642 => 222,  636 => 218,  625 => 216,  621 => 215,  617 => 214,  611 => 211,  608 => 210,  606 => 209,  603 => 208,  598 => 205,  589 => 202,  583 => 198,  577 => 197,  569 => 195,  566 => 194,  562 => 193,  554 => 190,  548 => 186,  541 => 184,  534 => 182,  532 => 181,  525 => 180,  521 => 179,  517 => 178,  511 => 177,  503 => 174,  495 => 172,  493 => 171,  490 => 170,  486 => 169,  483 => 168,  481 => 167,  475 => 163,  469 => 161,  467 => 160,  464 => 159,  456 => 157,  454 => 156,  451 => 155,  447 => 154,  443 => 152,  431 => 150,  427 => 149,  424 => 148,  422 => 147,  418 => 145,  409 => 142,  403 => 140,  401 => 139,  398 => 138,  396 => 137,  393 => 136,  388 => 134,  381 => 130,  378 => 129,  376 => 128,  370 => 125,  363 => 120,  349 => 108,  345 => 106,  331 => 104,  328 => 103,  307 => 100,  304 => 99,  300 => 98,  297 => 97,  295 => 96,  292 => 95,  270 => 92,  267 => 91,  261 => 89,  259 => 88,  256 => 87,  250 => 85,  248 => 84,  245 => 83,  239 => 81,  237 => 80,  233 => 78,  231 => 77,  223 => 75,  221 => 74,  212 => 68,  207 => 67,  204 => 66,  201 => 65,  198 => 64,  195 => 63,  192 => 62,  189 => 61,  187 => 60,  183 => 59,  178 => 57,  174 => 55,  163 => 53,  159 => 52,  155 => 50,  149 => 46,  143 => 44,  138 => 42,  133 => 41,  131 => 40,  125 => 37,  121 => 35,  115 => 33,  109 => 31,  107 => 30,  102 => 27,  96 => 25,  94 => 24,  90 => 22,  84 => 20,  79 => 18,  74 => 17,  72 => 16,  66 => 13,  62 => 11,  56 => 9,  50 => 7,  48 => 6,  44 => 4,  42 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "basel/template/product/product.twig", "");
    }
}
