<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* basel/template/extension/module/basel_products.twig */
class __TwigTemplate_ee864cba7078f7f8613a7d11bd4b2e582158b6044c2f73aec8d4a8e0b1b61db0 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"widget module";
        echo ($context["module"] ?? null);
        echo " ";
        if ((($context["columns"] ?? null) != "list")) {
            echo " grid";
        }
        if (($context["contrast"] ?? null)) {
            echo " contrast-bg";
        }
        if ((($context["carousel"] ?? null) && (($context["rows"] ?? null) > 1))) {
            echo "  multiple-rows";
        }
        echo "\" ";
        if (($context["use_margin"] ?? null)) {
            echo "style=\"margin-bottom: ";
            echo ($context["margin"] ?? null);
            echo "\"";
        }
        echo ">
    ";
        // line 2
        if (($context["block_title"] ?? null)) {
            // line 3
            echo "        <!-- Block Title -->
        <div class=\"widget-title\">
            ";
            // line 5
            if (($context["title_preline"] ?? null)) {
                echo "<p class=\"pre-line\">";
                echo ($context["title_preline"] ?? null);
                echo "</p>";
            }
            // line 6
            echo "            ";
            if (($context["title"] ?? null)) {
                // line 7
                echo "                <p class=\"main-title\"><span>";
                echo ($context["title"] ?? null);
                echo "</span></p>
                <p class=\"widget-title-separator\"><i class=\"icon-line-cross\"></i></p>
            ";
            }
            // line 10
            echo "            ";
            if (($context["title_subline"] ?? null)) {
                // line 11
                echo "                <p class=\"sub-line\"><span>";
                echo ($context["title_subline"] ?? null);
                echo "</span></p>
            ";
            }
            // line 13
            echo "        </div>
    ";
        }
        // line 15
        echo "    ";
        if ((twig_length_filter($this->env, ($context["tabs"] ?? null)) > 1)) {
            // line 16
            echo "        <!-- Tabs -->
        <ul id=\"tabs-";
            // line 17
            echo ($context["module"] ?? null);
            echo "\" class=\"nav nav-tabs ";
            echo ($context["tabstyle"] ?? null);
            echo "\" data-tabs=\"tabs\" style=\"\">
            ";
            // line 18
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["tabs"] ?? null));
            foreach ($context['_seq'] as $context["keyTab"] => $context["tab"]) {
                // line 19
                echo "                ";
                if (($context["keyTab"] == 0)) {
                    // line 20
                    echo "                    <li class=\"active\"><a href=\"#tab";
                    echo ($context["module"] ?? null);
                    echo $context["keyTab"];
                    echo "\" data-toggle=\"tab\">";
                    echo twig_get_attribute($this->env, $this->source, $context["tab"], "title", [], "any", false, false, false, 20);
                    echo "</a></li>
                ";
                } else {
                    // line 22
                    echo "                    <li><a href=\"#tab";
                    echo ($context["module"] ?? null);
                    echo $context["keyTab"];
                    echo "\" data-toggle=\"tab\">";
                    echo twig_get_attribute($this->env, $this->source, $context["tab"], "title", [], "any", false, false, false, 22);
                    echo "</a></li>
                ";
                }
                // line 24
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['keyTab'], $context['tab'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 25
            echo "        </ul>
    ";
        }
        // line 27
        echo "    <div class=\"tab-content has-carousel ";
        if ( !($context["carousel"] ?? null)) {
            echo "overflow-hidden";
        }
        echo "\">
        <!-- Product Group(s) -->
        ";
        // line 29
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["tabs"] ?? null));
        foreach ($context['_seq'] as $context["key"] => $context["tab"]) {
            // line 30
            echo "            <div class=\"tab-pane";
            if (($context["key"] == 0)) {
                echo " active in";
            }
            echo " fade\" id=\"tab";
            echo ($context["module"] ?? null);
            echo $context["key"];
            echo "\">
                <div class=\"grid-holder grid";
            // line 31
            echo ($context["columns"] ?? null);
            echo " prod_module";
            echo ($context["module"] ?? null);
            if (($context["carousel"] ?? null)) {
                echo " carousel";
            }
            if ((($context["carousel_a"] ?? null) && (($context["rows"] ?? null) > 1))) {
                echo " sticky-arrows";
            }
            echo "\">
                    ";
            // line 32
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["tab"], "products", [], "any", false, false, false, 32));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 33
                echo "                        <div class=\"item single-product\">
                            <div class=\"image\"";
                // line 34
                if ((($context["columns"] ?? null) == "list")) {
                    echo " style=\"width:";
                    echo ($context["img_width"] ?? null);
                    echo "px\"";
                }
                echo ">
                                <a href=\"";
                // line 35
                echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 35);
                echo "\">
                                    <img src=\"";
                // line 36
                echo twig_get_attribute($this->env, $this->source, $context["product"], "thumb", [], "any", false, false, false, 36);
                echo "\" alt=\"";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 36);
                echo "\" title=\"";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 36);
                echo "\"/>
                                    ";
                // line 37
                if (twig_get_attribute($this->env, $this->source, $context["product"], "thumb2", [], "any", false, false, false, 37)) {
                    // line 38
                    echo "                                        <img class=\"thumb2\" src=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "thumb2", [], "any", false, false, false, 38);
                    echo "\" alt=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 38);
                    echo "\" title=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 38);
                    echo "\"/>
                                    ";
                }
                // line 40
                echo "                                </a>
                                ";
                // line 41
                if (((twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 41) && twig_get_attribute($this->env, $this->source, $context["product"], "special", [], "any", false, false, false, 41)) && twig_get_attribute($this->env, $this->source, $context["product"], "sale_badge", [], "any", false, false, false, 41))) {
                    // line 42
                    echo "                                    <div class=\"sale-counter id";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 42);
                    echo "\"></div>
                                    <span class=\"badge sale_badge\"><i>";
                    // line 43
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "sale_badge", [], "any", false, false, false, 43);
                    echo "</i></span>
                                ";
                }
                // line 45
                echo "                                ";
                if (twig_get_attribute($this->env, $this->source, $context["product"], "new_label", [], "any", false, false, false, 45)) {
                    // line 46
                    echo "                                    <span class=\"badge new_badge\"><i>";
                    echo ($context["basel_text_new"] ?? null);
                    echo "</i></span>
                                ";
                }
                // line 48
                echo "                                ";
                if (((twig_get_attribute($this->env, $this->source, $context["product"], "quantity", [], "any", false, false, false, 48) < 1) && ($context["stock_badge_status"] ?? null))) {
                    // line 49
                    echo "                                    <span class=\"badge out_of_stock_badge\"><i>";
                    echo ($context["basel_text_out_of_stock"] ?? null);
                    echo "</i></span>
                                    ";
                    // line 50
                    $context["button_cart"] = ($context["basel_text_out_of_stock"] ?? null);
                    // line 51
                    echo "                                ";
                } else {
                    // line 52
                    echo "                                    ";
                    $context["button_cart"] = ($context["default_button_cart"] ?? null);
                    // line 53
                    echo "                                ";
                }
                // line 54
                echo "                                <a class=\"img-overlay\" href=\"";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 54);
                echo "\"></a>
                                <div class=\"btn-center catalog_hide\"><a class=\"btn btn-light-outline btn-thin\" onclick=\"cart.add('";
                // line 55
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 55);
                echo "', '";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "minimum", [], "any", false, false, false, 55);
                echo "');\">";
                echo ($context["button_cart"] ?? null);
                echo "</a></div>
                                <div class=\"icons-wrapper\">
                                    <!-- <a class=\"icon is-cart catalog_hide\" data-toggle=\"tooltip\" data-placement=\"";
                // line 57
                echo ($context["tooltip_align"] ?? null);
                echo "\" data-title=\"";
                echo ($context["button_cart"] ?? null);
                echo "\" onclick=\"cart.add('";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 57);
                echo "', '";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "minimum", [], "any", false, false, false, 57);
                echo "');\"><span class=\"global-cart\"></span></a>-->
                                    <a class=\"icon is_wishlist\" data-toggle=\"tooltip\" data-placement=\"";
                // line 58
                echo ($context["tooltip_align"] ?? null);
                echo "\" data-title=\"";
                echo ($context["button_wishlist"] ?? null);
                echo "\" onclick=\"wishlist.add('";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 58);
                echo "');\"><span class=\"icon-heart\"></span></a>
                                    <a class=\"icon is_compare\" onclick=\"compare.add('";
                // line 59
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 59);
                echo "');\" data-toggle=\"tooltip\" data-placement=\"";
                echo ($context["tooltip_align"] ?? null);
                echo "\" data-title=\"";
                echo ($context["button_compare"] ?? null);
                echo "\"><span class=\"icon-refresh\"></span></a>
                                    <a class=\"icon is_quickview hidden-xs\" onclick=\"quickview('";
                // line 60
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 60);
                echo "');\" data-toggle=\"tooltip\" data-placement=\"";
                echo ($context["tooltip_align"] ?? null);
                echo "\" data-title=\"";
                echo ($context["basel_button_quickview"] ?? null);
                echo "\"><span class=\"icon-magnifier-add\"></span></a>
                                </div> <!-- .icons-wrapper -->
                            </div><!-- .image ends -->
                            <div class=\"caption\">
                                <a class=\"product-name\" href=\"";
                // line 64
                echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 64);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 64);
                echo "</a>
                                ";
                // line 65
                if (twig_get_attribute($this->env, $this->source, $context["product"], "rating", [], "any", false, false, false, 65)) {
                    // line 66
                    echo "                                    <div class=\"rating\">
                                        <span class=\"rating_stars rating r";
                    // line 67
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "rating", [], "any", false, false, false, 67);
                    echo "\">
                                        <i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>
                                        </span>
                                    </div>
                                ";
                }
                // line 72
                echo "

                                <div class=\"price-wrapper\">
                                    <div class=\"row\">
                                        <div class=\"col-lg-4\">
                                            ";
                // line 77
                if (twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 77)) {
                    // line 78
                    echo "
                                                <div class=\"price\">

                                                    ";
                    // line 81
                    if (twig_get_attribute($this->env, $this->source, $context["product"], "special", [], "any", false, false, false, 81)) {
                        // line 82
                        echo "                                                        <span class=\"price-old price\">";
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 82);
                        echo "</span>
                                                        <span class=\"price-new\">";
                        // line 83
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "special", [], "any", false, false, false, 83);
                        echo "</span>
                                                    ";
                    } else {
                        // line 85
                        echo "                                                        <span class=\"price\">Cijena</span>
                                                        <span>";
                        // line 86
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 86);
                        echo "</span>
                                                    ";
                    }
                    // line 88
                    echo "                                                    ";
                    if (twig_get_attribute($this->env, $this->source, $context["product"], "tax", [], "any", false, false, false, 88)) {
                        // line 89
                        echo "                                                        <span class=\"price-tax\">";
                        echo ($context["text_tax"] ?? null);
                        echo " ";
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "tax", [], "any", false, false, false, 89);
                        echo "</span>
                                                    ";
                    }
                    // line 91
                    echo "                                                </div><!-- .price -->
                                            ";
                }
                // line 93
                echo "                                        </div>

                                        <div class=\"col-lg-8\">

                                            <div class=\"input-group\">
                                                <select name=\"";
                // line 98
                echo $context["key"];
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 98);
                echo "\" class=\"form-control num";
                echo $context["key"];
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 98);
                echo "\">
                                                    ";
                // line 99
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["product"], "scale", [], "any", false, false, false, 99), "items", [], "any", false, false, false, 99));
                foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                    // line 100
                    echo "                                                        <option value=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["item"], "value", [], "any", false, false, false, 100);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["item"], "label", [], "any", false, false, false, 100);
                    echo "</option>
                                                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 102
                echo "                                                    ";
                if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["product"], "scale", [], "any", false, false, false, 102), "entries", [], "any", false, false, false, 102) == 1)) {
                    // line 103
                    echo "                                                        <option value=\"+\">+</option>
                                                    ";
                }
                // line 105
                echo "                                                </select>
                                                <span class=\"input-group-btn\">
                                                    <button class=\"btn btn-neutral btn-green\" onclick=\"cart.add('";
                // line 107
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 107);
                echo "', \$(this).parent().parent().find('.num";
                echo $context["key"];
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 107);
                echo "').val());\">
                                                        <span class=\"global-cart\"></span>
                                                    </button>
                                                </span>
                                            </div><!-- /input-group -->

                                            <script>
                                                \$('[name=\"";
                // line 114
                echo $context["key"];
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 114);
                echo "\"]').otherDropdown({classes: 'form-control', value: '+', placeholder: 'Upiši'})
                                            </script>

                                        </div>
                                    </div>

                                </div><!-- .price-wrapper -->


                                <div class=\"plain-links\">
                                    <a class=\"icon is_wishlist link-hover-color\" onclick=\"wishlist.add('";
                // line 124
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 124);
                echo "');\"><span class=\"icon-heart\"></span> ";
                echo ($context["button_wishlist"] ?? null);
                echo "</a>
                                    <a class=\"icon is_compare link-hover-color\" onclick=\"compare.add('";
                // line 125
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 125);
                echo "');\"><span class=\"icon-refresh\"></span> ";
                echo ($context["button_compare"] ?? null);
                echo "</a>
                                    <a class=\"icon is_quickview link-hover-color\" onclick=\"quickview('";
                // line 126
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 126);
                echo "');\"><span class=\"icon-magnifier-add\"></span> ";
                echo ($context["basel_button_quickview"] ?? null);
                echo "</a>
                                </div><!-- .plain-links-->
                            </div><!-- .caption-->
                            ";
                // line 129
                if ((twig_get_attribute($this->env, $this->source, $context["product"], "sale_end_date", [], "any", false, false, false, 129) && ($context["countdown_status"] ?? null))) {
                    // line 130
                    echo "                                <script>
                                    \$(function () {
                                        \$(\".module";
                    // line 132
                    echo ($context["module"] ?? null);
                    echo " .sale-counter.id";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 132);
                    echo "\").countdown(\"";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "sale_end_date", [], "any", false, false, false, 132);
                    echo "\").on('update.countdown', function (event) {
                                            var \$this = \$(this).html(event.strftime(''
                                                + '<div>'
                                                + '%D<i>";
                    // line 135
                    echo ($context["basel_text_days"] ?? null);
                    echo "</i></div><div>'
                                                + '%H <i>";
                    // line 136
                    echo ($context["basel_text_hours"] ?? null);
                    echo "</i></div><div>'
                                                + '%M <i>";
                    // line 137
                    echo ($context["basel_text_mins"] ?? null);
                    echo "</i></div><div>'
                                                + '%S <i>";
                    // line 138
                    echo ($context["basel_text_secs"] ?? null);
                    echo "</i></div></div>'));
                                        });
                                    });
                                </script>
                            ";
                }
                // line 143
                echo "                        </div><!-- .single-product ends -->
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 145
            echo "                </div>
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['tab'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 148
        echo "        ";
        if (($context["use_button"] ?? null)) {
            // line 149
            echo "            <!-- Button -->
            <div class=\"widget_bottom_btn ";
            // line 150
            if ((($context["carousel"] ?? null) && ($context["carousel_b"] ?? null))) {
                echo "has-dots";
            }
            echo "\">
                <a class=\"btn btn-contrast\" href=\"";
            // line 151
            echo ((($context["link_href"] ?? null)) ? (($context["link_href"] ?? null)) : (""));
            echo "\">";
            echo ($context["link_title"] ?? null);
            echo "</a>
            </div>
        ";
        }
        // line 154
        echo "    </div>
    <div class=\"clearfix\"></div>
</div>
";
        // line 157
        if (($context["carousel"] ?? null)) {
            // line 158
            echo "    <script>
        \$('.grid-holder.prod_module";
            // line 159
            echo ($context["module"] ?? null);
            echo "').slick({
            ";
            // line 160
            if (($context["carousel_a"] ?? null)) {
                // line 161
                echo "            prevArrow:    \"<a class=\\\"arrow-left icon-arrow-left\\\"></a>\",
            nextArrow:    \"<a class=\\\"arrow-right icon-arrow-right\\\"></a>\",
            ";
            } else {
                // line 164
                echo "            arrows:       false,
            ";
            }
            // line 166
            echo "            ";
            if ((($context["direction"] ?? null) == "rtl")) {
                // line 167
                echo "            rtl:          true,
            ";
            }
            // line 169
            echo "            ";
            if (($context["carousel_b"] ?? null)) {
                // line 170
                echo "            dots:         true,
            ";
            }
            // line 172
            echo "            respondTo:    'min',
            rows:";
            // line 173
            echo ($context["rows"] ?? null);
            echo ",
            ";
            // line 174
            if ((($context["columns"] ?? null) == "5")) {
                // line 175
                echo "            slidesToShow: 5, slidesToScroll: 5, responsive: [{breakpoint: 1100, settings: {slidesToShow: 4, slidesToScroll: 4}}, {breakpoint: 960, settings: {slidesToShow: 3, slidesToScroll: 3}}, {breakpoint: 600, settings: {slidesToShow: 2, slidesToScroll: 2}},
                ";
            } elseif ((            // line 176
($context["columns"] ?? null) == "4")) {
                // line 177
                echo "                slidesToShow
        :
        4, slidesToScroll
        :
        4, responsive
        :
        [{breakpoint: 960, settings: {slidesToShow: 3, slidesToScroll: 3}}, {breakpoint: 600, settings: {slidesToShow: 2, slidesToScroll: 2}},
            ";
            } elseif ((            // line 184
($context["columns"] ?? null) == "3")) {
                // line 185
                echo "            slidesToShow
        :
        3, slidesToScroll
        :
        3, responsive
        :
        [{breakpoint: 600, settings: {slidesToShow: 2, slidesToScroll: 2}},
            ";
            } elseif ((            // line 192
($context["columns"] ?? null) == "2")) {
                // line 193
                echo "            slidesToShow
        :
        2, slidesToScroll
        :
        2, responsive
        :
        [
            ";
            } elseif (((            // line 200
($context["columns"] ?? null) == "1") || (($context["columns"] ?? null) == "list"))) {
                // line 201
                echo "            adaptiveHeight
        :
        true, slidesToShow
        :
        1, slidesToScroll
        :
        1, responsive
        :
        [
            ";
            }
            // line 211
            echo "            ";
            if (($context["items_mobile_fw"] ?? null)) {
                // line 212
                echo "            {breakpoint: 420, settings: {slidesToShow: 1, slidesToScroll: 1}}
            ";
            }
            // line 214
            echo "        ]
        })
        ;
        \$('.product-style2 .single-product .icon').attr('data-placement', 'top');
        \$('[data-toggle=\\'tooltip\\']').tooltip({container: 'body'});
        ";
            // line 219
            if ((($context["carousel_a"] ?? null) && (($context["rows"] ?? null) > 1))) {
                // line 220
                echo "        \$(window).load(function () {
            var p_c_o = \$('.prod_module";
                // line 221
                echo ($context["module"] ?? null);
                echo "').offset().top;
            var p_c_o_b = \$('.prod_module";
                // line 222
                echo ($context["module"] ?? null);
                echo "').offset().top + \$('.prod_module";
                echo ($context["module"] ?? null);
                echo "').outerHeight(true) - 100;
            var p_sticky_arrows = function () {
                var p_m_o = \$(window).scrollTop() + (\$(window).height() / 2);
                if (p_m_o > p_c_o && p_m_o < p_c_o_b) {
                    \$('.prod_module";
                // line 226
                echo ($context["module"] ?? null);
                echo " .slick-arrow').addClass('visible').css('top', p_m_o - p_c_o + 'px');
                } else {
                    \$('.prod_module";
                // line 228
                echo ($context["module"] ?? null);
                echo " .slick-arrow').removeClass('visible');
                }
            };
            \$(window).scroll(function () {
                p_sticky_arrows();
            });
        });
        ";
            }
            // line 236
            echo "    </script>
";
        }
    }

    public function getTemplateName()
    {
        return "basel/template/extension/module/basel_products.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  680 => 236,  669 => 228,  664 => 226,  655 => 222,  651 => 221,  648 => 220,  646 => 219,  639 => 214,  635 => 212,  632 => 211,  620 => 201,  618 => 200,  609 => 193,  607 => 192,  598 => 185,  596 => 184,  587 => 177,  585 => 176,  582 => 175,  580 => 174,  576 => 173,  573 => 172,  569 => 170,  566 => 169,  562 => 167,  559 => 166,  555 => 164,  550 => 161,  548 => 160,  544 => 159,  541 => 158,  539 => 157,  534 => 154,  526 => 151,  520 => 150,  517 => 149,  514 => 148,  506 => 145,  499 => 143,  491 => 138,  487 => 137,  483 => 136,  479 => 135,  469 => 132,  465 => 130,  463 => 129,  455 => 126,  449 => 125,  443 => 124,  429 => 114,  416 => 107,  412 => 105,  408 => 103,  405 => 102,  394 => 100,  390 => 99,  382 => 98,  375 => 93,  371 => 91,  363 => 89,  360 => 88,  355 => 86,  352 => 85,  347 => 83,  342 => 82,  340 => 81,  335 => 78,  333 => 77,  326 => 72,  318 => 67,  315 => 66,  313 => 65,  307 => 64,  296 => 60,  288 => 59,  280 => 58,  270 => 57,  261 => 55,  256 => 54,  253 => 53,  250 => 52,  247 => 51,  245 => 50,  240 => 49,  237 => 48,  231 => 46,  228 => 45,  223 => 43,  218 => 42,  216 => 41,  213 => 40,  203 => 38,  201 => 37,  193 => 36,  189 => 35,  181 => 34,  178 => 33,  174 => 32,  162 => 31,  152 => 30,  148 => 29,  140 => 27,  136 => 25,  130 => 24,  121 => 22,  112 => 20,  109 => 19,  105 => 18,  99 => 17,  96 => 16,  93 => 15,  89 => 13,  83 => 11,  80 => 10,  73 => 7,  70 => 6,  64 => 5,  60 => 3,  58 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "basel/template/extension/module/basel_products.twig", "");
    }
}
