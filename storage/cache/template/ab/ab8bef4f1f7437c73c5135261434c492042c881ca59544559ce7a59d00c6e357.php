<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* basel/template/extension/quickcheckout/shipping_method.twig */
class __TwigTemplate_464a36865ecb39b3d98d1067678027a2af0167f9316f0d984396907dec9f551b extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["error_warning"] ?? null)) {
            // line 2
            echo "    <div class=\"alert alert-danger\">";
            echo ($context["error_warning"] ?? null);
            echo "</div>
";
        }
        // line 4
        if (($context["shipping_methods"] ?? null)) {
            // line 5
            echo "    <p>";
            echo ($context["text_shipping_method"] ?? null);
            echo "</p>
    ";
            // line 6
            if (($context["shipping"] ?? null)) {
                // line 7
                echo "        <table class=\"table\">
            ";
                // line 8
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["shipping_methods"] ?? null));
                foreach ($context['_seq'] as $context["key"] => $context["shipping_method"]) {
                    // line 9
                    echo "                ";
                    if ( !twig_get_attribute($this->env, $this->source, $context["shipping_method"], "error", [], "any", false, false, false, 9)) {
                        // line 10
                        echo "                    ";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["shipping_method"], "quote", [], "any", false, false, false, 10));
                        foreach ($context['_seq'] as $context["_key"] => $context["quote"]) {
                            // line 11
                            echo "                        ";
                            if (($context["key"] != "collector")) {
                                // line 12
                                echo "                            <tr>
                                <td>";
                                // line 13
                                if ((twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 13) == ($context["code"] ?? null))) {
                                    // line 14
                                    echo "                                        <input type=\"radio\" name=\"shipping_method\" value=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 14);
                                    echo "\" id=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 14);
                                    echo "\" checked=\"checked\" />
                                    ";
                                } else {
                                    // line 16
                                    echo "                                        <input type=\"radio\" name=\"shipping_method\" value=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 16);
                                    echo "\" id=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 16);
                                    echo "\" />
                                    ";
                                }
                                // line 17
                                echo "</td>
                                <td style=\"width:100%;padding-left:10px;\">
                                    <label for=\"";
                                // line 19
                                echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 19);
                                echo "\">
                                        ";
                                // line 20
                                if ((($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["shipping_logo"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4[$context["key"]] ?? null) : null)) {
                                    // line 21
                                    echo "                                            <img src=\"";
                                    echo (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = ($context["shipping_logo"] ?? null)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144[$context["key"]] ?? null) : null);
                                    echo "\" alt=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["shipping_method"], "title", [], "any", false, false, false, 21);
                                    echo "\" title=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["shipping_method"], "title", [], "any", false, false, false, 21);
                                    echo "\" />
                                        ";
                                }
                                // line 23
                                echo "                                        ";
                                echo twig_get_attribute($this->env, $this->source, $context["quote"], "title", [], "any", false, false, false, 23);
                                echo "</label></td>
                                <td style=\"text-align: right;\"><label for=\"";
                                // line 24
                                echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 24);
                                echo "\">";
                                echo twig_get_attribute($this->env, $this->source, $context["quote"], "text", [], "any", false, false, false, 24);
                                echo "</label></td>
                            </tr>
                        ";
                            } else {
                                // line 27
                                echo "                            <tr>
                                <td>";
                                // line 28
                                if ((twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 28) == ($context["code"] ?? null))) {
                                    // line 29
                                    echo "                                        <input type=\"radio\" name=\"shipping_method\" value=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 29);
                                    echo "\" id=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 29);
                                    echo "\" checked=\"checked\" />
                                    ";
                                } else {
                                    // line 31
                                    echo "                                        <input type=\"radio\" name=\"shipping_method\" value=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 31);
                                    echo "\" id=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 31);
                                    echo "\" />
                                    ";
                                }
                                // line 32
                                echo "</td>
                                <td style=\"width:100%;padding-left:10px;\">
                                    <label for=\"";
                                // line 34
                                echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 34);
                                echo "\">
                                        ";
                                // line 35
                                if ((($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = ($context["shipping_logo"] ?? null)) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b[$context["key"]] ?? null) : null)) {
                                    // line 36
                                    echo "                                            <img src=\"";
                                    echo (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = ($context["shipping_logo"] ?? null)) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002[$context["key"]] ?? null) : null);
                                    echo "\" alt=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["shipping_method"], "title", [], "any", false, false, false, 36);
                                    echo "\" title=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["shipping_method"], "title", [], "any", false, false, false, 36);
                                    echo "\" />
                                        ";
                                }
                                // line 38
                                echo "                                        ";
                                echo twig_get_attribute($this->env, $this->source, $context["quote"], "title", [], "any", false, false, false, 38);
                                echo "</label></td>
                                <td style=\"text-align: right;\"><label for=\"";
                                // line 39
                                echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 39);
                                echo "\">";
                                echo twig_get_attribute($this->env, $this->source, $context["quote"], "text", [], "any", false, false, false, 39);
                                echo "</label></td>
                            </tr>
                            <tr id=\"collector-select\"";
                                // line 41
                                echo ">
                                <td colspan=\"3\">
                                    <select name=\"collector_pick\" class=\"form-control\" id=\"input-collector-pick\">
                                        <option value=\"NULL\">Test select...</option>
                                        ";
                                // line 52
                                echo "                                    </select>
                                </td>
                            </tr>
                        ";
                            }
                            // line 56
                            echo "                    ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['quote'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 57
                        echo "                ";
                    } else {
                        // line 58
                        echo "                    <tr>
                        <td colspan=\"3\"><div class=\"error\">";
                        // line 59
                        echo twig_get_attribute($this->env, $this->source, $context["shipping_method"], "error", [], "any", false, false, false, 59);
                        echo "</div></td>
                    </tr>
                ";
                    }
                    // line 62
                    echo "            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['shipping_method'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 63
                echo "        </table>
    ";
            } else {
                // line 65
                echo "        <select class=\"form-control\" name=\"shipping_method\">
            ";
                // line 66
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["shipping_methods"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["shipping_method"]) {
                    // line 67
                    echo "                ";
                    if ( !twig_get_attribute($this->env, $this->source, $context["shipping_method"], "error", [], "any", false, false, false, 67)) {
                        // line 68
                        echo "                    ";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["shipping_method"], "quote", [], "any", false, false, false, 68));
                        foreach ($context['_seq'] as $context["_key"] => $context["quote"]) {
                            // line 69
                            echo "                        ";
                            if ((twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 69) == ($context["code"] ?? null))) {
                                // line 70
                                echo "                            ";
                                $context["code"] = twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 70);
                                // line 71
                                echo "                            ";
                                $context["exists"] = true;
                                // line 72
                                echo "                        <option value=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 72);
                                echo "\" selected=\"selected\">
                        ";
                            } else {
                                // line 74
                                echo "                            <option value=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 74);
                                echo "\">
                        ";
                            }
                            // line 76
                            echo "                        ";
                            echo twig_get_attribute($this->env, $this->source, $context["quote"], "title", [], "any", false, false, false, 76);
                            echo "&nbsp;&nbsp;(";
                            echo twig_get_attribute($this->env, $this->source, $context["quote"], "text", [], "any", false, false, false, 76);
                            echo ")</option>
                    ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['quote'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 78
                        echo "                ";
                    }
                    // line 79
                    echo "            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['shipping_method'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 80
                echo "        </select><br />
    ";
            }
            // line 82
            echo "    <br />
";
        }
        // line 84
        if ((($context["delivery"] ?? null) && (( !($context["delivery_delivery_time"] ?? null) || (($context["delivery_delivery_time"] ?? null) == "1")) || (($context["delivery_delivery_time"] ?? null) == "3")))) {
            // line 85
            echo "    <div";
            echo ((($context["delivery_required"] ?? null)) ? (" class=\"required\"") : (""));
            echo ">
        <label class=\"control-label\"><strong>";
            // line 86
            echo ($context["text_delivery"] ?? null);
            echo "</strong></label>
        ";
            // line 87
            if ((($context["delivery_delivery_time"] ?? null) == "1")) {
                // line 88
                echo "            <input type=\"text\" name=\"delivery_date\" value=\"";
                echo ($context["delivery_date"] ?? null);
                echo "\" class=\"form-control date\" readonly=\"true\" style=\"background:#ffffff;\" />
        ";
            } else {
                // line 90
                echo "            <input type=\"text\" name=\"delivery_date\" value=\"";
                echo ($context["delivery_date"] ?? null);
                echo "\" class=\"form-control date\" readonly=\"true\" style=\"background:#ffffff;\" />
        ";
            }
            // line 92
            echo "        ";
            if ((($context["delivery_delivery_time"] ?? null) == "3")) {
                echo "<br />
            <select name=\"delivery_time\" class=\"form-control\">";
                // line 93
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["delivery_times"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["quickcheckout_delivery_time"]) {
                    // line 94
                    echo "                    ";
                    if ((($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = $context["quickcheckout_delivery_time"]) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4[($context["language_id"] ?? null)] ?? null) : null)) {
                        // line 95
                        echo "                        ";
                        if ((($context["delivery_time"] ?? null) == (($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = $context["quickcheckout_delivery_time"]) && is_array($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) || $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 instanceof ArrayAccess ? ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666[($context["language_id"] ?? null)] ?? null) : null))) {
                            // line 96
                            echo "                            <option value=\"";
                            echo (($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = $context["quickcheckout_delivery_time"]) && is_array($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e) || $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e instanceof ArrayAccess ? ($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e[($context["language_id"] ?? null)] ?? null) : null);
                            echo "\" selected=\"selected\">";
                            echo (($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 = $context["quickcheckout_delivery_time"]) && is_array($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52) || $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 instanceof ArrayAccess ? ($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52[($context["language_id"] ?? null)] ?? null) : null);
                            echo "</option>
                        ";
                        } else {
                            // line 98
                            echo "                            <option value=\"";
                            echo (($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 = $context["quickcheckout_delivery_time"]) && is_array($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136) || $__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 instanceof ArrayAccess ? ($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136[($context["language_id"] ?? null)] ?? null) : null);
                            echo "\">";
                            echo (($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 = $context["quickcheckout_delivery_time"]) && is_array($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386) || $__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 instanceof ArrayAccess ? ($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386[($context["language_id"] ?? null)] ?? null) : null);
                            echo "</option>
                        ";
                        }
                        // line 100
                        echo "                    ";
                    }
                    // line 101
                    echo "                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['quickcheckout_delivery_time'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                echo "</select>
        ";
            }
            // line 103
            echo "    </div>
";
        } elseif ((        // line 104
($context["delivery_delivery_time"] ?? null) && (($context["delivery_delivery_time"] ?? null) == "2"))) {
            // line 105
            echo "    <input type=\"text\" name=\"delivery_date\" value=\"\" class=\"hide\" />
    <select name=\"delivery_time\" class=\"hide\"><option value=\"\"></option></select>
    <strong>";
            // line 107
            echo ($context["text_estimated_delivery"] ?? null);
            echo "</strong><br />
    ";
            // line 108
            echo ($context["estimated_delivery"] ?? null);
            echo "<br />
    ";
            // line 109
            echo ($context["estimated_delivery_time"] ?? null);
            echo "
";
        } else {
            // line 111
            echo "    <input type=\"text\" name=\"delivery_date\" value=\"\" class=\"hide\" />
    <select name=\"delivery_time\" class=\"hide\"><option value=\"\"></option></select>
";
        }
        // line 114
        echo "
<script type=\"text/javascript\"><!--
    \$('#shipping-method input[name=\\'shipping_method\\'], #shipping-method select[name=\\'shipping_method\\']').on('change', function() {

        // fj.agmedia.hr
        let checked = \$('#shipping-method input[name=\\'shipping_method\\']:checked').val();
        console.log(checked)
        if (checked == 'collector.collector') {
            \$('#collector-select').removeClass('hide');
        } else {
            \$('#collector-select').addClass('hide');
        }


        ";
        // line 128
        if ( !($context["logged"] ?? null)) {
            // line 129
            echo "        if (\$('#payment-address input[name=\\'shipping_address\\']:checked').val()) {
            var post_data = \$('#payment-address input[type=\\'text\\'], #payment-address input[type=\\'checkbox\\']:checked, #payment-address input[type=\\'radio\\']:checked, #payment-address input[type=\\'hidden\\'], #payment-address select, #shipping-method input[type=\\'text\\'], #shipping-method input[type=\\'checkbox\\']:checked, #shipping-method input[type=\\'radio\\']:checked, #shipping-method input[type=\\'hidden\\'], #shipping-method select, #shipping-method textarea');
        } else {
            var post_data = \$('#shipping-address input[type=\\'text\\'], #shipping-address input[type=\\'checkbox\\']:checked, #shipping-address input[type=\\'radio\\']:checked, #shipping-address input[type=\\'hidden\\'], #shipping-address select, #shipping-method input[type=\\'text\\'], #shipping-method input[type=\\'checkbox\\']:checked, #shipping-method input[type=\\'radio\\']:checked, #shipping-method input[type=\\'hidden\\'], #shipping-method select, #shipping-method textarea');
        }

        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/shipping_method/set',
            type: 'post',
            data: post_data,
            dataType: 'html',
            cache: false,
            success: function(html) {
                ";
            // line 142
            if (($context["cart"] ?? null)) {
                // line 143
                echo "                loadCart();
                ";
            }
            // line 145
            echo "
                ";
            // line 146
            if (($context["shipping_reload"] ?? null)) {
                // line 147
                echo "                reloadPaymentMethod();
                ";
            }
            // line 149
            echo "            },
            ";
            // line 150
            if (($context["debug"] ?? null)) {
                // line 151
                echo "            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
            }
            // line 155
            echo "        });
        ";
        } else {
            // line 157
            echo "        if (\$('#shipping-address input[name=\\'shipping_address\\']:checked').val() == 'new') {
            var url = 'index.php?route=extension/quickcheckout/shipping_method/set';
            var post_data = \$('#shipping-address input[type=\\'text\\'], #shipping-address input[type=\\'checkbox\\']:checked, #shipping-address input[type=\\'radio\\']:checked, #shipping-address input[type=\\'hidden\\'], #shipping-address select, #shipping-method input[type=\\'text\\'], #shipping-method input[type=\\'checkbox\\']:checked, #shipping-method input[type=\\'radio\\']:checked, #shipping-method input[type=\\'hidden\\'], #shipping-method select, #shipping-method textarea');
        } else {
            var url = 'index.php?route=extension/quickcheckout/shipping_method/set&address_id=' + \$('#shipping-address select[name=\\'address_id\\']').val();
            var post_data = \$('#shipping-method input[type=\\'text\\'], #shipping-method input[type=\\'checkbox\\']:checked, #shipping-method input[type=\\'radio\\']:checked, #shipping-method input[type=\\'hidden\\'], #shipping-method select, #shipping-method textarea');
        }

        \$.ajax({
            url: url,
            type: 'post',
            data: post_data,
            dataType: 'html',
            cache: false,
            success: function(html) {
                ";
            // line 172
            if (($context["cart"] ?? null)) {
                // line 173
                echo "                loadCart();
                ";
            }
            // line 175
            echo "
                ";
            // line 176
            if (($context["shipping_reload"] ?? null)) {
                // line 177
                echo "                if (\$('#payment-address input[name=\\'payment_address\\']').val() == 'new') {
                    reloadPaymentMethod();
                } else {
                    reloadPaymentMethodById(\$('#payment-address select[name=\\'address_id\\']').val());
                }
                ";
            }
            // line 183
            echo "            },
            ";
            // line 184
            if (($context["debug"] ?? null)) {
                // line 185
                echo "            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
            }
            // line 189
            echo "        });
        ";
        }
        // line 191
        echo "    });

    \$(document).ready(function() {
        \$('#shipping-method input[name=\\'shipping_method\\']:checked, #shipping-method select[name=\\'shipping_method\\']').trigger('change');
    });

    ";
        // line 197
        if ((($context["delivery"] ?? null) && (($context["delivery_delivery_time"] ?? null) == "1"))) {
            // line 198
            echo "    \$(document).ready(function() {
        \$('input[name=\\'delivery_date\\']').datetimepicker({
            format: 'YYYY-MM-DD HH:mm',
            minDate: '";
            // line 201
            echo ($context["delivery_min"] ?? null);
            echo "',
            maxDate: '";
            // line 202
            echo ($context["delivery_max"] ?? null);
            echo "',
            disabledDates: [";
            // line 203
            echo ($context["delivery_unavailable"] ?? null);
            echo "],
            enabledHours: [";
            // line 204
            echo ($context["hours"] ?? null);
            echo "],
            ignoreReadonly: true,
            ";
            // line 206
            if ((($context["delivery_days_of_week"] ?? null) != "")) {
                // line 207
                echo "            daysOfWeekDisabled: [";
                echo ($context["delivery_days_of_week"] ?? null);
                echo "]
            ";
            }
            // line 209
            echo "        });
    });
    ";
        } elseif ((        // line 211
($context["delivery"] ?? null) && ((($context["delivery_delivery_time"] ?? null) == "3") || (($context["delivery_delivery_time"] ?? null) == "0")))) {
            // line 212
            echo "    \$('input[name=\\'delivery_date\\']').datetimepicker({
        format: 'YYYY-MM-DD',
        minDate: '";
            // line 214
            echo ($context["delivery_min"] ?? null);
            echo "',
        maxDate: '";
            // line 215
            echo ($context["delivery_max"] ?? null);
            echo "',
        disabledDates: [";
            // line 216
            echo ($context["delivery_unavailable"] ?? null);
            echo "],
        ignoreReadonly: true,
        ";
            // line 218
            if ((($context["delivery_days_of_week"] ?? null) != "")) {
                // line 219
                echo "        daysOfWeekDisabled: [";
                echo ($context["delivery_days_of_week"] ?? null);
                echo "]
        ";
            }
            // line 221
            echo "    });
    ";
        }
        // line 223
        echo "    //--></script>";
    }

    public function getTemplateName()
    {
        return "basel/template/extension/quickcheckout/shipping_method.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  567 => 223,  563 => 221,  557 => 219,  555 => 218,  550 => 216,  546 => 215,  542 => 214,  538 => 212,  536 => 211,  532 => 209,  526 => 207,  524 => 206,  519 => 204,  515 => 203,  511 => 202,  507 => 201,  502 => 198,  500 => 197,  492 => 191,  488 => 189,  482 => 185,  480 => 184,  477 => 183,  469 => 177,  467 => 176,  464 => 175,  460 => 173,  458 => 172,  441 => 157,  437 => 155,  431 => 151,  429 => 150,  426 => 149,  422 => 147,  420 => 146,  417 => 145,  413 => 143,  411 => 142,  396 => 129,  394 => 128,  378 => 114,  373 => 111,  368 => 109,  364 => 108,  360 => 107,  356 => 105,  354 => 104,  351 => 103,  342 => 101,  339 => 100,  331 => 98,  323 => 96,  320 => 95,  317 => 94,  313 => 93,  308 => 92,  302 => 90,  296 => 88,  294 => 87,  290 => 86,  285 => 85,  283 => 84,  279 => 82,  275 => 80,  269 => 79,  266 => 78,  255 => 76,  249 => 74,  243 => 72,  240 => 71,  237 => 70,  234 => 69,  229 => 68,  226 => 67,  222 => 66,  219 => 65,  215 => 63,  209 => 62,  203 => 59,  200 => 58,  197 => 57,  191 => 56,  185 => 52,  179 => 41,  172 => 39,  167 => 38,  157 => 36,  155 => 35,  151 => 34,  147 => 32,  139 => 31,  131 => 29,  129 => 28,  126 => 27,  118 => 24,  113 => 23,  103 => 21,  101 => 20,  97 => 19,  93 => 17,  85 => 16,  77 => 14,  75 => 13,  72 => 12,  69 => 11,  64 => 10,  61 => 9,  57 => 8,  54 => 7,  52 => 6,  47 => 5,  45 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "basel/template/extension/quickcheckout/shipping_method.twig", "");
    }
}
