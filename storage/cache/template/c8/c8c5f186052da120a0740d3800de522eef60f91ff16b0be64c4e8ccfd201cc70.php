<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* basel/template/extension/total/voucher.twig */
class __TwigTemplate_4c6e9dab969e555291f57fd8e51c07e1f3cbb244a42270bd5cd5cd90c1db5493 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "
    <h4><b>";
        // line 2
        echo ($context["heading_title"] ?? null);
        echo "</b></h4>
 
<div class=\"form-vertical\">
 <label class=\"control-label\" for=\"input-coupon\">";
        // line 5
        echo ($context["entry_voucher"] ?? null);
        echo "</label>
        <input type=\"text\" name=\"voucher\" value=\"";
        // line 6
        echo ($context["voucher"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_voucher"] ?? null);
        echo "\" id=\"input-voucher\" class=\"form-control margin-b10\" />
        
        <input type=\"submit\" value=\"";
        // line 8
        echo ($context["button_voucher"] ?? null);
        echo "\" id=\"button-voucher\" data-loading-text=\"";
        echo ($context["text_loading"] ?? null);
        echo "\"  class=\"btn btn-primary\" />
</div>
      
<script><!--
\$('#button-voucher').on('click', function() {
  \$.ajax({
    url: 'index.php?route=extension/total/voucher/voucher',
    type: 'post',
    data: 'voucher=' + encodeURIComponent(\$('input[name=\\'voucher\\']').val()),
    dataType: 'json',
    beforeSend: function() {
      \$('#button-voucher').button('loading');
    },
    complete: function() {
      \$('#button-voucher').button('reset');
    },
    success: function(json) {
      \$('.alert').remove();

      if (json['error']) {
\t\t\$('#content').before('<div class=\"col-sm-12\"><div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error'] + '<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div></div>');

        \$('html, body').animate({ scrollTop: 0 }, 'slow');
      }

      if (json['redirect']) {
        location = json['redirect'];
      }
    }
  });
});
//--></script>";
    }

    public function getTemplateName()
    {
        return "basel/template/extension/total/voucher.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  50 => 6,  46 => 5,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "basel/template/extension/total/voucher.twig", "");
    }
}
