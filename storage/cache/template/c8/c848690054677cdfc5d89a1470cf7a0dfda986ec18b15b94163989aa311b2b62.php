<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* basel/template/extension/total/coupon.twig */
class __TwigTemplate_825bbb6c7a76e86ed2558689b86b761747689eb96baa05d04c255a2afcea1627 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "


    <h4><b>
        <a class=\"block\" role=\"button\" data-toggle=\"collapse\" href=\"#collapsecoupon\" aria-expanded=\"false\" aria-controls=\"collapsecoupon\">
            ";
        // line 6
        echo ($context["heading_title"] ?? null);
        echo "  <b class=\"pull-right visible-xs\">+</b>
        </a>
        </b></h4>
    <div class=\"collapse in\" id=\"collapsecoupon\">

<div class=\"form-vertical\">
      <label class=\"control-label\" for=\"input-coupon\">";
        // line 12
        echo ($context["entry_coupon"] ?? null);
        echo "</label>
      
        <input type=\"text\" name=\"coupon\" value=\"";
        // line 14
        echo ($context["coupon"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_coupon"] ?? null);
        echo "\" id=\"input-coupon\" class=\"form-control margin-b10\" />
        
<input type=\"button\" value=\"";
        // line 16
        echo ($context["button_coupon"] ?? null);
        echo "\" id=\"button-coupon\" data-loading-text=\"";
        echo ($context["text_loading"] ?? null);
        echo "\"  class=\"btn btn-outline\" />


</div>

    </div>

    <script>
        \$(document).ready(function(){
            if (\$(window).width() <= 768) {
                \$(\"#collapsecoupon\").removeClass(\"in\");

            }
        });
    </script>
        
      <script><!--
\$('#button-coupon').on('click', function() {
\t\$.ajax({
\t\turl: 'index.php?route=extension/total/coupon/coupon',
\t\ttype: 'post',
\t\tdata: 'coupon=' + encodeURIComponent(\$('input[name=\\'coupon\\']').val()),
\t\tdataType: 'json',
\t\tbeforeSend: function() {
\t\t\t\$('#button-coupon').button('loading');
\t\t},
\t\tcomplete: function() {
\t\t\t\$('#button-coupon').button('reset');
\t\t},
\t\tsuccess: function(json) {
\t\t\t\$('.alert').remove();

\t\t\tif (json['error']) {
\t\t\t\t\$('#content').before('<div class=\"col-sm-12\"><div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error'] + '<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div></div>');

\t\t\t\t\$('html, body').animate({ scrollTop: 0 }, 'slow');
\t\t\t}

\t\t\tif (json['redirect']) {
\t\t\t\tlocation = json['redirect'];
\t\t\t}
\t\t}
\t});
});
//--></script>";
    }

    public function getTemplateName()
    {
        return "basel/template/extension/total/coupon.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 16,  58 => 14,  53 => 12,  44 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "basel/template/extension/total/coupon.twig", "");
    }
}
