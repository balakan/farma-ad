<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* basel/template/common/headers/header1.twig */
class __TwigTemplate_6c48c18906304a0541892ac4f8c8d82c15d2a87ae9051878bf5c923772ac614b extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"header-wrapper header1 fixed-header-possible\">
";
        // line 2
        if (($context["top_line_style"] ?? null)) {
            // line 3
            echo "<div class=\"top_line\">
  <div class=\"container ";
            // line 4
            echo ($context["top_line_width"] ?? null);
            echo "\">
  \t<div class=\"table\">
        <div class=\"table-cell left sm-text-center xs-text-center\">
            <div class=\"promo-message\">";
            // line 7
            echo ($context["promo_message"] ?? null);
            echo "</div>
        </div>
        <div class=\"table-cell text-right hidden-xs hidden-sm\">
            <div class=\"links\">
            <ul>
            ";
            // line 12
            $this->loadTemplate("basel/template/common/static_links.twig", "basel/template/common/headers/header1.twig", 12)->display($context);
            // line 13
            echo "            </ul>
            ";
            // line 14
            if (($context["lang_curr_title"] ?? null)) {
                // line 15
                echo "            <div class=\"setting-ul\">
            <div class=\"setting-li dropdown-wrapper from-left lang-curr-trigger nowrap\"><a>
            <span>";
                // line 17
                echo ($context["lang_curr_title"] ?? null);
                echo "</span>
            </a>
            <div class=\"dropdown-content dropdown-right lang-curr-wrapper\">
            ";
                // line 20
                echo ($context["language"] ?? null);
                echo "
            ";
                // line 21
                echo ($context["currency"] ?? null);
                echo "
            </div>
            </div>
            </div>
            ";
            }
            // line 26
            echo "            </div>
        </div>
    </div> <!-- .table ends -->
  </div> <!-- .container ends -->
</div> <!-- .top_line ends -->
";
        }
        // line 32
        echo "<span class=\"table header-main sticky-header-placeholder\">&nbsp;</span>
<div class=\"sticky-header outer-container header-style\">
<div class=\"container ";
        // line 34
        echo ($context["main_header_width"] ?? null);
        echo "\">
<div class=\"table header-main\">
<div class=\"table-cell w40 menu-cell hidden-xs hidden-sm\">
";
        // line 37
        if (($context["primary_menu"] ?? null)) {
            // line 38
            echo "<div class=\"main-menu menu-stay-left\">
\t<ul class=\"categories\">
\t\t";
            // line 40
            if ((($context["primary_menu"] ?? null) == "oc")) {
                // line 41
                echo "        <!-- Default menu -->
        ";
                // line 42
                echo ($context["default_menu"] ?? null);
                echo "
      ";
            } elseif (            // line 43
array_key_exists("primary_menu", $context)) {
                // line 44
                echo "        <!-- Mega menu -->
        ";
                // line 45
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["primary_menu_desktop"] ?? null));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["key"] => $context["row"]) {
                    // line 46
                    echo "        ";
                    $this->loadTemplate("basel/template/common/menus/mega_menu.twig", "basel/template/common/headers/header1.twig", 46)->display($context);
                    // line 47
                    echo "        ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['row'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 48
                echo "      ";
            }
            // line 49
            echo "    </ul>
</div>
";
        }
        // line 52
        echo "</div>
<div class=\"table-cell w20 logo text-center\">
    ";
        // line 54
        if (($context["logo"] ?? null)) {
            // line 55
            echo "    <div id=\"logo\">
    <a href=\"";
            // line 56
            echo ($context["home"] ?? null);
            echo "\"><img src=\"";
            echo ($context["logo"] ?? null);
            echo "\" title=\"";
            echo ($context["name"] ?? null);
            echo "\" alt=\"";
            echo ($context["name"] ?? null);
            echo "\" /></a>
    </div>
    ";
        }
        // line 59
        echo "</div>
<div class=\"table-cell w40 shortcuts text-right\"> 
   <div class=\"font-zero\">
    ";
        // line 62
        if (($context["header_login"] ?? null)) {
            // line 63
            echo "        <div class=\"shortcut-wrapper sign-in hidden-sx hidden-sm hidden-xs\">
        ";
            // line 64
            if (($context["logged"] ?? null)) {
                // line 65
                echo "        <a class=\"anim-underline\" href=\"";
                echo ($context["account"] ?? null);
                echo "\">";
                echo ($context["text_account"] ?? null);
                echo "</a> &nbsp;/&nbsp; 
        <a class=\"anim-underline\" href=\"";
                // line 66
                echo ($context["logout"] ?? null);
                echo "\">";
                echo ($context["text_logout"] ?? null);
                echo "</a>
        ";
            } else {
                // line 68
                echo "        <a class=\"anim-underline\" href=\"";
                echo ($context["login"] ?? null);
                echo "\">";
                echo ($context["text_login"] ?? null);
                echo " / ";
                echo ($context["text_register"] ?? null);
                echo "</a>
        ";
            }
            // line 70
            echo "        </div>
    ";
        }
        // line 72
        echo "    ";
        if (($context["header_search"] ?? null)) {
            // line 73
            echo "    <div class=\"icon-element\">
    <div class=\"dropdown-wrapper-click from-top hidden-sx hidden-sm hidden-xs\">
    <a class=\"shortcut-wrapper search-trigger from-top clicker\">
    <i class=\"icon-magnifier icon\"></i>
    </a>
    <div class=\"dropdown-content dropdown-right\">
    <div class=\"search-dropdown-holder\">
    <div class=\"search-holder\">
    ";
            // line 81
            echo ($context["basel_search"] ?? null);
            echo "
    </div>
    </div>
    </div>
    </div>
    </div>
    ";
        }
        // line 88
        echo "    <div class=\"icon-element is_wishlist\">
    <a class=\"shortcut-wrapper wishlist\" href=\"";
        // line 89
        echo ($context["wishlist"] ?? null);
        echo "\">
    <div class=\"wishlist-hover\"><i class=\"icon-heart icon\"></i><span class=\"counter wishlist-counter\">";
        // line 90
        echo ($context["wishlist_counter"] ?? null);
        echo "</span></div>
    </a>
    </div>
    <div class=\"icon-element catalog_hide\">
    <div id=\"cart\" class=\"dropdown-wrapper from-top\">
    <a href=\"";
        // line 95
        echo ($context["shopping_cart"] ?? null);
        echo "\" class=\"shortcut-wrapper cart\">
    <i id=\"cart-icon\" class=\"global-cart icon\"></i> <span id=\"cart-total\" class=\"nowrap\">
    <span class=\"counter cart-total-items\">";
        // line 97
        echo ($context["cart_items"] ?? null);
        echo "</span> <span class=\"slash hidden-md hidden-sm hidden-xs\">/</span>&nbsp;<b class=\"cart-total-amount hidden-sm hidden-xs\">";
        echo ($context["cart_amount"] ?? null);
        echo "</b>
    </span>
    </a>
    <div class=\"dropdown-content dropdown-right hidden-sm hidden-xs\">
    ";
        // line 101
        echo ($context["cart"] ?? null);
        echo "
    </div>
    </div>
    </div>
    <div class=\"icon-element\">
    <a class=\"shortcut-wrapper menu-trigger hidden-md hidden-lg\">
    <i class=\"icon-line-menu icon\"></i>
    </a>
    </div>
   </div>
</div>
</div> <!-- .table.header_main ends -->
</div> <!-- .container ends -->
</div> <!-- .sticky ends -->
</div> <!-- .header_wrapper ends -->";
    }

    public function getTemplateName()
    {
        return "basel/template/common/headers/header1.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  281 => 101,  272 => 97,  267 => 95,  259 => 90,  255 => 89,  252 => 88,  242 => 81,  232 => 73,  229 => 72,  225 => 70,  215 => 68,  208 => 66,  201 => 65,  199 => 64,  196 => 63,  194 => 62,  189 => 59,  177 => 56,  174 => 55,  172 => 54,  168 => 52,  163 => 49,  160 => 48,  146 => 47,  143 => 46,  126 => 45,  123 => 44,  121 => 43,  117 => 42,  114 => 41,  112 => 40,  108 => 38,  106 => 37,  100 => 34,  96 => 32,  88 => 26,  80 => 21,  76 => 20,  70 => 17,  66 => 15,  64 => 14,  61 => 13,  59 => 12,  51 => 7,  45 => 4,  42 => 3,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "basel/template/common/headers/header1.twig", "/Users/alive/Sites/Agmedia/Live/kaonekad/upload/catalog/view/theme/basel/template/common/headers/header1.twig");
    }
}
