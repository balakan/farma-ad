<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* basel/template/extension/module/basel_products.twig */
class __TwigTemplate_f82b48474d9a760a614f0f2f5805b8a9fdbffe3ef919c833ce72b67bd548720f extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"widget module";
        echo ($context["module"] ?? null);
        echo " ";
        if ((($context["columns"] ?? null) != "list")) {
            echo " grid";
        }
        if (($context["contrast"] ?? null)) {
            echo " contrast-bg";
        }
        if ((($context["carousel"] ?? null) && (($context["rows"] ?? null) > 1))) {
            echo "  multiple-rows";
        }
        echo "\" ";
        if (($context["use_margin"] ?? null)) {
            echo "style=\"margin-bottom: ";
            echo ($context["margin"] ?? null);
            echo "\"";
        }
        echo "> 
";
        // line 2
        if (($context["block_title"] ?? null)) {
            // line 3
            echo "<!-- Block Title -->
<div class=\"widget-title\">
";
            // line 5
            if (($context["title_preline"] ?? null)) {
                echo "<p class=\"pre-line\">";
                echo ($context["title_preline"] ?? null);
                echo "</p>";
            }
            // line 6
            if (($context["title"] ?? null)) {
                echo " 
<p class=\"main-title\"><span>";
                // line 7
                echo ($context["title"] ?? null);
                echo "</span></p>
<p class=\"widget-title-separator\"><i class=\"icon-line-cross\"></i></p>
";
            }
            // line 10
            if (($context["title_subline"] ?? null)) {
                // line 11
                echo "<p class=\"sub-line\"><span>";
                echo ($context["title_subline"] ?? null);
                echo "</span></p>
";
            }
            // line 13
            echo "</div>
";
        }
        // line 15
        if ((twig_length_filter($this->env, ($context["tabs"] ?? null)) > 1)) {
            // line 16
            echo "<!-- Tabs -->
<ul id=\"tabs-";
            // line 17
            echo ($context["module"] ?? null);
            echo "\" class=\"nav nav-tabs ";
            echo ($context["tabstyle"] ?? null);
            echo "\" data-tabs=\"tabs\" style=\"\">
    ";
            // line 18
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["tabs"] ?? null));
            foreach ($context['_seq'] as $context["keyTab"] => $context["tab"]) {
                // line 19
                echo "        ";
                if (($context["keyTab"] == 0)) {
                    // line 20
                    echo "        <li class=\"active\"><a href=\"#tab";
                    echo ($context["module"] ?? null);
                    echo $context["keyTab"];
                    echo "\" data-toggle=\"tab\">";
                    echo twig_get_attribute($this->env, $this->source, $context["tab"], "title", [], "any", false, false, false, 20);
                    echo "</a></li>
        ";
                } else {
                    // line 22
                    echo "        <li><a href=\"#tab";
                    echo ($context["module"] ?? null);
                    echo $context["keyTab"];
                    echo "\" data-toggle=\"tab\">";
                    echo twig_get_attribute($this->env, $this->source, $context["tab"], "title", [], "any", false, false, false, 22);
                    echo "</a></li>
        ";
                }
                // line 24
                echo "    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['keyTab'], $context['tab'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 25
            echo "</ul>
";
        }
        // line 27
        echo "<div class=\"tab-content has-carousel ";
        if ( !($context["carousel"] ?? null)) {
            echo "overflow-hidden";
        }
        echo "\">
<!-- Product Group(s) -->
";
        // line 29
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["tabs"] ?? null));
        foreach ($context['_seq'] as $context["key"] => $context["tab"]) {
            // line 30
            echo "<div class=\"tab-pane";
            if (($context["key"] == 0)) {
                echo " active in";
            }
            echo " fade\" id=\"tab";
            echo ($context["module"] ?? null);
            echo $context["key"];
            echo "\">
    <div class=\"grid-holder grid";
            // line 31
            echo ($context["columns"] ?? null);
            echo " prod_module";
            echo ($context["module"] ?? null);
            if (($context["carousel"] ?? null)) {
                echo " carousel";
            }
            if ((($context["carousel_a"] ?? null) && (($context["rows"] ?? null) > 1))) {
                echo " sticky-arrows";
            }
            echo "\">
        ";
            // line 32
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["tab"], "products", [], "any", false, false, false, 32));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 33
                echo "            <div class=\"item single-product\">
            <div class=\"image\"";
                // line 34
                if ((($context["columns"] ?? null) == "list")) {
                    echo " style=\"width:";
                    echo ($context["img_width"] ?? null);
                    echo "px\"";
                }
                echo ">
                <a href=\"";
                // line 35
                echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 35);
                echo "\">
                <img src=\"";
                // line 36
                echo twig_get_attribute($this->env, $this->source, $context["product"], "thumb", [], "any", false, false, false, 36);
                echo "\" alt=\"";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 36);
                echo "\" title=\"";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 36);
                echo "\" />
                ";
                // line 37
                if (twig_get_attribute($this->env, $this->source, $context["product"], "thumb2", [], "any", false, false, false, 37)) {
                    // line 38
                    echo "                <img class=\"thumb2\" src=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "thumb2", [], "any", false, false, false, 38);
                    echo "\" alt=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 38);
                    echo "\" title=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 38);
                    echo "\" />
                ";
                }
                // line 40
                echo "                </a>
            ";
                // line 41
                if (((twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 41) && twig_get_attribute($this->env, $this->source, $context["product"], "special", [], "any", false, false, false, 41)) && twig_get_attribute($this->env, $this->source, $context["product"], "sale_badge", [], "any", false, false, false, 41))) {
                    // line 42
                    echo "                <div class=\"sale-counter id";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 42);
                    echo "\"></div>
\t\t\t\t<span class=\"badge sale_badge\"><i>";
                    // line 43
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "sale_badge", [], "any", false, false, false, 43);
                    echo "</i></span>
            ";
                }
                // line 45
                echo "            ";
                if (twig_get_attribute($this->env, $this->source, $context["product"], "new_label", [], "any", false, false, false, 45)) {
                    // line 46
                    echo "                <span class=\"badge new_badge\"><i>";
                    echo ($context["basel_text_new"] ?? null);
                    echo "</i></span>
            ";
                }
                // line 48
                echo "\t\t\t";
                if (((twig_get_attribute($this->env, $this->source, $context["product"], "quantity", [], "any", false, false, false, 48) < 1) && ($context["stock_badge_status"] ?? null))) {
                    // line 49
                    echo "\t\t\t\t<span class=\"badge out_of_stock_badge\"><i>";
                    echo ($context["basel_text_out_of_stock"] ?? null);
                    echo "</i></span>
\t\t\t\t";
                    // line 50
                    $context["button_cart"] = ($context["basel_text_out_of_stock"] ?? null);
                    // line 51
                    echo "\t\t\t";
                } else {
                    // line 52
                    echo "\t\t\t\t";
                    $context["button_cart"] = ($context["default_button_cart"] ?? null);
                    // line 53
                    echo "\t\t\t";
                }
                // line 54
                echo "            <a class=\"img-overlay\" href=\"";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 54);
                echo "\"></a>
            <div class=\"btn-center catalog_hide\"><a class=\"btn btn-light-outline btn-thin\" onclick=\"cart.add('";
                // line 55
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 55);
                echo "', '";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "minimum", [], "any", false, false, false, 55);
                echo "');\">";
                echo ($context["button_cart"] ?? null);
                echo "</a></div>
            <div class=\"icons-wrapper\">
           <!-- <a class=\"icon is-cart catalog_hide\" data-toggle=\"tooltip\" data-placement=\"";
                // line 57
                echo ($context["tooltip_align"] ?? null);
                echo "\" data-title=\"";
                echo ($context["button_cart"] ?? null);
                echo "\" onclick=\"cart.add('";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 57);
                echo "', '";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "minimum", [], "any", false, false, false, 57);
                echo "');\"><span class=\"global-cart\"></span></a>-->
            <a class=\"icon is_wishlist\" data-toggle=\"tooltip\" data-placement=\"";
                // line 58
                echo ($context["tooltip_align"] ?? null);
                echo "\"  data-title=\"";
                echo ($context["button_wishlist"] ?? null);
                echo "\" onclick=\"wishlist.add('";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 58);
                echo "');\"><span class=\"icon-heart\"></span></a>
            <a class=\"icon is_compare\" onclick=\"compare.add('";
                // line 59
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 59);
                echo "');\" data-toggle=\"tooltip\" data-placement=\"";
                echo ($context["tooltip_align"] ?? null);
                echo "\" data-title=\"";
                echo ($context["button_compare"] ?? null);
                echo "\"><span class=\"icon-refresh\"></span></a>
            <a class=\"icon is_quickview hidden-xs\" onclick=\"quickview('";
                // line 60
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 60);
                echo "');\" data-toggle=\"tooltip\" data-placement=\"";
                echo ($context["tooltip_align"] ?? null);
                echo "\" data-title=\"";
                echo ($context["basel_button_quickview"] ?? null);
                echo "\"><span class=\"icon-magnifier-add\"></span></a>
            </div> <!-- .icons-wrapper -->
            </div><!-- .image ends -->
            <div class=\"caption\">
            <a class=\"product-name\" href=\"";
                // line 64
                echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 64);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 64);
                echo "</a>
            ";
                // line 65
                if (twig_get_attribute($this->env, $this->source, $context["product"], "rating", [], "any", false, false, false, 65)) {
                    echo "      
                <div class=\"rating\">
                <span class=\"rating_stars rating r";
                    // line 67
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "rating", [], "any", false, false, false, 67);
                    echo "\">
                <i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>
                </span>
                </div>
            ";
                }
                // line 72
                echo "

                <div class=\"price-wrapper\">
                    <div class=\"row\">
                        <div class=\"col-lg-4\">
                            ";
                // line 77
                if (twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 77)) {
                    // line 78
                    echo "
                                <div class=\"price\">

                                    ";
                    // line 81
                    if (twig_get_attribute($this->env, $this->source, $context["product"], "special", [], "any", false, false, false, 81)) {
                        // line 82
                        echo "                                        <span class=\"price-old price\">";
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 82);
                        echo "</span>
                                        <span class=\"price-new\">";
                        // line 83
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "special", [], "any", false, false, false, 83);
                        echo "</span>
                                    ";
                    } else {
                        // line 85
                        echo "                                        <span class=\"price\">Cijena</span>
                                        <span>";
                        // line 86
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 86);
                        echo "</span>
                                    ";
                    }
                    // line 88
                    echo "                                    ";
                    if (twig_get_attribute($this->env, $this->source, $context["product"], "tax", [], "any", false, false, false, 88)) {
                        // line 89
                        echo "                                        <span class=\"price-tax\">";
                        echo ($context["text_tax"] ?? null);
                        echo " ";
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "tax", [], "any", false, false, false, 89);
                        echo "</span>
                                    ";
                    }
                    // line 91
                    echo "                                </div><!-- .price -->
                            ";
                }
                // line 93
                echo "                        </div>

                        <div class=\"col-lg-8\">

                            <div class=\"input-group\">
                                ";
                // line 98
                if (twig_get_attribute($this->env, $this->source, $context["product"], "options", [], "any", false, false, false, 98)) {
                    // line 99
                    echo "                                    ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["product"], "options", [], "any", false, false, false, 99));
                    foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
                        // line 100
                        echo "                                        ";
                        if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 100) == "select")) {
                            // line 101
                            echo "
                                            <select name=\"option[";
                            // line 102
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 102);
                            echo "]\" id=\"input-option";
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 102);
                            echo "\" class=\"form-control num";
                            echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 102);
                            echo "\">
                                                ";
                            // line 104
                            echo "                                                ";
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["option"], "product_option_value", [], "any", false, false, false, 104));
                            foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                                // line 105
                                echo "                                                    <option value=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["option_value"], "product_option_value_id", [], "any", false, false, false, 105);
                                echo "\">";
                                echo twig_get_attribute($this->env, $this->source, $context["option_value"], "name", [], "any", false, false, false, 105);
                                echo "</option>
                                                ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 107
                            echo "                                                <option value=\"+\">+</option>
                                            </select>

                                            <script>
                                                \$('[name=\"option[";
                            // line 111
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 111);
                            echo "]\"]').otherDropdown({classes:'form-control', value:'+', placeholder:'Upiši', max: '";
                            echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["product"], "scale", [], "any", false, false, false, 111), "max", [], "any", false, false, false, 111);
                            echo "' })
                                            </script>

                                            ";
                            // line 114
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["option"], "product_option_value", [], "any", false, false, false, 114));
                            foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                                // line 115
                                echo "                                                ";
                                if ((twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 115) == 0)) {
                                    // line 116
                                    echo "                                                    <input type=\"hidden\" id=\"input-default";
                                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 116);
                                    echo "\" value=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["option_value"], "product_option_value_id", [], "any", false, false, false, 116);
                                    echo "\">
                                                ";
                                }
                                // line 118
                                echo "                                            ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 119
                            echo "
                                            <span class=\"input-group-btn\">
                                                <button class=\"btn btn-neutral btn-green\" onclick=\"javascript:void(0); kosarica_dodaj(";
                            // line 121
                            echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 121);
                            echo ", '";
                            echo twig_escape_filter($this->env, json_encode(twig_get_attribute($this->env, $this->source, $context["product"], "scale", [], "any", false, false, false, 121)));
                            echo "', '";
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 121);
                            echo "');\">
                                                    <span class=\"global-cart\"></span>
                                                </button>
                                            </span>

                                        ";
                        }
                        // line 127
                        echo "                                    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    echo " <!-- foreach option -->
                                ";
                }
                // line 129
                echo "
                          </span>

                            </div><!-- /input-group -->

                            <script>
   \$('[name=\"";
                // line 135
                echo ($context["module"] ?? null);
                echo $context["key"];
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 135);
                echo "\"]').otherDropdown({classes:'form-control', value:'+', placeholder:'Upiši' })
                            </script>

                        </div>
                    </div>

                </div><!-- .price-wrapper -->









            <div class=\"plain-links\">
            <a class=\"icon is_wishlist link-hover-color\" onclick=\"wishlist.add('";
                // line 152
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 152);
                echo "');\"><span class=\"icon-heart\"></span> ";
                echo ($context["button_wishlist"] ?? null);
                echo "</a>
            <a class=\"icon is_compare link-hover-color\" onclick=\"compare.add('";
                // line 153
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 153);
                echo "');\"><span class=\"icon-refresh\"></span> ";
                echo ($context["button_compare"] ?? null);
                echo "</a>
            <a class=\"icon is_quickview link-hover-color\" onclick=\"quickview('";
                // line 154
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 154);
                echo "');\"><span class=\"icon-magnifier-add\"></span> ";
                echo ($context["basel_button_quickview"] ?? null);
                echo "</a>
            </div><!-- .plain-links-->
            </div><!-- .caption-->
            ";
                // line 157
                if ((twig_get_attribute($this->env, $this->source, $context["product"], "sale_end_date", [], "any", false, false, false, 157) && ($context["countdown_status"] ?? null))) {
                    // line 158
                    echo "            <script>
\t\t\t  \$(function() {
\t\t\t\t\$(\".module";
                    // line 160
                    echo ($context["module"] ?? null);
                    echo " .sale-counter.id";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 160);
                    echo "\").countdown(\"";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "sale_end_date", [], "any", false, false, false, 160);
                    echo "\").on('update.countdown', function(event) {
\t\t\t  var \$this = \$(this).html(event.strftime(''
\t\t\t\t+ '<div>'
\t\t\t\t+ '%D<i>";
                    // line 163
                    echo ($context["basel_text_days"] ?? null);
                    echo "</i></div><div>'
\t\t\t\t+ '%H <i>";
                    // line 164
                    echo ($context["basel_text_hours"] ?? null);
                    echo "</i></div><div>'
\t\t\t\t+ '%M <i>";
                    // line 165
                    echo ($context["basel_text_mins"] ?? null);
                    echo "</i></div><div>'
\t\t\t\t+ '%S <i>";
                    // line 166
                    echo ($context["basel_text_secs"] ?? null);
                    echo "</i></div></div>'));
\t\t\t});
\t\t\t  });
\t\t\t</script>
            ";
                }
                // line 171
                echo "            </div><!-- .single-product ends -->
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 173
            echo "    </div>
</div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['tab'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 176
        if (($context["use_button"] ?? null)) {
            // line 177
            echo "<!-- Button -->
<div class=\"widget_bottom_btn ";
            // line 178
            if ((($context["carousel"] ?? null) && ($context["carousel_b"] ?? null))) {
                echo "has-dots";
            }
            echo "\">
<a class=\"btn btn-contrast\" href=\"";
            // line 179
            echo ((($context["link_href"] ?? null)) ? (($context["link_href"] ?? null)) : (""));
            echo "\">";
            echo ($context["link_title"] ?? null);
            echo "</a>
</div>
";
        }
        // line 182
        echo "</div>
<div class=\"clearfix\"></div>
</div>
";
        // line 185
        if (($context["carousel"] ?? null)) {
            // line 186
            echo "<script>
\$('.grid-holder.prod_module";
            // line 187
            echo ($context["module"] ?? null);
            echo "').slick({
";
            // line 188
            if (($context["carousel_a"] ?? null)) {
                // line 189
                echo "prevArrow: \"<a class=\\\"arrow-left icon-arrow-left\\\"></a>\",
nextArrow: \"<a class=\\\"arrow-right icon-arrow-right\\\"></a>\",
";
            } else {
                // line 192
                echo "arrows: false,
";
            }
            // line 194
            if ((($context["direction"] ?? null) == "rtl")) {
                // line 195
                echo "rtl: true,
";
            }
            // line 197
            if (($context["carousel_b"] ?? null)) {
                // line 198
                echo "dots:true,
";
            }
            // line 200
            echo "respondTo:'min',
rows:";
            // line 201
            echo ($context["rows"] ?? null);
            echo ",
";
            // line 202
            if ((($context["columns"] ?? null) == "5")) {
                // line 203
                echo "slidesToShow:5,slidesToScroll:5,responsive:[{breakpoint:1100,settings:{slidesToShow:4,slidesToScroll:4}},{breakpoint:960,settings:{slidesToShow:3,slidesToScroll:3}},{breakpoint:600,settings:{slidesToShow:2,slidesToScroll:2}},
";
            } elseif ((            // line 204
($context["columns"] ?? null) == "4")) {
                // line 205
                echo "slidesToShow:4,slidesToScroll:4,responsive:[{breakpoint:960,settings:{slidesToShow:3,slidesToScroll:3}},{breakpoint:600,settings:{slidesToShow:2,slidesToScroll:2}},
";
            } elseif ((            // line 206
($context["columns"] ?? null) == "3")) {
                // line 207
                echo "slidesToShow:3,slidesToScroll:3,responsive:[{breakpoint:600,settings:{slidesToShow:2,slidesToScroll:2}},
";
            } elseif ((            // line 208
($context["columns"] ?? null) == "2")) {
                // line 209
                echo "slidesToShow:2,slidesToScroll:2,responsive:[
";
            } elseif (((            // line 210
($context["columns"] ?? null) == "1") || (($context["columns"] ?? null) == "list"))) {
                // line 211
                echo "adaptiveHeight:true,slidesToShow:1,slidesToScroll:1,responsive:[
";
            }
            // line 213
            if (($context["items_mobile_fw"] ?? null)) {
                // line 214
                echo "{breakpoint:420,settings:{slidesToShow:1,slidesToScroll:1}}
";
            }
            // line 216
            echo "]
});
\$('.product-style2 .single-product .icon').attr('data-placement', 'top');
\$('[data-toggle=\\'tooltip\\']').tooltip({container: 'body'});
";
            // line 220
            if ((($context["carousel_a"] ?? null) && (($context["rows"] ?? null) > 1))) {
                // line 221
                echo "\$(window).load(function() {
var p_c_o = \$('.prod_module";
                // line 222
                echo ($context["module"] ?? null);
                echo "').offset().top;
var p_c_o_b = \$('.prod_module";
                // line 223
                echo ($context["module"] ?? null);
                echo "').offset().top + \$('.prod_module";
                echo ($context["module"] ?? null);
                echo "').outerHeight(true) - 100;
var p_sticky_arrows = function(){
var p_m_o = \$(window).scrollTop() + (\$(window).height()/2);
if (p_m_o > p_c_o && p_m_o < p_c_o_b) {
\$('.prod_module";
                // line 227
                echo ($context["module"] ?? null);
                echo " .slick-arrow').addClass('visible').css('top', p_m_o - p_c_o + 'px');
} else {
\$('.prod_module";
                // line 229
                echo ($context["module"] ?? null);
                echo " .slick-arrow').removeClass('visible');
}
};
\$(window).scroll(function() {p_sticky_arrows();});
});
";
            }
            // line 235
            echo "</script>
    <script type=\"application/javascript\">
        function kosarica_dodaj(product_id, scale, select) {
            let selected = \$('#input-option' + select)[0];
            let default_id = \$('#input-default' + select)[0].value;

            if (selected.selectedIndex > JSON.parse(scale).items.length) {
                cart.add(product_id, Number(selected.value), 'option[' + product_id + ']=' + default_id);
            } else {
                cart.add(product_id, 1, 'option[' + product_id + ']=' + selected.value);
            }
        }
    </script>
";
        }
    }

    public function getTemplateName()
    {
        return "basel/template/extension/module/basel_products.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  712 => 235,  703 => 229,  698 => 227,  689 => 223,  685 => 222,  682 => 221,  680 => 220,  674 => 216,  670 => 214,  668 => 213,  664 => 211,  662 => 210,  659 => 209,  657 => 208,  654 => 207,  652 => 206,  649 => 205,  647 => 204,  644 => 203,  642 => 202,  638 => 201,  635 => 200,  631 => 198,  629 => 197,  625 => 195,  623 => 194,  619 => 192,  614 => 189,  612 => 188,  608 => 187,  605 => 186,  603 => 185,  598 => 182,  590 => 179,  584 => 178,  581 => 177,  579 => 176,  571 => 173,  564 => 171,  556 => 166,  552 => 165,  548 => 164,  544 => 163,  534 => 160,  530 => 158,  528 => 157,  520 => 154,  514 => 153,  508 => 152,  486 => 135,  478 => 129,  469 => 127,  456 => 121,  452 => 119,  446 => 118,  438 => 116,  435 => 115,  431 => 114,  423 => 111,  417 => 107,  406 => 105,  401 => 104,  393 => 102,  390 => 101,  387 => 100,  382 => 99,  380 => 98,  373 => 93,  369 => 91,  361 => 89,  358 => 88,  353 => 86,  350 => 85,  345 => 83,  340 => 82,  338 => 81,  333 => 78,  331 => 77,  324 => 72,  316 => 67,  311 => 65,  305 => 64,  294 => 60,  286 => 59,  278 => 58,  268 => 57,  259 => 55,  254 => 54,  251 => 53,  248 => 52,  245 => 51,  243 => 50,  238 => 49,  235 => 48,  229 => 46,  226 => 45,  221 => 43,  216 => 42,  214 => 41,  211 => 40,  201 => 38,  199 => 37,  191 => 36,  187 => 35,  179 => 34,  176 => 33,  172 => 32,  160 => 31,  150 => 30,  146 => 29,  138 => 27,  134 => 25,  128 => 24,  119 => 22,  110 => 20,  107 => 19,  103 => 18,  97 => 17,  94 => 16,  92 => 15,  88 => 13,  82 => 11,  80 => 10,  74 => 7,  70 => 6,  64 => 5,  60 => 3,  58 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "basel/template/extension/module/basel_products.twig", "");
    }
}
