<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* basel/template/checkout/login.twig */
class __TwigTemplate_d8e9fb782a3e1858c90c685e28373e96827adbeca404f1ab29389849b4574632 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"row\">
  <div class=\"col-sm-6\">
    <h2>";
        // line 3
        echo ($context["text_new_customer"] ?? null);
        echo "</h2>
    <p>";
        // line 4
        echo ($context["text_checkout"] ?? null);
        echo "</p>
    <div class=\"radio\">
      <label>
        ";
        // line 7
        if ((($context["account"] ?? null) == "register")) {
            // line 8
            echo "        <input type=\"radio\" name=\"account\" value=\"register\" checked=\"checked\" />
        ";
        } else {
            // line 10
            echo "        <input type=\"radio\" name=\"account\" value=\"register\" />
        ";
        }
        // line 12
        echo "        ";
        echo ($context["text_register"] ?? null);
        echo "</label>
    </div>
    ";
        // line 14
        if (($context["checkout_guest"] ?? null)) {
            // line 15
            echo "    <div class=\"radio\">
      <label>
        ";
            // line 17
            if ((($context["account"] ?? null) == "guest")) {
                // line 18
                echo "        <input type=\"radio\" name=\"account\" value=\"guest\" checked=\"checked\" />
        ";
            } else {
                // line 20
                echo "        <input type=\"radio\" name=\"account\" value=\"guest\" />
        ";
            }
            // line 22
            echo "        ";
            echo ($context["text_guest"] ?? null);
            echo "</label>
    </div>
    ";
        }
        // line 25
        echo "    <p>";
        echo ($context["text_register_account"] ?? null);
        echo "</p>
    <input type=\"button\" value=\"";
        // line 26
        echo ($context["button_continue"] ?? null);
        echo "\" id=\"button-account\" data-loading-text=\"";
        echo ($context["text_loading"] ?? null);
        echo "\" class=\"btn btn-primary\" />
  </div>
  <div class=\"col-sm-6\">
    <h2>";
        // line 29
        echo ($context["text_returning_customer"] ?? null);
        echo "</h2>
    <p>";
        // line 30
        echo ($context["text_i_am_returning_customer"] ?? null);
        echo "</p>
    <div class=\"form-group\">
      <label class=\"control-label\" for=\"input-email\">";
        // line 32
        echo ($context["entry_email"] ?? null);
        echo "</label>
      <input type=\"text\" name=\"email\" value=\"\" placeholder=\"";
        // line 33
        echo ($context["entry_email"] ?? null);
        echo "\" id=\"input-email\" class=\"form-control\" />
    </div>
    <div class=\"form-group\">
      <label class=\"control-label\" for=\"input-password\">";
        // line 36
        echo ($context["entry_password"] ?? null);
        echo "</label>
      <a href=\"";
        // line 37
        echo ($context["forgotten"] ?? null);
        echo "\" class=\"label-link pull-right\">";
        echo ($context["text_forgotten"] ?? null);
        echo "</a>
      <input type=\"password\" name=\"password\" value=\"\" placeholder=\"";
        // line 38
        echo ($context["entry_password"] ?? null);
        echo "\" id=\"input-password\" class=\"form-control margin-b15\" />
    <input type=\"button\" value=\"";
        // line 39
        echo ($context["button_login"] ?? null);
        echo "\" id=\"button-login\" data-loading-text=\"";
        echo ($context["text_loading"] ?? null);
        echo "\" class=\"btn btn-primary\" />
  </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "basel/template/checkout/login.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  136 => 39,  132 => 38,  126 => 37,  122 => 36,  116 => 33,  112 => 32,  107 => 30,  103 => 29,  95 => 26,  90 => 25,  83 => 22,  79 => 20,  75 => 18,  73 => 17,  69 => 15,  67 => 14,  61 => 12,  57 => 10,  53 => 8,  51 => 7,  45 => 4,  41 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "basel/template/checkout/login.twig", "");
    }
}
