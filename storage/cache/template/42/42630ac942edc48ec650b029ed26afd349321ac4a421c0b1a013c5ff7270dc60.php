<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* basel/template/product/product.twig */
class __TwigTemplate_dd8ac755d73a2369d7c673bd09b53d35ffcebba5de895f01456751123548294d extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo "

";
        // line 3
        if ((($context["product_layout"] ?? null) != "full-width")) {
            // line 4
            echo "    <style>
        .product-page .image-area {
        ";
            // line 6
            if (((($context["product_layout"] ?? null) == "images-left") && ($context["images"] ?? null))) {
                // line 7
                echo "            width: ";
                echo ((($context["img_w"] ?? null) + ($context["img_a_w"] ?? null)) + 20);
                echo "px;
        ";
            } else {
                // line 9
                echo "            width: ";
                echo ($context["img_w"] ?? null);
                echo "px;
        ";
            }
            // line 11
            echo "        }
        .product-page .main-image {
            width:";
            // line 13
            echo ($context["img_w"] ?? null);
            echo "px;
        }
        .product-page .image-additional {
        ";
            // line 16
            if ((($context["product_layout"] ?? null) == "images-left")) {
                // line 17
                echo "            width: ";
                echo ($context["img_a_w"] ?? null);
                echo "px;
            height: ";
                // line 18
                echo ($context["img_h"] ?? null);
                echo "px;
        ";
            } else {
                // line 20
                echo "            width: ";
                echo ($context["img_w"] ?? null);
                echo "px;
        ";
            }
            // line 22
            echo "        }
        .product-page .image-additional.has-arrows {
        ";
            // line 24
            if ((($context["product_layout"] ?? null) == "images-left")) {
                // line 25
                echo "            height: ";
                echo (($context["img_h"] ?? null) - 40);
                echo "px;
        ";
            }
            // line 27
            echo "        }
        @media (min-width: 992px) and (max-width: 1199px) {
            .product-page .image-area {
            ";
            // line 30
            if ((($context["product_layout"] ?? null) == "images-left")) {
                // line 31
                echo "                width: ";
                echo (((($context["img_w"] ?? null) + ($context["img_a_w"] ?? null)) / 1.25) + 20);
                echo "px;
            ";
            } else {
                // line 33
                echo "                width: ";
                echo (($context["img_w"] ?? null) / 1.25);
                echo "px;
            ";
            }
            // line 35
            echo "            }
            .product-page .main-image {
                width:";
            // line 37
            echo (($context["img_w"] ?? null) / 1.25);
            echo "px;
            }
            .product-page .image-additional {
            ";
            // line 40
            if ((($context["product_layout"] ?? null) == "images-left")) {
                // line 41
                echo "                width: ";
                echo (($context["img_a_w"] ?? null) / 1.25);
                echo "px;
                height: ";
                // line 42
                echo (($context["img_h"] ?? null) / 1.25);
                echo "px;
            ";
            } else {
                // line 44
                echo "                width: ";
                echo (($context["img_w"] ?? null) / 1.25);
                echo "px;
            ";
            }
            // line 46
            echo "            }
        }
    </style>
";
        }
        // line 50
        echo "
<ul class=\"breadcrumb\">
    ";
        // line 52
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 53
            echo "        <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 53);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 53);
            echo "</a></li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "</ul>

<div class=\"container product-layout ";
        // line 57
        echo ($context["product_layout"] ?? null);
        echo "\">

    <div class=\"row\">";
        // line 59
        echo ($context["column_left"] ?? null);
        echo "
        ";
        // line 60
        if ((($context["column_left"] ?? null) && ($context["column_right"] ?? null))) {
            // line 61
            echo "            ";
            $context["class"] = "col-sm-6";
            // line 62
            echo "        ";
        } elseif ((($context["column_left"] ?? null) || ($context["column_right"] ?? null))) {
            // line 63
            echo "            ";
            $context["class"] = "col-md-9 col-sm-8";
            // line 64
            echo "        ";
        } else {
            // line 65
            echo "            ";
            $context["class"] = "col-sm-12";
            // line 66
            echo "        ";
        }
        // line 67
        echo "        <div id=\"content\" class=\"product-main no-min-height ";
        echo ($context["class"] ?? null);
        echo "\">
            ";
        // line 68
        echo ($context["content_top"] ?? null);
        echo "

            <div class=\"table product-info product-page\">

                <div class=\"table-cell left\">

                    ";
        // line 74
        if ((($context["thumb"] ?? null) || ($context["images"] ?? null))) {
            // line 75
            echo "                    <div class=\"image-area ";
            if ( !($context["hover_zoom"] ?? null)) {
                echo "hover-zoom-disabled";
            }
            echo "\" id=\"gallery\">

                        ";
            // line 77
            if (($context["thumb"] ?? null)) {
                // line 78
                echo "                            <div class=\"main-image\">

                                ";
                // line 80
                if (((($context["price"] ?? null) && ($context["special"] ?? null)) && ($context["sale_badge"] ?? null))) {
                    // line 81
                    echo "                                    <span class=\"badge sale_badge\"><i>";
                    echo ($context["sale_badge"] ?? null);
                    echo "</i></span>
                                ";
                }
                // line 83
                echo "
                                ";
                // line 84
                if (($context["is_new"] ?? null)) {
                    // line 85
                    echo "                                    <span class=\"badge new_badge\"><i>";
                    echo ($context["basel_text_new"] ?? null);
                    echo "</i></span>
                                ";
                }
                // line 87
                echo "
                                ";
                // line 88
                if (((($context["qty"] ?? null) < 1) && ($context["stock_badge_status"] ?? null))) {
                    // line 89
                    echo "                                    <span class=\"badge out_of_stock_badge\"><i>";
                    echo ($context["basel_text_out_of_stock"] ?? null);
                    echo "</i></span>
                                ";
                }
                // line 91
                echo "
                                <a class=\"";
                // line 92
                if ( !($context["images"] ?? null)) {
                    echo "link cloud-zoom";
                }
                echo " ";
                if ((($context["product_layout"] ?? null) == "full-width")) {
                    echo "link";
                } else {
                    echo "cloud-zoom";
                }
                echo "\" id=\"main-image\" href=\"";
                echo ($context["popup"] ?? null);
                echo "\" rel=\"position:'inside', showTitle: false\"><img src=\"";
                echo ($context["thumb"] ?? null);
                echo "\" title=\"";
                echo ($context["heading_title"] ?? null);
                echo "\" alt=\"";
                echo ($context["heading_title"] ?? null);
                echo "\" /></a>
                            </div>
                        ";
            }
            // line 95
            echo "
                        ";
            // line 96
            if (($context["images"] ?? null)) {
                // line 97
                echo "                            <ul class=\"image-additional\">
                                ";
                // line 98
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["images"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                    // line 99
                    echo "                                    <li>
                                        <a class=\"link ";
                    // line 100
                    if ((($context["product_layout"] ?? null) != "full-width")) {
                        echo "cloud-zoom-gallery locked";
                    }
                    echo "\" href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["image"], "popup", [], "any", false, false, false, 100);
                    echo "\" rel=\"useZoom: 'main-image', smallImage: '";
                    echo twig_get_attribute($this->env, $this->source, $context["image"], "thumb_lg", [], "any", false, false, false, 100);
                    echo "'\"><img src=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["image"], "thumb", [], "any", false, false, false, 100);
                    echo "\" title=\"";
                    echo ($context["heading_title"] ?? null);
                    echo "\" alt=\"";
                    echo ($context["heading_title"] ?? null);
                    echo "\" /></a>
                                    </li>
                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 103
                echo "                                ";
                if ((($context["thumb"] ?? null) && (($context["product_layout"] ?? null) != "full-width"))) {
                    // line 104
                    echo "                                    <li><a class=\"link cloud-zoom-gallery locked active\" href=\"";
                    echo ($context["popup"] ?? null);
                    echo "\" rel=\"useZoom: 'main-image', smallImage: '";
                    echo ($context["thumb"] ?? null);
                    echo "'\"><img src=\"";
                    echo ($context["thumb_sm"] ?? null);
                    echo "\" title=\"";
                    echo ($context["heading_title"] ?? null);
                    echo "\" alt=\"";
                    echo ($context["heading_title"] ?? null);
                    echo "\" /></a></li>
                                ";
                }
                // line 106
                echo "                            </ul>
                        ";
            }
            // line 108
            echo "
                        <div class=\"shipnotice\">
                            <div class=\"alert alert-shipping\">
                                <i class=\"fa fa-truck\"></i> Brza zagrebačka dostava <span>Između 11h i 21h istoga dana
                 </span>
                            </div>
                        </div>

                    </div> <!-- .table-cell.left ends -->

                </div> <!-- .image-area ends -->
                ";
        }
        // line 120
        echo "
                <div class=\"table-cell w100 right\">
                    <div class=\"inner\">

                        <div class=\"product-h1\">
                            <h1 id=\"page-title\">";
        // line 125
        echo ($context["heading_title"] ?? null);
        echo "</h1>
                        </div>

                        ";
        // line 128
        if ((($context["review_status"] ?? null) && (($context["review_qty"] ?? null) > 0))) {
            // line 129
            echo "                            <div class=\"rating\">
    <span class=\"rating_stars rating r";
            // line 130
            echo ($context["rating"] ?? null);
            echo "\">
    <i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>
    </span>
                            </div>
                            <span class=\"review_link\">(<a class=\"hover_uline to_tabs\" onclick=\"\$('a[href=\\'#tab-review\\']').trigger('click'); return false;\">";
            // line 134
            echo ($context["reviews"] ?? null);
            echo "</a>)</span>
                        ";
        }
        // line 136
        echo "
                        ";
        // line 137
        if (($context["price"] ?? null)) {
            // line 138
            echo "                            <ul class=\"list-unstyled price\">
                                ";
            // line 139
            if ( !($context["special"] ?? null)) {
                // line 140
                echo "                                    <li><span class=\"live-price\">";
                echo ($context["price"] ?? null);
                echo "<span></li>
                                ";
            } else {
                // line 142
                echo "                                    <li><span class=\"price-old\">";
                echo ($context["price"] ?? null);
                echo "</span><span class=\"live-price-new\">";
                echo ($context["special"] ?? null);
                echo "<span></li>
                                    <span id=\"special_countdown\"></span>
                                ";
            }
            // line 145
            echo "                            </ul>

                            ";
            // line 147
            if (($context["discounts"] ?? null)) {
                // line 148
                echo "                                <p class=\"discount\">
                                    ";
                // line 149
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["discounts"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["discount"]) {
                    // line 150
                    echo "                                        <span>";
                    echo twig_get_attribute($this->env, $this->source, $context["discount"], "quantity", [], "any", false, false, false, 150);
                    echo ($context["text_discount"] ?? null);
                    echo "<i class=\"price\">";
                    echo twig_get_attribute($this->env, $this->source, $context["discount"], "price", [], "any", false, false, false, 150);
                    echo "</i></span>
                                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['discount'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 152
                echo "                                </p>
                            ";
            }
            // line 154
            echo "
                        ";
        }
        // line 155
        echo " <!-- if price ends -->
                        ";
        // line 156
        if ((($context["price"] ?? null) && ($context["tax"] ?? null))) {
            // line 157
            echo "                            <p class=\"info p-tax\"><b>";
            echo ($context["text_tax"] ?? null);
            echo "</b> <span class=\"live-price-tax\">";
            echo ($context["tax"] ?? null);
            echo "</span></p>
                        ";
        }
        // line 159
        echo "
                        ";
        // line 160
        if ((($context["meta_description_status"] ?? null) && ($context["meta_description"] ?? null))) {
            // line 161
            echo "                            <p class=\"meta_description\">";
            echo ($context["meta_description"] ?? null);
            echo "</p>
                        ";
        }
        // line 163
        echo "

                        <div id=\"product\">

                            ";
        // line 167
        if (($context["options"] ?? null)) {
            // line 168
            echo "                                <div class=\"options \">
                                    ";
            // line 169
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["options"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
                // line 170
                echo "
                                        ";
                // line 171
                if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 171) == "select")) {
                    // line 172
                    echo "                                            <div class=\"form-group";
                    if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 172)) {
                        echo " required";
                    }
                    echo " row nbmargin\">
                                                <div class=\" name col-md-12\">
                                                    <label class=\"control-label\" for=\"input-option";
                    // line 174
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 174);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 174);
                    echo "</label>
                                                </div>
                                                <div class=\" col-md-12\">
                                                    <select name=\"option[";
                    // line 177
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 177);
                    echo "]\" id=\"input-option";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 177);
                    echo "\" class=\"form-control kolicina\">
                                                        <option value=\"\">";
                    // line 178
                    echo ($context["text_select"] ?? null);
                    echo "</option>
                                                        ";
                    // line 179
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["option"], "product_option_value", [], "any", false, false, false, 179));
                    foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                        // line 180
                        echo "                                                            <option value=\"";
                        echo twig_get_attribute($this->env, $this->source, $context["option_value"], "product_option_value_id", [], "any", false, false, false, 180);
                        echo "\">";
                        echo twig_get_attribute($this->env, $this->source, $context["option_value"], "name", [], "any", false, false, false, 180);
                        echo "
                                                                ";
                        // line 181
                        if (twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 181)) {
                            // line 182
                            echo "                                                                    (";
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price_prefix", [], "any", false, false, false, 182);
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 182);
                            echo ")
                                                                ";
                        }
                        // line 184
                        echo "                                                            </option>
                                                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 186
                    echo "                                                        <option value=\"+\">+</option>
                                                    </select>

                                                    <script>
                                                        \$('[name=\"option[";
                    // line 190
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 190);
                    echo "]\"]').otherDropdown({classes:'form-control', value:'+', placeholder:'Upiši', max: '";
                    echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "scale", [], "any", false, false, false, 190), "max", [], "any", false, false, false, 190);
                    echo "' })
                                                    </script>

                                                    ";
                    // line 193
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["option"], "product_option_value", [], "any", false, false, false, 193));
                    foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                        // line 194
                        echo "                                                        ";
                        if ((twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 194) == 0)) {
                            // line 195
                            echo "                                                            <input type=\"hidden\" id=\"input-default";
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 195);
                            echo "\" value=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "product_option_value_id", [], "any", false, false, false, 195);
                            echo "\">
                                                        ";
                        }
                        // line 197
                        echo "                                                    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 198
                    echo "
                                                </div>
                                            </div>
                                        ";
                }
                // line 202
                echo "


                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 205
            echo " <!-- foreach option -->
                                </div>
                            ";
        }
        // line 208
        echo "
                            ";
        // line 209
        if (($context["recurrings"] ?? null)) {
            // line 210
            echo "                                <hr>
                                <h3>";
            // line 211
            echo ($context["text_payment_recurring"] ?? null);
            echo "</h3>
                                <div class=\"form-group required\">
                                    <select name=\"recurring_id\" class=\"form-control\">
                                        <option value=\"\">";
            // line 214
            echo ($context["text_select"] ?? null);
            echo "</option>
                                        ";
            // line 215
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["recurrings"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["recurring"]) {
                // line 216
                echo "                                            <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["recurring"], "recurring_id", [], "any", false, false, false, 216);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["recurring"], "name", [], "any", false, false, false, 216);
                echo "</option>
                                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['recurring'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 218
            echo "                                    </select>
                                    <div class=\"help-block\" id=\"recurring-description\"></div>
                                </div>
                            ";
        }
        // line 222
        echo "
                            <div class=\"form-group buy catalog_hide \">

                                <!-- <input type=\"number\" step=\"1\" min=\"";
        // line 225
        echo ($context["minimum"] ?? null);
        echo "\" name=\"quantity\" value=\"";
        echo ($context["minimum"] ?? null);
        echo "\" id=\"input-quantity\" class=\"form-control input-quantity\" /> -->
                                <input type=\"hidden\" name=\"product_id\" value=\"";
        // line 226
        echo ($context["product_id"] ?? null);
        echo "\" />
                                <button type=\"button\" id=\"button-cart\" data-loading-text=\"";
        // line 227
        echo ($context["text_loading"] ?? null);
        echo "\" class=\"btn btn-primary\">";
        if (((($context["qty"] ?? null) < 1) && ($context["stock_badge_status"] ?? null))) {
            echo ($context["basel_text_out_of_stock"] ?? null);
        } else {
            echo "<i id=\"cart-icon\" class=\"global-cart icon\"></i> ";
            echo ($context["button_cart"] ?? null);
        }
        echo "</button>
                            </div>
                            ";
        // line 229
        if ((($context["minimum"] ?? null) > 1)) {
            // line 230
            echo "                                <div class=\"alert alert-sm alert-info\"><i class=\"fa fa-info-circle\"></i> ";
            echo ($context["text_minimum"] ?? null);
            echo "</div>
                            ";
        }
        // line 232
        echo "
                        </div> <!-- #product ends -->


                        <p class=\"info is_wishlist\"><a onclick=\"wishlist.add('";
        // line 236
        echo ($context["product_id"] ?? null);
        echo "');\"><i class=\"icon-heart\"></i> ";
        echo ($context["button_wishlist"] ?? null);
        echo "</a></p>
                        <p class=\"info is_compare\"><a onclick=\"compare.add('";
        // line 237
        echo ($context["product_id"] ?? null);
        echo "');\"><i class=\"icon-refresh\"></i> ";
        echo ($context["button_compare"] ?? null);
        echo "</a></p>
                        ";
        // line 238
        if (($context["question_status"] ?? null)) {
            // line 239
            echo "                            <p class=\"info is_ask\"><a class=\"to_tabs\" onclick=\"\$('a[href=\\'#tab-questions\\']').trigger('click'); return false;\"><i class=\"icon-question\"></i> ";
            echo ($context["basel_button_ask"] ?? null);
            echo "</a></p>
                        ";
        }
        // line 241
        echo "
                        <div class=\"clearfix\"></div>

                        <div class=\"info-holder\">



                            ";
        // line 248
        if ((($context["price"] ?? null) && ($context["points"] ?? null))) {
            // line 249
            echo "                                <p class=\"info\"><b>";
            echo ($context["text_points"] ?? null);
            echo "</b> ";
            echo ($context["points"] ?? null);
            echo "</p>
                            ";
        }
        // line 251
        echo "
                            <p class=\"info ";
        // line 252
        if ((($context["qty"] ?? null) > 0)) {
            echo "in_stock";
        }
        echo "\"><b>";
        echo ($context["text_stock"] ?? null);
        echo "</b> ";
        echo ($context["stock"] ?? null);
        echo "</p>

                            ";
        // line 254
        if (($context["manufacturer"] ?? null)) {
            // line 255
            echo "                                <p class=\"info\"><b>";
            echo ($context["text_manufacturer"] ?? null);
            echo "</b> <a class=\"hover_uline\" href=\"";
            echo ($context["manufacturers"] ?? null);
            echo "\">";
            echo ($context["manufacturer"] ?? null);
            echo "</a></p>
                            ";
        }
        // line 257
        echo "
                            <p class=\"info\"><b>";
        // line 258
        echo ($context["text_model"] ?? null);
        echo "</b> ";
        echo ($context["model"] ?? null);
        echo "</p>

                            ";
        // line 260
        if (($context["reward"] ?? null)) {
            // line 261
            echo "                                <p class=\"info\"><b>";
            echo ($context["text_reward"] ?? null);
            echo "</b> ";
            echo ($context["reward"] ?? null);
            echo "</p>
                            ";
        }
        // line 263
        echo "
                            ";
        // line 264
        if (($context["tags"] ?? null)) {
            // line 265
            echo "                                <p class=\"info tags\"><b>";
            echo ($context["text_tags"] ?? null);
            echo "</b> &nbsp;<span>";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["tags"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["tag"]) {
                echo "<a class=\"hover_uline\" href=\"";
                echo twig_get_attribute($this->env, $this->source, $context["tag"], "href", [], "any", false, false, false, 265);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["tag"], "tag", [], "any", false, false, false, 265);
                echo "</a>";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tag'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo "</span></p>
                            ";
        }
        // line 267
        echo "
                            ";
        // line 268
        if (($context["basel_share_btn"] ?? null)) {
            // line 269
            echo "                                ";
            if ((($context["basel_sharing_style"] ?? null) == "large")) {
                // line 270
                echo "                                    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5f1aa4948aa12700134b0c47&product=inline-share-buttons' async='async'></script>
                                    <!-- ShareThis BEGIN --><div class=\"sharethis-inline-share-buttons\"></div><!-- ShareThis END -->
                                ";
            } else {
                // line 273
                echo "                                <hr>
                                    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5f1aa4948aa12700134b0c47&product=inline-share-buttons' async='async'></script>
                                    <!-- ShareThis BEGIN --><div class=\"sharethis-inline-share-buttons\"></div><!-- ShareThis END -->
                                ";
            }
            // line 277
            echo "                            ";
        }
        // line 278
        echo "
                        </div> <!-- .info-holder ends -->

                    </div> <!-- .inner ends -->


                    ";
        // line 284
        if (($context["full_width_tabs"] ?? null)) {
            // line 285
            echo "                </div> <!-- main column ends -->
                ";
            // line 286
            echo ($context["column_right"] ?? null);
            echo "
            </div> <!-- .row ends -->
        </div> <!-- .container ends -->
        ";
        }
        // line 290
        echo "
        ";
        // line 291
        if (($context["full_width_tabs"] ?? null)) {
            // line 292
            echo "        <div class=\"outer-container product-tabs-wrapper\">
            <div class=\"container\">
                ";
        } else {
            // line 295
            echo "                <div class=\"inline-tabs\">
                    ";
        }
        // line 297
        echo "
                    <!-- Tabs area start -->
                    <div class=\"row\">
                        <div class=\"col-sm-12\">

                            <ul class=\"nav nav-tabs ";
        // line 302
        echo ($context["product_tabs_style"] ?? null);
        echo " main_tabs\">
                                <li class=\"active\"><a href=\"#tab-description\" data-toggle=\"tab\">";
        // line 303
        echo ($context["tab_description"] ?? null);
        echo "</a></li>
                                ";
        // line 304
        if (($context["product_tabs"] ?? null)) {
            // line 305
            echo "                                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["product_tabs"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["tab"]) {
                // line 306
                echo "                                        <li><a href=\"#custom-tab-";
                echo twig_get_attribute($this->env, $this->source, $context["tab"], "tab_id", [], "any", false, false, false, 306);
                echo "\" data-toggle=\"tab\">";
                echo twig_get_attribute($this->env, $this->source, $context["tab"], "name", [], "any", false, false, false, 306);
                echo "</a></li>
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tab'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 308
            echo "                                ";
        }
        // line 309
        echo "                                ";
        if (($context["attribute_groups"] ?? null)) {
            // line 310
            echo "                                    <li><a href=\"#tab-specification\" data-toggle=\"tab\">";
            echo ($context["tab_attribute"] ?? null);
            echo "</a></li>
                                ";
        }
        // line 312
        echo "                                ";
        if (($context["review_status"] ?? null)) {
            // line 313
            echo "                                    <li><a href=\"#tab-review\" data-toggle=\"tab\">";
            echo ($context["tab_review"] ?? null);
            echo "</a></li>
                                ";
        }
        // line 315
        echo "                                ";
        if (($context["question_status"] ?? null)) {
            // line 316
            echo "                                    <li><a href=\"#tab-questions\" data-toggle=\"tab\">";
            echo ($context["basel_tab_questions"] ?? null);
            echo " (";
            echo ($context["questions_total"] ?? null);
            echo ")</a></li>
                                ";
        }
        // line 318
        echo "                            </ul>

                            <div class=\"tab-content\">

                                <div class=\"tab-pane active\" id=\"tab-description\">
                                    ";
        // line 323
        echo ($context["description"] ?? null);
        echo "
                                </div>

                                ";
        // line 326
        if (($context["product_tabs"] ?? null)) {
            // line 327
            echo "                                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["product_tabs"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["tab"]) {
                // line 328
                echo "                                        <div class=\"tab-pane\" id=\"custom-tab-";
                echo twig_get_attribute($this->env, $this->source, $context["tab"], "tab_id", [], "any", false, false, false, 328);
                echo "\">
                                            ";
                // line 329
                echo twig_get_attribute($this->env, $this->source, $context["tab"], "description", [], "any", false, false, false, 329);
                echo "
                                        </div>
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tab'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 332
            echo "                                ";
        }
        // line 333
        echo "
                                ";
        // line 334
        if (($context["attribute_groups"] ?? null)) {
            // line 335
            echo "                                    <div class=\"tab-pane\" id=\"tab-specification\">
                                        <table class=\"table specification\">
                                            ";
            // line 337
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["attribute_groups"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["attribute_group"]) {
                // line 338
                echo "                                                <thead>
                                                <tr>
                                                    <td colspan=\"2\">";
                // line 340
                echo twig_get_attribute($this->env, $this->source, $context["attribute_group"], "name", [], "any", false, false, false, 340);
                echo "</td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                ";
                // line 344
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["attribute_group"], "attribute", [], "any", false, false, false, 344));
                foreach ($context['_seq'] as $context["_key"] => $context["attribute"]) {
                    // line 345
                    echo "                                                    <tr>
                                                        <td class=\"text-left\"><b>";
                    // line 346
                    echo twig_get_attribute($this->env, $this->source, $context["attribute"], "name", [], "any", false, false, false, 346);
                    echo "</b></td>
                                                        <td class=\"text-right\">";
                    // line 347
                    echo twig_get_attribute($this->env, $this->source, $context["attribute"], "text", [], "any", false, false, false, 347);
                    echo "</td>
                                                    </tr>
                                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attribute'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 350
                echo "                                                </tbody>
                                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attribute_group'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 352
            echo "                                        </table>
                                    </div>
                                ";
        }
        // line 355
        echo "
                                ";
        // line 356
        if (($context["question_status"] ?? null)) {
            // line 357
            echo "                                    <div class=\"tab-pane\" id=\"tab-questions\">
                                        ";
            // line 358
            echo ($context["product_questions"] ?? null);
            echo "
                                    </div>
                                ";
        }
        // line 361
        echo "
                                ";
        // line 362
        if (($context["review_status"] ?? null)) {
            // line 363
            echo "                                <div class=\"tab-pane\" id=\"tab-review\">
                                    <div class=\"row\">
                                        <div class=\"col-sm-6\">
                                            <h4><b>";
            // line 366
            echo ($context["button_reviews"] ?? null);
            echo "</b></h4>

                                            <div id=\"review\">
                                                ";
            // line 369
            if (($context["seo_reviews"] ?? null)) {
                // line 370
                echo "                                                    ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["seo_reviews"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["review"]) {
                    // line 371
                    echo "                                                        <div class=\"table\">
                                                            <div class=\"table-cell\"><i class=\"fa fa-user\"></i></div>
                                                            <div class=\"table-cell right\">
                                                                <p class=\"author\"><b>";
                    // line 374
                    echo twig_get_attribute($this->env, $this->source, $context["review"], "author", [], "any", false, false, false, 374);
                    echo "</b>  -  ";
                    echo twig_get_attribute($this->env, $this->source, $context["review"], "date_added", [], "any", false, false, false, 374);
                    echo "
                                                                    <span class=\"rating\">
\t\t<span class=\"rating_stars rating r";
                    // line 376
                    echo twig_get_attribute($this->env, $this->source, $context["review"], "rating", [], "any", false, false, false, 376);
                    echo "\">
\t\t<i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>
\t\t</span>
\t\t</span>
                                                                </p>
                                                                ";
                    // line 381
                    echo twig_get_attribute($this->env, $this->source, $context["review"], "text", [], "any", false, false, false, 381);
                    echo "
                                                            </div>
                                                        </div>
                                                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['review'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 385
                echo "                                                    ";
                if (($context["pagination"] ?? null)) {
                    // line 386
                    echo "                                                        <div class=\"pagination-holder\">";
                    echo ($context["pagination"] ?? null);
                    echo "</div>
                                                    ";
                }
                // line 388
                echo "                                                ";
            } else {
                // line 389
                echo "                                                    <p>";
                echo ($context["text_no_reviews"] ?? null);
                echo "</p>
                                                ";
            }
            // line 391
            echo "                                            </div>

                                        </div>
                                        <div class=\"col-sm-6 right\">
                                            <form class=\"form-horizontal\" id=\"form-review\">

                                                <h4 id=\"review-notification\"><b>";
            // line 397
            echo ($context["text_write"] ?? null);
            echo "</b></h4>
                                                ";
            // line 398
            if (($context["review_guest"] ?? null)) {
                // line 399
                echo "
                                                    <div class=\"form-group required\">
                                                        <div class=\"col-sm-12 rating-stars\">
                                                            <label class=\"control-label\">";
                // line 402
                echo ($context["entry_rating"] ?? null);
                echo "</label>

                                                            <input type=\"radio\" value=\"1\" name=\"rating\" id=\"rating1\" />
                                                            <label for=\"rating1\"><i class=\"fa fa-star-o\"></i></label>

                                                            <input type=\"radio\" value=\"2\" name=\"rating\" id=\"rating2\" />
                                                            <label for=\"rating2\"><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i></label>

                                                            <input type=\"radio\" value=\"3\" name=\"rating\" id=\"rating3\" />
                                                            <label for=\"rating3\"><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i></label>

                                                            <input type=\"radio\" value=\"4\" name=\"rating\" id=\"rating4\" />
                                                            <label for=\"rating4\"><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i></label>

                                                            <input type=\"radio\" value=\"5\" name=\"rating\" id=\"rating5\" />
                                                            <label for=\"rating5\"><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i></label>
                                                        </div>
                                                    </div>

                                                    <div class=\"form-group required\">
                                                        <div class=\"col-sm-12\">
                                                            <label class=\"control-label\" for=\"input-name\">";
                // line 423
                echo ($context["entry_name"] ?? null);
                echo "</label>
                                                            <input type=\"text\" name=\"name\" value=\"";
                // line 424
                echo ($context["customer_name"] ?? null);
                echo "\" id=\"input-name\" class=\"form-control grey\" />
                                                        </div>
                                                    </div>
                                                    <div class=\"form-group required\">
                                                        <div class=\"col-sm-12\">
                                                            <label class=\"control-label\" for=\"input-review\">";
                // line 429
                echo ($context["entry_review"] ?? null);
                echo "</label>
                                                            <textarea name=\"text\" rows=\"5\" id=\"input-review\" class=\"form-control grey\"></textarea>
                                                            <small>";
                // line 431
                echo ($context["text_note"] ?? null);
                echo "</small>
                                                        </div>
                                                    </div>

                                                    <div class=\"form-group required\">
                                                        <div class=\"col-sm-12\">
                                                            ";
                // line 437
                echo ($context["captcha"] ?? null);
                echo "
                                                        </div>
                                                    </div>

                                                    <div class=\"buttons clearfix\">
                                                        <div class=\"text-right\">
                                                            <button type=\"button\" id=\"button-review\" data-loading-text=\"";
                // line 443
                echo ($context["text_loading"] ?? null);
                echo "\" class=\"btn btn-outline\">";
                echo ($context["button_continue"] ?? null);
                echo "</button>
                                                        </div>
                                                    </div>
                                                ";
            } else {
                // line 447
                echo "                                                    ";
                echo ($context["text_login"] ?? null);
                echo "
                                                ";
            }
            // line 449
            echo "                                            </form>
                                        </div>
                                    </div>
                                </div>
                                ";
        }
        // line 453
        echo "<!-- if review-status ends -->

                            </div> <!-- .tab-content ends -->
                        </div> <!-- .col-sm-12 ends -->
                    </div> <!-- .row ends -->
                    <!-- Tabs area ends -->

                    ";
        // line 460
        if (($context["full_width_tabs"] ?? null)) {
            // line 461
            echo "                </div>
                ";
        }
        // line 463
        echo "            </div>


        </div> <!-- .table-cell.right ends -->

    </div> <!-- .product-info ends -->

           <!-- Related Products -->

    ";
        // line 472
        if (($context["full_width_tabs"] ?? null)) {
            // line 473
            echo "    <div class=\"container c10padd\">
        ";
        }
        // line 475
        echo "
        ";
        // line 476
        if (($context["products"] ?? null)) {
            // line 477
            echo "            <div class=\"widget widget-related\">

                <div class=\"widget-title\">
                    <p class=\"main-title\"><span>";
            // line 480
            echo ($context["text_related"] ?? null);
            echo "</span></p>
                    <p class=\"widget-title-separator\"><i class=\"icon-line-cross\"></i></p>
                </div>

                <div class=\"grid grid-holder related carousel grid";
            // line 484
            echo ($context["basel_rel_prod_grid"] ?? null);
            echo "\">
                    ";
            // line 485
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 486
                echo "                        ";
                $this->loadTemplate("basel/template/product/single_product.twig", "basel/template/product/product.twig", 486)->display($context);
                // line 487
                echo "                    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 488
            echo "                </div>
            </div>
        ";
        }
        // line 491
        echo "
        ";
        // line 492
        echo ($context["content_bottom"] ?? null);
        echo "

        ";
        // line 494
        if (($context["full_width_tabs"] ?? null)) {
            // line 495
            echo "    </div>
    ";
        }
        // line 497
        echo "

    ";
        // line 499
        if ( !($context["full_width_tabs"] ?? null)) {
            // line 500
            echo "</div> <!-- main column ends -->
";
            // line 501
            echo ($context["column_right"] ?? null);
            echo "
    </div> <!-- .row ends -->
    </div> <!-- .container ends -->
";
        }
        // line 505
        echo "
<script src=\"catalog/view/theme/basel/js/lightgallery/js/lightgallery.min.js\"></script>
<script src=\"catalog/view/theme/basel/js/lightgallery/js/lg-zoom.min.js\"></script>
<script src=\"catalog/view/theme/basel/js/cloudzoom/cloud-zoom.1.0.2.min.js\"></script>
";
        // line 509
        if (($context["basel_price_update"] ?? null)) {
            // line 510
            echo "    <script src=\"index.php?route=extension/basel/live_options/js&product_id=";
            echo ($context["product_id"] ?? null);
            echo "\"></script>
";
        }
        // line 512
        echo "
";
        // line 513
        if (($context["products"] ?? null)) {
            // line 514
            echo "    <script><!--
        \$('.grid-holder.related').slick({
            prevArrow: \"<a class=\\\"arrow-left icon-arrow-left\\\"></a>\",
            nextArrow: \"<a class=\\\"arrow-right icon-arrow-right\\\"></a>\",
            dots:true,
            ";
            // line 519
            if ((($context["direction"] ?? null) == "rtl")) {
                // line 520
                echo "            rtl: true,
            ";
            }
            // line 522
            echo "            respondTo:'min',
            ";
            // line 523
            if ((($context["basel_rel_prod_grid"] ?? null) == "5")) {
                // line 524
                echo "            slidesToShow:5,slidesToScroll:5,responsive:[{breakpoint:1100,settings:{slidesToShow:4,slidesToScroll:4}},{breakpoint:960,settings:{slidesToShow:3,slidesToScroll:3}},{breakpoint:600,settings:{slidesToShow:2,slidesToScroll:2}},
                ";
            } elseif ((            // line 525
($context["basel_rel_prod_grid"] ?? null) == "4")) {
                // line 526
                echo "                slidesToShow:4,slidesToScroll:4,responsive:[{breakpoint:960,settings:{slidesToShow:3,slidesToScroll:3}},{breakpoint:600,settings:{slidesToShow:2,slidesToScroll:2}},
            ";
            } elseif ((            // line 527
($context["basel_rel_prod_grid"] ?? null) == "3")) {
                // line 528
                echo "            slidesToShow:3,slidesToScroll:3,responsive:[{breakpoint:600,settings:{slidesToShow:2,slidesToScroll:2}},
            ";
            } elseif ((            // line 529
($context["basel_rel_prod_grid"] ?? null) == "2")) {
                // line 530
                echo "            slidesToShow:2,slidesToScroll:2,responsive:[
            ";
            }
            // line 532
            echo "            ";
            if (($context["items_mobile_fw"] ?? null)) {
                // line 533
                echo "            {breakpoint:320,settings:{slidesToShow:1,slidesToScroll:1}}
            ";
            }
            // line 535
            echo "        ]
        });
        \$('.product-style2 .single-product .icon').attr('data-placement', 'top');
        \$('[data-toggle=\\'tooltip\\']').tooltip({container: 'body'});
        //--></script>
";
        }
        // line 541
        echo "
";
        // line 542
        if ((($context["sale_end_date"] ?? null) && ($context["product_page_countdown"] ?? null))) {
            // line 543
            echo "    <script>
        \$(function() {
            \$('#special_countdown').countdown('";
            // line 545
            echo ($context["sale_end_date"] ?? null);
            echo "').on('update.countdown', function(event) {
                var \$this = \$(this).html(event.strftime(''
                    + '<div class=\\\"special_countdown\\\"></span><p><span class=\\\"icon-clock\\\"></span> ";
            // line 547
            echo ($context["basel_text_offer_ends"] ?? null);
            echo "</p><div>'
                    + '%D<i>";
            // line 548
            echo ($context["basel_text_days"] ?? null);
            echo "</i></div><div>'
                    + '%H <i>";
            // line 549
            echo ($context["basel_text_hours"] ?? null);
            echo "</i></div><div>'
                    + '%M <i>";
            // line 550
            echo ($context["basel_text_mins"] ?? null);
            echo "</i></div><div>'
                    + '%S <i>";
            // line 551
            echo ($context["basel_text_secs"] ?? null);
            echo "</i></div></div>'));
            });
        });
    </script>
";
        }
        // line 556
        echo "
<script><!--
    \$('select[name=\\'recurring_id\\'], input[name=\"quantity\"]').change(function(){
        \$.ajax({
            url: 'index.php?route=product/product/getRecurringDescription',
            type: 'post',
            data: \$('input[name=\\'product_id\\'], input[name=\\'quantity\\'], select[name=\\'recurring_id\\']'),
            dataType: 'json',
            beforeSend: function() {
                \$('#recurring-description').html('');
            },
            success: function(json) {
                \$('.alert-dismissible, .text-danger').remove();

                if (json['success']) {
                    \$('#recurring-description').html(json['success']);
                }
            }
        });
    });
    //--></script>

<script><!--
    \$('#button-cart').on('click', function() {
        \$.ajax({
            url: 'index.php?route=extension/basel/basel_features/add_to_cart',
            type: 'post',
            data: \$('#product input[type=\\'text\\'], #product input[type=\\'hidden\\'], #product input[type=\\'number\\'], #product input[type=\\'radio\\']:checked, #product input[type=\\'checkbox\\']:checked, #product select, #product textarea'),
            dataType: 'json',
            beforeSend: function(json) {
                \$('body').append('<span class=\"basel-spinner ajax-call\"></span>');
            },

            success: function(json) {
                \$('.alert, .text-danger').remove();
                \$('.table-cell').removeClass('has-error');

                if (json.error) {
                    \$('.basel-spinner.ajax-call').remove();
                    if (json.error.option) {
                        for (i in json.error.option) {
                            var element = \$('#input-option' + i.replace('_', '-'));

                            if (element.parent().hasClass('input-group')) {
                                element.parent().after('<div class=\"text-danger\">' + json.error.option[i] + '</div>');
                            } else {
                                element.after('<div class=\"text-danger\">' + json.error.option[i] + '</div>');
                            }
                        }
                    }

                    if (json.error.recurring) {
                        \$('select[name=\\'recurring_id\\']').after('<div class=\"text-danger\">' + json['error']['recurring'] + '</div>');
                    }

                    // Highlight any found errors
                    \$('.text-danger').parent().addClass('has-error');
                }

                if (json.success_redirect) {

                    location = json.success_redirect;

                } else if (json.success) {

                    \$('.table-cell').removeClass('has-error');
                    \$('.alert, .popup-note, .basel-spinner.ajax-call, .text-danger').remove();

                    html = '<div class=\"popup-note\">';
                    html += '<div class=\"inner\">';
                    html += '<a class=\"popup-note-close\" onclick=\"\$(this).parent().parent().remove()\">&times;</a>';
                    html += '<div class=\"table\">';
                    html += '<div class=\"table-cell v-top img\"><img src=\"' + json.image + '\" /></div>';
                    html += '<div class=\"table-cell v-top\">' + json.success + '</div>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    \$('body').append(html);
                    setTimeout(function() {\$('.popup-note').hide();}, 8100);
                    // Need to set timeout otherwise it wont update the total
                    setTimeout(function () {
                        \$('.cart-total-items').html( json.total_items );
                        \$('.cart-total-amount').html( json.total_amount );
                    }, 100);

                    \$('#cart-content').load('index.php?route=common/cart/info #cart-content > *');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
        });
    });
    //--></script>
<script><!--
    \$('.date').datetimepicker({
        pickTime: false
    });

    \$('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });

    \$('.time').datetimepicker({
        pickDate: false
    });

    \$('button[id^=\\'button-upload\\']').on('click', function() {
        var node = this;

        \$('#form-upload').remove();

        \$('body').prepend('<form enctype=\"multipart/form-data\" id=\"form-upload\" style=\"display: none;\"><input type=\"file\" name=\"file\" /></form>');

        \$('#form-upload input[name=\\'file\\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function() {
            if (\$('#form-upload input[name=\\'file\\']').val() != '') {
                clearInterval(timer);

                \$.ajax({
                    url: 'index.php?route=tool/upload',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData(\$('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        \$(node).button('loading');
                    },
                    complete: function() {
                        \$(node).button('reset');
                    },
                    success: function(json) {
                        \$('.text-danger').remove();

                        if (json['error']) {
                            \$(node).parent().find('input').after('<div class=\"text-danger\">' + json['error'] + '</div>');
                        }

                        if (json['success']) {
                            alert(json['success']);

                            \$(node).parent().find('input').val(json['code']);
                        }
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                    }
                });
            }
        }, 500);
    });
    //--></script>
<script><!--
    \$('#review').delegate('.pagination a', 'click', function(e) {
        e.preventDefault();
        \$(\"html,body\").animate({scrollTop:((\$(\"#review\").offset().top)-50)},500);
        \$('#review').fadeOut(50);

        \$('#review').load(this.href);

        \$('#review').fadeIn(500);

    });


    \$('#button-review').on('click', function() {
        \$.ajax({
            url: 'index.php?route=product/product/write&product_id=";
        // line 731
        echo ($context["product_id"] ?? null);
        echo "',
            type: 'post',
            dataType: 'json',
            data: \$(\"#form-review\").serialize(),
            beforeSend: function() {
                \$('#button-review').button('loading');
            },
            complete: function() {
                \$('#button-review').button('reset');
            },
            success: function(json) {
                \$('.alert-success, .alert-danger').remove();

                if (json.error) {
                    \$('#review-notification').after('<div class=\"alert alert-sm alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ' + json.error + '</div>');
                }

                if (json.success) {
                    \$('#review-notification').after('<div class=\"alert alert-sm alert-success\"><i class=\"fa fa-check-circle\"></i> ' + json.success + '</div>');

                    \$('input[name=\\'name\\']').val('');
                    \$('textarea[name=\\'text\\']').val('');
                    \$('input[name=\\'rating\\']:checked').prop('checked', false);
                }
            }
        });
    });

    \$(document).ready(function() {
        ";
        // line 760
        if ((($context["product_layout"] ?? null) == "full-width")) {
            // line 761
            echo "// Sticky information
        \$('.table-cell.right .inner').theiaStickySidebar({containerSelector:'.product-info'});
        ";
        }
        // line 764
        echo "
// Reviews/Question scroll link
        \$(\".to_tabs\").click(function() {
            \$('html, body').animate({
                scrollTop: (\$(\".main_tabs\").offset().top - 100)
            }, 1000);
        });

// Sharing buttons
        ";
        // line 773
        if (($context["basel_share_btn"] ?? null)) {
            // line 774
            echo "        var share_url = encodeURIComponent(window.location.href);
        var page_title = '";
            // line 775
            echo ($context["heading_title"] ?? null);
            echo "';
        ";
            // line 776
            if (($context["thumb"] ?? null)) {
                // line 777
                echo "        var thumb = '";
                echo ($context["thumb"] ?? null);
                echo "';
        ";
            }
            // line 779
            echo "        \$('.fb_share').attr(\"href\", 'https://www.facebook.com/sharer/sharer.php?u=' + share_url + '');
        \$('.twitter_share').attr(\"href\", 'https://twitter.com/intent/tweet?source=' + share_url + '&text=' + page_title + ': ' + share_url + '');
        \$('.google_share').attr(\"href\", 'https://plus.google.com/share?url=' + share_url + '');
        \$('.pinterest_share').attr(\"href\", 'http://pinterest.com/pin/create/button/?url=' + share_url + '&media=' + thumb + '&description=' + page_title + '');
        \$('.vk_share').attr(\"href\", 'http://vkontakte.ru/share.php?url=' + share_url + '');
        ";
        }
        // line 785
        echo "    });
    //--></script>

";
        // line 788
        if ((($context["product_layout"] ?? null) != "full-width")) {
            // line 789
            echo "    <script>
        \$(document).ready(function() {
            \$('.image-additional a.link').click(function (e) {
                if (\$(this).hasClass(\"locked\")) {
                    e.stopImmediatePropagation();
                }
                \$('.image-additional a.link.active').removeClass('active');
                \$(this).addClass('active')
            });

            ";
            // line 799
            if (($context["images"] ?? null)) {
                // line 800
                echo "            \$('.cloud-zoom-wrap').click(function (e) {
                e.preventDefault();
                \$('.image-additional a.link.active').removeClass('locked').trigger('click').addClass('locked');
            });
            ";
            } else {
                // line 805
                echo "            \$('.cloud-zoom-wrap').click(function (e) {
                e.preventDefault();
                \$('#main-image').trigger('click');
            });
            ";
            }
            // line 810
            echo "
            \$('.image-additional').slick({
                prevArrow: \"<a class=\\\"icon-arrow-left\\\"></a>\",
                nextArrow: \"<a class=\\\"icon-arrow-right\\\"></a>\",
                appendArrows: '.image-additional .slick-list',
                arrows:true,
                ";
            // line 816
            if ((($context["direction"] ?? null) == "rtl")) {
                // line 817
                echo "                rtl: true,
                ";
            }
            // line 819
            echo "                infinite:false,
                ";
            // line 820
            if ((($context["product_layout"] ?? null) == "images-left")) {
                // line 821
                echo "                slidesToShow: ";
                echo twig_round((($context["img_h"] ?? null) / ($context["img_a_h"] ?? null)), 0, "floor");
                echo ",
                vertical:true,
                verticalSwiping:true,
                ";
            } else {
                // line 825
                echo "                slidesToShow: ";
                echo twig_round((($context["img_w"] ?? null) / ($context["img_a_w"] ?? null)));
                echo ",
                ";
            }
            // line 827
            echo "                responsive: [
                    {
                        breakpoint: 992,
                        settings: {
                            vertical:false,
                            verticalSwiping:false
                        }
                    }]
            });

        });
        //--></script>
";
        }
        // line 840
        echo "<script>
    \$(document).ready(function() {
// Image Gallery
        \$(\"#gallery\").lightGallery({
            selector: '.link',
            download:false,
            hideBarsDelay:99999
        });
    });
    //--></script>
<script type=\"application/ld+json\">
{
\"@context\": \"http://schema.org\",
\"@type\": \"Product\",
\"image\": [
";
        // line 855
        if (($context["thumb"] ?? null)) {
            // line 856
            echo "\"";
            echo ($context["thumb"] ?? null);
            echo "\"
";
        }
        // line 858
        echo "],
\"description\": \"";
        // line 859
        echo ($context["meta_description"] ?? null);
        echo "\",
";
        // line 860
        if (($context["review_qty"] ?? null)) {
            // line 861
            echo "\"aggregateRating\": {
\"@type\": \"AggregateRating\",
\"ratingValue\": \"";
            // line 863
            echo ($context["rating"] ?? null);
            echo "\",
\"reviewCount\": \"";
            // line 864
            echo ($context["review_qty"] ?? null);
            echo "\"},
";
        }
        // line 866
        echo "\"name\": \"";
        echo ($context["heading_title"] ?? null);
        echo "\",
\"sku\": \"";
        // line 867
        echo ($context["model"] ?? null);
        echo "\",
";
        // line 868
        if (($context["manufacturer"] ?? null)) {
            // line 869
            echo "\"brand\": \"";
            echo ($context["manufacturer"] ?? null);
            echo "\",
";
        }
        // line 871
        echo "\"offers\": {
\"@type\": \"Offer\",
";
        // line 873
        if ((($context["qty"] ?? null) > 0)) {
            // line 874
            echo "\"availability\": \"http://schema.org/InStock\",
";
        } else {
            // line 876
            echo "\"availability\": \"http://schema.org/OutOfStock\",
";
        }
        // line 878
        if (($context["price"] ?? null)) {
            // line 879
            echo "    ";
            if (($context["special"] ?? null)) {
                // line 880
                echo "\"price\": \"";
                echo ($context["special_snippet"] ?? null);
                echo "\",
";
            } else {
                // line 882
                echo "\"price\": \"";
                echo ($context["price_snippet"] ?? null);
                echo "\",
";
            }
            // line 884
            echo "\"priceCurrency\": \"";
            echo ($context["currency_code"] ?? null);
            echo "\"
";
        }
        // line 886
        echo "}
}
</script>
";
        // line 889
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "basel/template/product/product.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1863 => 889,  1858 => 886,  1852 => 884,  1846 => 882,  1840 => 880,  1837 => 879,  1835 => 878,  1831 => 876,  1827 => 874,  1825 => 873,  1821 => 871,  1815 => 869,  1813 => 868,  1809 => 867,  1804 => 866,  1799 => 864,  1795 => 863,  1791 => 861,  1789 => 860,  1785 => 859,  1782 => 858,  1776 => 856,  1774 => 855,  1757 => 840,  1742 => 827,  1736 => 825,  1728 => 821,  1726 => 820,  1723 => 819,  1719 => 817,  1717 => 816,  1709 => 810,  1702 => 805,  1695 => 800,  1693 => 799,  1681 => 789,  1679 => 788,  1674 => 785,  1666 => 779,  1660 => 777,  1658 => 776,  1654 => 775,  1651 => 774,  1649 => 773,  1638 => 764,  1633 => 761,  1631 => 760,  1599 => 731,  1422 => 556,  1414 => 551,  1410 => 550,  1406 => 549,  1402 => 548,  1398 => 547,  1393 => 545,  1389 => 543,  1387 => 542,  1384 => 541,  1376 => 535,  1372 => 533,  1369 => 532,  1365 => 530,  1363 => 529,  1360 => 528,  1358 => 527,  1355 => 526,  1353 => 525,  1350 => 524,  1348 => 523,  1345 => 522,  1341 => 520,  1339 => 519,  1332 => 514,  1330 => 513,  1327 => 512,  1321 => 510,  1319 => 509,  1313 => 505,  1306 => 501,  1303 => 500,  1301 => 499,  1297 => 497,  1293 => 495,  1291 => 494,  1286 => 492,  1283 => 491,  1278 => 488,  1264 => 487,  1261 => 486,  1244 => 485,  1240 => 484,  1233 => 480,  1228 => 477,  1226 => 476,  1223 => 475,  1219 => 473,  1217 => 472,  1206 => 463,  1202 => 461,  1200 => 460,  1191 => 453,  1184 => 449,  1178 => 447,  1169 => 443,  1160 => 437,  1151 => 431,  1146 => 429,  1138 => 424,  1134 => 423,  1110 => 402,  1105 => 399,  1103 => 398,  1099 => 397,  1091 => 391,  1085 => 389,  1082 => 388,  1076 => 386,  1073 => 385,  1063 => 381,  1055 => 376,  1048 => 374,  1043 => 371,  1038 => 370,  1036 => 369,  1030 => 366,  1025 => 363,  1023 => 362,  1020 => 361,  1014 => 358,  1011 => 357,  1009 => 356,  1006 => 355,  1001 => 352,  994 => 350,  985 => 347,  981 => 346,  978 => 345,  974 => 344,  967 => 340,  963 => 338,  959 => 337,  955 => 335,  953 => 334,  950 => 333,  947 => 332,  938 => 329,  933 => 328,  928 => 327,  926 => 326,  920 => 323,  913 => 318,  905 => 316,  902 => 315,  896 => 313,  893 => 312,  887 => 310,  884 => 309,  881 => 308,  870 => 306,  865 => 305,  863 => 304,  859 => 303,  855 => 302,  848 => 297,  844 => 295,  839 => 292,  837 => 291,  834 => 290,  827 => 286,  824 => 285,  822 => 284,  814 => 278,  811 => 277,  805 => 273,  800 => 270,  797 => 269,  795 => 268,  792 => 267,  773 => 265,  771 => 264,  768 => 263,  760 => 261,  758 => 260,  751 => 258,  748 => 257,  738 => 255,  736 => 254,  725 => 252,  722 => 251,  714 => 249,  712 => 248,  703 => 241,  697 => 239,  695 => 238,  689 => 237,  683 => 236,  677 => 232,  671 => 230,  669 => 229,  657 => 227,  653 => 226,  647 => 225,  642 => 222,  636 => 218,  625 => 216,  621 => 215,  617 => 214,  611 => 211,  608 => 210,  606 => 209,  603 => 208,  598 => 205,  589 => 202,  583 => 198,  577 => 197,  569 => 195,  566 => 194,  562 => 193,  554 => 190,  548 => 186,  541 => 184,  534 => 182,  532 => 181,  525 => 180,  521 => 179,  517 => 178,  511 => 177,  503 => 174,  495 => 172,  493 => 171,  490 => 170,  486 => 169,  483 => 168,  481 => 167,  475 => 163,  469 => 161,  467 => 160,  464 => 159,  456 => 157,  454 => 156,  451 => 155,  447 => 154,  443 => 152,  431 => 150,  427 => 149,  424 => 148,  422 => 147,  418 => 145,  409 => 142,  403 => 140,  401 => 139,  398 => 138,  396 => 137,  393 => 136,  388 => 134,  381 => 130,  378 => 129,  376 => 128,  370 => 125,  363 => 120,  349 => 108,  345 => 106,  331 => 104,  328 => 103,  307 => 100,  304 => 99,  300 => 98,  297 => 97,  295 => 96,  292 => 95,  270 => 92,  267 => 91,  261 => 89,  259 => 88,  256 => 87,  250 => 85,  248 => 84,  245 => 83,  239 => 81,  237 => 80,  233 => 78,  231 => 77,  223 => 75,  221 => 74,  212 => 68,  207 => 67,  204 => 66,  201 => 65,  198 => 64,  195 => 63,  192 => 62,  189 => 61,  187 => 60,  183 => 59,  178 => 57,  174 => 55,  163 => 53,  159 => 52,  155 => 50,  149 => 46,  143 => 44,  138 => 42,  133 => 41,  131 => 40,  125 => 37,  121 => 35,  115 => 33,  109 => 31,  107 => 30,  102 => 27,  96 => 25,  94 => 24,  90 => 22,  84 => 20,  79 => 18,  74 => 17,  72 => 16,  66 => 13,  62 => 11,  56 => 9,  50 => 7,  48 => 6,  44 => 4,  42 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "basel/template/product/product.twig", "");
    }
}
