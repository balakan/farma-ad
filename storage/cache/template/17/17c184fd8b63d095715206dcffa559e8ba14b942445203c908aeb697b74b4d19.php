<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* basel/template/extension/quickcheckout/shipping_method.twig */
class __TwigTemplate_9e0d0dde0951a5fe6ffb56ad18dba01bfcf4a8b9f7d72339dcfa31272193c030 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["error_warning"] ?? null)) {
            // line 2
            echo "    <div class=\"alert alert-danger\">";
            echo ($context["error_warning"] ?? null);
            echo "</div>
";
        }
        // line 4
        if (($context["shipping_methods"] ?? null)) {
            // line 5
            echo "    <p>";
            echo ($context["text_shipping_method"] ?? null);
            echo "</p>
    ";
            // line 6
            if (($context["shipping"] ?? null)) {
                // line 7
                echo "        <table class=\"table\">
            ";
                // line 8
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["shipping_methods"] ?? null));
                foreach ($context['_seq'] as $context["key"] => $context["shipping_method"]) {
                    // line 9
                    echo "                ";
                    if ( !twig_get_attribute($this->env, $this->source, $context["shipping_method"], "error", [], "any", false, false, false, 9)) {
                        // line 10
                        echo "                    ";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["shipping_method"], "quote", [], "any", false, false, false, 10));
                        foreach ($context['_seq'] as $context["_key"] => $context["quote"]) {
                            // line 11
                            echo "                        ";
                            if (($context["key"] != "collector")) {
                                // line 12
                                echo "                            <tr>
                                <td>";
                                // line 13
                                if ((twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 13) == ($context["code"] ?? null))) {
                                    // line 14
                                    echo "                                        <input type=\"radio\" name=\"shipping_method\" value=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 14);
                                    echo "\" id=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 14);
                                    echo "\" checked=\"checked\" />
                                    ";
                                } else {
                                    // line 16
                                    echo "                                        <input type=\"radio\" name=\"shipping_method\" value=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 16);
                                    echo "\" id=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 16);
                                    echo "\" />
                                    ";
                                }
                                // line 17
                                echo "</td>
                                <td style=\"width:100%;padding-left:10px;\">
                                    <label for=\"";
                                // line 19
                                echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 19);
                                echo "\">
                                        ";
                                // line 20
                                if ((($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["shipping_logo"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4[$context["key"]] ?? null) : null)) {
                                    // line 21
                                    echo "                                            <img src=\"";
                                    echo (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = ($context["shipping_logo"] ?? null)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144[$context["key"]] ?? null) : null);
                                    echo "\" alt=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["shipping_method"], "title", [], "any", false, false, false, 21);
                                    echo "\" title=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["shipping_method"], "title", [], "any", false, false, false, 21);
                                    echo "\" />
                                        ";
                                }
                                // line 23
                                echo "                                        ";
                                echo twig_get_attribute($this->env, $this->source, $context["quote"], "title", [], "any", false, false, false, 23);
                                echo "</label></td>
                                <td style=\"text-align: right;\"><label for=\"";
                                // line 24
                                echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 24);
                                echo "\">";
                                echo twig_get_attribute($this->env, $this->source, $context["quote"], "text", [], "any", false, false, false, 24);
                                echo "</label></td>
                            </tr>
                        ";
                            } else {
                                // line 27
                                echo "                            <tr>
                                <td>";
                                // line 28
                                if ((twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 28) == ($context["code"] ?? null))) {
                                    // line 29
                                    echo "                                        <input type=\"radio\" name=\"shipping_method\" value=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 29);
                                    echo "\" id=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 29);
                                    echo "\" checked=\"checked\" />
                                    ";
                                } else {
                                    // line 31
                                    echo "                                        <input type=\"radio\" name=\"shipping_method\" value=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 31);
                                    echo "\" id=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 31);
                                    echo "\" />
                                    ";
                                }
                                // line 32
                                echo "</td>
                                <td style=\"width:100%;padding-left:10px;\">
                                    <label for=\"";
                                // line 34
                                echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 34);
                                echo "\">
                                        ";
                                // line 35
                                if ((($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = ($context["shipping_logo"] ?? null)) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b[$context["key"]] ?? null) : null)) {
                                    // line 36
                                    echo "                                            <img src=\"";
                                    echo (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = ($context["shipping_logo"] ?? null)) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002[$context["key"]] ?? null) : null);
                                    echo "\" alt=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["shipping_method"], "title", [], "any", false, false, false, 36);
                                    echo "\" title=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["shipping_method"], "title", [], "any", false, false, false, 36);
                                    echo "\" />
                                        ";
                                }
                                // line 38
                                echo "                                        ";
                                echo twig_get_attribute($this->env, $this->source, $context["quote"], "title", [], "any", false, false, false, 38);
                                echo "</label></td>
                                <td style=\"text-align: right;\"><label for=\"";
                                // line 39
                                echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 39);
                                echo "\">";
                                echo twig_get_attribute($this->env, $this->source, $context["quote"], "text", [], "any", false, false, false, 39);
                                echo "</label></td>
                            </tr>
                            <tr id=\"collector-select\" class=\"hide\">
                                <td colspan=\"3\">
                                    <select name=\"shipping_collector_id\" class=\"block\" id=\"input-collector-pick\" style=\"width: 100% !important;\">
                                        <option value=\"NULL\">Odaberite vrijeme...</option>
                                        ";
                                // line 45
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable(($context["collector_list"] ?? null));
                                foreach ($context['_seq'] as $context["_key"] => $context["pick"]) {
                                    // line 46
                                    echo "                                            ";
                                    if ((twig_get_attribute($this->env, $this->source, $context["pick"], "id", [], "any", false, false, false, 46) == ($context["collector_picked"] ?? null))) {
                                        // line 47
                                        echo "                                                <option value=\"";
                                        echo twig_get_attribute($this->env, $this->source, $context["pick"], "value", [], "any", false, false, false, 47);
                                        echo "\" selected=\"selected\">";
                                        echo twig_get_attribute($this->env, $this->source, $context["pick"], "label", [], "any", false, false, false, 47);
                                        echo "</option>
                                            ";
                                    } else {
                                        // line 49
                                        echo "                                                <option value=\"";
                                        echo twig_get_attribute($this->env, $this->source, $context["pick"], "value", [], "any", false, false, false, 49);
                                        echo "\">";
                                        echo twig_get_attribute($this->env, $this->source, $context["pick"], "label", [], "any", false, false, false, 49);
                                        echo "</option>
                                            ";
                                    }
                                    // line 51
                                    echo "
                                        ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pick'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 53
                                echo "                                    </select>
                                </td>
                            </tr>
                        ";
                            }
                            // line 57
                            echo "                    ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['quote'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 58
                        echo "                ";
                    } else {
                        // line 59
                        echo "                    <tr>
                        <td colspan=\"3\"><div class=\"error\">";
                        // line 60
                        echo twig_get_attribute($this->env, $this->source, $context["shipping_method"], "error", [], "any", false, false, false, 60);
                        echo "</div></td>
                    </tr>
                ";
                    }
                    // line 63
                    echo "            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['shipping_method'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 64
                echo "        </table>
    ";
            } else {
                // line 66
                echo "        <select class=\"form-control\" name=\"shipping_method\">
            ";
                // line 67
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["shipping_methods"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["shipping_method"]) {
                    // line 68
                    echo "                ";
                    if ( !twig_get_attribute($this->env, $this->source, $context["shipping_method"], "error", [], "any", false, false, false, 68)) {
                        // line 69
                        echo "                    ";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["shipping_method"], "quote", [], "any", false, false, false, 69));
                        foreach ($context['_seq'] as $context["_key"] => $context["quote"]) {
                            // line 70
                            echo "                        ";
                            if ((twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 70) == ($context["code"] ?? null))) {
                                // line 71
                                echo "                            ";
                                $context["code"] = twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 71);
                                // line 72
                                echo "                            ";
                                $context["exists"] = true;
                                // line 73
                                echo "                        <option value=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 73);
                                echo "\" selected=\"selected\">
                        ";
                            } else {
                                // line 75
                                echo "                            <option value=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["quote"], "code", [], "any", false, false, false, 75);
                                echo "\">
                        ";
                            }
                            // line 77
                            echo "                        ";
                            echo twig_get_attribute($this->env, $this->source, $context["quote"], "title", [], "any", false, false, false, 77);
                            echo "&nbsp;&nbsp;(";
                            echo twig_get_attribute($this->env, $this->source, $context["quote"], "text", [], "any", false, false, false, 77);
                            echo ")</option>
                    ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['quote'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 79
                        echo "                ";
                    }
                    // line 80
                    echo "            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['shipping_method'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 81
                echo "        </select><br />
    ";
            }
            // line 83
            echo "    <br />
";
        }
        // line 85
        if ((($context["delivery"] ?? null) && (( !($context["delivery_delivery_time"] ?? null) || (($context["delivery_delivery_time"] ?? null) == "1")) || (($context["delivery_delivery_time"] ?? null) == "3")))) {
            // line 86
            echo "    <div";
            echo ((($context["delivery_required"] ?? null)) ? (" class=\"required\"") : (""));
            echo ">
        <label class=\"control-label\"><strong>";
            // line 87
            echo ($context["text_delivery"] ?? null);
            echo "</strong></label>
        ";
            // line 88
            if ((($context["delivery_delivery_time"] ?? null) == "1")) {
                // line 89
                echo "            <input type=\"text\" name=\"delivery_date\" value=\"";
                echo ($context["delivery_date"] ?? null);
                echo "\" class=\"form-control date\" readonly=\"true\" style=\"background:#ffffff;\" />
        ";
            } else {
                // line 91
                echo "            <input type=\"text\" name=\"delivery_date\" value=\"";
                echo ($context["delivery_date"] ?? null);
                echo "\" class=\"form-control date\" readonly=\"true\" style=\"background:#ffffff;\" />
        ";
            }
            // line 93
            echo "        ";
            if ((($context["delivery_delivery_time"] ?? null) == "3")) {
                echo "<br />
            <select name=\"delivery_time\" class=\"form-control\">";
                // line 94
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["delivery_times"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["quickcheckout_delivery_time"]) {
                    // line 95
                    echo "                    ";
                    if ((($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = $context["quickcheckout_delivery_time"]) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4[($context["language_id"] ?? null)] ?? null) : null)) {
                        // line 96
                        echo "                        ";
                        if ((($context["delivery_time"] ?? null) == (($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = $context["quickcheckout_delivery_time"]) && is_array($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) || $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 instanceof ArrayAccess ? ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666[($context["language_id"] ?? null)] ?? null) : null))) {
                            // line 97
                            echo "                            <option value=\"";
                            echo (($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = $context["quickcheckout_delivery_time"]) && is_array($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e) || $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e instanceof ArrayAccess ? ($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e[($context["language_id"] ?? null)] ?? null) : null);
                            echo "\" selected=\"selected\">";
                            echo (($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 = $context["quickcheckout_delivery_time"]) && is_array($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52) || $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 instanceof ArrayAccess ? ($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52[($context["language_id"] ?? null)] ?? null) : null);
                            echo "</option>
                        ";
                        } else {
                            // line 99
                            echo "                            <option value=\"";
                            echo (($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 = $context["quickcheckout_delivery_time"]) && is_array($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136) || $__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 instanceof ArrayAccess ? ($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136[($context["language_id"] ?? null)] ?? null) : null);
                            echo "\">";
                            echo (($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 = $context["quickcheckout_delivery_time"]) && is_array($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386) || $__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 instanceof ArrayAccess ? ($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386[($context["language_id"] ?? null)] ?? null) : null);
                            echo "</option>
                        ";
                        }
                        // line 101
                        echo "                    ";
                    }
                    // line 102
                    echo "                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['quickcheckout_delivery_time'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                echo "</select>
        ";
            }
            // line 104
            echo "    </div>
";
        } elseif ((        // line 105
($context["delivery_delivery_time"] ?? null) && (($context["delivery_delivery_time"] ?? null) == "2"))) {
            // line 106
            echo "    <input type=\"text\" name=\"delivery_date\" value=\"\" class=\"hide\" />
    <select name=\"delivery_time\" class=\"hide\"><option value=\"\"></option></select>
    <strong>";
            // line 108
            echo ($context["text_estimated_delivery"] ?? null);
            echo "</strong><br />
    ";
            // line 109
            echo ($context["estimated_delivery"] ?? null);
            echo "<br />
    ";
            // line 110
            echo ($context["estimated_delivery_time"] ?? null);
            echo "
";
        } else {
            // line 112
            echo "    <input type=\"text\" name=\"delivery_date\" value=\"\" class=\"hide\" />
    <select name=\"delivery_time\" class=\"hide\"><option value=\"\"></option></select>
";
        }
        // line 115
        echo "
<link href=\"catalog/view/javascript/select2/select2.css\" rel=\"stylesheet\" />
<script src=\"catalog/view/javascript/select2/select2.min.js\"></script>

<script type=\"text/javascript\"><!--
    \$('#shipping-method input[name=\\'shipping_method\\'], #shipping-method select[name=\\'shipping_method\\']').on('change', function() {
        checkShipping();
        ";
        // line 122
        if ( !($context["logged"] ?? null)) {
            // line 123
            echo "        if (\$('#payment-address input[name=\\'shipping_address\\']:checked').val()) {
            var post_data = \$('#payment-address input[type=\\'text\\'], #payment-address input[type=\\'checkbox\\']:checked, #payment-address input[type=\\'radio\\']:checked, #payment-address input[type=\\'hidden\\'], #payment-address select, #shipping-method input[type=\\'text\\'], #shipping-method input[type=\\'checkbox\\']:checked, #shipping-method input[type=\\'radio\\']:checked, #shipping-method input[type=\\'hidden\\'], #shipping-method select, #shipping-method textarea');
        } else {
            var post_data = \$('#shipping-address input[type=\\'text\\'], #shipping-address input[type=\\'checkbox\\']:checked, #shipping-address input[type=\\'radio\\']:checked, #shipping-address input[type=\\'hidden\\'], #shipping-address select, #shipping-method input[type=\\'text\\'], #shipping-method input[type=\\'checkbox\\']:checked, #shipping-method input[type=\\'radio\\']:checked, #shipping-method input[type=\\'hidden\\'], #shipping-method select, #shipping-method textarea');
        }

        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/shipping_method/set',
            type: 'post',
            data: post_data,
            dataType: 'html',
            cache: false,
            success: function(html) {
                ";
            // line 136
            if (($context["cart"] ?? null)) {
                // line 137
                echo "                loadCart();
                ";
            }
            // line 139
            echo "
                ";
            // line 140
            if (($context["shipping_reload"] ?? null)) {
                // line 141
                echo "                reloadPaymentMethod();
                ";
            }
            // line 143
            echo "            },
            ";
            // line 144
            if (($context["debug"] ?? null)) {
                // line 145
                echo "            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
            }
            // line 149
            echo "        });
        ";
        } else {
            // line 151
            echo "        if (\$('#shipping-address input[name=\\'shipping_address\\']:checked').val() == 'new') {
            var url = 'index.php?route=extension/quickcheckout/shipping_method/set';
            var post_data = \$('#shipping-address input[type=\\'text\\'], #shipping-address input[type=\\'checkbox\\']:checked, #shipping-address input[type=\\'radio\\']:checked, #shipping-address input[type=\\'hidden\\'], #shipping-address select, #shipping-method input[type=\\'text\\'], #shipping-method input[type=\\'checkbox\\']:checked, #shipping-method input[type=\\'radio\\']:checked, #shipping-method input[type=\\'hidden\\'], #shipping-method select, #shipping-method textarea');
        } else {
            var url = 'index.php?route=extension/quickcheckout/shipping_method/set&address_id=' + \$('#shipping-address select[name=\\'address_id\\']').val();
            var post_data = \$('#shipping-method input[type=\\'text\\'], #shipping-method input[type=\\'checkbox\\']:checked, #shipping-method input[type=\\'radio\\']:checked, #shipping-method input[type=\\'hidden\\'], #shipping-method select, #shipping-method textarea');
        }

        \$.ajax({
            url: url,
            type: 'post',
            data: post_data,
            dataType: 'html',
            cache: false,
            success: function(html) {
                ";
            // line 166
            if (($context["cart"] ?? null)) {
                // line 167
                echo "                loadCart();
                ";
            }
            // line 169
            echo "
                ";
            // line 170
            if (($context["shipping_reload"] ?? null)) {
                // line 171
                echo "                if (\$('#payment-address input[name=\\'payment_address\\']').val() == 'new') {
                    reloadPaymentMethod();
                } else {
                    reloadPaymentMethodById(\$('#payment-address select[name=\\'address_id\\']').val());
                }
                ";
            }
            // line 177
            echo "            },
            ";
            // line 178
            if (($context["debug"] ?? null)) {
                // line 179
                echo "            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
            }
            // line 183
            echo "        });
        ";
        }
        // line 185
        echo "    });

    \$('#input-collector-pick').on('change', () => {
        \$('#shipping-method input[name=\\'shipping_method\\']:checked, #shipping-method select[name=\\'shipping_method\\']').trigger('change');
    });

    \$(document).ready(function() {
        \$('#shipping-method input[name=\\'shipping_method\\']:checked, #shipping-method select[name=\\'shipping_method\\']').trigger('change');

        \$('#input-collector-pick').select2();
    });

    ";
        // line 197
        if ((($context["delivery"] ?? null) && (($context["delivery_delivery_time"] ?? null) == "1"))) {
            // line 198
            echo "    \$(document).ready(function() {
        \$('input[name=\\'delivery_date\\']').datetimepicker({
            format: 'YYYY-MM-DD HH:mm',
            minDate: '";
            // line 201
            echo ($context["delivery_min"] ?? null);
            echo "',
            maxDate: '";
            // line 202
            echo ($context["delivery_max"] ?? null);
            echo "',
            disabledDates: [";
            // line 203
            echo ($context["delivery_unavailable"] ?? null);
            echo "],
            enabledHours: [";
            // line 204
            echo ($context["hours"] ?? null);
            echo "],
            ignoreReadonly: true,
            ";
            // line 206
            if ((($context["delivery_days_of_week"] ?? null) != "")) {
                // line 207
                echo "            daysOfWeekDisabled: [";
                echo ($context["delivery_days_of_week"] ?? null);
                echo "]
            ";
            }
            // line 209
            echo "        });
    });
    ";
        } elseif ((        // line 211
($context["delivery"] ?? null) && ((($context["delivery_delivery_time"] ?? null) == "3") || (($context["delivery_delivery_time"] ?? null) == "0")))) {
            // line 212
            echo "    \$('input[name=\\'delivery_date\\']').datetimepicker({
        format: 'YYYY-MM-DD',
        minDate: '";
            // line 214
            echo ($context["delivery_min"] ?? null);
            echo "',
        maxDate: '";
            // line 215
            echo ($context["delivery_max"] ?? null);
            echo "',
        disabledDates: [";
            // line 216
            echo ($context["delivery_unavailable"] ?? null);
            echo "],
        ignoreReadonly: true,
        ";
            // line 218
            if ((($context["delivery_days_of_week"] ?? null) != "")) {
                // line 219
                echo "        daysOfWeekDisabled: [";
                echo ($context["delivery_days_of_week"] ?? null);
                echo "]
        ";
            }
            // line 221
            echo "    });
    ";
        }
        // line 223
        echo "
    function checkShipping() {
        let checked = \$('#shipping-method input[name=\\'shipping_method\\']:checked').val();

        if (checked == 'collector.collector') {
            \$('#collector-select').removeClass('hide');
        } else {
            \$('#collector-select').addClass('hide');
        }
    }
    //--></script>";
    }

    public function getTemplateName()
    {
        return "basel/template/extension/quickcheckout/shipping_method.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  594 => 223,  590 => 221,  584 => 219,  582 => 218,  577 => 216,  573 => 215,  569 => 214,  565 => 212,  563 => 211,  559 => 209,  553 => 207,  551 => 206,  546 => 204,  542 => 203,  538 => 202,  534 => 201,  529 => 198,  527 => 197,  513 => 185,  509 => 183,  503 => 179,  501 => 178,  498 => 177,  490 => 171,  488 => 170,  485 => 169,  481 => 167,  479 => 166,  462 => 151,  458 => 149,  452 => 145,  450 => 144,  447 => 143,  443 => 141,  441 => 140,  438 => 139,  434 => 137,  432 => 136,  417 => 123,  415 => 122,  406 => 115,  401 => 112,  396 => 110,  392 => 109,  388 => 108,  384 => 106,  382 => 105,  379 => 104,  370 => 102,  367 => 101,  359 => 99,  351 => 97,  348 => 96,  345 => 95,  341 => 94,  336 => 93,  330 => 91,  324 => 89,  322 => 88,  318 => 87,  313 => 86,  311 => 85,  307 => 83,  303 => 81,  297 => 80,  294 => 79,  283 => 77,  277 => 75,  271 => 73,  268 => 72,  265 => 71,  262 => 70,  257 => 69,  254 => 68,  250 => 67,  247 => 66,  243 => 64,  237 => 63,  231 => 60,  228 => 59,  225 => 58,  219 => 57,  213 => 53,  206 => 51,  198 => 49,  190 => 47,  187 => 46,  183 => 45,  172 => 39,  167 => 38,  157 => 36,  155 => 35,  151 => 34,  147 => 32,  139 => 31,  131 => 29,  129 => 28,  126 => 27,  118 => 24,  113 => 23,  103 => 21,  101 => 20,  97 => 19,  93 => 17,  85 => 16,  77 => 14,  75 => 13,  72 => 12,  69 => 11,  64 => 10,  61 => 9,  57 => 8,  54 => 7,  52 => 6,  47 => 5,  45 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "basel/template/extension/quickcheckout/shipping_method.twig", "");
    }
}
