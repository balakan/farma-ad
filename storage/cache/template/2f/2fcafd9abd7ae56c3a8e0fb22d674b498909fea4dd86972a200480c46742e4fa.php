<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* basel/template/extension/module/basel_products.twig */
class __TwigTemplate_988eba37f01cd231f95bc9f7fdcc37834e166f50012cac192cee6ebb8f7f3cf0 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"widget module";
        echo ($context["module"] ?? null);
        echo " ";
        if ((($context["columns"] ?? null) != "list")) {
            echo " grid";
        }
        if (($context["contrast"] ?? null)) {
            echo " contrast-bg";
        }
        if ((($context["carousel"] ?? null) && (($context["rows"] ?? null) > 1))) {
            echo "  multiple-rows";
        }
        echo "\" ";
        if (($context["use_margin"] ?? null)) {
            echo "style=\"margin-bottom: ";
            echo ($context["margin"] ?? null);
            echo "\"";
        }
        echo ">
    ";
        // line 2
        if (($context["block_title"] ?? null)) {
            // line 3
            echo "        <!-- Block Title -->
        <div class=\"widget-title\">
            ";
            // line 5
            if (($context["title_preline"] ?? null)) {
                echo "<p class=\"pre-line\">";
                echo ($context["title_preline"] ?? null);
                echo "</p>";
            }
            // line 6
            echo "            ";
            if (($context["title"] ?? null)) {
                // line 7
                echo "                <p class=\"main-title\"><span>";
                echo ($context["title"] ?? null);
                echo "</span></p>
                <p class=\"widget-title-separator\"><i class=\"icon-line-cross\"></i></p>
            ";
            }
            // line 10
            echo "            ";
            if (($context["title_subline"] ?? null)) {
                // line 11
                echo "                <p class=\"sub-line\"><span>";
                echo ($context["title_subline"] ?? null);
                echo "</span></p>
            ";
            }
            // line 13
            echo "        </div>
    ";
        }
        // line 15
        echo "    ";
        if ((twig_length_filter($this->env, ($context["tabs"] ?? null)) > 1)) {
            // line 16
            echo "        <!-- Tabs -->
        <ul id=\"tabs-";
            // line 17
            echo ($context["module"] ?? null);
            echo "\" class=\"nav nav-tabs ";
            echo ($context["tabstyle"] ?? null);
            echo "\" data-tabs=\"tabs\" style=\"\">
            ";
            // line 18
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["tabs"] ?? null));
            foreach ($context['_seq'] as $context["keyTab"] => $context["tab"]) {
                // line 19
                echo "                ";
                if (($context["keyTab"] == 0)) {
                    // line 20
                    echo "                    <li class=\"active\"><a href=\"#tab";
                    echo ($context["module"] ?? null);
                    echo $context["keyTab"];
                    echo "\" data-toggle=\"tab\">";
                    echo twig_get_attribute($this->env, $this->source, $context["tab"], "title", [], "any", false, false, false, 20);
                    echo "</a></li>
                ";
                } else {
                    // line 22
                    echo "                    <li><a href=\"#tab";
                    echo ($context["module"] ?? null);
                    echo $context["keyTab"];
                    echo "\" data-toggle=\"tab\">";
                    echo twig_get_attribute($this->env, $this->source, $context["tab"], "title", [], "any", false, false, false, 22);
                    echo "</a></li>
                ";
                }
                // line 24
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['keyTab'], $context['tab'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 25
            echo "        </ul>
    ";
        }
        // line 27
        echo "    <div class=\"tab-content has-carousel ";
        if ( !($context["carousel"] ?? null)) {
            echo "overflow-hidden";
        }
        echo "\">
        <!-- Product Group(s) -->
        ";
        // line 29
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["tabs"] ?? null));
        foreach ($context['_seq'] as $context["key"] => $context["tab"]) {
            // line 30
            echo "            <div class=\"tab-pane";
            if (($context["key"] == 0)) {
                echo " active in";
            }
            echo " fade\" id=\"tab";
            echo ($context["module"] ?? null);
            echo $context["key"];
            echo "\">
                <div class=\"grid-holder grid";
            // line 31
            echo ($context["columns"] ?? null);
            echo " prod_module";
            echo ($context["module"] ?? null);
            if (($context["carousel"] ?? null)) {
                echo " carousel";
            }
            if ((($context["carousel_a"] ?? null) && (($context["rows"] ?? null) > 1))) {
                echo " sticky-arrows";
            }
            echo "\">
                    ";
            // line 32
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["tab"], "products", [], "any", false, false, false, 32));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 33
                echo "                        <div class=\"item single-product\">
                            <div class=\"image\"";
                // line 34
                if ((($context["columns"] ?? null) == "list")) {
                    echo " style=\"width:";
                    echo ($context["img_width"] ?? null);
                    echo "px\"";
                }
                echo ">
                                <a href=\"";
                // line 35
                echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 35);
                echo "\">
                                    <img src=\"";
                // line 36
                echo twig_get_attribute($this->env, $this->source, $context["product"], "thumb", [], "any", false, false, false, 36);
                echo "\" alt=\"";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 36);
                echo "\" title=\"";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 36);
                echo "\"/>
                                    ";
                // line 37
                if (twig_get_attribute($this->env, $this->source, $context["product"], "thumb2", [], "any", false, false, false, 37)) {
                    // line 38
                    echo "                                        <img class=\"thumb2\" src=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "thumb2", [], "any", false, false, false, 38);
                    echo "\" alt=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 38);
                    echo "\" title=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 38);
                    echo "\"/>
                                    ";
                }
                // line 40
                echo "                                </a>
                                ";
                // line 41
                if (((twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 41) && twig_get_attribute($this->env, $this->source, $context["product"], "special", [], "any", false, false, false, 41)) && twig_get_attribute($this->env, $this->source, $context["product"], "sale_badge", [], "any", false, false, false, 41))) {
                    // line 42
                    echo "                                    <div class=\"sale-counter id";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 42);
                    echo "\"></div>
                                    <span class=\"badge sale_badge\"><i>";
                    // line 43
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "sale_badge", [], "any", false, false, false, 43);
                    echo "</i></span>
                                ";
                }
                // line 45
                echo "                                ";
                if (twig_get_attribute($this->env, $this->source, $context["product"], "new_label", [], "any", false, false, false, 45)) {
                    // line 46
                    echo "                                    <span class=\"badge new_badge\"><i>";
                    echo ($context["basel_text_new"] ?? null);
                    echo "</i></span>
                                ";
                }
                // line 48
                echo "                                ";
                if (((twig_get_attribute($this->env, $this->source, $context["product"], "quantity", [], "any", false, false, false, 48) < 1) && ($context["stock_badge_status"] ?? null))) {
                    // line 49
                    echo "                                    <span class=\"badge out_of_stock_badge\"><i>";
                    echo ($context["basel_text_out_of_stock"] ?? null);
                    echo "</i></span>
                                    ";
                    // line 50
                    $context["button_cart"] = ($context["basel_text_out_of_stock"] ?? null);
                    // line 51
                    echo "                                ";
                } else {
                    // line 52
                    echo "                                    ";
                    $context["button_cart"] = ($context["default_button_cart"] ?? null);
                    // line 53
                    echo "                                ";
                }
                // line 54
                echo "                                <a class=\"img-overlay\" href=\"";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 54);
                echo "\"></a>
                                <div class=\"btn-center catalog_hide\"><a class=\"btn btn-light-outline btn-thin\" onclick=\"cart.add('";
                // line 55
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 55);
                echo "', '";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "minimum", [], "any", false, false, false, 55);
                echo "');\">";
                echo ($context["button_cart"] ?? null);
                echo "</a></div>
                                <div class=\"icons-wrapper\">
                                    <!-- <a class=\"icon is-cart catalog_hide\" data-toggle=\"tooltip\" data-placement=\"";
                // line 57
                echo ($context["tooltip_align"] ?? null);
                echo "\" data-title=\"";
                echo ($context["button_cart"] ?? null);
                echo "\" onclick=\"cart.add('";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 57);
                echo "', '";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "minimum", [], "any", false, false, false, 57);
                echo "');\"><span class=\"global-cart\"></span></a>-->
                                    <a class=\"icon is_wishlist\" data-toggle=\"tooltip\" data-placement=\"";
                // line 58
                echo ($context["tooltip_align"] ?? null);
                echo "\" data-title=\"";
                echo ($context["button_wishlist"] ?? null);
                echo "\" onclick=\"wishlist.add('";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 58);
                echo "');\"><span class=\"icon-heart\"></span></a>
                                    <a class=\"icon is_compare\" onclick=\"compare.add('";
                // line 59
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 59);
                echo "');\" data-toggle=\"tooltip\" data-placement=\"";
                echo ($context["tooltip_align"] ?? null);
                echo "\" data-title=\"";
                echo ($context["button_compare"] ?? null);
                echo "\"><span class=\"icon-refresh\"></span></a>
                                    <a class=\"icon is_quickview hidden-xs\" onclick=\"quickview('";
                // line 60
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 60);
                echo "');\" data-toggle=\"tooltip\" data-placement=\"";
                echo ($context["tooltip_align"] ?? null);
                echo "\" data-title=\"";
                echo ($context["basel_button_quickview"] ?? null);
                echo "\"><span class=\"icon-magnifier-add\"></span></a>
                                </div> <!-- .icons-wrapper -->
                            </div><!-- .image ends -->
                            <div class=\"caption\">
                                <a class=\"product-name\" href=\"";
                // line 64
                echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 64);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 64);
                echo "</a>
                                ";
                // line 65
                if (twig_get_attribute($this->env, $this->source, $context["product"], "rating", [], "any", false, false, false, 65)) {
                    // line 66
                    echo "                                    <div class=\"rating\">
                                        <span class=\"rating_stars rating r";
                    // line 67
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "rating", [], "any", false, false, false, 67);
                    echo "\">
                                        <i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>
                                        </span>
                                    </div>
                                ";
                }
                // line 72
                echo "

                                <div class=\"price-wrapper\">
                                    <div class=\"row\">
                                        <div class=\"col-lg-4\">
                                            ";
                // line 77
                if (twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 77)) {
                    // line 78
                    echo "
                                                <div class=\"price\">

                                                    ";
                    // line 81
                    if (twig_get_attribute($this->env, $this->source, $context["product"], "special", [], "any", false, false, false, 81)) {
                        // line 82
                        echo "                                                        <span class=\"price-old price\">";
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 82);
                        echo "</span>
                                                        <span class=\"price-new\">";
                        // line 83
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "special", [], "any", false, false, false, 83);
                        echo "</span>
                                                    ";
                    } else {
                        // line 85
                        echo "                                                        <span class=\"price\">Cijena</span>
                                                        <span>";
                        // line 86
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 86);
                        echo "</span>
                                                    ";
                    }
                    // line 88
                    echo "                                                    ";
                    if (twig_get_attribute($this->env, $this->source, $context["product"], "tax", [], "any", false, false, false, 88)) {
                        // line 89
                        echo "                                                        <span class=\"price-tax\">";
                        echo ($context["text_tax"] ?? null);
                        echo " ";
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "tax", [], "any", false, false, false, 89);
                        echo "</span>
                                                    ";
                    }
                    // line 91
                    echo "                                                </div><!-- .price -->
                                            ";
                }
                // line 93
                echo "                                        </div>

                                        <div class=\"col-lg-8\">

                                            <div class=\"input-group\" rel=\"basel-";
                // line 97
                echo (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = twig_get_attribute($this->env, $this->source, $context["product"], "scale", [], "any", false, false, false, 97)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["scale"] ?? null) : null);
                echo "\">
                                                <select name=\"";
                // line 98
                echo $context["key"];
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 98);
                echo "\" class=\"form-control num";
                echo $context["key"];
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 98);
                echo "\">
                                                    <option value=\"1\">1</option>
                                                    <option value=\"2\">2</option>
                                                    <option value=\"3\">3</option>
                                                    <option value=\"4\">4</option>
                                                    <option value=\"5\">5</option>
                                                    <option value=\"+\">+</option>
                                                </select>
                                                <span class=\"input-group-btn\">
                                                    <button class=\"btn btn-neutral btn-green\" onclick=\"cart.add('";
                // line 107
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 107);
                echo "', \$(this).parent().parent().find('.num";
                echo $context["key"];
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 107);
                echo "').val());\">
                                                        <span class=\"global-cart\"></span>
                                                    </button>
                                                </span>

                                            </div><!-- /input-group -->

                                            <script>
                                                \$('[name=\"";
                // line 115
                echo $context["key"];
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 115);
                echo "\"]').otherDropdown({classes: 'form-control', value: '+', placeholder: 'Upiši'})
                                            </script>

                                        </div>
                                    </div>

                                </div><!-- .price-wrapper -->


                                <div class=\"plain-links\">
                                    <a class=\"icon is_wishlist link-hover-color\" onclick=\"wishlist.add('";
                // line 125
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 125);
                echo "');\"><span class=\"icon-heart\"></span> ";
                echo ($context["button_wishlist"] ?? null);
                echo "</a>
                                    <a class=\"icon is_compare link-hover-color\" onclick=\"compare.add('";
                // line 126
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 126);
                echo "');\"><span class=\"icon-refresh\"></span> ";
                echo ($context["button_compare"] ?? null);
                echo "</a>
                                    <a class=\"icon is_quickview link-hover-color\" onclick=\"quickview('";
                // line 127
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 127);
                echo "');\"><span class=\"icon-magnifier-add\"></span> ";
                echo ($context["basel_button_quickview"] ?? null);
                echo "</a>
                                </div><!-- .plain-links-->
                            </div><!-- .caption-->
                            ";
                // line 130
                if ((twig_get_attribute($this->env, $this->source, $context["product"], "sale_end_date", [], "any", false, false, false, 130) && ($context["countdown_status"] ?? null))) {
                    // line 131
                    echo "                                <script>
                                    \$(function () {
                                        \$(\".module";
                    // line 133
                    echo ($context["module"] ?? null);
                    echo " .sale-counter.id";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 133);
                    echo "\").countdown(\"";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "sale_end_date", [], "any", false, false, false, 133);
                    echo "\").on('update.countdown', function (event) {
                                            var \$this = \$(this).html(event.strftime(''
                                                + '<div>'
                                                + '%D<i>";
                    // line 136
                    echo ($context["basel_text_days"] ?? null);
                    echo "</i></div><div>'
                                                + '%H <i>";
                    // line 137
                    echo ($context["basel_text_hours"] ?? null);
                    echo "</i></div><div>'
                                                + '%M <i>";
                    // line 138
                    echo ($context["basel_text_mins"] ?? null);
                    echo "</i></div><div>'
                                                + '%S <i>";
                    // line 139
                    echo ($context["basel_text_secs"] ?? null);
                    echo "</i></div></div>'));
                                        });
                                    });
                                </script>
                            ";
                }
                // line 144
                echo "                        </div><!-- .single-product ends -->
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 146
            echo "                </div>
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['tab'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 149
        echo "        ";
        if (($context["use_button"] ?? null)) {
            // line 150
            echo "            <!-- Button -->
            <div class=\"widget_bottom_btn ";
            // line 151
            if ((($context["carousel"] ?? null) && ($context["carousel_b"] ?? null))) {
                echo "has-dots";
            }
            echo "\">
                <a class=\"btn btn-contrast\" href=\"";
            // line 152
            echo ((($context["link_href"] ?? null)) ? (($context["link_href"] ?? null)) : (""));
            echo "\">";
            echo ($context["link_title"] ?? null);
            echo "</a>
            </div>
        ";
        }
        // line 155
        echo "    </div>
    <div class=\"clearfix\"></div>
</div>
";
        // line 158
        if (($context["carousel"] ?? null)) {
            // line 159
            echo "    <script>
        \$('.grid-holder.prod_module";
            // line 160
            echo ($context["module"] ?? null);
            echo "').slick({
            ";
            // line 161
            if (($context["carousel_a"] ?? null)) {
                // line 162
                echo "            prevArrow:    \"<a class=\\\"arrow-left icon-arrow-left\\\"></a>\",
            nextArrow:    \"<a class=\\\"arrow-right icon-arrow-right\\\"></a>\",
            ";
            } else {
                // line 165
                echo "            arrows:       false,
            ";
            }
            // line 167
            echo "            ";
            if ((($context["direction"] ?? null) == "rtl")) {
                // line 168
                echo "            rtl:          true,
            ";
            }
            // line 170
            echo "            ";
            if (($context["carousel_b"] ?? null)) {
                // line 171
                echo "            dots:         true,
            ";
            }
            // line 173
            echo "            respondTo:    'min',
            rows:";
            // line 174
            echo ($context["rows"] ?? null);
            echo ",
            ";
            // line 175
            if ((($context["columns"] ?? null) == "5")) {
                // line 176
                echo "            slidesToShow: 5, slidesToScroll: 5, responsive: [{breakpoint: 1100, settings: {slidesToShow: 4, slidesToScroll: 4}}, {breakpoint: 960, settings: {slidesToShow: 3, slidesToScroll: 3}}, {breakpoint: 600, settings: {slidesToShow: 2, slidesToScroll: 2}},
                ";
            } elseif ((            // line 177
($context["columns"] ?? null) == "4")) {
                // line 178
                echo "                slidesToShow
        :
        4, slidesToScroll
        :
        4, responsive
        :
        [{breakpoint: 960, settings: {slidesToShow: 3, slidesToScroll: 3}}, {breakpoint: 600, settings: {slidesToShow: 2, slidesToScroll: 2}},
            ";
            } elseif ((            // line 185
($context["columns"] ?? null) == "3")) {
                // line 186
                echo "            slidesToShow
        :
        3, slidesToScroll
        :
        3, responsive
        :
        [{breakpoint: 600, settings: {slidesToShow: 2, slidesToScroll: 2}},
            ";
            } elseif ((            // line 193
($context["columns"] ?? null) == "2")) {
                // line 194
                echo "            slidesToShow
        :
        2, slidesToScroll
        :
        2, responsive
        :
        [
            ";
            } elseif (((            // line 201
($context["columns"] ?? null) == "1") || (($context["columns"] ?? null) == "list"))) {
                // line 202
                echo "            adaptiveHeight
        :
        true, slidesToShow
        :
        1, slidesToScroll
        :
        1, responsive
        :
        [
            ";
            }
            // line 212
            echo "            ";
            if (($context["items_mobile_fw"] ?? null)) {
                // line 213
                echo "            {breakpoint: 420, settings: {slidesToShow: 1, slidesToScroll: 1}}
            ";
            }
            // line 215
            echo "        ]
        })
        ;
        \$('.product-style2 .single-product .icon').attr('data-placement', 'top');
        \$('[data-toggle=\\'tooltip\\']').tooltip({container: 'body'});
        ";
            // line 220
            if ((($context["carousel_a"] ?? null) && (($context["rows"] ?? null) > 1))) {
                // line 221
                echo "        \$(window).load(function () {
            var p_c_o = \$('.prod_module";
                // line 222
                echo ($context["module"] ?? null);
                echo "').offset().top;
            var p_c_o_b = \$('.prod_module";
                // line 223
                echo ($context["module"] ?? null);
                echo "').offset().top + \$('.prod_module";
                echo ($context["module"] ?? null);
                echo "').outerHeight(true) - 100;
            var p_sticky_arrows = function () {
                var p_m_o = \$(window).scrollTop() + (\$(window).height() / 2);
                if (p_m_o > p_c_o && p_m_o < p_c_o_b) {
                    \$('.prod_module";
                // line 227
                echo ($context["module"] ?? null);
                echo " .slick-arrow').addClass('visible').css('top', p_m_o - p_c_o + 'px');
                } else {
                    \$('.prod_module";
                // line 229
                echo ($context["module"] ?? null);
                echo " .slick-arrow').removeClass('visible');
                }
            };
            \$(window).scroll(function () {
                p_sticky_arrows();
            });
        });
        ";
            }
            // line 237
            echo "    </script>
";
        }
    }

    public function getTemplateName()
    {
        return "basel/template/extension/module/basel_products.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  666 => 237,  655 => 229,  650 => 227,  641 => 223,  637 => 222,  634 => 221,  632 => 220,  625 => 215,  621 => 213,  618 => 212,  606 => 202,  604 => 201,  595 => 194,  593 => 193,  584 => 186,  582 => 185,  573 => 178,  571 => 177,  568 => 176,  566 => 175,  562 => 174,  559 => 173,  555 => 171,  552 => 170,  548 => 168,  545 => 167,  541 => 165,  536 => 162,  534 => 161,  530 => 160,  527 => 159,  525 => 158,  520 => 155,  512 => 152,  506 => 151,  503 => 150,  500 => 149,  492 => 146,  485 => 144,  477 => 139,  473 => 138,  469 => 137,  465 => 136,  455 => 133,  451 => 131,  449 => 130,  441 => 127,  435 => 126,  429 => 125,  415 => 115,  401 => 107,  385 => 98,  381 => 97,  375 => 93,  371 => 91,  363 => 89,  360 => 88,  355 => 86,  352 => 85,  347 => 83,  342 => 82,  340 => 81,  335 => 78,  333 => 77,  326 => 72,  318 => 67,  315 => 66,  313 => 65,  307 => 64,  296 => 60,  288 => 59,  280 => 58,  270 => 57,  261 => 55,  256 => 54,  253 => 53,  250 => 52,  247 => 51,  245 => 50,  240 => 49,  237 => 48,  231 => 46,  228 => 45,  223 => 43,  218 => 42,  216 => 41,  213 => 40,  203 => 38,  201 => 37,  193 => 36,  189 => 35,  181 => 34,  178 => 33,  174 => 32,  162 => 31,  152 => 30,  148 => 29,  140 => 27,  136 => 25,  130 => 24,  121 => 22,  112 => 20,  109 => 19,  105 => 18,  99 => 17,  96 => 16,  93 => 15,  89 => 13,  83 => 11,  80 => 10,  73 => 7,  70 => 6,  64 => 5,  60 => 3,  58 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "basel/template/extension/module/basel_products.twig", "");
    }
}
