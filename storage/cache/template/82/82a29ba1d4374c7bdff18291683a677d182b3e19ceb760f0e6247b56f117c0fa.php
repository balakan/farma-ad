<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* basel/template/common/headers/header6.twig */
class __TwigTemplate_d7b7eb4d5835723b0564020ff2ea0082626ac67981434718bf60ca514fee51e1 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"header-wrapper header6\">

";
        // line 3
        if (($context["top_line_style"] ?? null)) {
            // line 4
            echo "<div class=\"top_line\">
  <div class=\"container ";
            // line 5
            echo ($context["top_line_width"] ?? null);
            echo "\">
  \t<div class=\"table\">
        <div class=\"table-cell left sm-text-center xs-text-center\">
            <div class=\"promo-message\">";
            // line 8
            echo ($context["promo_message"] ?? null);
            echo "</div>
        </div>
        <div class=\"table-cell text-right hidden-xs hidden-sm\">
            <div class=\"links\">
            <ul>
            ";
            // line 13
            $this->loadTemplate("basel/template/common/static_links.twig", "basel/template/common/headers/header6.twig", 13)->display($context);
            // line 14
            echo "            </ul>
            ";
            // line 15
            if (($context["lang_curr_title"] ?? null)) {
                // line 16
                echo "            <div class=\"setting-ul\">
            <div class=\"setting-li dropdown-wrapper from-left lang-curr-trigger nowrap\"><a>
            <span>";
                // line 18
                echo ($context["lang_curr_title"] ?? null);
                echo "</span>
            </a>
            <div class=\"dropdown-content dropdown-right lang-curr-wrapper\">
            ";
                // line 21
                echo ($context["language"] ?? null);
                echo "
            ";
                // line 22
                echo ($context["currency"] ?? null);
                echo "
            </div>
            </div>
            </div>
            ";
            }
            // line 27
            echo "            </div>
        </div>
    </div> <!-- .table ends -->
  </div> <!-- .container ends -->
</div> <!-- .top_line ends -->
";
        }
        // line 33
        echo "
<span class=\"table header-main sticky-header-placeholder slidedown \">&nbsp;</span>
<div class=\" outer-container header-style sticky-header\">
  <div class=\"container ";
        // line 36
        echo ($context["main_header_width"] ?? null);
        echo "\">
    <div class=\"table header-main ";
        // line 37
        echo ($context["main_menu_align"] ?? null);
        echo "\">
    
    <div class=\"table-cell text-left w20 logo\">
    \t";
        // line 40
        if (($context["logo"] ?? null)) {
            // line 41
            echo "        <div id=\"logo\">
    \t<a href=\"";
            // line 42
            echo ($context["home"] ?? null);
            echo "\"><img src=\"image/logo-kao-nekad.svg\" title=\"";
            echo ($context["name"] ?? null);
            echo "\" alt=\"";
            echo ($context["name"] ?? null);
            echo "\" /></a>
        </div>
    \t";
        }
        // line 45
        echo "    </div>
    
    ";
        // line 47
        if (($context["primary_menu"] ?? null)) {
            // line 48
            echo "    <div class=\"table-cell text-center w60 menu-cell hidden-xs hidden-sm\">
        <div class=\"main-menu\">
            <ul class=\"categories\">
              ";
            // line 51
            if ((($context["primary_menu"] ?? null) == "oc")) {
                // line 52
                echo "                <!-- Default menu -->
                ";
                // line 53
                echo ($context["default_menu"] ?? null);
                echo "
              ";
            } elseif (            // line 54
array_key_exists("primary_menu", $context)) {
                echo " 
                <!-- Mega menu -->
                ";
                // line 56
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["primary_menu_desktop"] ?? null));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["key"] => $context["row"]) {
                    // line 57
                    echo "                ";
                    $this->loadTemplate("basel/template/common/menus/mega_menu.twig", "basel/template/common/headers/header6.twig", 57)->display($context);
                    // line 58
                    echo "                ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['row'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 59
                echo "              ";
            }
            // line 60
            echo "            </ul>
        </div>
    </div>
    ";
        }
        // line 64
        echo "    
    <div class=\"table-cell w20 shortcuts text-right\">
       
       <div class=\"font-zero\">
           <div class=\"icon-element is_wishlist\">

               ";
        // line 70
        if (($context["logged"] ?? null)) {
            // line 71
            echo "                   <a class=\"shortcut-wrapper wishlist \" title=\"Korisnički račun\" href=\"";
            echo ($context["login"] ?? null);
            echo "\"><i class=\"icon-user icon\"></i></a><a class=\"shortcut-wrapper wishlist\" title=\"Odjava\" href=\"";
            echo ($context["logout"] ?? null);
            echo "\"> Odjava  <div class=\"wishlist-hover\"></div>
               </a>
               ";
        } else {
            // line 74
            echo "                   <a class=\"shortcut-wrapper wishlist\" title=\"Korisnički račun\" href=\"";
            echo ($context["login"] ?? null);
            echo "\">
                       <div class=\"wishlist-hover\"><i class=\"icon-user icon\"></i></div>
                   </a>
               ";
        }
        // line 78
        echo "           </div>


           <div class=\"icon-element is_wishlist hidden-xs\">
        <a class=\"shortcut-wrapper wishlist\" href=\"";
        // line 82
        echo ($context["wishlist"] ?? null);
        echo "\">
        <div class=\"wishlist-hover\"><i class=\"icon-heart icon\"></i><span class=\"counter wishlist-counter\">";
        // line 83
        echo ($context["wishlist_counter"] ?? null);
        echo "</span></div>
        </a>
        </div>
        
        <div class=\"icon-element catalog_hide\">
        <div id=\"cart\" class=\"dropdown-wrapper from-top\">
        <a href=\"";
        // line 89
        echo ($context["shopping_cart"] ?? null);
        echo "\" class=\"shortcut-wrapper cart\">
        <i id=\"cart-icon\" class=\"global-cart icon\"></i> <span id=\"cart-total\" class=\"nowrap\">
        <span class=\"counter cart-total-items\">";
        // line 91
        echo ($context["cart_items"] ?? null);
        echo "</span>
        </a>
        <div class=\"dropdown-content dropdown-right hidden-sm hidden-xs\">
        ";
        // line 94
        echo ($context["cart"] ?? null);
        echo "
        </div>
\t\t</div>
        </div>
        
        <div class=\"icon-element\">
        <a class=\"shortcut-wrapper menu-trigger hidden-md hidden-lg\">
        <i class=\"icon-line-menu icon\"></i>
        </a>
        </div>
        
       </div>
        
    </div>
    
    </div> <!-- .table.header_main ends -->
  </div> <!-- .container ends -->
</div> <!-- .sticky ends -->

<div class=\"slidedown outer-container header-bottom hidden-xs hidden-sm sticky-header\">
<div class=\"container ";
        // line 114
        echo ($context["main_header_width"] ?? null);
        echo "\">
\t<div class=\"table\">
    \t";
        // line 116
        if (($context["secondary_menu"] ?? null)) {
            // line 117
            echo "    \t<div class=\"table-cell w225\">
        <div class=\"main-menu vertical vertical-menu-bg dropdown-wrapper from-bottom\">
        <h4 class=\"menu-heading\"><b>";
            // line 119
            echo ($context["basel_text_category"] ?? null);
            echo "<i class=\"fa fa-angle-down\"></i></b></h4>
            <ul class=\"categories vertical-menu-bg dropdown-content\">
              ";
            // line 121
            if ((($context["secondary_menu"] ?? null) == "oc")) {
                // line 122
                echo "                <!-- Default menu -->
                ";
                // line 123
                echo ($context["default_menu"] ?? null);
                echo "
              ";
            } elseif (            // line 124
array_key_exists("secondary_menu", $context)) {
                echo " 
                <!-- Mega menu -->
                ";
                // line 126
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["secondary_menu_desktop"] ?? null));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["key"] => $context["row"]) {
                    // line 127
                    echo "                ";
                    $this->loadTemplate("basel/template/common/menus/mega_menu.twig", "basel/template/common/headers/header6.twig", 127)->display($context);
                    // line 128
                    echo "                ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['row'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 129
                echo "              ";
            }
            // line 130
            echo "            </ul>
        </div>
        </div>
        ";
        }
        // line 134
        echo "        
        <div class=\"table-cell w55 search-cell ";
        // line 135
        echo ($context["basel_search_scheme"] ?? null);
        echo "\">
         ";
        // line 136
        echo ($context["basel_search"] ?? null);
        echo "
        </div>

        <div class=\"table-cell w225 search-cell\">
            <div class=\"support\">
            <a href=\"tel:+385\" class=\"contact-tel\">0800 111 111</a>
            <div class=\"radno-vrijeme\">Pon - Pet od 8 do 16h </div>
            </div>
        </div>
    </div>
</div><!-- .container ends -->
</div>

</div> <!-- .header_wrapper ends -->";
    }

    public function getTemplateName()
    {
        return "basel/template/common/headers/header6.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  355 => 136,  351 => 135,  348 => 134,  342 => 130,  339 => 129,  325 => 128,  322 => 127,  305 => 126,  300 => 124,  296 => 123,  293 => 122,  291 => 121,  286 => 119,  282 => 117,  280 => 116,  275 => 114,  252 => 94,  246 => 91,  241 => 89,  232 => 83,  228 => 82,  222 => 78,  214 => 74,  205 => 71,  203 => 70,  195 => 64,  189 => 60,  186 => 59,  172 => 58,  169 => 57,  152 => 56,  147 => 54,  143 => 53,  140 => 52,  138 => 51,  133 => 48,  131 => 47,  127 => 45,  117 => 42,  114 => 41,  112 => 40,  106 => 37,  102 => 36,  97 => 33,  89 => 27,  81 => 22,  77 => 21,  71 => 18,  67 => 16,  65 => 15,  62 => 14,  60 => 13,  52 => 8,  46 => 5,  43 => 4,  41 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "basel/template/common/headers/header6.twig", "/Users/alive/Sites/Agmedia/Live/kaonekad/upload/catalog/view/theme/basel/template/common/headers/header6.twig");
    }
}
