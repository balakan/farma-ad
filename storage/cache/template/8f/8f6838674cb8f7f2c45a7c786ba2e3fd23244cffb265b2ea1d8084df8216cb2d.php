<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* basel/template/common/header.twig */
class __TwigTemplate_cc65980e5784c0789c8e43e99ec1ef4ce2c16b0aeb9ab84c7fa3f6582843d8da extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir=\"";
        // line 3
        echo ($context["direction"] ?? null);
        echo "\" lang=\"";
        echo ($context["lang"] ?? null);
        echo "\" class=\"ie8\"><![endif]-->
<!--[if IE 9 ]><html dir=\"";
        // line 4
        echo ($context["direction"] ?? null);
        echo "\" lang=\"";
        echo ($context["lang"] ?? null);
        echo "\" class=\"ie9\"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir=\"";
        // line 6
        echo ($context["direction"] ?? null);
        echo "\" lang=\"";
        echo ($context["lang"] ?? null);
        echo "\">
<!--<![endif]-->
<head>
<meta charset=\"UTF-8\" />
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
<title>";
        // line 12
        echo ($context["title"] ?? null);
        echo "</title>
<base href=\"";
        // line 13
        echo ($context["base"] ?? null);
        echo "\" />
";
        // line 14
        if (($context["description"] ?? null)) {
            echo "<meta name=\"description\" content=\"";
            echo ($context["description"] ?? null);
            echo "\" />";
        }
        // line 15
        if (($context["keywords"] ?? null)) {
            echo "<meta name=\"keywords\" content= \"";
            echo ($context["keywords"] ?? null);
            echo "\" />";
        }
        // line 16
        echo "<!-- Load essential resources -->
<script src=\"catalog/view/javascript/jquery/jquery-2.1.1.min.js\"></script>
<link href=\"catalog/view/javascript/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" media=\"screen\" />
<script src=\"catalog/view/javascript/bootstrap/js/bootstrap.min.js\"></script>
<script src=\"catalog/view/theme/basel/js/slick.min.js\"></script>
  <script type=\"text/javascript\" src=\"catalog/view/theme/basel/js/jquery.otherdropdown.js\"></script>
<script src=\"catalog/view/theme/basel/js/basel_common.js\"></script>
<!-- Main stylesheet -->
<link href=\"catalog/view/theme/basel/stylesheet/stylesheet.css\" rel=\"stylesheet\">
<!-- Mandatory Theme Settings CSS -->
<style id=\"basel-mandatory-css\">";
        // line 26
        echo ($context["basel_mandatory_css"] ?? null);
        echo "</style>
<!-- Plugin Stylesheet(s) -->
";
        // line 28
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["styles"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["style"]) {
            // line 29
            echo "<link href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["style"], "href", [], "any", false, false, false, 29);
            echo "\" rel=\"";
            echo twig_get_attribute($this->env, $this->source, $context["style"], "rel", [], "any", false, false, false, 29);
            echo "\" media=\"";
            echo twig_get_attribute($this->env, $this->source, $context["style"], "media", [], "any", false, false, false, 29);
            echo "\" />
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['style'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        if ((($context["direction"] ?? null) == "rtl")) {
            // line 32
            echo "<link href=\"catalog/view/theme/basel/stylesheet/rtl.css\" rel=\"stylesheet\">
";
        }
        // line 34
        echo "<!-- Pluing scripts(s) -->
";
        // line 35
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["scripts"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["script"]) {
            // line 36
            echo "<script src=\"";
            echo $context["script"];
            echo "\"></script>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['script'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "<!-- Page specific meta information -->
";
        // line 39
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["links"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["link"]) {
            // line 40
            if ((twig_get_attribute($this->env, $this->source, $context["link"], "rel", [], "any", false, false, false, 40) == "image")) {
                // line 41
                echo "<meta property=\"og:image\" content=\"";
                echo twig_get_attribute($this->env, $this->source, $context["link"], "href", [], "any", false, false, false, 41);
                echo "\" />
";
            } else {
                // line 43
                echo "<link href=\"";
                echo twig_get_attribute($this->env, $this->source, $context["link"], "href", [], "any", false, false, false, 43);
                echo "\" rel=\"";
                echo twig_get_attribute($this->env, $this->source, $context["link"], "rel", [], "any", false, false, false, 43);
                echo "\" />
";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['link'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 46
        echo "<!-- Analytic tools -->
";
        // line 47
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["analytics"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["analytic"]) {
            // line 48
            echo $context["analytic"];
            echo "
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['analytic'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 50
        if (($context["basel_styles_status"] ?? null)) {
            // line 51
            echo "<!-- Custom Color Scheme -->
<style id=\"basel-color-scheme\">";
            // line 52
            echo ($context["basel_styles_cache"] ?? null);
            echo ";</style>
";
        }
        // line 54
        if (($context["basel_typo_status"] ?? null)) {
            // line 55
            echo "<!-- Custom Fonts -->
<style id=\"basel-fonts\">";
            // line 56
            echo ($context["basel_fonts_cache"] ?? null);
            echo "</style>
";
        }
        // line 58
        if (($context["basel_custom_css_status"] ?? null)) {
            // line 59
            echo "<!-- Custom CSS -->
<style id=\"basel-custom-css\">
";
            // line 61
            echo ($context["basel_custom_css"] ?? null);
            echo "
</style>
";
        }
        // line 64
        if (($context["basel_custom_js_status"] ?? null)) {
            // line 65
            echo "<!-- Custom Javascript -->
<script>
";
            // line 67
            echo ($context["basel_custom_js"] ?? null);
            echo "
</script>
";
        }
        // line 70
        echo "</head>
<body class=\"";
        // line 71
        echo ($context["class"] ?? null);
        echo ($context["basel_body_class"] ?? null);
        echo "\">
";
        // line 72
        $this->loadTemplate("basel/template/common/mobile-nav.twig", "basel/template/common/header.twig", 72)->display($context);
        // line 73
        echo "<div class=\"outer-container main-wrapper\">
";
        // line 74
        if (($context["notification_status"] ?? null)) {
            // line 75
            echo "<div class=\"top_notificaiton\">
  <div class=\"container";
            // line 76
            if (($context["top_promo_close"] ?? null)) {
                echo " has-close";
            }
            echo " ";
            echo ($context["top_promo_width"] ?? null);
            echo " ";
            echo ($context["top_promo_align"] ?? null);
            echo "\">
    <div class=\"table\">
    <div class=\"table-cell w100\"><div class=\"ellipsis-wrap\">";
            // line 78
            echo ($context["top_promo_text"] ?? null);
            echo "</div></div>
    ";
            // line 79
            if (($context["top_promo_close"] ?? null)) {
                // line 80
                echo "    <div class=\"table-cell text-right\">
    <a onClick=\"addCookie('basel_top_promo', 1, 30);\$(this).closest('.top_notificaiton').slideUp();\" class=\"top_promo_close\">&times;</a>
    </div>
    ";
            }
            // line 84
            echo "    </div>
  </div>
</div>
";
        }
        // line 88
        $this->loadTemplate((("basel/template/common/headers/" . ($context["basel_header"] ?? null)) . ".twig"), "basel/template/common/header.twig", 88)->display($context);
        // line 89
        echo "<!-- breadcrumb -->
<div class=\"breadcrumb-holder\">
<div class=\"container\">
<span id=\"title-holder\">&nbsp;</span>
<div class=\"links-holder\">
<a class=\"basel-back-btn\" onClick=\"history.go(-1); return false;\"><i></i></a><span>&nbsp;</span>
</div>
</div>
</div>
<div class=\"container\">
";
        // line 99
        echo ($context["position_top"] ?? null);
        echo "
</div>";
    }

    public function getTemplateName()
    {
        return "basel/template/common/header.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  288 => 99,  276 => 89,  274 => 88,  268 => 84,  262 => 80,  260 => 79,  256 => 78,  245 => 76,  242 => 75,  240 => 74,  237 => 73,  235 => 72,  230 => 71,  227 => 70,  221 => 67,  217 => 65,  215 => 64,  209 => 61,  205 => 59,  203 => 58,  198 => 56,  195 => 55,  193 => 54,  188 => 52,  185 => 51,  183 => 50,  175 => 48,  171 => 47,  168 => 46,  156 => 43,  150 => 41,  148 => 40,  144 => 39,  141 => 38,  132 => 36,  128 => 35,  125 => 34,  121 => 32,  119 => 31,  106 => 29,  102 => 28,  97 => 26,  85 => 16,  79 => 15,  73 => 14,  69 => 13,  65 => 12,  54 => 6,  47 => 4,  41 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "basel/template/common/header.twig", "");
    }
}
