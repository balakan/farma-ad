<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* basel/template/product/product.twig */
class __TwigTemplate_7616e37c6a6d0e6cc2fee49e30ecb5db89eb7da6391bc33dd25ff1ebd3889f43 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo "

";
        // line 3
        if ((($context["product_layout"] ?? null) != "full-width")) {
            // line 4
            echo "    <style>
        .product-page .image-area {
        ";
            // line 6
            if (((($context["product_layout"] ?? null) == "images-left") && ($context["images"] ?? null))) {
                // line 7
                echo "            width: ";
                echo ((($context["img_w"] ?? null) + ($context["img_a_w"] ?? null)) + 20);
                echo "px;
        ";
            } else {
                // line 9
                echo "            width: ";
                echo ($context["img_w"] ?? null);
                echo "px;
        ";
            }
            // line 11
            echo "        }
        .product-page .main-image {
            width:";
            // line 13
            echo ($context["img_w"] ?? null);
            echo "px;
        }
        .product-page .image-additional {
        ";
            // line 16
            if ((($context["product_layout"] ?? null) == "images-left")) {
                // line 17
                echo "            width: ";
                echo ($context["img_a_w"] ?? null);
                echo "px;
            height: ";
                // line 18
                echo ($context["img_h"] ?? null);
                echo "px;
        ";
            } else {
                // line 20
                echo "            width: ";
                echo ($context["img_w"] ?? null);
                echo "px;
        ";
            }
            // line 22
            echo "        }
        .product-page .image-additional.has-arrows {
        ";
            // line 24
            if ((($context["product_layout"] ?? null) == "images-left")) {
                // line 25
                echo "            height: ";
                echo (($context["img_h"] ?? null) - 40);
                echo "px;
        ";
            }
            // line 27
            echo "        }
        @media (min-width: 992px) and (max-width: 1199px) {
            .product-page .image-area {
            ";
            // line 30
            if ((($context["product_layout"] ?? null) == "images-left")) {
                // line 31
                echo "                width: ";
                echo (((($context["img_w"] ?? null) + ($context["img_a_w"] ?? null)) / 1.25) + 20);
                echo "px;
            ";
            } else {
                // line 33
                echo "                width: ";
                echo (($context["img_w"] ?? null) / 1.25);
                echo "px;
            ";
            }
            // line 35
            echo "            }
            .product-page .main-image {
                width:";
            // line 37
            echo (($context["img_w"] ?? null) / 1.25);
            echo "px;
            }
            .product-page .image-additional {
            ";
            // line 40
            if ((($context["product_layout"] ?? null) == "images-left")) {
                // line 41
                echo "                width: ";
                echo (($context["img_a_w"] ?? null) / 1.25);
                echo "px;
                height: ";
                // line 42
                echo (($context["img_h"] ?? null) / 1.25);
                echo "px;
            ";
            } else {
                // line 44
                echo "                width: ";
                echo (($context["img_w"] ?? null) / 1.25);
                echo "px;
            ";
            }
            // line 46
            echo "            }
        }
    </style>
";
        }
        // line 50
        echo "
<ul class=\"breadcrumb\">
    ";
        // line 52
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 53
            echo "        <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 53);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 53);
            echo "</a></li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "</ul>

<div class=\"container product-layout ";
        // line 57
        echo ($context["product_layout"] ?? null);
        echo "\">

    <div class=\"row\">";
        // line 59
        echo ($context["column_left"] ?? null);
        echo "
        ";
        // line 60
        if ((($context["column_left"] ?? null) && ($context["column_right"] ?? null))) {
            // line 61
            echo "            ";
            $context["class"] = "col-sm-6";
            // line 62
            echo "        ";
        } elseif ((($context["column_left"] ?? null) || ($context["column_right"] ?? null))) {
            // line 63
            echo "            ";
            $context["class"] = "col-md-9 col-sm-8";
            // line 64
            echo "        ";
        } else {
            // line 65
            echo "            ";
            $context["class"] = "col-sm-12";
            // line 66
            echo "        ";
        }
        // line 67
        echo "        <div id=\"content\" class=\"product-main no-min-height ";
        echo ($context["class"] ?? null);
        echo "\">
            ";
        // line 68
        echo ($context["content_top"] ?? null);
        echo "

            <div class=\"table product-info product-page\">

                <div class=\"table-cell left\">

                    ";
        // line 74
        if ((($context["thumb"] ?? null) || ($context["images"] ?? null))) {
            // line 75
            echo "                    <div class=\"image-area ";
            if ( !($context["hover_zoom"] ?? null)) {
                echo "hover-zoom-disabled";
            }
            echo "\" id=\"gallery\">

                        ";
            // line 77
            if (($context["thumb"] ?? null)) {
                // line 78
                echo "                            <div class=\"main-image\">

                                ";
                // line 80
                if (((($context["price"] ?? null) && ($context["special"] ?? null)) && ($context["sale_badge"] ?? null))) {
                    // line 81
                    echo "                                    <span class=\"badge sale_badge\"><i>";
                    echo ($context["sale_badge"] ?? null);
                    echo "</i></span>
                                ";
                }
                // line 83
                echo "
                                ";
                // line 84
                if (($context["is_new"] ?? null)) {
                    // line 85
                    echo "                                    <span class=\"badge new_badge\"><i>";
                    echo ($context["basel_text_new"] ?? null);
                    echo "</i></span>
                                ";
                }
                // line 87
                echo "
                                ";
                // line 88
                if (((($context["qty"] ?? null) < 1) && ($context["stock_badge_status"] ?? null))) {
                    // line 89
                    echo "                                    <span class=\"badge out_of_stock_badge\"><i>";
                    echo ($context["basel_text_out_of_stock"] ?? null);
                    echo "</i></span>
                                ";
                }
                // line 91
                echo "
                                <a class=\"";
                // line 92
                if ( !($context["images"] ?? null)) {
                    echo "link cloud-zoom";
                }
                echo " ";
                if ((($context["product_layout"] ?? null) == "full-width")) {
                    echo "link";
                } else {
                    echo "cloud-zoom";
                }
                echo "\" id=\"main-image\" href=\"";
                echo ($context["popup"] ?? null);
                echo "\" rel=\"position:'inside', showTitle: false\"><img src=\"";
                echo ($context["thumb"] ?? null);
                echo "\" title=\"";
                echo ($context["heading_title"] ?? null);
                echo "\" alt=\"";
                echo ($context["heading_title"] ?? null);
                echo "\" /></a>
                            </div>
                        ";
            }
            // line 95
            echo "
                        ";
            // line 96
            if (($context["images"] ?? null)) {
                // line 97
                echo "                            <ul class=\"image-additional\">
                                ";
                // line 98
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["images"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                    // line 99
                    echo "                                    <li>
                                        <a class=\"link ";
                    // line 100
                    if ((($context["product_layout"] ?? null) != "full-width")) {
                        echo "cloud-zoom-gallery locked";
                    }
                    echo "\" href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["image"], "popup", [], "any", false, false, false, 100);
                    echo "\" rel=\"useZoom: 'main-image', smallImage: '";
                    echo twig_get_attribute($this->env, $this->source, $context["image"], "thumb_lg", [], "any", false, false, false, 100);
                    echo "'\"><img src=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["image"], "thumb", [], "any", false, false, false, 100);
                    echo "\" title=\"";
                    echo ($context["heading_title"] ?? null);
                    echo "\" alt=\"";
                    echo ($context["heading_title"] ?? null);
                    echo "\" /></a>
                                    </li>
                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 103
                echo "                                ";
                if ((($context["thumb"] ?? null) && (($context["product_layout"] ?? null) != "full-width"))) {
                    // line 104
                    echo "                                    <li><a class=\"link cloud-zoom-gallery locked active\" href=\"";
                    echo ($context["popup"] ?? null);
                    echo "\" rel=\"useZoom: 'main-image', smallImage: '";
                    echo ($context["thumb"] ?? null);
                    echo "'\"><img src=\"";
                    echo ($context["thumb_sm"] ?? null);
                    echo "\" title=\"";
                    echo ($context["heading_title"] ?? null);
                    echo "\" alt=\"";
                    echo ($context["heading_title"] ?? null);
                    echo "\" /></a></li>
                                ";
                }
                // line 106
                echo "                            </ul>
                        ";
            }
            // line 108
            echo "
                    </div> <!-- .table-cell.left ends -->

                </div> <!-- .image-area ends -->
                ";
        }
        // line 113
        echo "
                <div class=\"table-cell w100 right\">
                    <div class=\"inner\">

                        <div class=\"product-h1\">
                            <h1 id=\"page-title\">";
        // line 118
        echo ($context["heading_title"] ?? null);
        echo "</h1>
                        </div>

                        ";
        // line 121
        if ((($context["review_status"] ?? null) && (($context["review_qty"] ?? null) > 0))) {
            // line 122
            echo "                            <div class=\"rating\">
                                <span class=\"rating_stars rating r";
            // line 123
            echo ($context["rating"] ?? null);
            echo "\">
                                    <i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>
                                </span>
                            </div>
                            <span class=\"review_link\">(<a class=\"hover_uline to_tabs\" onclick=\"\$('a[href=\\'#tab-review\\']').trigger('click'); return false;\">";
            // line 127
            echo ($context["reviews"] ?? null);
            echo "</a>)</span>
                        ";
        }
        // line 129
        echo "
                        ";
        // line 130
        if (($context["price"] ?? null)) {
            // line 131
            echo "                            <ul class=\"list-unstyled price\">
                                ";
            // line 132
            if ( !($context["special"] ?? null)) {
                // line 133
                echo "                                    <li><span class=\"live-price\">";
                echo ($context["price"] ?? null);
                echo "</span></li>
                                ";
            } else {
                // line 135
                echo "                                    <li><span class=\"price-old\">";
                echo ($context["price"] ?? null);
                echo "</span><span class=\"live-price-new\">";
                echo ($context["special"] ?? null);
                echo "</span></li>
                                    <span id=\"special_countdown\"></span>
                                ";
            }
            // line 138
            echo "                            </ul>

                            ";
            // line 140
            if (($context["discounts"] ?? null)) {
                // line 141
                echo "                                <p class=\"discount\">
                                    ";
                // line 142
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["discounts"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["discount"]) {
                    // line 143
                    echo "                                        <span>";
                    echo twig_get_attribute($this->env, $this->source, $context["discount"], "quantity", [], "any", false, false, false, 143);
                    echo ($context["text_discount"] ?? null);
                    echo "<i class=\"price\">";
                    echo twig_get_attribute($this->env, $this->source, $context["discount"], "price", [], "any", false, false, false, 143);
                    echo "</i></span>
                                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['discount'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 145
                echo "                                </p>
                            ";
            }
            // line 147
            echo "
                        ";
        }
        // line 148
        echo " <!-- if price ends -->
                        ";
        // line 149
        if ((($context["price"] ?? null) && ($context["tax"] ?? null))) {
            // line 150
            echo "                            <p class=\"info p-tax\"><b>";
            echo ($context["text_tax"] ?? null);
            echo "</b> <span class=\"live-price-tax\">";
            echo ($context["tax"] ?? null);
            echo "</span></p>
                        ";
        }
        // line 152
        echo "
                        ";
        // line 153
        if ((($context["meta_description_status"] ?? null) && ($context["meta_description"] ?? null))) {
            // line 154
            echo "                            <p class=\"meta_description\">";
            echo ($context["meta_description"] ?? null);
            echo "</p>
                        ";
        }
        // line 156
        echo "

                        <div id=\"product\">

                            ";
        // line 160
        if (($context["options"] ?? null)) {
            // line 161
            echo "                                <div class=\"options\">
                                    ";
            // line 162
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["options"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
                // line 163
                echo "
                                        ";
                // line 164
                if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 164) == "select")) {
                    // line 165
                    echo "                                            <div class=\"form-group";
                    if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 165)) {
                        echo " required";
                    }
                    echo " table-row\">
                                                <div class=\"table-cell name\">
                                                    <label class=\"control-label\" for=\"input-option";
                    // line 167
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 167);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 167);
                    echo "</label>
                                                </div>
                                                <div class=\"table-cell\">
                                                    <select name=\"option[";
                    // line 170
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 170);
                    echo "]\" id=\"input-option";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 170);
                    echo "\" class=\"form-control\">
                                                        <option value=\"\">";
                    // line 171
                    echo ($context["text_select"] ?? null);
                    echo "</option>
                                                        ";
                    // line 172
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["option"], "product_option_value", [], "any", false, false, false, 172));
                    foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                        // line 173
                        echo "                                                            <option value=\"";
                        echo twig_get_attribute($this->env, $this->source, $context["option_value"], "product_option_value_id", [], "any", false, false, false, 173);
                        echo "\">";
                        echo twig_get_attribute($this->env, $this->source, $context["option_value"], "name", [], "any", false, false, false, 173);
                        echo "
                                                                ";
                        // line 174
                        if (twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 174)) {
                            // line 175
                            echo "                                                                    (";
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price_prefix", [], "any", false, false, false, 175);
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 175);
                            echo ")
                                                                ";
                        }
                        // line 177
                        echo "                                                            </option>
                                                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 179
                    echo "                                                    </select>
                                                </div>
                                            </div>
                                        ";
                }
                // line 183
                echo "
                                        ";
                // line 184
                if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 184) == "radio")) {
                    // line 185
                    echo "                                            <div class=\"form-group";
                    if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 185)) {
                        echo " required";
                    }
                    echo " table-row\">
                                                <div class=\"table-cell radio-cell name\">
                                                    <label class=\"control-label\">";
                    // line 187
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 187);
                    echo "</label>
                                                </div>
                                                <div class=\"table-cell radio-cell\">
                                                    <div id=\"input-option";
                    // line 190
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 190);
                    echo "\">
                                                        ";
                    // line 191
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["option"], "product_option_value", [], "any", false, false, false, 191));
                    foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                        // line 192
                        echo "                                                            <div class=\"radio";
                        if (twig_get_attribute($this->env, $this->source, $context["option_value"], "image", [], "any", false, false, false, 192)) {
                            echo " has-image";
                        }
                        echo "\">
                                                                <label>
                                                                    <input type=\"radio\" name=\"option[";
                        // line 194
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 194);
                        echo "]\" value=\"";
                        echo twig_get_attribute($this->env, $this->source, $context["option_value"], "product_option_value_id", [], "any", false, false, false, 194);
                        echo "\" />
                                                                    ";
                        // line 195
                        if (twig_get_attribute($this->env, $this->source, $context["option_value"], "image", [], "any", false, false, false, 195)) {
                            // line 196
                            echo "                                                                        <img src=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "image", [], "any", false, false, false, 196);
                            echo "\" alt=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "name", [], "any", false, false, false, 196);
                            if (twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 196)) {
                                echo "(";
                                echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price_prefix", [], "any", false, false, false, 196);
                                echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 196);
                                echo ")";
                            }
                            echo "\" data-toggle=\"tooltip\" data-title=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "name", [], "any", false, false, false, 196);
                            if (twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 196)) {
                                echo " (";
                                echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price_prefix", [], "any", false, false, false, 196);
                                echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 196);
                                echo ")";
                            }
                            echo "\" />
                                                                    ";
                        }
                        // line 198
                        echo "                                                                    <span class=\"name\">
                    ";
                        // line 199
                        echo twig_get_attribute($this->env, $this->source, $context["option_value"], "name", [], "any", false, false, false, 199);
                        echo "
                                                                        ";
                        // line 200
                        if (twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 200)) {
                            // line 201
                            echo "                                                                            (";
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price_prefix", [], "any", false, false, false, 201);
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 201);
                            echo ")
                                                                        ";
                        }
                        // line 203
                        echo "                    </span>
                                                                </label>
                                                            </div>
                                                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 207
                    echo "                                                    </div>
                                                </div>
                                            </div>
                                        ";
                }
                // line 211
                echo "
                                        ";
                // line 212
                if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 212) == "checkbox")) {
                    // line 213
                    echo "                                            <div class=\"form-group";
                    if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 213)) {
                        echo " required";
                    }
                    echo " table-row\">
                                                <div class=\"table-cell checkbox-cell name\">
                                                    <label class=\"control-label\">";
                    // line 215
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 215);
                    echo "</label>
                                                </div>
                                                <div class=\"table-cell checkbox-cell\">
                                                    <div id=\"input-option";
                    // line 218
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 218);
                    echo "\">
                                                        ";
                    // line 219
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["option"], "product_option_value", [], "any", false, false, false, 219));
                    foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                        // line 220
                        echo "                                                            <div class=\"checkbox";
                        if (twig_get_attribute($this->env, $this->source, $context["option_value"], "image", [], "any", false, false, false, 220)) {
                            echo " has-image";
                        }
                        echo "\">
                                                                <label>
                                                                    <input type=\"checkbox\" name=\"option[";
                        // line 222
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 222);
                        echo "][]\" value=\"";
                        echo twig_get_attribute($this->env, $this->source, $context["option_value"], "product_option_value_id", [], "any", false, false, false, 222);
                        echo "\" />
                                                                    ";
                        // line 223
                        if (twig_get_attribute($this->env, $this->source, $context["option_value"], "image", [], "any", false, false, false, 223)) {
                            // line 224
                            echo "                                                                        <img src=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "image", [], "any", false, false, false, 224);
                            echo "\" alt=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "name", [], "any", false, false, false, 224);
                            if (twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 224)) {
                                echo "(";
                                echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price_prefix", [], "any", false, false, false, 224);
                                echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 224);
                                echo ")";
                            }
                            echo "\" data-toggle=\"tooltip\" data-title=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "name", [], "any", false, false, false, 224);
                            if (twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 224)) {
                                echo " (";
                                echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price_prefix", [], "any", false, false, false, 224);
                                echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 224);
                                echo ")";
                            }
                            echo "\" />
                                                                    ";
                        }
                        // line 226
                        echo "                                                                    <span class=\"name\">
                    ";
                        // line 227
                        echo twig_get_attribute($this->env, $this->source, $context["option_value"], "name", [], "any", false, false, false, 227);
                        echo "
                                                                        ";
                        // line 228
                        if (twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 228)) {
                            // line 229
                            echo "                                                                            (";
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price_prefix", [], "any", false, false, false, 229);
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 229);
                            echo ")
                                                                        ";
                        }
                        // line 231
                        echo "                    </span>
                                                                </label>
                                                            </div>
                                                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 235
                    echo "                                                    </div>
                                                </div>
                                            </div>
                                        ";
                }
                // line 239
                echo "

                                        ";
                // line 241
                if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 241) == "text")) {
                    // line 242
                    echo "                                            <div class=\"form-group";
                    if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 242)) {
                        echo " required";
                    }
                    echo " table-row\">
                                                <div class=\"table-cell name\">
                                                    <label class=\"control-label\" for=\"input-option";
                    // line 244
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 244);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 244);
                    echo "</label>
                                                </div>
                                                <div class=\"table-cell\">
                                                    <input type=\"text\" name=\"option[";
                    // line 247
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 247);
                    echo "]\" value=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "value", [], "any", false, false, false, 247);
                    echo "\" placeholder=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 247);
                    echo "\" id=\"input-option";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 247);
                    echo "\" class=\"form-control\" />
                                                </div>
                                            </div>
                                        ";
                }
                // line 251
                echo "
                                        ";
                // line 252
                if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 252) == "textarea")) {
                    // line 253
                    echo "                                            <div class=\"form-group";
                    if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 253)) {
                        echo " required";
                    }
                    echo " table-row\">
                                                <div class=\"table-cell name\">
                                                    <label class=\"control-label\" for=\"input-option";
                    // line 255
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 255);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 255);
                    echo "</label>
                                                </div>
                                                <div class=\"table-cell\">
                                                    <textarea name=\"option[";
                    // line 258
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 258);
                    echo "]\" rows=\"5\" placeholder=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 258);
                    echo "\" id=\"input-option";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 258);
                    echo "\" class=\"form-control\">";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "value", [], "any", false, false, false, 258);
                    echo "</textarea>
                                                </div>
                                            </div>
                                        ";
                }
                // line 262
                echo "
                                        ";
                // line 263
                if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 263) == "file")) {
                    // line 264
                    echo "                                            <div class=\"form-group";
                    if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 264)) {
                        echo " required";
                    }
                    echo " table-row\">
                                                <div class=\"table-cell name\">
                                                    <label class=\"control-label\">";
                    // line 266
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 266);
                    echo "</label>
                                                </div>
                                                <div class=\"table-cell\">
                                                    <button type=\"button\" id=\"button-upload";
                    // line 269
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 269);
                    echo "\" data-loading-text=\"";
                    echo ($context["text_loading"] ?? null);
                    echo "\" class=\"btn btn-default btn-block\"><i class=\"fa fa-upload\"></i> ";
                    echo ($context["button_upload"] ?? null);
                    echo "</button>
                                                    <input type=\"hidden\" name=\"option[";
                    // line 270
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 270);
                    echo "]\" value=\"\" id=\"input-option";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 270);
                    echo "\" />
                                                </div>
                                            </div>
                                        ";
                }
                // line 274
                echo "
                                        ";
                // line 275
                if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 275) == "date")) {
                    // line 276
                    echo "                                            <div class=\"form-group";
                    if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 276)) {
                        echo " required";
                    }
                    echo " table-row\">
                                                <div class=\"table-cell name\">
                                                    <label class=\"control-label\" for=\"input-option";
                    // line 278
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 278);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 278);
                    echo "</label>
                                                </div>
                                                <div class=\"table-cell\">
                                                    <div class=\"input-group date\">
                                                        <input type=\"text\" name=\"option[";
                    // line 282
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 282);
                    echo "]\" value=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "value", [], "any", false, false, false, 282);
                    echo "\" data-date-format=\"YYYY-MM-DD\" id=\"input-option";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 282);
                    echo "\" class=\"form-control\" />
                                                        <span class=\"input-group-btn\">
                <button class=\"btn btn-default\" type=\"button\"><i class=\"fa fa-calendar\"></i></button>
                </span></div>
                                                </div>
                                            </div>
                                        ";
                }
                // line 289
                echo "
                                        ";
                // line 290
                if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 290) == "datetime")) {
                    // line 291
                    echo "                                            <div class=\"form-group";
                    if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 291)) {
                        echo " required";
                    }
                    echo " table-row\">
                                                <div class=\"table-cell name\">
                                                    <label class=\"control-label\" for=\"input-option";
                    // line 293
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 293);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 293);
                    echo "</label>
                                                </div>
                                                <div class=\"table-cell\">
                                                    <div class=\"input-group datetime\">
                                                        <input type=\"text\" name=\"option[";
                    // line 297
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 297);
                    echo "]\" value=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "value", [], "any", false, false, false, 297);
                    echo "\" data-date-format=\"YYYY-MM-DD HH:mm\" id=\"input-option";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 297);
                    echo "\" class=\"form-control\" />
                                                        <span class=\"input-group-btn\">
                <button type=\"button\" class=\"btn btn-default\"><i class=\"fa fa-calendar\"></i></button>
                </span></div>
                                                </div>
                                            </div>
                                        ";
                }
                // line 304
                echo "
                                        ";
                // line 305
                if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 305) == "time")) {
                    // line 306
                    echo "                                            <div class=\"form-group";
                    if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 306)) {
                        echo " required";
                    }
                    echo " table-row\">
                                                <div class=\"table-cell name\">
                                                    <label class=\"control-label\" for=\"input-option";
                    // line 308
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 308);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 308);
                    echo "</label>
                                                </div>
                                                <div class=\"table-cell\">
                                                    <div class=\"input-group time\">
                                                        <input type=\"text\" name=\"option[";
                    // line 312
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 312);
                    echo "]\" value=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "value", [], "any", false, false, false, 312);
                    echo "\" data-date-format=\"HH:mm\" id=\"input-option";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 312);
                    echo "\" class=\"form-control\" />
                                                        <span class=\"input-group-btn\">
                                                            <button type=\"button\" class=\"btn btn-default\"><i class=\"fa fa-calendar\"></i></button>
                                                        </span></div>
                                                </div>
                                            </div>
                                        ";
                }
                // line 319
                echo "
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 320
            echo " <!-- foreach option -->
                                </div>
                            ";
        }
        // line 323
        echo "
                            ";
        // line 324
        if (($context["recurrings"] ?? null)) {
            // line 325
            echo "                                <hr>
                                <h3>";
            // line 326
            echo ($context["text_payment_recurring"] ?? null);
            echo "</h3>
                                <div class=\"form-group required\">
                                    <select name=\"recurring_id\" class=\"form-control\">
                                        <option value=\"\">";
            // line 329
            echo ($context["text_select"] ?? null);
            echo "</option>
                                        ";
            // line 330
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["recurrings"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["recurring"]) {
                // line 331
                echo "                                            <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["recurring"], "recurring_id", [], "any", false, false, false, 331);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["recurring"], "name", [], "any", false, false, false, 331);
                echo "</option>
                                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['recurring'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 333
            echo "                                    </select>
                                    <div class=\"help-block\" id=\"recurring-description\"></div>
                                </div>
                            ";
        }
        // line 337
        echo "
                            <div class=\"form-group buy catalog_hide\">
                                <select name=\"";
        // line 339
        echo ($context["product_id"] ?? null);
        echo "\" class=\"form-control num";
        echo twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "product_id", [], "any", false, false, false, 339);
        echo "\" style=\"width: 90px;\">
                                    ";
        // line 340
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["scale"] ?? null), "items", [], "any", false, false, false, 340));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 341
            echo "                                        <option value=\"";
            echo twig_get_attribute($this->env, $this->source, $context["item"], "value", [], "any", false, false, false, 341);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["item"], "label", [], "any", false, false, false, 341);
            echo "</option>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 343
        echo "                                    ";
        if ((twig_get_attribute($this->env, $this->source, ($context["scale"] ?? null), "entries", [], "any", false, false, false, 343) == 1)) {
            // line 344
            echo "                                        <option value=\"+\">+</option>
                                    ";
        }
        // line 346
        echo "                                </select>
                                <button class=\"";
        // line 347
        echo "btn btn-primary\" data-loading-text=\"";
        echo ($context["text_loading"] ?? null);
        echo "\" onclick=\"cart.add('";
        echo ($context["product_id"] ?? null);
        echo "', \$(this).parent().parent().find('.num";
        echo ($context["product_id"] ?? null);
        echo "').val());\">
                                    ";
        // line 348
        if (((($context["qty"] ?? null) < 1) && ($context["stock_badge_status"] ?? null))) {
            echo ($context["basel_text_out_of_stock"] ?? null);
        } else {
            echo "<i id=\"cart-icon\" class=\"global-cart icon\"></i> ";
            echo ($context["button_cart"] ?? null);
        }
        // line 349
        echo "                                </button>

                                ";
        // line 354
        echo "                            </div>
                            ";
        // line 355
        if ((($context["minimum"] ?? null) > 1)) {
            // line 356
            echo "                                <div class=\"alert alert-sm alert-info\"><i class=\"fa fa-info-circle\"></i> ";
            echo ($context["text_minimum"] ?? null);
            echo "</div>
                            ";
        }
        // line 358
        echo "
                        </div> <!-- #product ends -->


                        <p class=\"info is_wishlist\"><a onclick=\"wishlist.add('";
        // line 362
        echo ($context["product_id"] ?? null);
        echo "');\"><i class=\"icon-heart\"></i> ";
        echo ($context["button_wishlist"] ?? null);
        echo "</a></p>
                        <p class=\"info is_compare\"><a onclick=\"compare.add('";
        // line 363
        echo ($context["product_id"] ?? null);
        echo "');\"><i class=\"icon-refresh\"></i> ";
        echo ($context["button_compare"] ?? null);
        echo "</a></p>
                        ";
        // line 364
        if (($context["question_status"] ?? null)) {
            // line 365
            echo "                            <p class=\"info is_ask\"><a class=\"to_tabs\" onclick=\"\$('a[href=\\'#tab-questions\\']').trigger('click'); return false;\"><i class=\"icon-question\"></i> ";
            echo ($context["basel_button_ask"] ?? null);
            echo "</a></p>
                        ";
        }
        // line 367
        echo "
                        <div class=\"clearfix\"></div>

                        <div class=\"info-holder\">



                            ";
        // line 374
        if ((($context["price"] ?? null) && ($context["points"] ?? null))) {
            // line 375
            echo "                                <p class=\"info\"><b>";
            echo ($context["text_points"] ?? null);
            echo "</b> ";
            echo ($context["points"] ?? null);
            echo "</p>
                            ";
        }
        // line 377
        echo "
                            <p class=\"info ";
        // line 378
        if ((($context["qty"] ?? null) > 0)) {
            echo "in_stock";
        }
        echo "\"><b>";
        echo ($context["text_stock"] ?? null);
        echo "</b> ";
        echo ($context["stock"] ?? null);
        echo "</p>

                            ";
        // line 380
        if (($context["manufacturer"] ?? null)) {
            // line 381
            echo "                                <p class=\"info\"><b>";
            echo ($context["text_manufacturer"] ?? null);
            echo "</b> <a class=\"hover_uline\" href=\"";
            echo ($context["manufacturers"] ?? null);
            echo "\">";
            echo ($context["manufacturer"] ?? null);
            echo "</a></p>
                            ";
        }
        // line 383
        echo "
                            <!--  <p class=\"info\"><b>";
        // line 384
        echo ($context["text_model"] ?? null);
        echo "</b> ";
        echo ($context["model"] ?? null);
        echo "</p> -->

                            ";
        // line 386
        if (($context["reward"] ?? null)) {
            // line 387
            echo "                                <p class=\"info\"><b>";
            echo ($context["text_reward"] ?? null);
            echo "</b> ";
            echo ($context["reward"] ?? null);
            echo "</p>
                            ";
        }
        // line 389
        echo "
                            ";
        // line 390
        if (($context["tags"] ?? null)) {
            // line 391
            echo "                                <p class=\"info tags\"><b>";
            echo ($context["text_tags"] ?? null);
            echo "</b> &nbsp;<span>";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["tags"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["tag"]) {
                echo "<a class=\"hover_uline\" href=\"";
                echo twig_get_attribute($this->env, $this->source, $context["tag"], "href", [], "any", false, false, false, 391);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["tag"], "tag", [], "any", false, false, false, 391);
                echo "</a>";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tag'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo "</span></p>
                            ";
        }
        // line 393
        echo "
                            ";
        // line 394
        if (($context["basel_share_btn"] ?? null)) {
            // line 395
            echo "                                ";
            if ((($context["basel_sharing_style"] ?? null) == "large")) {
                // line 396
                echo "                                    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5f1aa4948aa12700134b0c47&product=inline-share-buttons' async='async'></script>
                                    <!-- ShareThis BEGIN --><div class=\"sharethis-inline-share-buttons\"></div><!-- ShareThis END -->
                                ";
            } else {
                // line 399
                echo "                                <hr>
                                    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5f1aa4948aa12700134b0c47&product=inline-share-buttons' async='async'></script>
                                    <!-- ShareThis BEGIN --><div class=\"sharethis-inline-share-buttons\"></div><!-- ShareThis END -->
                                ";
            }
            // line 403
            echo "                            ";
        }
        // line 404
        echo "
                        </div> <!-- .info-holder ends -->

                    </div> <!-- .inner ends -->


                    ";
        // line 410
        if (($context["full_width_tabs"] ?? null)) {
            // line 411
            echo "                </div> <!-- main column ends -->
                ";
            // line 412
            echo ($context["column_right"] ?? null);
            echo "
            </div> <!-- .row ends -->
        </div> <!-- .container ends -->
        ";
        }
        // line 416
        echo "
        ";
        // line 417
        if (($context["full_width_tabs"] ?? null)) {
            // line 418
            echo "        <div class=\"outer-container product-tabs-wrapper\">
            <div class=\"container\">
                ";
        } else {
            // line 421
            echo "                <div class=\"inline-tabs\">
                    ";
        }
        // line 423
        echo "
                    <!-- Tabs area start -->
                    <div class=\"row\">
                        <div class=\"col-sm-12\">

                            <ul class=\"nav nav-tabs ";
        // line 428
        echo ($context["product_tabs_style"] ?? null);
        echo " main_tabs\">
                                <li class=\"active\"><a href=\"#tab-description\" data-toggle=\"tab\">";
        // line 429
        echo ($context["tab_description"] ?? null);
        echo "</a></li>
                                ";
        // line 430
        if (($context["product_tabs"] ?? null)) {
            // line 431
            echo "                                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["product_tabs"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["tab"]) {
                // line 432
                echo "                                        <li><a href=\"#custom-tab-";
                echo twig_get_attribute($this->env, $this->source, $context["tab"], "tab_id", [], "any", false, false, false, 432);
                echo "\" data-toggle=\"tab\">";
                echo twig_get_attribute($this->env, $this->source, $context["tab"], "name", [], "any", false, false, false, 432);
                echo "</a></li>
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tab'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 434
            echo "                                ";
        }
        // line 435
        echo "                                ";
        if (($context["attribute_groups"] ?? null)) {
            // line 436
            echo "                                    <li><a href=\"#tab-specification\" data-toggle=\"tab\">";
            echo ($context["tab_attribute"] ?? null);
            echo "</a></li>
                                ";
        }
        // line 438
        echo "                                ";
        if (($context["review_status"] ?? null)) {
            // line 439
            echo "                                    <li><a href=\"#tab-review\" data-toggle=\"tab\">";
            echo ($context["tab_review"] ?? null);
            echo "</a></li>
                                ";
        }
        // line 441
        echo "                                ";
        if (($context["question_status"] ?? null)) {
            // line 442
            echo "                                    <li><a href=\"#tab-questions\" data-toggle=\"tab\">";
            echo ($context["basel_tab_questions"] ?? null);
            echo " (";
            echo ($context["questions_total"] ?? null);
            echo ")</a></li>
                                ";
        }
        // line 444
        echo "                            </ul>

                            <div class=\"tab-content\">

                                <div class=\"tab-pane active\" id=\"tab-description\">
                                    ";
        // line 449
        echo ($context["description"] ?? null);
        echo "
                                </div>

                                ";
        // line 452
        if (($context["product_tabs"] ?? null)) {
            // line 453
            echo "                                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["product_tabs"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["tab"]) {
                // line 454
                echo "                                        <div class=\"tab-pane\" id=\"custom-tab-";
                echo twig_get_attribute($this->env, $this->source, $context["tab"], "tab_id", [], "any", false, false, false, 454);
                echo "\">
                                            ";
                // line 455
                echo twig_get_attribute($this->env, $this->source, $context["tab"], "description", [], "any", false, false, false, 455);
                echo "
                                        </div>
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tab'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 458
            echo "                                ";
        }
        // line 459
        echo "
                                ";
        // line 460
        if (($context["attribute_groups"] ?? null)) {
            // line 461
            echo "                                    <div class=\"tab-pane\" id=\"tab-specification\">
                                        <table class=\"table specification\">
                                            ";
            // line 463
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["attribute_groups"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["attribute_group"]) {
                // line 464
                echo "                                                <thead>
                                                <tr>
                                                    <td colspan=\"2\">";
                // line 466
                echo twig_get_attribute($this->env, $this->source, $context["attribute_group"], "name", [], "any", false, false, false, 466);
                echo "</td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                ";
                // line 470
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["attribute_group"], "attribute", [], "any", false, false, false, 470));
                foreach ($context['_seq'] as $context["_key"] => $context["attribute"]) {
                    // line 471
                    echo "                                                    <tr>
                                                        <td class=\"text-left\"><b>";
                    // line 472
                    echo twig_get_attribute($this->env, $this->source, $context["attribute"], "name", [], "any", false, false, false, 472);
                    echo "</b></td>
                                                        <td class=\"text-right\">";
                    // line 473
                    echo twig_get_attribute($this->env, $this->source, $context["attribute"], "text", [], "any", false, false, false, 473);
                    echo "</td>
                                                    </tr>
                                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attribute'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 476
                echo "                                                </tbody>
                                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attribute_group'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 478
            echo "                                        </table>
                                    </div>
                                ";
        }
        // line 481
        echo "
                                ";
        // line 482
        if (($context["question_status"] ?? null)) {
            // line 483
            echo "                                    <div class=\"tab-pane\" id=\"tab-questions\">
                                        ";
            // line 484
            echo ($context["product_questions"] ?? null);
            echo "
                                    </div>
                                ";
        }
        // line 487
        echo "
                                ";
        // line 488
        if (($context["review_status"] ?? null)) {
            // line 489
            echo "                                <div class=\"tab-pane\" id=\"tab-review\">
                                    <div class=\"row\">
                                        <div class=\"col-sm-6\">
                                            <h4><b>";
            // line 492
            echo ($context["button_reviews"] ?? null);
            echo "</b></h4>

                                            <div id=\"review\">
                                                ";
            // line 495
            if (($context["seo_reviews"] ?? null)) {
                // line 496
                echo "                                                    ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["seo_reviews"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["review"]) {
                    // line 497
                    echo "                                                        <div class=\"table\">
                                                            <div class=\"table-cell\"><i class=\"fa fa-user\"></i></div>
                                                            <div class=\"table-cell right\">
                                                                <p class=\"author\"><b>";
                    // line 500
                    echo twig_get_attribute($this->env, $this->source, $context["review"], "author", [], "any", false, false, false, 500);
                    echo "</b>  -  ";
                    echo twig_get_attribute($this->env, $this->source, $context["review"], "date_added", [], "any", false, false, false, 500);
                    echo "
                                                                    <span class=\"rating\">
\t\t<span class=\"rating_stars rating r";
                    // line 502
                    echo twig_get_attribute($this->env, $this->source, $context["review"], "rating", [], "any", false, false, false, 502);
                    echo "\">
\t\t<i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>
\t\t</span>
\t\t</span>
                                                                </p>
                                                                ";
                    // line 507
                    echo twig_get_attribute($this->env, $this->source, $context["review"], "text", [], "any", false, false, false, 507);
                    echo "
                                                            </div>
                                                        </div>
                                                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['review'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 511
                echo "                                                    ";
                if (($context["pagination"] ?? null)) {
                    // line 512
                    echo "                                                        <div class=\"pagination-holder\">";
                    echo ($context["pagination"] ?? null);
                    echo "</div>
                                                    ";
                }
                // line 514
                echo "                                                ";
            } else {
                // line 515
                echo "                                                    <p>";
                echo ($context["text_no_reviews"] ?? null);
                echo "</p>
                                                ";
            }
            // line 517
            echo "                                            </div>

                                        </div>
                                        <div class=\"col-sm-6 right\">
                                            <form class=\"form-horizontal\" id=\"form-review\">

                                                <h4 id=\"review-notification\"><b>";
            // line 523
            echo ($context["text_write"] ?? null);
            echo "</b></h4>
                                                ";
            // line 524
            if (($context["review_guest"] ?? null)) {
                // line 525
                echo "
                                                    <div class=\"form-group required\">
                                                        <div class=\"col-sm-12 rating-stars\">
                                                            <label class=\"control-label\">";
                // line 528
                echo ($context["entry_rating"] ?? null);
                echo "</label>

                                                            <input type=\"radio\" value=\"1\" name=\"rating\" id=\"rating1\" />
                                                            <label for=\"rating1\"><i class=\"fa fa-star-o\"></i></label>

                                                            <input type=\"radio\" value=\"2\" name=\"rating\" id=\"rating2\" />
                                                            <label for=\"rating2\"><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i></label>

                                                            <input type=\"radio\" value=\"3\" name=\"rating\" id=\"rating3\" />
                                                            <label for=\"rating3\"><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i></label>

                                                            <input type=\"radio\" value=\"4\" name=\"rating\" id=\"rating4\" />
                                                            <label for=\"rating4\"><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i></label>

                                                            <input type=\"radio\" value=\"5\" name=\"rating\" id=\"rating5\" />
                                                            <label for=\"rating5\"><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i></label>
                                                        </div>
                                                    </div>

                                                    <div class=\"form-group required\">
                                                        <div class=\"col-sm-12\">
                                                            <label class=\"control-label\" for=\"input-name\">";
                // line 549
                echo ($context["entry_name"] ?? null);
                echo "</label>
                                                            <input type=\"text\" name=\"name\" value=\"";
                // line 550
                echo ($context["customer_name"] ?? null);
                echo "\" id=\"input-name\" class=\"form-control grey\" />
                                                        </div>
                                                    </div>
                                                    <div class=\"form-group required\">
                                                        <div class=\"col-sm-12\">
                                                            <label class=\"control-label\" for=\"input-review\">";
                // line 555
                echo ($context["entry_review"] ?? null);
                echo "</label>
                                                            <textarea name=\"text\" rows=\"5\" id=\"input-review\" class=\"form-control grey\"></textarea>
                                                            <small>";
                // line 557
                echo ($context["text_note"] ?? null);
                echo "</small>
                                                        </div>
                                                    </div>

                                                    <div class=\"form-group required\">
                                                        <div class=\"col-sm-12\">
                                                            ";
                // line 563
                echo ($context["captcha"] ?? null);
                echo "
                                                        </div>
                                                    </div>

                                                    <div class=\"buttons clearfix\">
                                                        <div class=\"text-right\">
                                                            <button type=\"button\" id=\"button-review\" data-loading-text=\"";
                // line 569
                echo ($context["text_loading"] ?? null);
                echo "\" class=\"btn btn-outline\">";
                echo ($context["button_continue"] ?? null);
                echo "</button>
                                                        </div>
                                                    </div>
                                                ";
            } else {
                // line 573
                echo "                                                    ";
                echo ($context["text_login"] ?? null);
                echo "
                                                ";
            }
            // line 575
            echo "                                            </form>
                                        </div>
                                    </div>
                                </div>
                                ";
        }
        // line 579
        echo "<!-- if review-status ends -->

                            </div> <!-- .tab-content ends -->
                        </div> <!-- .col-sm-12 ends -->
                    </div> <!-- .row ends -->
                    <!-- Tabs area ends -->

                    ";
        // line 586
        if (($context["full_width_tabs"] ?? null)) {
            // line 587
            echo "                </div>
                ";
        }
        // line 589
        echo "            </div>


        </div> <!-- .table-cell.right ends -->

    </div> <!-- .product-info ends -->

           <!-- Related Products -->

    ";
        // line 598
        if (($context["full_width_tabs"] ?? null)) {
            // line 599
            echo "    <div class=\"container c10padd\">
        ";
        }
        // line 601
        echo "
        ";
        // line 602
        if (($context["products"] ?? null)) {
            // line 603
            echo "            <div class=\"widget widget-related\">

                <div class=\"widget-title\">
                    <p class=\"main-title\"><span>";
            // line 606
            echo ($context["text_related"] ?? null);
            echo "</span></p>
                    <p class=\"widget-title-separator\"><i class=\"icon-line-cross\"></i></p>
                </div>

                <div class=\"grid grid-holder related carousel grid";
            // line 610
            echo ($context["basel_rel_prod_grid"] ?? null);
            echo "\">
                    ";
            // line 611
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 612
                echo "                        ";
                $this->loadTemplate("basel/template/product/single_product.twig", "basel/template/product/product.twig", 612)->display($context);
                // line 613
                echo "                    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 614
            echo "                </div>
            </div>
        ";
        }
        // line 617
        echo "
        ";
        // line 618
        echo ($context["content_bottom"] ?? null);
        echo "

        ";
        // line 620
        if (($context["full_width_tabs"] ?? null)) {
            // line 621
            echo "    </div>
    ";
        }
        // line 623
        echo "

    ";
        // line 625
        if ( !($context["full_width_tabs"] ?? null)) {
            // line 626
            echo "</div> <!-- main column ends -->
";
            // line 627
            echo ($context["column_right"] ?? null);
            echo "
    </div> <!-- .row ends -->
    </div> <!-- .container ends -->
";
        }
        // line 631
        echo "
<script src=\"catalog/view/theme/basel/js/lightgallery/js/lightgallery.min.js\"></script>
<script src=\"catalog/view/theme/basel/js/lightgallery/js/lg-zoom.min.js\"></script>
<script src=\"catalog/view/theme/basel/js/cloudzoom/cloud-zoom.1.0.2.min.js\"></script>
";
        // line 635
        if (($context["basel_price_update"] ?? null)) {
            // line 636
            echo "    <script src=\"index.php?route=extension/basel/live_options/js&product_id=";
            echo ($context["product_id"] ?? null);
            echo "\"></script>
";
        }
        // line 638
        echo "
";
        // line 639
        if (($context["products"] ?? null)) {
            // line 640
            echo "    <script><!--
        \$('.grid-holder.related').slick({
            prevArrow: \"<a class=\\\"arrow-left icon-arrow-left\\\"></a>\",
            nextArrow: \"<a class=\\\"arrow-right icon-arrow-right\\\"></a>\",
            dots:true,
            ";
            // line 645
            if ((($context["direction"] ?? null) == "rtl")) {
                // line 646
                echo "            rtl: true,
            ";
            }
            // line 648
            echo "            respondTo:'min',
            ";
            // line 649
            if ((($context["basel_rel_prod_grid"] ?? null) == "5")) {
                // line 650
                echo "            slidesToShow:5,slidesToScroll:5,responsive:[{breakpoint:1100,settings:{slidesToShow:4,slidesToScroll:4}},{breakpoint:960,settings:{slidesToShow:3,slidesToScroll:3}},{breakpoint:600,settings:{slidesToShow:2,slidesToScroll:2}},
                ";
            } elseif ((            // line 651
($context["basel_rel_prod_grid"] ?? null) == "4")) {
                // line 652
                echo "                slidesToShow:4,slidesToScroll:4,responsive:[{breakpoint:960,settings:{slidesToShow:3,slidesToScroll:3}},{breakpoint:600,settings:{slidesToShow:2,slidesToScroll:2}},
            ";
            } elseif ((            // line 653
($context["basel_rel_prod_grid"] ?? null) == "3")) {
                // line 654
                echo "            slidesToShow:3,slidesToScroll:3,responsive:[{breakpoint:600,settings:{slidesToShow:2,slidesToScroll:2}},
            ";
            } elseif ((            // line 655
($context["basel_rel_prod_grid"] ?? null) == "2")) {
                // line 656
                echo "            slidesToShow:2,slidesToScroll:2,responsive:[
            ";
            }
            // line 658
            echo "            ";
            if (($context["items_mobile_fw"] ?? null)) {
                // line 659
                echo "            {breakpoint:320,settings:{slidesToShow:1,slidesToScroll:1}}
            ";
            }
            // line 661
            echo "        ]
        });
        \$('.product-style2 .single-product .icon').attr('data-placement', 'top');
        \$('[data-toggle=\\'tooltip\\']').tooltip({container: 'body'});
        //--></script>
";
        }
        // line 667
        echo "
";
        // line 668
        if ((($context["sale_end_date"] ?? null) && ($context["product_page_countdown"] ?? null))) {
            // line 669
            echo "    <script>
        \$(function() {
            \$('#special_countdown').countdown('";
            // line 671
            echo ($context["sale_end_date"] ?? null);
            echo "').on('update.countdown', function(event) {
                var \$this = \$(this).html(event.strftime(''
                    + '<div class=\\\"special_countdown\\\"></span><p><span class=\\\"icon-clock\\\"></span> ";
            // line 673
            echo ($context["basel_text_offer_ends"] ?? null);
            echo "</p><div>'
                    + '%D<i>";
            // line 674
            echo ($context["basel_text_days"] ?? null);
            echo "</i></div><div>'
                    + '%H <i>";
            // line 675
            echo ($context["basel_text_hours"] ?? null);
            echo "</i></div><div>'
                    + '%M <i>";
            // line 676
            echo ($context["basel_text_mins"] ?? null);
            echo "</i></div><div>'
                    + '%S <i>";
            // line 677
            echo ($context["basel_text_secs"] ?? null);
            echo "</i></div></div>'));
            });
        });
    </script>
";
        }
        // line 682
        echo "
<script><!--
    \$('select[name=\\'recurring_id\\'], input[name=\"quantity\"]').change(function(){
        \$.ajax({
            url: 'index.php?route=product/product/getRecurringDescription',
            type: 'post',
            data: \$('input[name=\\'product_id\\'], input[name=\\'quantity\\'], select[name=\\'recurring_id\\']'),
            dataType: 'json',
            beforeSend: function() {
                \$('#recurring-description').html('');
            },
            success: function(json) {
                \$('.alert-dismissible, .text-danger').remove();

                if (json['success']) {
                    \$('#recurring-description').html(json['success']);
                }
            }
        });
    });
    //--></script>

<script><!--
    \$('#button-cart').on('click', function() {
        \$.ajax({
            url: 'index.php?route=extension/basel/basel_features/add_to_cart',
            type: 'post',
            data: \$('#product input[type=\\'text\\'], #product input[type=\\'hidden\\'], #product input[type=\\'number\\'], #product input[type=\\'radio\\']:checked, #product input[type=\\'checkbox\\']:checked, #product select, #product textarea'),
            dataType: 'json',
            beforeSend: function(json) {
                \$('body').append('<span class=\"basel-spinner ajax-call\"></span>');
            },

            success: function(json) {
                \$('.alert, .text-danger').remove();
                \$('.table-cell').removeClass('has-error');

                if (json.error) {
                    \$('.basel-spinner.ajax-call').remove();
                    if (json.error.option) {
                        for (i in json.error.option) {
                            var element = \$('#input-option' + i.replace('_', '-'));

                            if (element.parent().hasClass('input-group')) {
                                element.parent().after('<div class=\"text-danger\">' + json.error.option[i] + '</div>');
                            } else {
                                element.after('<div class=\"text-danger\">' + json.error.option[i] + '</div>');
                            }
                        }
                    }

                    if (json.error.recurring) {
                        \$('select[name=\\'recurring_id\\']').after('<div class=\"text-danger\">' + json['error']['recurring'] + '</div>');
                    }

                    // Highlight any found errors
                    \$('.text-danger').parent().addClass('has-error');
                }

                if (json.success_redirect) {

                    location = json.success_redirect;

                } else if (json.success) {

                    \$('.table-cell').removeClass('has-error');
                    \$('.alert, .popup-note, .basel-spinner.ajax-call, .text-danger').remove();

                    html = '<div class=\"popup-note\">';
                    html += '<div class=\"inner\">';
                    html += '<a class=\"popup-note-close\" onclick=\"\$(this).parent().parent().remove()\">&times;</a>';
                    html += '<div class=\"table\">';
                    html += '<div class=\"table-cell v-top img\"><img src=\"' + json.image + '\" /></div>';
                    html += '<div class=\"table-cell v-top\">' + json.success + '</div>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    \$('body').append(html);
                    setTimeout(function() {\$('.popup-note').hide();}, 8100);
                    // Need to set timeout otherwise it wont update the total
                    setTimeout(function () {
                        \$('.cart-total-items').html( json.total_items );
                        \$('.cart-total-amount').html( json.total_amount );
                    }, 100);

                    \$('#cart-content').load('index.php?route=common/cart/info #cart-content > *');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
        });
    });
    //--></script>
<script><!--
    \$('.date').datetimepicker({
        pickTime: false
    });

    \$('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });

    \$('.time').datetimepicker({
        pickDate: false
    });

    \$('button[id^=\\'button-upload\\']').on('click', function() {
        var node = this;

        \$('#form-upload').remove();

        \$('body').prepend('<form enctype=\"multipart/form-data\" id=\"form-upload\" style=\"display: none;\"><input type=\"file\" name=\"file\" /></form>');

        \$('#form-upload input[name=\\'file\\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function() {
            if (\$('#form-upload input[name=\\'file\\']').val() != '') {
                clearInterval(timer);

                \$.ajax({
                    url: 'index.php?route=tool/upload',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData(\$('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        \$(node).button('loading');
                    },
                    complete: function() {
                        \$(node).button('reset');
                    },
                    success: function(json) {
                        \$('.text-danger').remove();

                        if (json['error']) {
                            \$(node).parent().find('input').after('<div class=\"text-danger\">' + json['error'] + '</div>');
                        }

                        if (json['success']) {
                            alert(json['success']);

                            \$(node).parent().find('input').val(json['code']);
                        }
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                    }
                });
            }
        }, 500);
    });
    //--></script>
<script><!--
    \$('#review').delegate('.pagination a', 'click', function(e) {
        e.preventDefault();
        \$(\"html,body\").animate({scrollTop:((\$(\"#review\").offset().top)-50)},500);
        \$('#review').fadeOut(50);

        \$('#review').load(this.href);

        \$('#review').fadeIn(500);

    });


    \$('#button-review').on('click', function() {
        \$.ajax({
            url: 'index.php?route=product/product/write&product_id=";
        // line 857
        echo ($context["product_id"] ?? null);
        echo "',
            type: 'post',
            dataType: 'json',
            data: \$(\"#form-review\").serialize(),
            beforeSend: function() {
                \$('#button-review').button('loading');
            },
            complete: function() {
                \$('#button-review').button('reset');
            },
            success: function(json) {
                \$('.alert-success, .alert-danger').remove();

                if (json.error) {
                    \$('#review-notification').after('<div class=\"alert alert-sm alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ' + json.error + '</div>');
                }

                if (json.success) {
                    \$('#review-notification').after('<div class=\"alert alert-sm alert-success\"><i class=\"fa fa-check-circle\"></i> ' + json.success + '</div>');

                    \$('input[name=\\'name\\']').val('');
                    \$('textarea[name=\\'text\\']').val('');
                    \$('input[name=\\'rating\\']:checked').prop('checked', false);
                }
            }
        });
    });

    \$(document).ready(function() {
        ";
        // line 886
        if ((($context["product_layout"] ?? null) == "full-width")) {
            // line 887
            echo "// Sticky information
        \$('.table-cell.right .inner').theiaStickySidebar({containerSelector:'.product-info'});
        ";
        }
        // line 890
        echo "
// Reviews/Question scroll link
        \$(\".to_tabs\").click(function() {
            \$('html, body').animate({
                scrollTop: (\$(\".main_tabs\").offset().top - 100)
            }, 1000);
        });

// Sharing buttons
        ";
        // line 899
        if (($context["basel_share_btn"] ?? null)) {
            // line 900
            echo "        var share_url = encodeURIComponent(window.location.href);
        var page_title = '";
            // line 901
            echo ($context["heading_title"] ?? null);
            echo "';
        ";
            // line 902
            if (($context["thumb"] ?? null)) {
                // line 903
                echo "        var thumb = '";
                echo ($context["thumb"] ?? null);
                echo "';
        ";
            }
            // line 905
            echo "        \$('.fb_share').attr(\"href\", 'https://www.facebook.com/sharer/sharer.php?u=' + share_url + '');
        \$('.twitter_share').attr(\"href\", 'https://twitter.com/intent/tweet?source=' + share_url + '&text=' + page_title + ': ' + share_url + '');
        \$('.google_share').attr(\"href\", 'https://plus.google.com/share?url=' + share_url + '');
        \$('.pinterest_share').attr(\"href\", 'http://pinterest.com/pin/create/button/?url=' + share_url + '&media=' + thumb + '&description=' + page_title + '');
        \$('.vk_share').attr(\"href\", 'http://vkontakte.ru/share.php?url=' + share_url + '');
        ";
        }
        // line 911
        echo "    });
    //--></script>

";
        // line 914
        if ((($context["product_layout"] ?? null) != "full-width")) {
            // line 915
            echo "    <script>
        \$(document).ready(function() {
            \$('.image-additional a.link').click(function (e) {
                if (\$(this).hasClass(\"locked\")) {
                    e.stopImmediatePropagation();
                }
                \$('.image-additional a.link.active').removeClass('active');
                \$(this).addClass('active')
            });

            ";
            // line 925
            if (($context["images"] ?? null)) {
                // line 926
                echo "            \$('.cloud-zoom-wrap').click(function (e) {
                e.preventDefault();
                \$('.image-additional a.link.active').removeClass('locked').trigger('click').addClass('locked');
            });
            ";
            } else {
                // line 931
                echo "            \$('.cloud-zoom-wrap').click(function (e) {
                e.preventDefault();
                \$('#main-image').trigger('click');
            });
            ";
            }
            // line 936
            echo "
            \$('.image-additional').slick({
                prevArrow: \"<a class=\\\"icon-arrow-left\\\"></a>\",
                nextArrow: \"<a class=\\\"icon-arrow-right\\\"></a>\",
                appendArrows: '.image-additional .slick-list',
                arrows:true,
                ";
            // line 942
            if ((($context["direction"] ?? null) == "rtl")) {
                // line 943
                echo "                rtl: true,
                ";
            }
            // line 945
            echo "                infinite:false,
                ";
            // line 946
            if ((($context["product_layout"] ?? null) == "images-left")) {
                // line 947
                echo "                slidesToShow: ";
                echo twig_round((($context["img_h"] ?? null) / ($context["img_a_h"] ?? null)), 0, "floor");
                echo ",
                vertical:true,
                verticalSwiping:true,
                ";
            } else {
                // line 951
                echo "                slidesToShow: ";
                echo twig_round((($context["img_w"] ?? null) / ($context["img_a_w"] ?? null)));
                echo ",
                ";
            }
            // line 953
            echo "                responsive: [
                    {
                        breakpoint: 992,
                        settings: {
                            vertical:false,
                            verticalSwiping:false
                        }
                    }]
            });

        });
        //--></script>
";
        }
        // line 966
        echo "<script>
    \$(document).ready(function() {
// Image Gallery
        \$(\"#gallery\").lightGallery({
            selector: '.link',
            download:false,
            hideBarsDelay:99999
        });
    });
    //--></script>
<script type=\"application/ld+json\">
{
\"@context\": \"http://schema.org\",
\"@type\": \"Product\",
\"image\": [
";
        // line 981
        if (($context["thumb"] ?? null)) {
            // line 982
            echo "\"";
            echo ($context["thumb"] ?? null);
            echo "\"
";
        }
        // line 984
        echo "],
\"description\": \"";
        // line 985
        echo ($context["meta_description"] ?? null);
        echo "\",
";
        // line 986
        if (($context["review_qty"] ?? null)) {
            // line 987
            echo "\"aggregateRating\": {
\"@type\": \"AggregateRating\",
\"ratingValue\": \"";
            // line 989
            echo ($context["rating"] ?? null);
            echo "\",
\"reviewCount\": \"";
            // line 990
            echo ($context["review_qty"] ?? null);
            echo "\"},
";
        }
        // line 992
        echo "\"name\": \"";
        echo ($context["heading_title"] ?? null);
        echo "\",
\"sku\": \"";
        // line 993
        echo ($context["model"] ?? null);
        echo "\",
";
        // line 994
        if (($context["manufacturer"] ?? null)) {
            // line 995
            echo "\"brand\": \"";
            echo ($context["manufacturer"] ?? null);
            echo "\",
";
        }
        // line 997
        echo "\"offers\": {
\"@type\": \"Offer\",
";
        // line 999
        if ((($context["qty"] ?? null) > 0)) {
            // line 1000
            echo "\"availability\": \"http://schema.org/InStock\",
";
        } else {
            // line 1002
            echo "\"availability\": \"http://schema.org/OutOfStock\",
";
        }
        // line 1004
        if (($context["price"] ?? null)) {
            // line 1005
            echo "    ";
            if (($context["special"] ?? null)) {
                // line 1006
                echo "\"price\": \"";
                echo ($context["special_snippet"] ?? null);
                echo "\",
";
            } else {
                // line 1008
                echo "\"price\": \"";
                echo ($context["price_snippet"] ?? null);
                echo "\",
";
            }
            // line 1010
            echo "\"priceCurrency\": \"";
            echo ($context["currency_code"] ?? null);
            echo "\"
";
        }
        // line 1012
        echo "}
}
</script>
";
        // line 1015
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "basel/template/product/product.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  2255 => 1015,  2250 => 1012,  2244 => 1010,  2238 => 1008,  2232 => 1006,  2229 => 1005,  2227 => 1004,  2223 => 1002,  2219 => 1000,  2217 => 999,  2213 => 997,  2207 => 995,  2205 => 994,  2201 => 993,  2196 => 992,  2191 => 990,  2187 => 989,  2183 => 987,  2181 => 986,  2177 => 985,  2174 => 984,  2168 => 982,  2166 => 981,  2149 => 966,  2134 => 953,  2128 => 951,  2120 => 947,  2118 => 946,  2115 => 945,  2111 => 943,  2109 => 942,  2101 => 936,  2094 => 931,  2087 => 926,  2085 => 925,  2073 => 915,  2071 => 914,  2066 => 911,  2058 => 905,  2052 => 903,  2050 => 902,  2046 => 901,  2043 => 900,  2041 => 899,  2030 => 890,  2025 => 887,  2023 => 886,  1991 => 857,  1814 => 682,  1806 => 677,  1802 => 676,  1798 => 675,  1794 => 674,  1790 => 673,  1785 => 671,  1781 => 669,  1779 => 668,  1776 => 667,  1768 => 661,  1764 => 659,  1761 => 658,  1757 => 656,  1755 => 655,  1752 => 654,  1750 => 653,  1747 => 652,  1745 => 651,  1742 => 650,  1740 => 649,  1737 => 648,  1733 => 646,  1731 => 645,  1724 => 640,  1722 => 639,  1719 => 638,  1713 => 636,  1711 => 635,  1705 => 631,  1698 => 627,  1695 => 626,  1693 => 625,  1689 => 623,  1685 => 621,  1683 => 620,  1678 => 618,  1675 => 617,  1670 => 614,  1656 => 613,  1653 => 612,  1636 => 611,  1632 => 610,  1625 => 606,  1620 => 603,  1618 => 602,  1615 => 601,  1611 => 599,  1609 => 598,  1598 => 589,  1594 => 587,  1592 => 586,  1583 => 579,  1576 => 575,  1570 => 573,  1561 => 569,  1552 => 563,  1543 => 557,  1538 => 555,  1530 => 550,  1526 => 549,  1502 => 528,  1497 => 525,  1495 => 524,  1491 => 523,  1483 => 517,  1477 => 515,  1474 => 514,  1468 => 512,  1465 => 511,  1455 => 507,  1447 => 502,  1440 => 500,  1435 => 497,  1430 => 496,  1428 => 495,  1422 => 492,  1417 => 489,  1415 => 488,  1412 => 487,  1406 => 484,  1403 => 483,  1401 => 482,  1398 => 481,  1393 => 478,  1386 => 476,  1377 => 473,  1373 => 472,  1370 => 471,  1366 => 470,  1359 => 466,  1355 => 464,  1351 => 463,  1347 => 461,  1345 => 460,  1342 => 459,  1339 => 458,  1330 => 455,  1325 => 454,  1320 => 453,  1318 => 452,  1312 => 449,  1305 => 444,  1297 => 442,  1294 => 441,  1288 => 439,  1285 => 438,  1279 => 436,  1276 => 435,  1273 => 434,  1262 => 432,  1257 => 431,  1255 => 430,  1251 => 429,  1247 => 428,  1240 => 423,  1236 => 421,  1231 => 418,  1229 => 417,  1226 => 416,  1219 => 412,  1216 => 411,  1214 => 410,  1206 => 404,  1203 => 403,  1197 => 399,  1192 => 396,  1189 => 395,  1187 => 394,  1184 => 393,  1165 => 391,  1163 => 390,  1160 => 389,  1152 => 387,  1150 => 386,  1143 => 384,  1140 => 383,  1130 => 381,  1128 => 380,  1117 => 378,  1114 => 377,  1106 => 375,  1104 => 374,  1095 => 367,  1089 => 365,  1087 => 364,  1081 => 363,  1075 => 362,  1069 => 358,  1063 => 356,  1061 => 355,  1058 => 354,  1054 => 349,  1047 => 348,  1038 => 347,  1035 => 346,  1031 => 344,  1028 => 343,  1017 => 341,  1013 => 340,  1007 => 339,  1003 => 337,  997 => 333,  986 => 331,  982 => 330,  978 => 329,  972 => 326,  969 => 325,  967 => 324,  964 => 323,  959 => 320,  952 => 319,  938 => 312,  929 => 308,  921 => 306,  919 => 305,  916 => 304,  902 => 297,  893 => 293,  885 => 291,  883 => 290,  880 => 289,  866 => 282,  857 => 278,  849 => 276,  847 => 275,  844 => 274,  835 => 270,  827 => 269,  821 => 266,  813 => 264,  811 => 263,  808 => 262,  795 => 258,  787 => 255,  779 => 253,  777 => 252,  774 => 251,  761 => 247,  753 => 244,  745 => 242,  743 => 241,  739 => 239,  733 => 235,  724 => 231,  717 => 229,  715 => 228,  711 => 227,  708 => 226,  686 => 224,  684 => 223,  678 => 222,  670 => 220,  666 => 219,  662 => 218,  656 => 215,  648 => 213,  646 => 212,  643 => 211,  637 => 207,  628 => 203,  621 => 201,  619 => 200,  615 => 199,  612 => 198,  590 => 196,  588 => 195,  582 => 194,  574 => 192,  570 => 191,  566 => 190,  560 => 187,  552 => 185,  550 => 184,  547 => 183,  541 => 179,  534 => 177,  527 => 175,  525 => 174,  518 => 173,  514 => 172,  510 => 171,  504 => 170,  496 => 167,  488 => 165,  486 => 164,  483 => 163,  479 => 162,  476 => 161,  474 => 160,  468 => 156,  462 => 154,  460 => 153,  457 => 152,  449 => 150,  447 => 149,  444 => 148,  440 => 147,  436 => 145,  424 => 143,  420 => 142,  417 => 141,  415 => 140,  411 => 138,  402 => 135,  396 => 133,  394 => 132,  391 => 131,  389 => 130,  386 => 129,  381 => 127,  374 => 123,  371 => 122,  369 => 121,  363 => 118,  356 => 113,  349 => 108,  345 => 106,  331 => 104,  328 => 103,  307 => 100,  304 => 99,  300 => 98,  297 => 97,  295 => 96,  292 => 95,  270 => 92,  267 => 91,  261 => 89,  259 => 88,  256 => 87,  250 => 85,  248 => 84,  245 => 83,  239 => 81,  237 => 80,  233 => 78,  231 => 77,  223 => 75,  221 => 74,  212 => 68,  207 => 67,  204 => 66,  201 => 65,  198 => 64,  195 => 63,  192 => 62,  189 => 61,  187 => 60,  183 => 59,  178 => 57,  174 => 55,  163 => 53,  159 => 52,  155 => 50,  149 => 46,  143 => 44,  138 => 42,  133 => 41,  131 => 40,  125 => 37,  121 => 35,  115 => 33,  109 => 31,  107 => 30,  102 => 27,  96 => 25,  94 => 24,  90 => 22,  84 => 20,  79 => 18,  74 => 17,  72 => 16,  66 => 13,  62 => 11,  56 => 9,  50 => 7,  48 => 6,  44 => 4,  42 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "basel/template/product/product.twig", "");
    }
}
