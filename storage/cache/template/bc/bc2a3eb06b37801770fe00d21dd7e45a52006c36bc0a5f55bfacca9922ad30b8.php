<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* basel/template/product/product.twig */
class __TwigTemplate_98f408dcd47ae49860150ef9702cd744426ab89b35c4f064c4526ca7868ec1dc extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo "

";
        // line 3
        if ((($context["product_layout"] ?? null) != "full-width")) {
            // line 4
            echo "    <style>
        .product-page .image-area {
        ";
            // line 6
            if (((($context["product_layout"] ?? null) == "images-left") && ($context["images"] ?? null))) {
                // line 7
                echo "            width: ";
                echo ((($context["img_w"] ?? null) + ($context["img_a_w"] ?? null)) + 20);
                echo "px;
        ";
            } else {
                // line 9
                echo "            width: ";
                echo ($context["img_w"] ?? null);
                echo "px;
        ";
            }
            // line 11
            echo "        }
        .product-page .main-image {
            width:";
            // line 13
            echo ($context["img_w"] ?? null);
            echo "px;
        }
        .product-page .image-additional {
        ";
            // line 16
            if ((($context["product_layout"] ?? null) == "images-left")) {
                // line 17
                echo "            width: ";
                echo ($context["img_a_w"] ?? null);
                echo "px;
            height: ";
                // line 18
                echo ($context["img_h"] ?? null);
                echo "px;
        ";
            } else {
                // line 20
                echo "            width: ";
                echo ($context["img_w"] ?? null);
                echo "px;
        ";
            }
            // line 22
            echo "        }
        .product-page .image-additional.has-arrows {
        ";
            // line 24
            if ((($context["product_layout"] ?? null) == "images-left")) {
                // line 25
                echo "            height: ";
                echo (($context["img_h"] ?? null) - 40);
                echo "px;
        ";
            }
            // line 27
            echo "        }
        @media (min-width: 992px) and (max-width: 1199px) {
            .product-page .image-area {
            ";
            // line 30
            if ((($context["product_layout"] ?? null) == "images-left")) {
                // line 31
                echo "                width: ";
                echo (((($context["img_w"] ?? null) + ($context["img_a_w"] ?? null)) / 1.25) + 20);
                echo "px;
            ";
            } else {
                // line 33
                echo "                width: ";
                echo (($context["img_w"] ?? null) / 1.25);
                echo "px;
            ";
            }
            // line 35
            echo "            }
            .product-page .main-image {
                width:";
            // line 37
            echo (($context["img_w"] ?? null) / 1.25);
            echo "px;
            }
            .product-page .image-additional {
            ";
            // line 40
            if ((($context["product_layout"] ?? null) == "images-left")) {
                // line 41
                echo "                width: ";
                echo (($context["img_a_w"] ?? null) / 1.25);
                echo "px;
                height: ";
                // line 42
                echo (($context["img_h"] ?? null) / 1.25);
                echo "px;
            ";
            } else {
                // line 44
                echo "                width: ";
                echo (($context["img_w"] ?? null) / 1.25);
                echo "px;
            ";
            }
            // line 46
            echo "            }
        }
    </style>
";
        }
        // line 50
        echo "
<ul class=\"breadcrumb\">
    ";
        // line 52
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 53
            echo "        <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 53);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 53);
            echo "</a></li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "</ul>

<div class=\"container product-layout ";
        // line 57
        echo ($context["product_layout"] ?? null);
        echo "\">

    <div class=\"row\">";
        // line 59
        echo ($context["column_left"] ?? null);
        echo "
        ";
        // line 60
        if ((($context["column_left"] ?? null) && ($context["column_right"] ?? null))) {
            // line 61
            echo "            ";
            $context["class"] = "col-sm-6";
            // line 62
            echo "        ";
        } elseif ((($context["column_left"] ?? null) || ($context["column_right"] ?? null))) {
            // line 63
            echo "            ";
            $context["class"] = "col-md-9 col-sm-8";
            // line 64
            echo "        ";
        } else {
            // line 65
            echo "            ";
            $context["class"] = "col-sm-12";
            // line 66
            echo "        ";
        }
        // line 67
        echo "        <div id=\"content\" class=\"product-main no-min-height ";
        echo ($context["class"] ?? null);
        echo "\">
            ";
        // line 68
        echo ($context["content_top"] ?? null);
        echo "

            <div class=\"table product-info product-page\">

                <div class=\"table-cell left\">

                    ";
        // line 74
        if ((($context["thumb"] ?? null) || ($context["images"] ?? null))) {
            // line 75
            echo "                    <div class=\"image-area ";
            if ( !($context["hover_zoom"] ?? null)) {
                echo "hover-zoom-disabled";
            }
            echo "\" id=\"gallery\">

                        ";
            // line 77
            if (($context["thumb"] ?? null)) {
                // line 78
                echo "                            <div class=\"main-image\">

                                ";
                // line 80
                if (((($context["price"] ?? null) && ($context["special"] ?? null)) && ($context["sale_badge"] ?? null))) {
                    // line 81
                    echo "                                    <span class=\"badge sale_badge\"><i>";
                    echo ($context["sale_badge"] ?? null);
                    echo "</i></span>
                                ";
                }
                // line 83
                echo "
                                ";
                // line 84
                if (($context["is_new"] ?? null)) {
                    // line 85
                    echo "                                    <span class=\"badge new_badge\"><i>";
                    echo ($context["basel_text_new"] ?? null);
                    echo "</i></span>
                                ";
                }
                // line 87
                echo "
                                ";
                // line 88
                if (((($context["qty"] ?? null) < 1) && ($context["stock_badge_status"] ?? null))) {
                    // line 89
                    echo "                                    <span class=\"badge out_of_stock_badge\"><i>";
                    echo ($context["basel_text_out_of_stock"] ?? null);
                    echo "</i></span>
                                ";
                }
                // line 91
                echo "
                                <a class=\"";
                // line 92
                if ( !($context["images"] ?? null)) {
                    echo "link cloud-zoom";
                }
                echo " ";
                if ((($context["product_layout"] ?? null) == "full-width")) {
                    echo "link";
                } else {
                    echo "cloud-zoom";
                }
                echo "\" id=\"main-image\" href=\"";
                echo ($context["popup"] ?? null);
                echo "\" rel=\"position:'inside', showTitle: false\"><img src=\"";
                echo ($context["thumb"] ?? null);
                echo "\" title=\"";
                echo ($context["heading_title"] ?? null);
                echo "\" alt=\"";
                echo ($context["heading_title"] ?? null);
                echo "\" /></a>
                            </div>
                        ";
            }
            // line 95
            echo "
                        ";
            // line 96
            if (($context["images"] ?? null)) {
                // line 97
                echo "                            <ul class=\"image-additional\">
                                ";
                // line 98
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["images"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                    // line 99
                    echo "                                    <li>
                                        <a class=\"link ";
                    // line 100
                    if ((($context["product_layout"] ?? null) != "full-width")) {
                        echo "cloud-zoom-gallery locked";
                    }
                    echo "\" href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["image"], "popup", [], "any", false, false, false, 100);
                    echo "\" rel=\"useZoom: 'main-image', smallImage: '";
                    echo twig_get_attribute($this->env, $this->source, $context["image"], "thumb_lg", [], "any", false, false, false, 100);
                    echo "'\"><img src=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["image"], "thumb", [], "any", false, false, false, 100);
                    echo "\" title=\"";
                    echo ($context["heading_title"] ?? null);
                    echo "\" alt=\"";
                    echo ($context["heading_title"] ?? null);
                    echo "\" /></a>
                                    </li>
                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 103
                echo "                                ";
                if ((($context["thumb"] ?? null) && (($context["product_layout"] ?? null) != "full-width"))) {
                    // line 104
                    echo "                                    <li><a class=\"link cloud-zoom-gallery locked active\" href=\"";
                    echo ($context["popup"] ?? null);
                    echo "\" rel=\"useZoom: 'main-image', smallImage: '";
                    echo ($context["thumb"] ?? null);
                    echo "'\"><img src=\"";
                    echo ($context["thumb_sm"] ?? null);
                    echo "\" title=\"";
                    echo ($context["heading_title"] ?? null);
                    echo "\" alt=\"";
                    echo ($context["heading_title"] ?? null);
                    echo "\" /></a></li>
                                ";
                }
                // line 106
                echo "                            </ul>
                        ";
            }
            // line 108
            echo "
                        <div class=\"shipnotice\">
                            <div class=\"alert alert-shipping\">
                                <i class=\"fa fa-truck\"></i> Brza zagrebačka dostava <span>Između 11h i 21h istoga dana
                 </span>
                            </div>
                        </div>

                    </div> <!-- .table-cell.left ends -->

                </div> <!-- .image-area ends -->
                ";
        }
        // line 120
        echo "
                <div class=\"table-cell w100 right\">
                    <div class=\"inner\">

                        <div class=\"product-h1\">
                            <h1 id=\"page-title\">";
        // line 125
        echo ($context["heading_title"] ?? null);
        echo "</h1>
                        </div>

                        ";
        // line 128
        if ((($context["review_status"] ?? null) && (($context["review_qty"] ?? null) > 0))) {
            // line 129
            echo "                            <div class=\"rating\">
    <span class=\"rating_stars rating r";
            // line 130
            echo ($context["rating"] ?? null);
            echo "\">
    <i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>
    </span>
                            </div>
                            <span class=\"review_link\">(<a class=\"hover_uline to_tabs\" onclick=\"\$('a[href=\\'#tab-review\\']').trigger('click'); return false;\">";
            // line 134
            echo ($context["reviews"] ?? null);
            echo "</a>)</span>
                        ";
        }
        // line 136
        echo "
                        ";
        // line 137
        if (($context["price"] ?? null)) {
            // line 138
            echo "                            <ul class=\"list-unstyled price\">
                                ";
            // line 139
            if ( !($context["special"] ?? null)) {
                // line 140
                echo "                                    <li><span class=\"live-price\">";
                echo ($context["price"] ?? null);
                echo "<span></li>
                                ";
            } else {
                // line 142
                echo "                                    <li><span class=\"price-old\">";
                echo ($context["price"] ?? null);
                echo "</span><span class=\"live-price-new\">";
                echo ($context["special"] ?? null);
                echo "<span></li>
                                    <span id=\"special_countdown\"></span>
                                ";
            }
            // line 145
            echo "                            </ul>

                            ";
            // line 147
            if (($context["discounts"] ?? null)) {
                // line 148
                echo "                                <p class=\"discount\">
                                    ";
                // line 149
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["discounts"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["discount"]) {
                    // line 150
                    echo "                                        <span>";
                    echo twig_get_attribute($this->env, $this->source, $context["discount"], "quantity", [], "any", false, false, false, 150);
                    echo ($context["text_discount"] ?? null);
                    echo "<i class=\"price\">";
                    echo twig_get_attribute($this->env, $this->source, $context["discount"], "price", [], "any", false, false, false, 150);
                    echo "</i></span>
                                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['discount'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 152
                echo "                                </p>
                            ";
            }
            // line 154
            echo "
                        ";
        }
        // line 155
        echo " <!-- if price ends -->
                        ";
        // line 156
        if ((($context["price"] ?? null) && ($context["tax"] ?? null))) {
            // line 157
            echo "                            <p class=\"info p-tax\"><b>";
            echo ($context["text_tax"] ?? null);
            echo "</b> <span class=\"live-price-tax\">";
            echo ($context["tax"] ?? null);
            echo "</span></p>
                        ";
        }
        // line 159
        echo "
                        ";
        // line 160
        if ((($context["meta_description_status"] ?? null) && ($context["meta_description"] ?? null))) {
            // line 161
            echo "                            <p class=\"meta_description\">";
            echo ($context["meta_description"] ?? null);
            echo "</p>
                        ";
        }
        // line 163
        echo "

                        <div id=\"product\">

                            ";
        // line 167
        if (($context["options"] ?? null)) {
            // line 168
            echo "                                <div class=\"options \">
                                    ";
            // line 169
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["options"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
                // line 170
                echo "
                                        ";
                // line 171
                if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 171) == "select")) {
                    // line 172
                    echo "                                            <div class=\"form-group";
                    if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 172)) {
                        echo " required";
                    }
                    echo " row nbmargin\">
                                                <div class=\" name col-md-12\">
                                                    <label class=\"control-label\" for=\"input-option";
                    // line 174
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 174);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 174);
                    echo "</label>
                                                </div>
                                                <div class=\" col-md-12\">
                                                    <select name=\"option[";
                    // line 177
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 177);
                    echo "]\" id=\"input-option";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 177);
                    echo "\" class=\"form-control kolicina\">
                                                        <option value=\"\">";
                    // line 178
                    echo ($context["text_select"] ?? null);
                    echo "</option>
                                                        ";
                    // line 179
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["option"], "product_option_value", [], "any", false, false, false, 179));
                    foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                        // line 180
                        echo "                                                            <option value=\"";
                        echo twig_get_attribute($this->env, $this->source, $context["option_value"], "product_option_value_id", [], "any", false, false, false, 180);
                        echo "\">";
                        echo twig_get_attribute($this->env, $this->source, $context["option_value"], "name", [], "any", false, false, false, 180);
                        echo "
                                                                ";
                        // line 181
                        if (twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 181)) {
                            // line 182
                            echo "                                                                    (";
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price_prefix", [], "any", false, false, false, 182);
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 182);
                            echo ")
                                                                ";
                        }
                        // line 184
                        echo "                                                            </option>
                                                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 186
                    echo "                                                    </select>
                                                </div>
                                            </div>
                                        ";
                }
                // line 190
                echo "


                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 193
            echo " <!-- foreach option -->
                                </div>
                            ";
        }
        // line 196
        echo "
                            ";
        // line 197
        if (($context["recurrings"] ?? null)) {
            // line 198
            echo "                                <hr>
                                <h3>";
            // line 199
            echo ($context["text_payment_recurring"] ?? null);
            echo "</h3>
                                <div class=\"form-group required\">
                                    <select name=\"recurring_id\" class=\"form-control\">
                                        <option value=\"\">";
            // line 202
            echo ($context["text_select"] ?? null);
            echo "</option>
                                        ";
            // line 203
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["recurrings"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["recurring"]) {
                // line 204
                echo "                                            <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["recurring"], "recurring_id", [], "any", false, false, false, 204);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["recurring"], "name", [], "any", false, false, false, 204);
                echo "</option>
                                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['recurring'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 206
            echo "                                    </select>
                                    <div class=\"help-block\" id=\"recurring-description\"></div>
                                </div>
                            ";
        }
        // line 210
        echo "
                            <div class=\"form-group buy catalog_hide \">

                                <!-- <input type=\"number\" step=\"1\" min=\"";
        // line 213
        echo ($context["minimum"] ?? null);
        echo "\" name=\"quantity\" value=\"";
        echo ($context["minimum"] ?? null);
        echo "\" id=\"input-quantity\" class=\"form-control input-quantity\" /> -->
                                <input type=\"hidden\" name=\"product_id\" value=\"";
        // line 214
        echo ($context["product_id"] ?? null);
        echo "\" />
                                <button type=\"button\" id=\"button-cart\" data-loading-text=\"";
        // line 215
        echo ($context["text_loading"] ?? null);
        echo "\" class=\"btn btn-primary\">";
        if (((($context["qty"] ?? null) < 1) && ($context["stock_badge_status"] ?? null))) {
            echo ($context["basel_text_out_of_stock"] ?? null);
        } else {
            echo "<i id=\"cart-icon\" class=\"global-cart icon\"></i> ";
            echo ($context["button_cart"] ?? null);
        }
        echo "</button>
                            </div>
                            ";
        // line 217
        if ((($context["minimum"] ?? null) > 1)) {
            // line 218
            echo "                                <div class=\"alert alert-sm alert-info\"><i class=\"fa fa-info-circle\"></i> ";
            echo ($context["text_minimum"] ?? null);
            echo "</div>
                            ";
        }
        // line 220
        echo "
                        </div> <!-- #product ends -->


                        <p class=\"info is_wishlist\"><a onclick=\"wishlist.add('";
        // line 224
        echo ($context["product_id"] ?? null);
        echo "');\"><i class=\"icon-heart\"></i> ";
        echo ($context["button_wishlist"] ?? null);
        echo "</a></p>
                        <p class=\"info is_compare\"><a onclick=\"compare.add('";
        // line 225
        echo ($context["product_id"] ?? null);
        echo "');\"><i class=\"icon-refresh\"></i> ";
        echo ($context["button_compare"] ?? null);
        echo "</a></p>
                        ";
        // line 226
        if (($context["question_status"] ?? null)) {
            // line 227
            echo "                            <p class=\"info is_ask\"><a class=\"to_tabs\" onclick=\"\$('a[href=\\'#tab-questions\\']').trigger('click'); return false;\"><i class=\"icon-question\"></i> ";
            echo ($context["basel_button_ask"] ?? null);
            echo "</a></p>
                        ";
        }
        // line 229
        echo "
                        <div class=\"clearfix\"></div>

                        <div class=\"info-holder\">



                            ";
        // line 236
        if ((($context["price"] ?? null) && ($context["points"] ?? null))) {
            // line 237
            echo "                                <p class=\"info\"><b>";
            echo ($context["text_points"] ?? null);
            echo "</b> ";
            echo ($context["points"] ?? null);
            echo "</p>
                            ";
        }
        // line 239
        echo "
                            <p class=\"info ";
        // line 240
        if ((($context["qty"] ?? null) > 0)) {
            echo "in_stock";
        }
        echo "\"><b>";
        echo ($context["text_stock"] ?? null);
        echo "</b> ";
        echo ($context["stock"] ?? null);
        echo "</p>

                            ";
        // line 242
        if (($context["manufacturer"] ?? null)) {
            // line 243
            echo "                                <p class=\"info\"><b>";
            echo ($context["text_manufacturer"] ?? null);
            echo "</b> <a class=\"hover_uline\" href=\"";
            echo ($context["manufacturers"] ?? null);
            echo "\">";
            echo ($context["manufacturer"] ?? null);
            echo "</a></p>
                            ";
        }
        // line 245
        echo "
                            <p class=\"info\"><b>";
        // line 246
        echo ($context["text_model"] ?? null);
        echo "</b> ";
        echo ($context["model"] ?? null);
        echo "</p>

                            ";
        // line 248
        if (($context["reward"] ?? null)) {
            // line 249
            echo "                                <p class=\"info\"><b>";
            echo ($context["text_reward"] ?? null);
            echo "</b> ";
            echo ($context["reward"] ?? null);
            echo "</p>
                            ";
        }
        // line 251
        echo "
                            ";
        // line 252
        if (($context["tags"] ?? null)) {
            // line 253
            echo "                                <p class=\"info tags\"><b>";
            echo ($context["text_tags"] ?? null);
            echo "</b> &nbsp;<span>";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["tags"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["tag"]) {
                echo "<a class=\"hover_uline\" href=\"";
                echo twig_get_attribute($this->env, $this->source, $context["tag"], "href", [], "any", false, false, false, 253);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["tag"], "tag", [], "any", false, false, false, 253);
                echo "</a>";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tag'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo "</span></p>
                            ";
        }
        // line 255
        echo "
                            ";
        // line 256
        if (($context["basel_share_btn"] ?? null)) {
            // line 257
            echo "                                ";
            if ((($context["basel_sharing_style"] ?? null) == "large")) {
                // line 258
                echo "                                    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5f1aa4948aa12700134b0c47&product=inline-share-buttons' async='async'></script>
                                    <!-- ShareThis BEGIN --><div class=\"sharethis-inline-share-buttons\"></div><!-- ShareThis END -->
                                ";
            } else {
                // line 261
                echo "                                <hr>
                                    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5f1aa4948aa12700134b0c47&product=inline-share-buttons' async='async'></script>
                                    <!-- ShareThis BEGIN --><div class=\"sharethis-inline-share-buttons\"></div><!-- ShareThis END -->
                                ";
            }
            // line 265
            echo "                            ";
        }
        // line 266
        echo "
                        </div> <!-- .info-holder ends -->

                    </div> <!-- .inner ends -->


                    ";
        // line 272
        if (($context["full_width_tabs"] ?? null)) {
            // line 273
            echo "                </div> <!-- main column ends -->
                ";
            // line 274
            echo ($context["column_right"] ?? null);
            echo "
            </div> <!-- .row ends -->
        </div> <!-- .container ends -->
        ";
        }
        // line 278
        echo "
        ";
        // line 279
        if (($context["full_width_tabs"] ?? null)) {
            // line 280
            echo "        <div class=\"outer-container product-tabs-wrapper\">
            <div class=\"container\">
                ";
        } else {
            // line 283
            echo "                <div class=\"inline-tabs\">
                    ";
        }
        // line 285
        echo "
                    <!-- Tabs area start -->
                    <div class=\"row\">
                        <div class=\"col-sm-12\">

                            <ul class=\"nav nav-tabs ";
        // line 290
        echo ($context["product_tabs_style"] ?? null);
        echo " main_tabs\">
                                <li class=\"active\"><a href=\"#tab-description\" data-toggle=\"tab\">";
        // line 291
        echo ($context["tab_description"] ?? null);
        echo "</a></li>
                                ";
        // line 292
        if (($context["product_tabs"] ?? null)) {
            // line 293
            echo "                                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["product_tabs"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["tab"]) {
                // line 294
                echo "                                        <li><a href=\"#custom-tab-";
                echo twig_get_attribute($this->env, $this->source, $context["tab"], "tab_id", [], "any", false, false, false, 294);
                echo "\" data-toggle=\"tab\">";
                echo twig_get_attribute($this->env, $this->source, $context["tab"], "name", [], "any", false, false, false, 294);
                echo "</a></li>
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tab'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 296
            echo "                                ";
        }
        // line 297
        echo "                                ";
        if (($context["attribute_groups"] ?? null)) {
            // line 298
            echo "                                    <li><a href=\"#tab-specification\" data-toggle=\"tab\">";
            echo ($context["tab_attribute"] ?? null);
            echo "</a></li>
                                ";
        }
        // line 300
        echo "                                ";
        if (($context["review_status"] ?? null)) {
            // line 301
            echo "                                    <li><a href=\"#tab-review\" data-toggle=\"tab\">";
            echo ($context["tab_review"] ?? null);
            echo "</a></li>
                                ";
        }
        // line 303
        echo "                                ";
        if (($context["question_status"] ?? null)) {
            // line 304
            echo "                                    <li><a href=\"#tab-questions\" data-toggle=\"tab\">";
            echo ($context["basel_tab_questions"] ?? null);
            echo " (";
            echo ($context["questions_total"] ?? null);
            echo ")</a></li>
                                ";
        }
        // line 306
        echo "                            </ul>

                            <div class=\"tab-content\">

                                <div class=\"tab-pane active\" id=\"tab-description\">
                                    ";
        // line 311
        echo ($context["description"] ?? null);
        echo "
                                </div>

                                ";
        // line 314
        if (($context["product_tabs"] ?? null)) {
            // line 315
            echo "                                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["product_tabs"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["tab"]) {
                // line 316
                echo "                                        <div class=\"tab-pane\" id=\"custom-tab-";
                echo twig_get_attribute($this->env, $this->source, $context["tab"], "tab_id", [], "any", false, false, false, 316);
                echo "\">
                                            ";
                // line 317
                echo twig_get_attribute($this->env, $this->source, $context["tab"], "description", [], "any", false, false, false, 317);
                echo "
                                        </div>
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tab'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 320
            echo "                                ";
        }
        // line 321
        echo "
                                ";
        // line 322
        if (($context["attribute_groups"] ?? null)) {
            // line 323
            echo "                                    <div class=\"tab-pane\" id=\"tab-specification\">
                                        <table class=\"table specification\">
                                            ";
            // line 325
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["attribute_groups"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["attribute_group"]) {
                // line 326
                echo "                                                <thead>
                                                <tr>
                                                    <td colspan=\"2\">";
                // line 328
                echo twig_get_attribute($this->env, $this->source, $context["attribute_group"], "name", [], "any", false, false, false, 328);
                echo "</td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                ";
                // line 332
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["attribute_group"], "attribute", [], "any", false, false, false, 332));
                foreach ($context['_seq'] as $context["_key"] => $context["attribute"]) {
                    // line 333
                    echo "                                                    <tr>
                                                        <td class=\"text-left\"><b>";
                    // line 334
                    echo twig_get_attribute($this->env, $this->source, $context["attribute"], "name", [], "any", false, false, false, 334);
                    echo "</b></td>
                                                        <td class=\"text-right\">";
                    // line 335
                    echo twig_get_attribute($this->env, $this->source, $context["attribute"], "text", [], "any", false, false, false, 335);
                    echo "</td>
                                                    </tr>
                                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attribute'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 338
                echo "                                                </tbody>
                                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attribute_group'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 340
            echo "                                        </table>
                                    </div>
                                ";
        }
        // line 343
        echo "
                                ";
        // line 344
        if (($context["question_status"] ?? null)) {
            // line 345
            echo "                                    <div class=\"tab-pane\" id=\"tab-questions\">
                                        ";
            // line 346
            echo ($context["product_questions"] ?? null);
            echo "
                                    </div>
                                ";
        }
        // line 349
        echo "
                                ";
        // line 350
        if (($context["review_status"] ?? null)) {
            // line 351
            echo "                                <div class=\"tab-pane\" id=\"tab-review\">
                                    <div class=\"row\">
                                        <div class=\"col-sm-6\">
                                            <h4><b>";
            // line 354
            echo ($context["button_reviews"] ?? null);
            echo "</b></h4>

                                            <div id=\"review\">
                                                ";
            // line 357
            if (($context["seo_reviews"] ?? null)) {
                // line 358
                echo "                                                    ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["seo_reviews"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["review"]) {
                    // line 359
                    echo "                                                        <div class=\"table\">
                                                            <div class=\"table-cell\"><i class=\"fa fa-user\"></i></div>
                                                            <div class=\"table-cell right\">
                                                                <p class=\"author\"><b>";
                    // line 362
                    echo twig_get_attribute($this->env, $this->source, $context["review"], "author", [], "any", false, false, false, 362);
                    echo "</b>  -  ";
                    echo twig_get_attribute($this->env, $this->source, $context["review"], "date_added", [], "any", false, false, false, 362);
                    echo "
                                                                    <span class=\"rating\">
\t\t<span class=\"rating_stars rating r";
                    // line 364
                    echo twig_get_attribute($this->env, $this->source, $context["review"], "rating", [], "any", false, false, false, 364);
                    echo "\">
\t\t<i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i>
\t\t</span>
\t\t</span>
                                                                </p>
                                                                ";
                    // line 369
                    echo twig_get_attribute($this->env, $this->source, $context["review"], "text", [], "any", false, false, false, 369);
                    echo "
                                                            </div>
                                                        </div>
                                                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['review'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 373
                echo "                                                    ";
                if (($context["pagination"] ?? null)) {
                    // line 374
                    echo "                                                        <div class=\"pagination-holder\">";
                    echo ($context["pagination"] ?? null);
                    echo "</div>
                                                    ";
                }
                // line 376
                echo "                                                ";
            } else {
                // line 377
                echo "                                                    <p>";
                echo ($context["text_no_reviews"] ?? null);
                echo "</p>
                                                ";
            }
            // line 379
            echo "                                            </div>

                                        </div>
                                        <div class=\"col-sm-6 right\">
                                            <form class=\"form-horizontal\" id=\"form-review\">

                                                <h4 id=\"review-notification\"><b>";
            // line 385
            echo ($context["text_write"] ?? null);
            echo "</b></h4>
                                                ";
            // line 386
            if (($context["review_guest"] ?? null)) {
                // line 387
                echo "
                                                    <div class=\"form-group required\">
                                                        <div class=\"col-sm-12 rating-stars\">
                                                            <label class=\"control-label\">";
                // line 390
                echo ($context["entry_rating"] ?? null);
                echo "</label>

                                                            <input type=\"radio\" value=\"1\" name=\"rating\" id=\"rating1\" />
                                                            <label for=\"rating1\"><i class=\"fa fa-star-o\"></i></label>

                                                            <input type=\"radio\" value=\"2\" name=\"rating\" id=\"rating2\" />
                                                            <label for=\"rating2\"><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i></label>

                                                            <input type=\"radio\" value=\"3\" name=\"rating\" id=\"rating3\" />
                                                            <label for=\"rating3\"><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i></label>

                                                            <input type=\"radio\" value=\"4\" name=\"rating\" id=\"rating4\" />
                                                            <label for=\"rating4\"><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i></label>

                                                            <input type=\"radio\" value=\"5\" name=\"rating\" id=\"rating5\" />
                                                            <label for=\"rating5\"><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i><i class=\"fa fa-star-o\"></i></label>
                                                        </div>
                                                    </div>

                                                    <div class=\"form-group required\">
                                                        <div class=\"col-sm-12\">
                                                            <label class=\"control-label\" for=\"input-name\">";
                // line 411
                echo ($context["entry_name"] ?? null);
                echo "</label>
                                                            <input type=\"text\" name=\"name\" value=\"";
                // line 412
                echo ($context["customer_name"] ?? null);
                echo "\" id=\"input-name\" class=\"form-control grey\" />
                                                        </div>
                                                    </div>
                                                    <div class=\"form-group required\">
                                                        <div class=\"col-sm-12\">
                                                            <label class=\"control-label\" for=\"input-review\">";
                // line 417
                echo ($context["entry_review"] ?? null);
                echo "</label>
                                                            <textarea name=\"text\" rows=\"5\" id=\"input-review\" class=\"form-control grey\"></textarea>
                                                            <small>";
                // line 419
                echo ($context["text_note"] ?? null);
                echo "</small>
                                                        </div>
                                                    </div>

                                                    <div class=\"form-group required\">
                                                        <div class=\"col-sm-12\">
                                                            ";
                // line 425
                echo ($context["captcha"] ?? null);
                echo "
                                                        </div>
                                                    </div>

                                                    <div class=\"buttons clearfix\">
                                                        <div class=\"text-right\">
                                                            <button type=\"button\" id=\"button-review\" data-loading-text=\"";
                // line 431
                echo ($context["text_loading"] ?? null);
                echo "\" class=\"btn btn-outline\">";
                echo ($context["button_continue"] ?? null);
                echo "</button>
                                                        </div>
                                                    </div>
                                                ";
            } else {
                // line 435
                echo "                                                    ";
                echo ($context["text_login"] ?? null);
                echo "
                                                ";
            }
            // line 437
            echo "                                            </form>
                                        </div>
                                    </div>
                                </div>
                                ";
        }
        // line 441
        echo "<!-- if review-status ends -->

                            </div> <!-- .tab-content ends -->
                        </div> <!-- .col-sm-12 ends -->
                    </div> <!-- .row ends -->
                    <!-- Tabs area ends -->

                    ";
        // line 448
        if (($context["full_width_tabs"] ?? null)) {
            // line 449
            echo "                </div>
                ";
        }
        // line 451
        echo "            </div>


        </div> <!-- .table-cell.right ends -->

    </div> <!-- .product-info ends -->

           <!-- Related Products -->

    ";
        // line 460
        if (($context["full_width_tabs"] ?? null)) {
            // line 461
            echo "    <div class=\"container c10padd\">
        ";
        }
        // line 463
        echo "
        ";
        // line 464
        if (($context["products"] ?? null)) {
            // line 465
            echo "            <div class=\"widget widget-related\">

                <div class=\"widget-title\">
                    <p class=\"main-title\"><span>";
            // line 468
            echo ($context["text_related"] ?? null);
            echo "</span></p>
                    <p class=\"widget-title-separator\"><i class=\"icon-line-cross\"></i></p>
                </div>

                <div class=\"grid grid-holder related carousel grid";
            // line 472
            echo ($context["basel_rel_prod_grid"] ?? null);
            echo "\">
                    ";
            // line 473
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 474
                echo "                        ";
                $this->loadTemplate("basel/template/product/single_product.twig", "basel/template/product/product.twig", 474)->display($context);
                // line 475
                echo "                    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 476
            echo "                </div>
            </div>
        ";
        }
        // line 479
        echo "
        ";
        // line 480
        echo ($context["content_bottom"] ?? null);
        echo "

        ";
        // line 482
        if (($context["full_width_tabs"] ?? null)) {
            // line 483
            echo "    </div>
    ";
        }
        // line 485
        echo "

    ";
        // line 487
        if ( !($context["full_width_tabs"] ?? null)) {
            // line 488
            echo "</div> <!-- main column ends -->
";
            // line 489
            echo ($context["column_right"] ?? null);
            echo "
    </div> <!-- .row ends -->
    </div> <!-- .container ends -->
";
        }
        // line 493
        echo "
<script src=\"catalog/view/theme/basel/js/lightgallery/js/lightgallery.min.js\"></script>
<script src=\"catalog/view/theme/basel/js/lightgallery/js/lg-zoom.min.js\"></script>
<script src=\"catalog/view/theme/basel/js/cloudzoom/cloud-zoom.1.0.2.min.js\"></script>
";
        // line 497
        if (($context["basel_price_update"] ?? null)) {
            // line 498
            echo "    <script src=\"index.php?route=extension/basel/live_options/js&product_id=";
            echo ($context["product_id"] ?? null);
            echo "\"></script>
";
        }
        // line 500
        echo "
";
        // line 501
        if (($context["products"] ?? null)) {
            // line 502
            echo "    <script><!--
        \$('.grid-holder.related').slick({
            prevArrow: \"<a class=\\\"arrow-left icon-arrow-left\\\"></a>\",
            nextArrow: \"<a class=\\\"arrow-right icon-arrow-right\\\"></a>\",
            dots:true,
            ";
            // line 507
            if ((($context["direction"] ?? null) == "rtl")) {
                // line 508
                echo "            rtl: true,
            ";
            }
            // line 510
            echo "            respondTo:'min',
            ";
            // line 511
            if ((($context["basel_rel_prod_grid"] ?? null) == "5")) {
                // line 512
                echo "            slidesToShow:5,slidesToScroll:5,responsive:[{breakpoint:1100,settings:{slidesToShow:4,slidesToScroll:4}},{breakpoint:960,settings:{slidesToShow:3,slidesToScroll:3}},{breakpoint:600,settings:{slidesToShow:2,slidesToScroll:2}},
                ";
            } elseif ((            // line 513
($context["basel_rel_prod_grid"] ?? null) == "4")) {
                // line 514
                echo "                slidesToShow:4,slidesToScroll:4,responsive:[{breakpoint:960,settings:{slidesToShow:3,slidesToScroll:3}},{breakpoint:600,settings:{slidesToShow:2,slidesToScroll:2}},
            ";
            } elseif ((            // line 515
($context["basel_rel_prod_grid"] ?? null) == "3")) {
                // line 516
                echo "            slidesToShow:3,slidesToScroll:3,responsive:[{breakpoint:600,settings:{slidesToShow:2,slidesToScroll:2}},
            ";
            } elseif ((            // line 517
($context["basel_rel_prod_grid"] ?? null) == "2")) {
                // line 518
                echo "            slidesToShow:2,slidesToScroll:2,responsive:[
            ";
            }
            // line 520
            echo "            ";
            if (($context["items_mobile_fw"] ?? null)) {
                // line 521
                echo "            {breakpoint:320,settings:{slidesToShow:1,slidesToScroll:1}}
            ";
            }
            // line 523
            echo "        ]
        });
        \$('.product-style2 .single-product .icon').attr('data-placement', 'top');
        \$('[data-toggle=\\'tooltip\\']').tooltip({container: 'body'});
        //--></script>
";
        }
        // line 529
        echo "
";
        // line 530
        if ((($context["sale_end_date"] ?? null) && ($context["product_page_countdown"] ?? null))) {
            // line 531
            echo "    <script>
        \$(function() {
            \$('#special_countdown').countdown('";
            // line 533
            echo ($context["sale_end_date"] ?? null);
            echo "').on('update.countdown', function(event) {
                var \$this = \$(this).html(event.strftime(''
                    + '<div class=\\\"special_countdown\\\"></span><p><span class=\\\"icon-clock\\\"></span> ";
            // line 535
            echo ($context["basel_text_offer_ends"] ?? null);
            echo "</p><div>'
                    + '%D<i>";
            // line 536
            echo ($context["basel_text_days"] ?? null);
            echo "</i></div><div>'
                    + '%H <i>";
            // line 537
            echo ($context["basel_text_hours"] ?? null);
            echo "</i></div><div>'
                    + '%M <i>";
            // line 538
            echo ($context["basel_text_mins"] ?? null);
            echo "</i></div><div>'
                    + '%S <i>";
            // line 539
            echo ($context["basel_text_secs"] ?? null);
            echo "</i></div></div>'));
            });
        });
    </script>
";
        }
        // line 544
        echo "
<script><!--
    \$('select[name=\\'recurring_id\\'], input[name=\"quantity\"]').change(function(){
        \$.ajax({
            url: 'index.php?route=product/product/getRecurringDescription',
            type: 'post',
            data: \$('input[name=\\'product_id\\'], input[name=\\'quantity\\'], select[name=\\'recurring_id\\']'),
            dataType: 'json',
            beforeSend: function() {
                \$('#recurring-description').html('');
            },
            success: function(json) {
                \$('.alert-dismissible, .text-danger').remove();

                if (json['success']) {
                    \$('#recurring-description').html(json['success']);
                }
            }
        });
    });
    //--></script>

<script><!--
    \$('#button-cart').on('click', function() {
        \$.ajax({
            url: 'index.php?route=extension/basel/basel_features/add_to_cart',
            type: 'post',
            data: \$('#product input[type=\\'text\\'], #product input[type=\\'hidden\\'], #product input[type=\\'number\\'], #product input[type=\\'radio\\']:checked, #product input[type=\\'checkbox\\']:checked, #product select, #product textarea'),
            dataType: 'json',
            beforeSend: function(json) {
                \$('body').append('<span class=\"basel-spinner ajax-call\"></span>');
            },

            success: function(json) {
                \$('.alert, .text-danger').remove();
                \$('.table-cell').removeClass('has-error');

                if (json.error) {
                    \$('.basel-spinner.ajax-call').remove();
                    if (json.error.option) {
                        for (i in json.error.option) {
                            var element = \$('#input-option' + i.replace('_', '-'));

                            if (element.parent().hasClass('input-group')) {
                                element.parent().after('<div class=\"text-danger\">' + json.error.option[i] + '</div>');
                            } else {
                                element.after('<div class=\"text-danger\">' + json.error.option[i] + '</div>');
                            }
                        }
                    }

                    if (json.error.recurring) {
                        \$('select[name=\\'recurring_id\\']').after('<div class=\"text-danger\">' + json['error']['recurring'] + '</div>');
                    }

                    // Highlight any found errors
                    \$('.text-danger').parent().addClass('has-error');
                }

                if (json.success_redirect) {

                    location = json.success_redirect;

                } else if (json.success) {

                    \$('.table-cell').removeClass('has-error');
                    \$('.alert, .popup-note, .basel-spinner.ajax-call, .text-danger').remove();

                    html = '<div class=\"popup-note\">';
                    html += '<div class=\"inner\">';
                    html += '<a class=\"popup-note-close\" onclick=\"\$(this).parent().parent().remove()\">&times;</a>';
                    html += '<div class=\"table\">';
                    html += '<div class=\"table-cell v-top img\"><img src=\"' + json.image + '\" /></div>';
                    html += '<div class=\"table-cell v-top\">' + json.success + '</div>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    \$('body').append(html);
                    setTimeout(function() {\$('.popup-note').hide();}, 8100);
                    // Need to set timeout otherwise it wont update the total
                    setTimeout(function () {
                        \$('.cart-total-items').html( json.total_items );
                        \$('.cart-total-amount').html( json.total_amount );
                    }, 100);

                    \$('#cart-content').load('index.php?route=common/cart/info #cart-content > *');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
        });
    });
    //--></script>
<script><!--
    \$('.date').datetimepicker({
        pickTime: false
    });

    \$('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });

    \$('.time').datetimepicker({
        pickDate: false
    });

    \$('button[id^=\\'button-upload\\']').on('click', function() {
        var node = this;

        \$('#form-upload').remove();

        \$('body').prepend('<form enctype=\"multipart/form-data\" id=\"form-upload\" style=\"display: none;\"><input type=\"file\" name=\"file\" /></form>');

        \$('#form-upload input[name=\\'file\\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function() {
            if (\$('#form-upload input[name=\\'file\\']').val() != '') {
                clearInterval(timer);

                \$.ajax({
                    url: 'index.php?route=tool/upload',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData(\$('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        \$(node).button('loading');
                    },
                    complete: function() {
                        \$(node).button('reset');
                    },
                    success: function(json) {
                        \$('.text-danger').remove();

                        if (json['error']) {
                            \$(node).parent().find('input').after('<div class=\"text-danger\">' + json['error'] + '</div>');
                        }

                        if (json['success']) {
                            alert(json['success']);

                            \$(node).parent().find('input').val(json['code']);
                        }
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                    }
                });
            }
        }, 500);
    });
    //--></script>
<script><!--
    \$('#review').delegate('.pagination a', 'click', function(e) {
        e.preventDefault();
        \$(\"html,body\").animate({scrollTop:((\$(\"#review\").offset().top)-50)},500);
        \$('#review').fadeOut(50);

        \$('#review').load(this.href);

        \$('#review').fadeIn(500);

    });


    \$('#button-review').on('click', function() {
        \$.ajax({
            url: 'index.php?route=product/product/write&product_id=";
        // line 719
        echo ($context["product_id"] ?? null);
        echo "',
            type: 'post',
            dataType: 'json',
            data: \$(\"#form-review\").serialize(),
            beforeSend: function() {
                \$('#button-review').button('loading');
            },
            complete: function() {
                \$('#button-review').button('reset');
            },
            success: function(json) {
                \$('.alert-success, .alert-danger').remove();

                if (json.error) {
                    \$('#review-notification').after('<div class=\"alert alert-sm alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ' + json.error + '</div>');
                }

                if (json.success) {
                    \$('#review-notification').after('<div class=\"alert alert-sm alert-success\"><i class=\"fa fa-check-circle\"></i> ' + json.success + '</div>');

                    \$('input[name=\\'name\\']').val('');
                    \$('textarea[name=\\'text\\']').val('');
                    \$('input[name=\\'rating\\']:checked').prop('checked', false);
                }
            }
        });
    });

    \$(document).ready(function() {
        ";
        // line 748
        if ((($context["product_layout"] ?? null) == "full-width")) {
            // line 749
            echo "// Sticky information
        \$('.table-cell.right .inner').theiaStickySidebar({containerSelector:'.product-info'});
        ";
        }
        // line 752
        echo "
// Reviews/Question scroll link
        \$(\".to_tabs\").click(function() {
            \$('html, body').animate({
                scrollTop: (\$(\".main_tabs\").offset().top - 100)
            }, 1000);
        });

// Sharing buttons
        ";
        // line 761
        if (($context["basel_share_btn"] ?? null)) {
            // line 762
            echo "        var share_url = encodeURIComponent(window.location.href);
        var page_title = '";
            // line 763
            echo ($context["heading_title"] ?? null);
            echo "';
        ";
            // line 764
            if (($context["thumb"] ?? null)) {
                // line 765
                echo "        var thumb = '";
                echo ($context["thumb"] ?? null);
                echo "';
        ";
            }
            // line 767
            echo "        \$('.fb_share').attr(\"href\", 'https://www.facebook.com/sharer/sharer.php?u=' + share_url + '');
        \$('.twitter_share').attr(\"href\", 'https://twitter.com/intent/tweet?source=' + share_url + '&text=' + page_title + ': ' + share_url + '');
        \$('.google_share').attr(\"href\", 'https://plus.google.com/share?url=' + share_url + '');
        \$('.pinterest_share').attr(\"href\", 'http://pinterest.com/pin/create/button/?url=' + share_url + '&media=' + thumb + '&description=' + page_title + '');
        \$('.vk_share').attr(\"href\", 'http://vkontakte.ru/share.php?url=' + share_url + '');
        ";
        }
        // line 773
        echo "    });
    //--></script>

";
        // line 776
        if ((($context["product_layout"] ?? null) != "full-width")) {
            // line 777
            echo "    <script>
        \$(document).ready(function() {
            \$('.image-additional a.link').click(function (e) {
                if (\$(this).hasClass(\"locked\")) {
                    e.stopImmediatePropagation();
                }
                \$('.image-additional a.link.active').removeClass('active');
                \$(this).addClass('active')
            });

            ";
            // line 787
            if (($context["images"] ?? null)) {
                // line 788
                echo "            \$('.cloud-zoom-wrap').click(function (e) {
                e.preventDefault();
                \$('.image-additional a.link.active').removeClass('locked').trigger('click').addClass('locked');
            });
            ";
            } else {
                // line 793
                echo "            \$('.cloud-zoom-wrap').click(function (e) {
                e.preventDefault();
                \$('#main-image').trigger('click');
            });
            ";
            }
            // line 798
            echo "
            \$('.image-additional').slick({
                prevArrow: \"<a class=\\\"icon-arrow-left\\\"></a>\",
                nextArrow: \"<a class=\\\"icon-arrow-right\\\"></a>\",
                appendArrows: '.image-additional .slick-list',
                arrows:true,
                ";
            // line 804
            if ((($context["direction"] ?? null) == "rtl")) {
                // line 805
                echo "                rtl: true,
                ";
            }
            // line 807
            echo "                infinite:false,
                ";
            // line 808
            if ((($context["product_layout"] ?? null) == "images-left")) {
                // line 809
                echo "                slidesToShow: ";
                echo twig_round((($context["img_h"] ?? null) / ($context["img_a_h"] ?? null)), 0, "floor");
                echo ",
                vertical:true,
                verticalSwiping:true,
                ";
            } else {
                // line 813
                echo "                slidesToShow: ";
                echo twig_round((($context["img_w"] ?? null) / ($context["img_a_w"] ?? null)));
                echo ",
                ";
            }
            // line 815
            echo "                responsive: [
                    {
                        breakpoint: 992,
                        settings: {
                            vertical:false,
                            verticalSwiping:false
                        }
                    }]
            });

        });
        //--></script>
";
        }
        // line 828
        echo "<script>
    \$(document).ready(function() {
// Image Gallery
        \$(\"#gallery\").lightGallery({
            selector: '.link',
            download:false,
            hideBarsDelay:99999
        });
    });
    //--></script>
<script type=\"application/ld+json\">
{
\"@context\": \"http://schema.org\",
\"@type\": \"Product\",
\"image\": [
";
        // line 843
        if (($context["thumb"] ?? null)) {
            // line 844
            echo "\"";
            echo ($context["thumb"] ?? null);
            echo "\"
";
        }
        // line 846
        echo "],
\"description\": \"";
        // line 847
        echo ($context["meta_description"] ?? null);
        echo "\",
";
        // line 848
        if (($context["review_qty"] ?? null)) {
            // line 849
            echo "\"aggregateRating\": {
\"@type\": \"AggregateRating\",
\"ratingValue\": \"";
            // line 851
            echo ($context["rating"] ?? null);
            echo "\",
\"reviewCount\": \"";
            // line 852
            echo ($context["review_qty"] ?? null);
            echo "\"},
";
        }
        // line 854
        echo "\"name\": \"";
        echo ($context["heading_title"] ?? null);
        echo "\",
\"sku\": \"";
        // line 855
        echo ($context["model"] ?? null);
        echo "\",
";
        // line 856
        if (($context["manufacturer"] ?? null)) {
            // line 857
            echo "\"brand\": \"";
            echo ($context["manufacturer"] ?? null);
            echo "\",
";
        }
        // line 859
        echo "\"offers\": {
\"@type\": \"Offer\",
";
        // line 861
        if ((($context["qty"] ?? null) > 0)) {
            // line 862
            echo "\"availability\": \"http://schema.org/InStock\",
";
        } else {
            // line 864
            echo "\"availability\": \"http://schema.org/OutOfStock\",
";
        }
        // line 866
        if (($context["price"] ?? null)) {
            // line 867
            echo "    ";
            if (($context["special"] ?? null)) {
                // line 868
                echo "\"price\": \"";
                echo ($context["special_snippet"] ?? null);
                echo "\",
";
            } else {
                // line 870
                echo "\"price\": \"";
                echo ($context["price_snippet"] ?? null);
                echo "\",
";
            }
            // line 872
            echo "\"priceCurrency\": \"";
            echo ($context["currency_code"] ?? null);
            echo "\"
";
        }
        // line 874
        echo "}
}
</script>
";
        // line 877
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "basel/template/product/product.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1828 => 877,  1823 => 874,  1817 => 872,  1811 => 870,  1805 => 868,  1802 => 867,  1800 => 866,  1796 => 864,  1792 => 862,  1790 => 861,  1786 => 859,  1780 => 857,  1778 => 856,  1774 => 855,  1769 => 854,  1764 => 852,  1760 => 851,  1756 => 849,  1754 => 848,  1750 => 847,  1747 => 846,  1741 => 844,  1739 => 843,  1722 => 828,  1707 => 815,  1701 => 813,  1693 => 809,  1691 => 808,  1688 => 807,  1684 => 805,  1682 => 804,  1674 => 798,  1667 => 793,  1660 => 788,  1658 => 787,  1646 => 777,  1644 => 776,  1639 => 773,  1631 => 767,  1625 => 765,  1623 => 764,  1619 => 763,  1616 => 762,  1614 => 761,  1603 => 752,  1598 => 749,  1596 => 748,  1564 => 719,  1387 => 544,  1379 => 539,  1375 => 538,  1371 => 537,  1367 => 536,  1363 => 535,  1358 => 533,  1354 => 531,  1352 => 530,  1349 => 529,  1341 => 523,  1337 => 521,  1334 => 520,  1330 => 518,  1328 => 517,  1325 => 516,  1323 => 515,  1320 => 514,  1318 => 513,  1315 => 512,  1313 => 511,  1310 => 510,  1306 => 508,  1304 => 507,  1297 => 502,  1295 => 501,  1292 => 500,  1286 => 498,  1284 => 497,  1278 => 493,  1271 => 489,  1268 => 488,  1266 => 487,  1262 => 485,  1258 => 483,  1256 => 482,  1251 => 480,  1248 => 479,  1243 => 476,  1229 => 475,  1226 => 474,  1209 => 473,  1205 => 472,  1198 => 468,  1193 => 465,  1191 => 464,  1188 => 463,  1184 => 461,  1182 => 460,  1171 => 451,  1167 => 449,  1165 => 448,  1156 => 441,  1149 => 437,  1143 => 435,  1134 => 431,  1125 => 425,  1116 => 419,  1111 => 417,  1103 => 412,  1099 => 411,  1075 => 390,  1070 => 387,  1068 => 386,  1064 => 385,  1056 => 379,  1050 => 377,  1047 => 376,  1041 => 374,  1038 => 373,  1028 => 369,  1020 => 364,  1013 => 362,  1008 => 359,  1003 => 358,  1001 => 357,  995 => 354,  990 => 351,  988 => 350,  985 => 349,  979 => 346,  976 => 345,  974 => 344,  971 => 343,  966 => 340,  959 => 338,  950 => 335,  946 => 334,  943 => 333,  939 => 332,  932 => 328,  928 => 326,  924 => 325,  920 => 323,  918 => 322,  915 => 321,  912 => 320,  903 => 317,  898 => 316,  893 => 315,  891 => 314,  885 => 311,  878 => 306,  870 => 304,  867 => 303,  861 => 301,  858 => 300,  852 => 298,  849 => 297,  846 => 296,  835 => 294,  830 => 293,  828 => 292,  824 => 291,  820 => 290,  813 => 285,  809 => 283,  804 => 280,  802 => 279,  799 => 278,  792 => 274,  789 => 273,  787 => 272,  779 => 266,  776 => 265,  770 => 261,  765 => 258,  762 => 257,  760 => 256,  757 => 255,  738 => 253,  736 => 252,  733 => 251,  725 => 249,  723 => 248,  716 => 246,  713 => 245,  703 => 243,  701 => 242,  690 => 240,  687 => 239,  679 => 237,  677 => 236,  668 => 229,  662 => 227,  660 => 226,  654 => 225,  648 => 224,  642 => 220,  636 => 218,  634 => 217,  622 => 215,  618 => 214,  612 => 213,  607 => 210,  601 => 206,  590 => 204,  586 => 203,  582 => 202,  576 => 199,  573 => 198,  571 => 197,  568 => 196,  563 => 193,  554 => 190,  548 => 186,  541 => 184,  534 => 182,  532 => 181,  525 => 180,  521 => 179,  517 => 178,  511 => 177,  503 => 174,  495 => 172,  493 => 171,  490 => 170,  486 => 169,  483 => 168,  481 => 167,  475 => 163,  469 => 161,  467 => 160,  464 => 159,  456 => 157,  454 => 156,  451 => 155,  447 => 154,  443 => 152,  431 => 150,  427 => 149,  424 => 148,  422 => 147,  418 => 145,  409 => 142,  403 => 140,  401 => 139,  398 => 138,  396 => 137,  393 => 136,  388 => 134,  381 => 130,  378 => 129,  376 => 128,  370 => 125,  363 => 120,  349 => 108,  345 => 106,  331 => 104,  328 => 103,  307 => 100,  304 => 99,  300 => 98,  297 => 97,  295 => 96,  292 => 95,  270 => 92,  267 => 91,  261 => 89,  259 => 88,  256 => 87,  250 => 85,  248 => 84,  245 => 83,  239 => 81,  237 => 80,  233 => 78,  231 => 77,  223 => 75,  221 => 74,  212 => 68,  207 => 67,  204 => 66,  201 => 65,  198 => 64,  195 => 63,  192 => 62,  189 => 61,  187 => 60,  183 => 59,  178 => 57,  174 => 55,  163 => 53,  159 => 52,  155 => 50,  149 => 46,  143 => 44,  138 => 42,  133 => 41,  131 => 40,  125 => 37,  121 => 35,  115 => 33,  109 => 31,  107 => 30,  102 => 27,  96 => 25,  94 => 24,  90 => 22,  84 => 20,  79 => 18,  74 => 17,  72 => 16,  66 => 13,  62 => 11,  56 => 9,  50 => 7,  48 => 6,  44 => 4,  42 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "basel/template/product/product.twig", "");
    }
}
