<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/module/shipping_collector_form.twig */
class __TwigTemplate_2004f3c9afcc21c589fecf1c5a745b3a1687b0fbf73869f28fabb84038ab2b6f extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <button type=\"submit\" form=\"form-module\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo ($context["button_save"] ?? null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
        <a href=\"";
        // line 7
        echo ($context["cancel"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_cancel"] ?? null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1>";
        // line 8
        echo ($context["heading_title"] ?? null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 11
            echo "        <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 11);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 11);
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">
    ";
        // line 17
        if (($context["error_warning"] ?? null)) {
            // line 18
            echo "    <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo ($context["error_warning"] ?? null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 22
        echo "    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i> ";
        // line 24
        echo ($context["text_form"] ?? null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
        <form action=\"";
        // line 27
        echo ($context["action"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-module\" class=\"form-horizontal\">

            <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-date\"><span data-toggle=\"tooltip\" title=\"";
        // line 30
        echo ($context["help_collect_date"] ?? null);
        echo "\">";
        echo ($context["entry_date"] ?? null);
        echo "</span></label>
                <div class=\"col-sm-3\">
                    <div class=\"input-group date\">
                        <input type=\"text\" name=\"collect_date\" value=\"";
        // line 33
        echo ($context["collect_date"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_date"] ?? null);
        echo "\" data-date-format=\"YYYY-MM-DD\" id=\"input-date\" class=\"form-control\" required />
                        <span class=\"input-group-btn\">
                    <button type=\"button\" class=\"btn btn-default\"><i class=\"fa fa-calendar\"></i></button>
                    </span></div>
                    ";
        // line 37
        if (($context["error_collect_date"] ?? null)) {
            // line 38
            echo "                        <p class=\"text-danger\" style=\"font-size: 10px;\">";
            echo ($context["error_collect_date"] ?? null);
            echo "</p>
                    ";
        }
        // line 40
        echo "                </div>
            </div>

            <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-time\"><span data-toggle=\"tooltip\" title=\"";
        // line 44
        echo ($context["help_collect_time"] ?? null);
        echo "\">";
        echo ($context["entry_time"] ?? null);
        echo "</span><p style=\"font-size: 11px; font-weight: normal;\">";
        echo ($context["help_time_default"] ?? null);
        echo "</p></label>
                <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"collect_time\" value=\"";
        // line 46
        echo ($context["collect_time"] ?? null);
        echo "\" placeholder=\"\" id=\"input-time\" class=\"form-control\" />
                    <div class=\"row\" style=\"margin-bottom: 10px;\">
                        ";
        // line 48
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["default_buttons"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["default_button"]) {
            // line 49
            echo "                            <div class=\"col-sm-6 col-md-3 col-lg-2\">
                                <button type=\"button\" data-toggle=\"tooltip\" title=\"";
            // line 50
            echo ($context["button_save"] ?? null);
            echo "\" class=\"btn btn-default btn-block\">";
            echo twig_get_attribute($this->env, $this->source, $context["default_button"], "label", [], "any", false, false, false, 50);
            echo "</button>
                            </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['default_button'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 53
        echo "                    </div>
                </div>
            </div>

            <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-max\"><span data-toggle=\"tooltip\" title=\"";
        // line 58
        echo ($context["help_collect_max"] ?? null);
        echo "\">";
        echo ($context["entry_max"] ?? null);
        echo "</span></label>
                <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"collect_max\" value=\"";
        // line 60
        echo ($context["collect_max"] ?? null);
        echo "\" placeholder=\"\" id=\"input-max\" class=\"form-control\" />
                </div>
            </div>

            <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-collected\"><span data-toggle=\"tooltip\" title=\"";
        // line 65
        echo ($context["help_collected"] ?? null);
        echo "\">";
        echo ($context["entry_collected"] ?? null);
        echo "</span></label>
                <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"collected\" value=\"";
        // line 67
        echo ($context["collected"] ?? null);
        echo "\" placeholder=\"\" id=\"input-collected\" class=\"form-control\" />
                </div>
            </div>

            <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-status\">";
        // line 72
        echo ($context["entry_status"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                    <select name=\"status\" id=\"input-status\" class=\"form-control\">
                        ";
        // line 75
        if (($context["status"] ?? null)) {
            // line 76
            echo "                            <option value=\"1\" selected=\"selected\">";
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                            <option value=\"0\">";
            // line 77
            echo ($context["text_disabled"] ?? null);
            echo "</option>
                        ";
        } else {
            // line 79
            echo "                            <option value=\"1\">";
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                            <option value=\"0\" selected=\"selected\">";
            // line 80
            echo ($context["text_disabled"] ?? null);
            echo "</option>
                        ";
        }
        // line 82
        echo "                    </select>
                </div>
            </div>

        </form>
      </div>
    </div>
  </div>
</div>
<script type=\"text/javascript\"><!--
  \$('.date').datetimepicker({
    language: '";
        // line 93
        echo ($context["datepicker"] ?? null);
        echo "',
    pickTime: false
  });
  //--></script></div>
";
        // line 97
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "extension/module/shipping_collector_form.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  256 => 97,  249 => 93,  236 => 82,  231 => 80,  226 => 79,  221 => 77,  216 => 76,  214 => 75,  208 => 72,  200 => 67,  193 => 65,  185 => 60,  178 => 58,  171 => 53,  160 => 50,  157 => 49,  153 => 48,  148 => 46,  139 => 44,  133 => 40,  127 => 38,  125 => 37,  116 => 33,  108 => 30,  102 => 27,  96 => 24,  92 => 22,  84 => 18,  82 => 17,  76 => 13,  65 => 11,  61 => 10,  56 => 8,  50 => 7,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "extension/module/shipping_collector_form.twig", "");
    }
}
