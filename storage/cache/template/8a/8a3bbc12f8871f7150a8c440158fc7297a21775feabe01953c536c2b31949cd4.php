<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* basel/template/common/footer.twig */
class __TwigTemplate_2da82984add2dde979e274499bb58b64b00483eaf1be9719febf3e8a4b039796 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"container\">
";
        // line 2
        echo ($context["position_bottom_half"] ?? null);
        echo "
</div>
<div class=\"container\">
";
        // line 5
        echo ($context["position_bottom"] ?? null);
        echo "
</div>
<div id=\"footer\">
<div class=\"container\">
";
        // line 9
        if ((($context["footer_block_1"] ?? null) && (($context["footer_block_1"] ?? null) != "<p><br></p>"))) {
            // line 10
            echo "<div class=\"footer-top-block hidden-xs\">
";
            // line 11
            echo ($context["footer_block_1"] ?? null);
            echo "
</div>
";
        }
        // line 14
        echo "<div class=\"row links-holder\">
<div class=\"col-xs-12 col-sm-8\">
  <div class=\"row\">
  ";
        // line 17
        if (($context["custom_links"] ?? null)) {
            // line 18
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["basel_footer_columns"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["column"]) {
                // line 19
                echo "    <div class=\"footer-column col-xs-12 col-sm-6 ";
                echo ($context["basel_columns_count"] ?? null);
                echo " eq_height\">
      ";
                // line 20
                if (twig_get_attribute($this->env, $this->source, $context["column"], "title", [], "any", false, false, false, 20)) {
                    // line 21
                    echo "        <h5>
          <a  role=\"button\" data-toggle=\"collapse\" href=\"#collapse";
                    // line 22
                    echo twig_get_attribute($this->env, $this->source, $context["column"], "sort", [], "any", false, false, false, 22);
                    echo "\" aria-expanded=\"false\" aria-controls=\"collapse";
                    echo twig_get_attribute($this->env, $this->source, $context["column"], "sort", [], "any", false, false, false, 22);
                    echo "\">
            ";
                    // line 23
                    echo twig_get_attribute($this->env, $this->source, $context["column"], "title", [], "any", false, false, false, 23);
                    echo "  <b class=\"pull-right visible-xs\">+</b>
          </a>
        </h5>
      ";
                }
                // line 27
                echo "



      <div class=\"collapse in\" id=\"collapse";
                // line 31
                echo twig_get_attribute($this->env, $this->source, $context["column"], "sort", [], "any", false, false, false, 31);
                echo "\">
        ";
                // line 32
                if (twig_get_attribute($this->env, $this->source, $context["column"], "links", [], "any", true, true, false, 32)) {
                    // line 33
                    echo "          <ul class=\"list-unstyled\">
            ";
                    // line 34
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["column"], "links", [], "any", false, false, false, 34));
                    foreach ($context['_seq'] as $context["_key"] => $context["link"]) {
                        // line 35
                        echo "              <li><a href=\"";
                        echo twig_get_attribute($this->env, $this->source, $context["link"], "target", [], "any", false, false, false, 35);
                        echo "\">";
                        echo twig_get_attribute($this->env, $this->source, $context["link"], "title", [], "any", false, false, false, 35);
                        echo "</a></li>
            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['link'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 37
                    echo "          </ul>
        ";
                }
                // line 39
                echo "
      </div>

      <script>
        \$(document).ready(function(){
          if (\$(window).width() <= 768) {
            \$(\"#collapse1\").removeClass(\"in\");
            \$(\"#collapse2\").removeClass(\"in\");
          }
        });
      </script>



    </div>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['column'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 55
            echo "  ";
        } else {
            // line 56
            echo "      ";
            if (($context["informations"] ?? null)) {
                // line 57
                echo "      <div class=\"footer-column col-xs-12 col-sm-4 eq_height\">
        <h5>";
                // line 58
                echo ($context["text_information"] ?? null);
                echo "</h5>
        <ul class=\"list-unstyled\">
          ";
                // line 60
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["informations"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["information"]) {
                    // line 61
                    echo "          <li><a href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["information"], "href", [], "any", false, false, false, 61);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["information"], "title", [], "any", false, false, false, 61);
                    echo "</a></li>
          ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['information'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 63
                echo "          <li><a href=\"";
                echo ($context["contact"] ?? null);
                echo "\">";
                echo ($context["text_contact"] ?? null);
                echo "</a></li>
        </ul>
      </div>
      ";
            }
            // line 67
            echo "      <div class=\"footer-column col-xs-12 col-sm-4 eq_height\">
        <h5>";
            // line 68
            echo ($context["text_extra"] ?? null);
            echo "</h5>
        <ul class=\"list-unstyled\">
          <li><a href=\"";
            // line 70
            echo ($context["manufacturer"] ?? null);
            echo "\">";
            echo ($context["text_manufacturer"] ?? null);
            echo "</a></li>
          <li><a href=\"";
            // line 71
            echo ($context["voucher"] ?? null);
            echo "\">";
            echo ($context["text_voucher"] ?? null);
            echo "</a></li>
          <li><a href=\"";
            // line 72
            echo ($context["affiliate"] ?? null);
            echo "\">";
            echo ($context["text_affiliate"] ?? null);
            echo "</a></li>
          <li><a href=\"";
            // line 73
            echo ($context["special"] ?? null);
            echo "\">";
            echo ($context["text_special"] ?? null);
            echo "</a></li>
          <li><a href=\"";
            // line 74
            echo ($context["sitemap"] ?? null);
            echo "\">";
            echo ($context["text_sitemap"] ?? null);
            echo "</a></li>
        </ul>
      </div>
      <div class=\"footer-column col-xs-12 col-sm-4 eq_height\">
        <h5>";
            // line 78
            echo ($context["text_account"] ?? null);
            echo "</h5>
        <ul class=\"list-unstyled\">
          <li><a href=\"";
            // line 80
            echo ($context["account"] ?? null);
            echo "\">";
            echo ($context["text_account"] ?? null);
            echo "</a></li>
          <li><a href=\"";
            // line 81
            echo ($context["order"] ?? null);
            echo "\">";
            echo ($context["text_order"] ?? null);
            echo "</a></li>
          <li><a href=\"";
            // line 82
            echo ($context["return"] ?? null);
            echo "\">";
            echo ($context["text_return"] ?? null);
            echo "</a></li>
          <li class=\"is_wishlist\"><a href=\"";
            // line 83
            echo ($context["wishlist"] ?? null);
            echo "\">";
            echo ($context["text_wishlist"] ?? null);
            echo "</a></li>
          <li><a href=\"";
            // line 84
            echo ($context["newsletter"] ?? null);
            echo "\">";
            echo ($context["text_newsletter"] ?? null);
            echo "</a></li>
        </ul>
      </div>
 ";
        }
        // line 88
        echo "</div><!-- .row ends -->
</div><!-- .col-md-8 ends -->
<div class=\"footer-column col-xs-12 col-sm-4\">
<div class=\"footer-custom-wrapper\">
";
        // line 92
        if (array_key_exists("footer_block_title", $context)) {
            // line 93
            echo "<h5>";
            echo ($context["footer_block_title"] ?? null);
            echo "</h5>
";
        }
        // line 95
        if ((($context["footer_block_2"] ?? null) && (($context["footer_block_2"] ?? null) != "<p><br></p>"))) {
            // line 96
            echo "<div class=\"custom_block\">";
            echo ($context["footer_block_2"] ?? null);
            echo "</div>
";
        }
        // line 98
        if (array_key_exists("footer_infoline_1", $context)) {
            // line 99
            echo "<p class=\"infoline\">";
            echo ($context["footer_infoline_1"] ?? null);
            echo "</p>
";
        }
        // line 101
        if (array_key_exists("footer_infoline_2", $context)) {
            // line 102
            echo "<p class=\"infoline\">";
            echo ($context["footer_infoline_2"] ?? null);
            echo "</p>
";
        }
        // line 104
        if (array_key_exists("footer_infoline_3", $context)) {
            // line 105
            echo "<p class=\"infoline\">";
            echo ($context["footer_infoline_3"] ?? null);
            echo "</p>
";
        }
        // line 107
        if (($context["payment_img"] ?? null)) {
            // line 108
            echo "<img class=\"payment_img\" src=\"";
            echo ($context["payment_img"] ?? null);
            echo "\" alt=\"\" />
";
        }
        // line 110
        echo "</div>
</div>
</div><!-- .row ends -->

  <!--<div class=\"col-xs-12 text-center\" style=\"margin-top:0px;display:block\">
    <a href=\"http://www.corvuspay.hr\" title=\"CorvusPay - internet Payment Gateway\" target=\"_blank\"><img src=\"image/catalog/credit-cards/CorvusPAY.svg\" style=\"max-width:180px\" alt=\"Corvus Payment\"></a>

  </div> -->

  <div class=\"col-xs-12 text-center cardspayment\" style=\"margin-top:15px;margin-bottom:15px\">
    <img style=\"width: 55px;margin-right:3px\" src=\"image/catalog/credit-cards/visa.svg\">
    <img style=\"width: 55px;;margin-right:3px\" src=\"image/catalog/credit-cards/maestro.svg\">
    <img style=\"width: 55px;;margin-right:3px\" src=\"image/catalog/credit-cards/mastercard.svg\">
    <img style=\"width: 55px;;margin-right:3px\" src=\"image/catalog/credit-cards/diners.svg\">
    <img style=\"width: 55px;;margin-right:3px\" src=\"image/catalog/credit-cards/wsPay.svg\">

  </div>
  <div class=\"clear\"></div>

</div>
  ";
        // line 130
        if (($context["basel_copyright"] ?? null)) {
            // line 131
            echo "    <div class=\"footer-copyright\">";
            echo ($context["basel_copyright"] ?? null);
            echo "</div>
  ";
        }
        // line 133
        echo "</div>
<link href=\"catalog/view/javascript/font-awesome/css/font-awesome.min.css\" rel=\"stylesheet\" />
<link href=\"catalog/view/theme/basel/js/lightgallery/css/lightgallery.css\" rel=\"stylesheet\" />
<script src=\"catalog/view/theme/basel/js/jquery.matchHeight.min.js\"></script>
<script src=\"catalog/view/theme/basel/js/countdown.js\"></script>
<script src=\"catalog/view/theme/basel/js/live_search.js\"></script>
<script src=\"catalog/view/theme/basel/js/featherlight.js\"></script>
";
        // line 140
        if (($context["view_popup"] ?? null)) {
            // line 141
            echo "<!-- Popup -->
<script>
\$(document).ready(function() {
if (\$(window).width() > ";
            // line 144
            echo ($context["popup_width_limit"] ?? null);
            echo ") {
setTimeout(function() {
\$.featherlight({ajax: 'index.php?route=extension/basel/basel_features/basel_popup', variant:'popup-wrapper'});
}, ";
            // line 147
            echo ($context["popup_delay"] ?? null);
            echo ");
}
});
</script>
";
        }
        // line 152
        if (($context["sticky_columns"] ?? null)) {
            // line 153
            echo "<!-- Sticky columns -->
<script>
if (\$(window).width() > 767) {
\$('#column-left, #column-right').theiaStickySidebar({containerSelector:\$(this).closest('.row'),additionalMarginTop:";
            // line 156
            echo ($context["sticky_columns_offset"] ?? null);
            echo "});
}
</script>
";
        }
        // line 160
        if (($context["view_cookie_bar"] ?? null)) {
            // line 161
            echo "<!-- Cookie bar -->
<div class=\"basel_cookie_bar\">
<div class=\"table\">
<div class=\"table-cell w100\">";
            // line 164
            echo ($context["basel_cookie_info"] ?? null);
            echo "</div>
<div class=\"table-cell button-cell\">
<a class=\"btn btn-tiny btn-light-outline\" onclick=\"\$(this).parent().parent().parent().fadeOut(400);\">";
            // line 166
            echo ($context["basel_cookie_btn_close"] ?? null);
            echo "</a>
";
            // line 167
            if (array_key_exists("href_more_info", $context)) {
                // line 168
                echo "<a class=\"more-info anim-underline light\" href=\"";
                echo ($context["href_more_info"] ?? null);
                echo "\">";
                echo ($context["basel_cookie_btn_more_info"] ?? null);
                echo "</a>
";
            }
            // line 170
            echo "</div>
</div>
</div>
";
        }
        // line 174
        echo "<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
BASEL VERSION ";
        // line 177
        echo ($context["basel_version"] ?? null);
        echo " - OPENCART VERSION 3 (";
        echo ($context["VERSION"] ?? null);
        echo ") 
//-->
</div><!-- .outer-container ends -->
<a class=\"scroll-to-top primary-bg-color hidden-sm hidden-xs\" onclick=\"\$('html, body').animate({scrollTop:0});\"><i class=\"icon-arrow-right\"></i></a>
<div id=\"featherlight-holder\"></div>
</body></html>";
    }

    public function getTemplateName()
    {
        return "basel/template/common/footer.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  437 => 177,  432 => 174,  426 => 170,  418 => 168,  416 => 167,  412 => 166,  407 => 164,  402 => 161,  400 => 160,  393 => 156,  388 => 153,  386 => 152,  378 => 147,  372 => 144,  367 => 141,  365 => 140,  356 => 133,  350 => 131,  348 => 130,  326 => 110,  320 => 108,  318 => 107,  312 => 105,  310 => 104,  304 => 102,  302 => 101,  296 => 99,  294 => 98,  288 => 96,  286 => 95,  280 => 93,  278 => 92,  272 => 88,  263 => 84,  257 => 83,  251 => 82,  245 => 81,  239 => 80,  234 => 78,  225 => 74,  219 => 73,  213 => 72,  207 => 71,  201 => 70,  196 => 68,  193 => 67,  183 => 63,  172 => 61,  168 => 60,  163 => 58,  160 => 57,  157 => 56,  154 => 55,  133 => 39,  129 => 37,  118 => 35,  114 => 34,  111 => 33,  109 => 32,  105 => 31,  99 => 27,  92 => 23,  86 => 22,  83 => 21,  81 => 20,  76 => 19,  71 => 18,  69 => 17,  64 => 14,  58 => 11,  55 => 10,  53 => 9,  46 => 5,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "basel/template/common/footer.twig", "");
    }
}
