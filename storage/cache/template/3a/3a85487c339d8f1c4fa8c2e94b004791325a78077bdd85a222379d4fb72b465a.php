<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* basel/template/extension/quickcheckout/guest.twig */
class __TwigTemplate_cf25714ef8910802e7137ac4d9cd42cbe475431f000476d5b1ae1d634fadb855 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["fields"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
            echo " 
  ";
            // line 2
            if (($context["field"] == "country")) {
                // line 3
                echo "    ";
                if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context, ("field_" . $context["field"]), [], "any", false, false, false, 3), "display", [], "any", false, false, false, 3)) {
                    echo "  
\t<div class=\"col-sm-6";
                    // line 4
                    echo ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context, ("field_" . $context["field"]), [], "any", false, false, false, 4), "required", [], "any", false, false, false, 4)) ? (" required") : (""));
                    echo "\">
\t  <label class=\"control-label\">";
                    // line 5
                    echo ($context["entry_country"] ?? null);
                    echo "</label>
\t  <select name=\"country_id\" class=\"form-control\" id=\"input-payment-country\">
\t  ";
                    // line 7
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["countries"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["country"]) {
                        echo " 
\t\t";
                        // line 8
                        if ((twig_get_attribute($this->env, $this->source, $context["country"], "country_id", [], "any", false, false, false, 8) == ($context["country_id"] ?? null))) {
                            echo " 
\t\t<option value=\"";
                            // line 9
                            echo twig_get_attribute($this->env, $this->source, $context["country"], "country_id", [], "any", false, false, false, 9);
                            echo "\" selected=\"selected\">";
                            echo twig_get_attribute($this->env, $this->source, $context["country"], "name", [], "any", false, false, false, 9);
                            echo "</option>
\t\t";
                        } else {
                            // line 10
                            echo "   
\t\t<option value=\"";
                            // line 11
                            echo twig_get_attribute($this->env, $this->source, $context["country"], "country_id", [], "any", false, false, false, 11);
                            echo "\">";
                            echo twig_get_attribute($this->env, $this->source, $context["country"], "name", [], "any", false, false, false, 11);
                            echo "</option>
\t\t";
                        }
                        // line 13
                        echo "\t  ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['country'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 14
                    echo "\t  </select>
\t</div>
\t";
                } else {
                    // line 16
                    echo "   
\t<select name=\"country_id\" class=\"hide\">
\t";
                    // line 18
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["countries"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["country"]) {
                        echo " 
\t  ";
                        // line 19
                        if ((twig_get_attribute($this->env, $this->source, $context["country"], "country_id", [], "any", false, false, false, 19) == ($context["country_id"] ?? null))) {
                            echo " 
\t  <option value=\"";
                            // line 20
                            echo twig_get_attribute($this->env, $this->source, $context["country"], "country_id", [], "any", false, false, false, 20);
                            echo "\" selected=\"selected\">";
                            echo twig_get_attribute($this->env, $this->source, $context["country"], "name", [], "any", false, false, false, 20);
                            echo "</option>
\t  ";
                        } else {
                            // line 21
                            echo "   
\t  <option value=\"";
                            // line 22
                            echo twig_get_attribute($this->env, $this->source, $context["country"], "country_id", [], "any", false, false, false, 22);
                            echo "\">";
                            echo twig_get_attribute($this->env, $this->source, $context["country"], "name", [], "any", false, false, false, 22);
                            echo "</option>
\t  ";
                        }
                        // line 24
                        echo "\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['country'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 25
                    echo "\t</select>
\t";
                }
                // line 27
                echo "  ";
            } elseif (($context["field"] == "zone")) {
                echo " 
    ";
                // line 28
                if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context, ("field_" . $context["field"]), [], "any", false, false, false, 28), "display", [], "any", false, false, false, 28)) {
                    echo "  
\t<div class=\"col-sm-6";
                    // line 29
                    echo ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context, ("field_" . $context["field"]), [], "any", false, false, false, 29), "required", [], "any", false, false, false, 29)) ? (" required") : (""));
                    echo "\">
\t  <label class=\"control-label\">";
                    // line 30
                    echo ($context["entry_zone"] ?? null);
                    echo "</label>
\t  <select name=\"zone_id\" class=\"form-control\" id=\"input-payment-zone\"></select>
\t</div>
\t";
                } else {
                    // line 33
                    echo "   
\t  <select name=\"zone_id\" class=\"hide\"></select>
\t";
                }
                // line 35
                echo " 
  ";
            } elseif ((            // line 36
$context["field"] == "customer_group")) {
                echo " 
    ";
                // line 37
                if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context, ("field_" . $context["field"]), [], "any", false, false, false, 37), "display", [], "any", false, false, false, 37)) {
                    echo "  
\t<div class=\"col-sm-6 required\"";
                    // line 38
                    echo (((twig_length_filter($this->env, ($context["customer_groups"] ?? null)) <= 1)) ? (" style=\"display:none !important\"") : (""));
                    echo ">
\t  <label class=\"control-label\">";
                    // line 39
                    echo ($context["entry_customer_group"] ?? null);
                    echo "</label>
\t  <select name=\"customer_group_id\" class=\"form-control\" id=\"input-payment-customer-group\">
\t\t";
                    // line 41
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["customer_groups"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["customer_group"]) {
                        echo " 
\t\t<option value=\"";
                        // line 42
                        echo twig_get_attribute($this->env, $this->source, $context["customer_group"], "customer_group_id", [], "any", false, false, false, 42);
                        echo "\"";
                        echo (((twig_get_attribute($this->env, $this->source, $context["customer_group"], "customer_group_id", [], "any", false, false, false, 42) == ($context["customer_group_id"] ?? null))) ? (" selected=\"selected\"") : (""));
                        echo ">";
                        echo twig_get_attribute($this->env, $this->source, $context["customer_group"], "name", [], "any", false, false, false, 42);
                        echo "</option>
\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['customer_group'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 43
                    echo " 
\t  </select>
\t</div>
\t";
                } else {
                    // line 46
                    echo "   
\t  <select name=\"customer_group_id\" class=\"hide\">
\t\t";
                    // line 48
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["customer_groups"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["customer_group"]) {
                        echo " 
\t\t<option value=\"";
                        // line 49
                        echo twig_get_attribute($this->env, $this->source, $context["customer_group"], "customer_group_id", [], "any", false, false, false, 49);
                        echo "\"";
                        echo (((twig_get_attribute($this->env, $this->source, $context["customer_group"], "customer_group_id", [], "any", false, false, false, 49) == ($context["customer_group_id"] ?? null))) ? (" selected=\"selected\"") : (""));
                        echo ">";
                        echo twig_get_attribute($this->env, $this->source, $context["customer_group"], "name", [], "any", false, false, false, 49);
                        echo "</option>
\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['customer_group'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 50
                    echo " 
\t  </select>
\t";
                }
                // line 53
                echo "  ";
            } else {
                echo "   
    ";
                // line 54
                if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context, ("field_" . $context["field"]), [], "any", false, false, false, 54), "display", [], "any", false, false, false, 54)) {
                    // line 55
                    echo "
\t\t";
                    // line 56
                    if (($context["field"] == "postcode")) {
                        // line 57
                        echo "

\t\t\t<div";
                        // line 59
                        echo ((($context["field"] == "postcode")) ? (" id=\"payment-postcode-required\"") : (""));
                        echo " class=\"col-sm-6";
                        echo ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context, ("field_" . $context["field"]), [], "any", false, false, false, 59), "required", [], "any", false, false, false, 59)) ? (" required") : (""));
                        echo "\">

\t\t\t<label class=\"control-label\" for=\"input-payment-";
                        // line 61
                        echo twig_replace_filter($context["field"], ["_" => "-"]);
                        echo "\">";
                        echo twig_get_attribute($this->env, $this->source, $context, ("entry_" . $context["field"]), [], "any", false, false, false, 61);
                        echo "</label>

\t\t\t\t<select name=\"postcode\" class=\"form-control\"  id=\"input-payment-";
                        // line 63
                        echo twig_replace_filter($context["field"], ["_" => "-"]);
                        echo "\" style=\"width: 100%\">

\t\t\t\t\t<option value=\"10000\">10000 Zagreb</option>
\t\t\t\t\t<option value=\"10010\">10010 Zagreb-Sloboština</option>
\t\t\t\t\t<option value=\"10020\">10020 Zagreb-Novi Zagreb</option>
\t\t\t\t\t<option value=\"10040\">10040 Zagreb-Dubrava</option>
\t\t\t\t\t<option value=\"10090\">10090 Zagreb-Susedgrad</option>
\t\t\t\t\t<option value=\"10104\">10104 Zagreb-Trešnjevka jug</option>
\t\t\t\t\t<option value=\"10105\">10105 Zagreb-Donji grad jug</option>
\t\t\t\t\t<option value=\"10108\">10108 Zagreb-Maksimir dio</option>
\t\t\t\t\t<option value=\"10109\">10109 Zagreb-Črnomerec dio</option>
\t\t\t\t\t<option value=\"10110\">10110 Zagreb-Trešnjevka sjever</option>
\t\t\t\t\t<option value=\"10135\">10135 Zagreb-Pešćenica Žitnjak</option>
\t\t\t\t\t<option value=\"10172\">10172 Zagreb-Podsused Vrapče dio</option>

\t\t\t\t</select>

\t\t\t</div>


\t\t";
                    } elseif ((                    // line 83
$context["field"] == "city")) {
                        // line 84
                        echo "

\t\t\t<div";
                        // line 86
                        echo ((($context["field"] == "postcode")) ? (" id=\"payment-postcode-required\"") : (""));
                        echo " class=\"col-sm-6";
                        echo ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context, ("field_" . $context["field"]), [], "any", false, false, false, 86), "required", [], "any", false, false, false, 86)) ? (" required") : (""));
                        echo "\">
\t\t\t\t<label class=\"control-label\" for=\"input-payment-";
                        // line 87
                        echo twig_replace_filter($context["field"], ["_" => "-"]);
                        echo "\">";
                        echo twig_get_attribute($this->env, $this->source, $context, ("entry_" . $context["field"]), [], "any", false, false, false, 87);
                        echo "</label>
\t\t\t\t<input type=\"text\" name=\"";
                        // line 88
                        echo $context["field"];
                        echo "\" placeholder=\"";
                        echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context, ("field_" . $context["field"]), [], "any", false, false, false, 88), "placeholder", [], "any", false, false, false, 88);
                        echo "\" value=\"Zagreb\" class=\"form-control\"  id=\"input-payment-";
                        echo twig_replace_filter($context["field"], ["_" => "-"]);
                        echo "\" disabled />
\t\t\t</div>


\t    ";
                    } else {
                        // line 93
                        echo "
\t<div";
                        // line 94
                        echo ((($context["field"] == "postcode")) ? (" id=\"payment-postcode-required\"") : (""));
                        echo " class=\"col-sm-6";
                        echo ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context, ("field_" . $context["field"]), [], "any", false, false, false, 94), "required", [], "any", false, false, false, 94)) ? (" required") : (""));
                        echo "\">
\t  <label class=\"control-label\" for=\"input-payment-";
                        // line 95
                        echo twig_replace_filter($context["field"], ["_" => "-"]);
                        echo "\">";
                        echo twig_get_attribute($this->env, $this->source, $context, ("entry_" . $context["field"]), [], "any", false, false, false, 95);
                        echo "</label>
\t  <input type=\"text\" name=\"";
                        // line 96
                        echo $context["field"];
                        echo "\" placeholder=\"";
                        echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context, ("field_" . $context["field"]), [], "any", false, false, false, 96), "placeholder", [], "any", false, false, false, 96);
                        echo "\" value=\"";
                        echo ((twig_get_attribute($this->env, $this->source, $context, $context["field"], [], "any", false, false, false, 96)) ? (twig_get_attribute($this->env, $this->source, $context, $context["field"], [], "any", false, false, false, 96)) : (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context, ("field_" . $context["field"]), [], "any", false, false, false, 96), "default", [], "any", false, false, false, 96)));
                        echo "\" class=\"form-control\"  id=\"input-payment-";
                        echo twig_replace_filter($context["field"], ["_" => "-"]);
                        echo "\" />
\t</div>

\t\t";
                    }
                    // line 100
                    echo "

\t";
                } else {
                    // line 102
                    echo "   
\t<input type=\"text\" name=\"";
                    // line 103
                    echo $context["field"];
                    echo "\" value=\"";
                    echo ((twig_get_attribute($this->env, $this->source, $context, $context["field"], [], "any", false, false, false, 103)) ? (twig_get_attribute($this->env, $this->source, $context, $context["field"], [], "any", false, false, false, 103)) : (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context, ("field_" . $context["field"]), [], "any", false, false, false, 103), "default", [], "any", false, false, false, 103)));
                    echo "\" class=\"hide\" />
\t";
                }
                // line 105
                echo "  ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 107
        echo "<!-- CUSTOM FIELDS -->
<div id=\"custom-field-payment\">
  ";
        // line 109
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["custom_fields"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["custom_field"]) {
            echo " 
  ";
            // line 110
            if (((twig_get_attribute($this->env, $this->source, $context["custom_field"], "location", [], "any", false, false, false, 110) == "account") || (twig_get_attribute($this->env, $this->source, $context["custom_field"], "location", [], "any", false, false, false, 110) == "address"))) {
                echo " 
\t<div class=\"col-sm-6 custom-field\" data-sort=\"";
                // line 111
                echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "sort_order", [], "any", false, false, false, 111);
                echo "\" id=\"payment-custom-field";
                echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 111);
                echo "\">
\t  <label class=\"control-label\" for=\"input-payment-custom-field";
                // line 112
                echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 112);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "name", [], "any", false, false, false, 112);
                echo "</label>
\t  ";
                // line 113
                if ((twig_get_attribute($this->env, $this->source, $context["custom_field"], "type", [], "any", false, false, false, 113) == "select")) {
                    echo " 
\t\t<select name=\"custom_field[";
                    // line 114
                    echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "location", [], "any", false, false, false, 114);
                    echo "][";
                    echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 114);
                    echo "]\" id=\"input-payment-custom-field";
                    echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 114);
                    echo "\" class=\"form-control\">
\t\t  <option value=\"\">";
                    // line 115
                    echo ($context["text_select"] ?? null);
                    echo "</option>
\t\t  ";
                    // line 116
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($context["custom_field"]);
                    foreach ($context['_seq'] as $context["_key"] => $context["custom_field_value"]) {
                        echo " 
\t\t  ";
                        // line 117
                        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["guest_custom_field"] ?? null), "custom_field", [], "any", false, false, false, 117), "custom_field_id", [], "any", false, false, false, 117) && (twig_get_attribute($this->env, $this->source, $context["custom_field_value"], "custom_field_value_id", [], "any", false, false, false, 117) == twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["guest_custom_field"] ?? null), "custom_field", [], "any", false, false, false, 117), "custom_field_id", [], "any", false, false, false, 117)))) {
                            echo " 
\t\t  <option value=\"";
                            // line 118
                            echo twig_get_attribute($this->env, $this->source, $context["custom_field_value"], "custom_field_value_id", [], "any", false, false, false, 118);
                            echo "\" selected=\"selected\">";
                            echo twig_get_attribute($this->env, $this->source, $context["custom_field_value"], "name", [], "any", false, false, false, 118);
                            echo "</option>
\t\t  ";
                        } else {
                            // line 119
                            echo "   
\t\t  <option value=\"";
                            // line 120
                            echo twig_get_attribute($this->env, $this->source, $context["custom_field_value"], "custom_field_value_id", [], "any", false, false, false, 120);
                            echo "\">";
                            echo twig_get_attribute($this->env, $this->source, $context["custom_field_value"], "name", [], "any", false, false, false, 120);
                            echo "</option>
\t\t  ";
                        }
                        // line 122
                        echo "\t\t  ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['custom_field_value'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 123
                    echo "\t\t</select>
\t  ";
                }
                // line 125
                echo "\t  ";
                if ((twig_get_attribute($this->env, $this->source, $context["custom_field"], "type", [], "any", false, false, false, 125) == "radio")) {
                    echo " 
\t\t";
                    // line 126
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($context["custom_field"]);
                    foreach ($context['_seq'] as $context["_key"] => $context["custom_field_value"]) {
                        echo " 
\t\t  <div class=\"radio\">
\t\t\t";
                        // line 128
                        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["guest_custom_field"] ?? null), "custom_field", [], "any", false, false, false, 128), "custom_field_id", [], "any", false, false, false, 128) && (twig_get_attribute($this->env, $this->source, $context["custom_field_value"], "custom_field_value_id", [], "any", false, false, false, 128) == twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["guest_custom_field"] ?? null), "custom_field", [], "any", false, false, false, 128), "custom_field_id", [], "any", false, false, false, 128)))) {
                            echo " 
\t\t\t<label>
\t\t\t  <input type=\"radio\" name=\"custom_field[";
                            // line 130
                            echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "location", [], "any", false, false, false, 130);
                            echo "][";
                            echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 130);
                            echo "]\" value=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["custom_field_value"], "custom_field_value_id", [], "any", false, false, false, 130);
                            echo "\" checked=\"checked\" />
\t\t\t  ";
                            // line 131
                            echo twig_get_attribute($this->env, $this->source, $context["custom_field_value"], "name", [], "any", false, false, false, 131);
                            echo "</label>
\t\t\t";
                        } else {
                            // line 132
                            echo "   
\t\t\t<label>
\t\t\t  <input type=\"radio\" name=\"custom_field[";
                            // line 134
                            echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "location", [], "any", false, false, false, 134);
                            echo "][";
                            echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 134);
                            echo "]\" value=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["custom_field_value"], "custom_field_value_id", [], "any", false, false, false, 134);
                            echo "\" />
\t\t\t  ";
                            // line 135
                            echo twig_get_attribute($this->env, $this->source, $context["custom_field_value"], "name", [], "any", false, false, false, 135);
                            echo "</label>
\t\t\t";
                        }
                        // line 137
                        echo "\t\t  </div>
\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['custom_field_value'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 139
                    echo "\t  ";
                }
                // line 140
                echo "\t  ";
                if ((twig_get_attribute($this->env, $this->source, $context["custom_field"], "type", [], "any", false, false, false, 140) == "checkbox")) {
                    echo " 
\t\t";
                    // line 141
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($context["custom_field"]);
                    foreach ($context['_seq'] as $context["_key"] => $context["custom_field_value"]) {
                        echo " 
\t\t  <div class=\"checkbox\">
\t\t\t";
                        // line 143
                        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["guest_custom_field"] ?? null), "custom_field", [], "any", false, false, false, 143), "custom_field_id", [], "any", false, false, false, 143) && twig_in_filter(twig_get_attribute($this->env, $this->source, $context["custom_field_value"], "custom_field_value_id", [], "any", false, false, false, 143), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["guest_custom_field"] ?? null), "custom_field", [], "any", false, false, false, 143), "custom_field_id", [], "any", false, false, false, 143)))) {
                            // line 144
                            echo "\t\t\t<label>
\t\t\t  <input type=\"checkbox\" name=\"custom_field[";
                            // line 145
                            echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "location", [], "any", false, false, false, 145);
                            echo "][";
                            echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 145);
                            echo "][]\" value=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["custom_field_value"], "custom_field_value_id", [], "any", false, false, false, 145);
                            echo "\" checked=\"checked\" />
\t\t\t  ";
                            // line 146
                            echo twig_get_attribute($this->env, $this->source, $context["custom_field_value"], "name", [], "any", false, false, false, 146);
                            echo "</label>
\t\t\t";
                        } else {
                            // line 147
                            echo "   
\t\t\t<label>
\t\t\t  <input type=\"checkbox\" name=\"custom_field[";
                            // line 149
                            echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "location", [], "any", false, false, false, 149);
                            echo "][";
                            echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 149);
                            echo "][]\" value=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["custom_field_value"], "custom_field_value_id", [], "any", false, false, false, 149);
                            echo "\" />
\t\t\t  ";
                            // line 150
                            echo twig_get_attribute($this->env, $this->source, $context["custom_field_value"], "name", [], "any", false, false, false, 150);
                            echo "</label>
\t\t\t";
                        }
                        // line 151
                        echo " 
\t\t  </div>
\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['custom_field_value'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 154
                    echo "\t  ";
                }
                // line 155
                echo "\t  ";
                if ((twig_get_attribute($this->env, $this->source, $context["custom_field"], "type", [], "any", false, false, false, 155) == "text")) {
                    echo " 
\t\t<input type=\"text\" name=\"custom_field[";
                    // line 156
                    echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "location", [], "any", false, false, false, 156);
                    echo "][";
                    echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 156);
                    echo "]\" value=\"";
                    echo (((($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["guest_custom_field"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4[twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 156)] ?? null) : null)) ? ((($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = ($context["guest_custom_field"] ?? null)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144[twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 156)] ?? null) : null)) : (twig_get_attribute($this->env, $this->source, $context["custom_field"], "value", [], "any", false, false, false, 156)));
                    echo "\" placeholder=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "name", [], "any", false, false, false, 156);
                    echo "\" id=\"input-payment-custom-field";
                    echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 156);
                    echo "\" class=\"form-control\" />
\t  ";
                }
                // line 157
                echo " 
\t  ";
                // line 158
                if ((twig_get_attribute($this->env, $this->source, $context["custom_field"], "type", [], "any", false, false, false, 158) == "textarea")) {
                    echo " 
\t\t<textarea name=\"custom_field[";
                    // line 159
                    echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "location", [], "any", false, false, false, 159);
                    echo "][";
                    echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 159);
                    echo "]\" rows=\"5\" placeholder=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "name", [], "any", false, false, false, 159);
                    echo "\" id=\"input-payment-custom-field";
                    echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 159);
                    echo "\" class=\"form-control\">";
                    echo (((($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = ($context["guest_custom_field"] ?? null)) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b[twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 159)] ?? null) : null)) ? ((($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = ($context["guest_custom_field"] ?? null)) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002[twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 159)] ?? null) : null)) : (twig_get_attribute($this->env, $this->source, $context["custom_field"], "value", [], "any", false, false, false, 159)));
                    echo "</textarea>
\t  ";
                }
                // line 160
                echo " 
\t  ";
                // line 161
                if ((twig_get_attribute($this->env, $this->source, $context["custom_field"], "type", [], "any", false, false, false, 161) == "file")) {
                    echo " 
\t\t<br />
\t\t<button type=\"button\" id=\"button-payment-custom-field";
                    // line 163
                    echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 163);
                    echo "\" data-loading-text=\"";
                    echo ($context["text_loading"] ?? null);
                    echo "\" class=\"btn btn-default\"><i class=\"fa fa-upload\"></i>";
                    echo ($context["button_upload"] ?? null);
                    echo "</button>
\t\t<input type=\"hidden\" name=\"custom_field[";
                    // line 164
                    echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "location", [], "any", false, false, false, 164);
                    echo "][";
                    echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 164);
                    echo "]\" value=\"";
                    echo (((($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = ($context["guest_custom_field"] ?? null)) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4[twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 164)] ?? null) : null)) ? ((($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = ($context["guest_custom_field"] ?? null)) && is_array($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) || $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 instanceof ArrayAccess ? ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666[twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 164)] ?? null) : null)) : (""));
                    echo "\" />
\t  ";
                }
                // line 165
                echo " 
\t  ";
                // line 166
                if ((twig_get_attribute($this->env, $this->source, $context["custom_field"], "type", [], "any", false, false, false, 166) == "date")) {
                    echo " 
\t\t<input type=\"text\" name=\"custom_field[";
                    // line 167
                    echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "location", [], "any", false, false, false, 167);
                    echo "][";
                    echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 167);
                    echo "]\" value=\"";
                    echo (((($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = ($context["guest_custom_field"] ?? null)) && is_array($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e) || $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e instanceof ArrayAccess ? ($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e[twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 167)] ?? null) : null)) ? ((($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 = ($context["guest_custom_field"] ?? null)) && is_array($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52) || $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 instanceof ArrayAccess ? ($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52[twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 167)] ?? null) : null)) : (twig_get_attribute($this->env, $this->source, $context["custom_field"], "value", [], "any", false, false, false, 167)));
                    echo "\" placeholder=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "name", [], "any", false, false, false, 167);
                    echo "\" id=\"input-payment-custom-field";
                    echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 167);
                    echo "\" class=\"form-control date\" />
\t  ";
                }
                // line 168
                echo " 
\t  ";
                // line 169
                if ((twig_get_attribute($this->env, $this->source, $context["custom_field"], "type", [], "any", false, false, false, 169) == "time")) {
                    echo " 
\t\t<input type=\"text\" name=\"custom_field[";
                    // line 170
                    echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "location", [], "any", false, false, false, 170);
                    echo "][";
                    echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 170);
                    echo "]\" value=\"";
                    echo (((($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 = ($context["guest_custom_field"] ?? null)) && is_array($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136) || $__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 instanceof ArrayAccess ? ($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136[twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 170)] ?? null) : null)) ? ((($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 = ($context["guest_custom_field"] ?? null)) && is_array($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386) || $__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 instanceof ArrayAccess ? ($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386[twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 170)] ?? null) : null)) : (twig_get_attribute($this->env, $this->source, $context["custom_field"], "value", [], "any", false, false, false, 170)));
                    echo "\" placeholder=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "name", [], "any", false, false, false, 170);
                    echo "\" id=\"input-payment-custom-field";
                    echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 170);
                    echo "\" class=\"form-control time\" />
\t  ";
                }
                // line 171
                echo " 
\t  ";
                // line 172
                if ((twig_get_attribute($this->env, $this->source, $context["custom_field"], "type", [], "any", false, false, false, 172) == "datetime")) {
                    echo " 
\t\t<input type=\"text\" name=\"custom_field[";
                    // line 173
                    echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "location", [], "any", false, false, false, 173);
                    echo "][";
                    echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 173);
                    echo "]\" value=\"";
                    echo (((($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 = ($context["guest_custom_field"] ?? null)) && is_array($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9) || $__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 instanceof ArrayAccess ? ($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9[twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 173)] ?? null) : null)) ? ((($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae = ($context["guest_custom_field"] ?? null)) && is_array($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae) || $__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae instanceof ArrayAccess ? ($__internal_f6dde3a1020453fdf35e718e94f93ce8eb8803b28cc77a665308e14bbe8572ae[twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 173)] ?? null) : null)) : (twig_get_attribute($this->env, $this->source, $context["custom_field"], "value", [], "any", false, false, false, 173)));
                    echo "\" placeholder=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "name", [], "any", false, false, false, 173);
                    echo "\" id=\"input-payment-custom-field";
                    echo twig_get_attribute($this->env, $this->source, $context["custom_field"], "custom_field_id", [], "any", false, false, false, 173);
                    echo "\" class=\"form-control datetime\" />
\t  ";
                }
                // line 174
                echo " 
    </div>
  ";
            }
            // line 177
            echo "  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['custom_field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 178
        echo "</div>
<div style=\"clear:both;\"></div>
<div class=\"labels-holder\">
  ";
        // line 181
        if (twig_get_attribute($this->env, $this->source, ($context["field_register"] ?? null), "display", [], "any", false, false, false, 181)) {
            echo " 
\t";
            // line 182
            if (( !($context["guest_checkout"] ?? null) || twig_get_attribute($this->env, $this->source, ($context["field_register"] ?? null), "required", [], "any", false, false, false, 182))) {
                echo " 
\t  <input type=\"checkbox\" name=\"create_account\" value=\"1\" id=\"create\" class=\"hide\" checked=\"checked\" />
\t";
            } else {
                // line 185
                echo "\t  <input type=\"checkbox\" name=\"create_account\" value=\"1\" id=\"create\"";
                echo ((($context["create_account"] ?? null)) ? (" checked=\"checked\"") : (""));
                echo " />
\t  <label for=\"create\">";
                // line 186
                echo ($context["text_create_account"] ?? null);
                echo "</label><br />
\t";
            }
            // line 187
            echo " 
\t<div id=\"create_account\">";
            // line 188
            echo ($context["register"] ?? null);
            echo "</div>
  ";
        } else {
            // line 190
            echo "    <input type=\"checkbox\" name=\"create_account\" value=\"1\" id=\"create\" class=\"hide\" />
  ";
        }
        // line 192
        echo "  ";
        if (($context["shipping_required"] ?? null)) {
            echo " 
    <input type=\"checkbox\" name=\"shipping_address\" value=\"1\" id=\"shipping\"";
            // line 193
            echo ((($context["shipping_address"] ?? null)) ? (" checked=\"checked\"") : (""));
            echo " />
    <label for=\"shipping\">";
            // line 194
            echo ($context["entry_shipping"] ?? null);
            echo "</label>
  ";
        } else {
            // line 195
            echo "   
    <input type=\"checkbox\" name=\"shipping_address\" value=\"1\" id=\"shipping\" checked=\"checked\" class=\"hide\" />
  ";
        }
        // line 198
        echo "</div>
<link href=\"catalog/view/javascript/select2/select2.css\" rel=\"stylesheet\" />
<script src=\"catalog/view/javascript/select2/select2.min.js\"></script>
<script type=\"text/javascript\"><!--
\$(document).ready(function() {
\t// Sort the custom fields

\t\$('#input-payment-postcode').select2();



\t\$('#custom-field-payment .custom-field[data-sort]').detach().each(function() {
\t\tif (\$(this).attr('data-sort') >= 0 && \$(this).attr('data-sort') <= \$('#payment-address .col-sm-6').length) {
\t\t\t\$('#payment-address .col-sm-6').eq(\$(this).attr('data-sort')).before(this);
\t\t} 
\t\t
\t\tif (\$(this).attr('data-sort') > \$('#payment-address .col-sm-6').length) {
\t\t\t\$('#payment-address .col-sm-6:last').after(this);
\t\t}
\t\t\t
\t\tif (\$(this).attr('data-sort') < -\$('#payment-address .col-sm-6').length) {
\t\t\t\$('#payment-address .col-sm-6:first').before(this);
\t\t}
\t});

\t\$('#payment-address select[name=\\'customer_group_id\\']').on('change', function() {
\t\t\$.ajax({
\t\t\turl: 'index.php?route=checkout/checkout/customfield&customer_group_id=' + this.value,
\t\t\tdataType: 'json',
\t\t\tsuccess: function(json) {
\t\t\t\t\$('#payment-address .custom-field').hide();
\t\t\t\t\$('#payment-address .custom-field').removeClass('required');

\t\t\t\tfor (i = 0; i < json.length; i++) {
\t\t\t\t\tcustom_field = json[i];

\t\t\t\t\t\$('#payment-custom-field' + custom_field['custom_field_id']).show();

\t\t\t\t\tif (custom_field['required']) {
\t\t\t\t\t\t\$('#payment-custom-field' + custom_field['custom_field_id']).addClass('required');
\t\t\t\t\t} else {
\t\t\t\t\t\t\$('#payment-custom-field' + custom_field['custom_field_id']).removeClass('required');
\t\t\t\t\t}
\t\t\t\t}
\t\t\t\t
\t\t\t\t";
        // line 243
        if (($context["shipping_required"] ?? null)) {
            echo " 
\t\t\t\t\$('#shipping-address .custom-field').hide();
\t\t\t\t\$('#shipping-address .custom-field').removeClass('required');

\t\t\t\tfor (i = 0; i < json.length; i++) {
\t\t\t\t\tcustom_field = json[i];

\t\t\t\t\t\$('#shipping-custom-field' + custom_field['custom_field_id']).show();

\t\t\t\t\tif (custom_field['required']) {
\t\t\t\t\t\t\$('#shipping-custom-field' + custom_field['custom_field_id']).addClass('required');
\t\t\t\t\t} else {
\t\t\t\t\t\t\$('#shipping-custom-field' + custom_field['custom_field_id']).removeClass('required');
\t\t\t\t\t}
\t\t\t\t}
\t\t\t\t";
        }
        // line 258
        echo " 
\t\t\t},
\t\t\t";
        // line 260
        if (($context["debug"] ?? null)) {
            echo " 
\t\t\terror: function(xhr, ajaxOptions, thrownError) {
\t\t\t\talert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t\t}
\t\t\t";
        }
        // line 264
        echo " 
\t\t});
\t});

\t\$('#payment-address select[name=\\'customer_group_id\\']').trigger('change');

\t\$('#payment-address button[id^=\\'button-payment-custom-field\\']').on('click', function() {
\t\tvar node = this;

\t\t\$('#form-upload').remove();

\t\t\$('body').prepend('<form enctype=\"multipart/form-data\" id=\"form-upload\" style=\"display: none;\"><input type=\"file\" name=\"file\" /></form>');

\t\t\$('#form-upload input[name=\\'file\\']').trigger('click');

\t\ttimer = setInterval(function() {
\t\t\tif (\$('#form-upload input[name=\\'file\\']').val() != '') {
\t\t\t\tclearInterval(timer);
\t\t\t
\t\t\t\t\$.ajax({
\t\t\t\t\turl: 'index.php?route=tool/upload',
\t\t\t\t\ttype: 'post',
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tdata: new FormData(\$('#form-upload')[0]),
\t\t\t\t\tcache: false,
\t\t\t\t\tcontentType: false,
\t\t\t\t\tprocessData: false,
\t\t\t\t\tbeforeSend: function() {
\t\t\t\t\t\t\$(node).button('loading');
\t\t\t\t\t},
\t\t\t\t\tcomplete: function() {
\t\t\t\t\t\t\$(node).button('reset');
\t\t\t\t\t},
\t\t\t\t\tsuccess: function(json) {
\t\t\t\t\t\t\$('.text-danger').remove();
\t\t\t\t\t\t
\t\t\t\t\t\tif (json['error']) {
\t\t\t\t\t\t\t\$(node).parent().find('input[name^=\\'custom_field\\']').after('<div class=\"text-danger\">' + json['error'] + '</div>');
\t\t\t\t\t\t}
\t\t
\t\t\t\t\t\tif (json['success']) {
\t\t\t\t\t\t\talert(json['success']);
\t\t
\t\t\t\t\t\t\t\$(node).parent().find('input[name^=\\'custom_field\\']').attr('value', json['file']);
\t\t\t\t\t\t}
\t\t\t\t\t},
\t\t\t\t\terror: function(xhr, ajaxOptions, thrownError) {
\t\t\t\t\t\talert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t\t\t\t}
\t\t\t\t});
\t\t\t}
\t\t}, 500);
\t});

\t\$('#payment-address select[name=\\'country_id\\']').on('change', function() {
\t\t\$.ajax({
\t\t\turl: 'index.php?route=extension/quickcheckout/checkout/country&country_id=' + this.value,
\t\t\tdataType: 'json',
\t\t\tcache: false,
\t\t\tbeforeSend: function() {
\t\t\t\t\$('#payment-address select[name=\\'country_id\\']').after('<i class=\"fa fa-spinner fa-spin\"></i>');
\t\t\t},
\t\t\tcomplete: function() {
\t\t\t\t\$('.fa-spinner').remove();
\t\t\t},\t\t\t
\t\t\tsuccess: function(json) {
\t\t\t\tif (json['postcode_required'] == '1') {
\t\t\t\t\t\$('#payment-postcode-required').addClass('required');
\t\t\t\t} else {
\t\t\t\t\t\$('#payment-postcode-required').removeClass('required');
\t\t\t\t}
\t\t\t\t
\t\t\t\tvar html = '';
\t\t\t\t
\t\t\t\tif (json['zone'] != '') {
\t\t\t\t\tfor (i = 0; i < json['zone'].length; i++) {
\t\t\t\t\t\thtml += '<option value=\"' + json['zone'][i]['zone_id'] + '\"';
\t\t\t\t\t\t
\t\t\t\t\t\tif (json['zone'][i]['zone_id'] == '";
        // line 342
        echo ($context["zone_id"] ?? null);
        echo "') {
\t\t\t\t\t\t\thtml += ' selected=\"selected\"';
\t\t\t\t\t\t}
\t\t
\t\t\t\t\t\thtml += '>' + json['zone'][i]['name'] + '</option>';
\t\t\t\t\t}
\t\t\t\t} else {
\t\t\t\t\thtml += '<option value=\"0\" selected=\"selected\">";
        // line 349
        echo ($context["text_none"] ?? null);
        echo "</option>';
\t\t\t\t}
\t\t\t\t
\t\t\t\t\$('#payment-address select[name=\\'zone_id\\']').html(html).trigger('change');
\t\t\t},
\t\t\t";
        // line 354
        if (($context["debug"] ?? null)) {
            echo " 
\t\t\terror: function(xhr, ajaxOptions, thrownError) {
\t\t\t\talert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t\t}
\t\t\t";
        }
        // line 358
        echo " 
\t\t});
\t});

\t\$('#payment-address select[name=\\'country_id\\']').trigger('change');

\t";
        // line 364
        if (($context["shipping_required"] ?? null)) {
            echo " 
\t\t// Guest Shipping Form
\t\t\$('#payment-address input[name=\\'shipping_address\\']').on('change', function() {
\t\t\tif (\$('#payment-address input[name=\\'shipping_address\\']:checked').val()) {
\t\t\t\t\$('#shipping-address').slideUp('slow');

\t\t\t\t";
            // line 370
            if (($context["shipping_required"] ?? null)) {
                echo " 
\t\t\t\treloadShippingMethod('payment');
\t\t\t\t";
            }
            // line 372
            echo " 
\t\t\t} else {
\t\t\t\t\$.ajax({
\t\t\t\t\turl: 'index.php?route=extension/quickcheckout/guest_shipping&customer_group_id=' + \$('#payment-address select[name=\\'customer_group_id\\']').val(),
\t\t\t\t\tdataType: 'html',
\t\t\t\t\tcache: false,
\t\t\t\t\tbeforeSend: function() {
\t\t\t\t\t\t// Nothing at the moment
\t\t\t\t\t},
\t\t\t\t\tsuccess: function(html) {
\t\t\t\t\t\t\$('#shipping-address .quickcheckout-content').html(html);
\t\t\t\t\t\t
\t\t\t\t\t\t\$('#shipping-address').slideDown('slow');
\t\t\t\t\t},
\t\t\t\t\t";
            // line 386
            if (($context["debug"] ?? null)) {
                echo " 
\t\t\t\t\terror: function(xhr, ajaxOptions, thrownError) {
\t\t\t\t\t\talert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t\t\t\t}
\t\t\t\t\t";
            }
            // line 390
            echo " 
\t\t\t\t});
\t\t\t}
\t\t});
\t\t
\t\t";
            // line 395
            if (($context["shipping_address"] ?? null)) {
                echo " 
\t\t\$('#shipping-address').hide();
\t\t";
            } else {
                // line 397
                echo "   
\t\t\$('#payment-address input[name=\\'shipping_address\\']').trigger('change');
\t\t";
            }
            // line 399
            echo " 
\t";
        }
        // line 401
        echo "
\t\$('#payment-address select[name=\\'zone_id\\']').on('change', function() {
\t\treloadPaymentMethod();
\t\t
\t\t";
        // line 405
        if (($context["shipping_required"] ?? null)) {
            echo " 
\t\tif (\$('#payment-address input[name=\\'shipping_address\\']:checked').val()) {
\t\t\treloadShippingMethod('payment');
\t\t}
\t\t";
        }
        // line 409
        echo " 
\t});

\t// Create account
\t\$('#payment-address input[name=\\'create_account\\']').on('change', function() {
\t\tif (\$('#payment-address input[name=\\'create_account\\']:checked').val()) {
\t\t\t\$('#create_account').slideDown('slow');
\t\t} else {
\t\t\t\$('#create_account').slideUp('slow');
\t\t}
\t});

\t";
        // line 421
        if (((($context["create_account"] ?? null) ||  !($context["guest_checkout"] ?? null)) || twig_get_attribute($this->env, $this->source, ($context["field_register"] ?? null), "required", [], "any", false, false, false, 421))) {
            echo " 
\t\$('#create_account').show();
\t";
        } else {
            // line 423
            echo "   
\t\$('#create_account').hide();
\t";
        }
        // line 426
        echo "});
//--></script>";
    }

    public function getTemplateName()
    {
        return "basel/template/extension/quickcheckout/guest.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1043 => 426,  1038 => 423,  1032 => 421,  1018 => 409,  1010 => 405,  1004 => 401,  1000 => 399,  995 => 397,  989 => 395,  982 => 390,  974 => 386,  958 => 372,  952 => 370,  943 => 364,  935 => 358,  927 => 354,  919 => 349,  909 => 342,  829 => 264,  821 => 260,  817 => 258,  798 => 243,  751 => 198,  746 => 195,  741 => 194,  737 => 193,  732 => 192,  728 => 190,  723 => 188,  720 => 187,  715 => 186,  710 => 185,  704 => 182,  700 => 181,  695 => 178,  689 => 177,  684 => 174,  671 => 173,  667 => 172,  664 => 171,  651 => 170,  647 => 169,  644 => 168,  631 => 167,  627 => 166,  624 => 165,  615 => 164,  607 => 163,  602 => 161,  599 => 160,  586 => 159,  582 => 158,  579 => 157,  566 => 156,  561 => 155,  558 => 154,  550 => 151,  545 => 150,  537 => 149,  533 => 147,  528 => 146,  520 => 145,  517 => 144,  515 => 143,  508 => 141,  503 => 140,  500 => 139,  493 => 137,  488 => 135,  480 => 134,  476 => 132,  471 => 131,  463 => 130,  458 => 128,  451 => 126,  446 => 125,  442 => 123,  436 => 122,  429 => 120,  426 => 119,  419 => 118,  415 => 117,  409 => 116,  405 => 115,  397 => 114,  393 => 113,  387 => 112,  381 => 111,  377 => 110,  371 => 109,  367 => 107,  360 => 105,  353 => 103,  350 => 102,  345 => 100,  332 => 96,  326 => 95,  320 => 94,  317 => 93,  305 => 88,  299 => 87,  293 => 86,  289 => 84,  287 => 83,  264 => 63,  257 => 61,  250 => 59,  246 => 57,  244 => 56,  241 => 55,  239 => 54,  234 => 53,  229 => 50,  217 => 49,  211 => 48,  207 => 46,  201 => 43,  189 => 42,  183 => 41,  178 => 39,  174 => 38,  170 => 37,  166 => 36,  163 => 35,  158 => 33,  151 => 30,  147 => 29,  143 => 28,  138 => 27,  134 => 25,  128 => 24,  121 => 22,  118 => 21,  111 => 20,  107 => 19,  101 => 18,  97 => 16,  92 => 14,  86 => 13,  79 => 11,  76 => 10,  69 => 9,  65 => 8,  59 => 7,  54 => 5,  50 => 4,  45 => 3,  43 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "basel/template/extension/quickcheckout/guest.twig", "");
    }
}
