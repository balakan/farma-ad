<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/module/quickcheckout.twig */
class __TwigTemplate_9c22d04b3b01fbc77edb66babe5a0708e17d7d53933ffe7a28966a64f0fd8688 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo " ";
        echo ($context["column_left"] ?? null);
        echo " 
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <button type=\"submit\" form=\"form\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo ($context["button_save"] ?? null);
        echo "\" class=\"btn btn-success\"><i class=\"fa fa-save\"></i></button>
        <a onclick=\"\$('#form').attr('action', '";
        // line 7
        echo ($context["continue"] ?? null);
        echo "');\$('#form').submit();\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_continue"] ?? null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-check\"></i></a>
        <a href=\"";
        // line 8
        echo ($context["cancel"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_cancel"] ?? null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1>";
        // line 9
        echo ($context["heading_title"] ?? null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            echo " 
        <li><a href=\"";
            // line 12
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 12);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 12);
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo " 
      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">
    ";
        // line 18
        if (($context["success"] ?? null)) {
            echo " 
    <div class=\"alert alert-success\"><i class=\"fa fa-check-circle\"></i>";
            // line 19
            echo ($context["success"] ?? null);
            echo " 
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 22
        echo " 
\t";
        // line 23
        if (($context["error_warning"] ?? null)) {
            echo " 
    <div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i>";
            // line 24
            echo ($context["error_warning"] ?? null);
            echo " 
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 27
        echo " 
\t<div class=\"alert alert-info\">
\t  ";
        // line 29
        echo ($context["entry_store"] ?? null);
        echo " 
\t  <select onchange=\"store();\" name=\"store_id\">
\t\t<option value=\"0\"";
        // line 31
        echo (((($context["store_id"] ?? null) == "0")) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_default_store"] ?? null);
        echo "</option>
\t\t";
        // line 32
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["stores"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["store"]) {
            echo " 
\t\t<option value=\"";
            // line 33
            echo twig_get_attribute($this->env, $this->source, $context["store"], "store_id", [], "any", false, false, false, 33);
            echo "\"";
            echo (((($context["store_id"] ?? null) == twig_get_attribute($this->env, $this->source, $context["store"], "store_id", [], "any", false, false, false, 33))) ? (" selected=\"selected\"") : (""));
            echo ">";
            echo twig_get_attribute($this->env, $this->source, $context["store"], "name", [], "any", false, false, false, 33);
            echo "</option>
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['store'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo " 
\t  </select>
    </div>
    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i>";
        // line 39
        echo ($context["text_edit"] ?? null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
        <form action=\"";
        // line 42
        echo ($context["action"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form\" class=\"form-horizontal\">
\t\t  <ul class=\"nav nav-tabs\">
\t\t\t<li class=\"active\"><a href=\"#tab-home\" data-toggle=\"tab\"><i data-toggle=\"tooltip\" title=\"";
        // line 44
        echo ($context["tab_home"] ?? null);
        echo "\" class=\"fa fa-home\"></i></a></li>
\t\t\t<li><a href=\"#tab-general\" data-toggle=\"tab\"><i data-toggle=\"tooltip\" title=\"";
        // line 45
        echo ($context["tab_general"] ?? null);
        echo "\" class=\"fa fa-gear\"></i></a></li>
\t\t\t<li><a href=\"#tab-design\" data-toggle=\"tab\"><i data-toggle=\"tooltip\" title=\"";
        // line 46
        echo ($context["tab_design"] ?? null);
        echo "\" class=\"fa fa-television\"></i></a></li>
\t\t\t<li><a href=\"#tab-field\" data-toggle=\"tab\"><i data-toggle=\"tooltip\" title=\"";
        // line 47
        echo ($context["tab_field"] ?? null);
        echo "\" class=\"fa fa-bars\"></i></a></li>
\t\t\t<li><a href=\"#tab-module\" data-toggle=\"tab\"><i data-toggle=\"tooltip\" title=\"";
        // line 48
        echo ($context["tab_module"] ?? null);
        echo "\" class=\"fa fa-puzzle-piece\"></i></a></li>
\t\t\t<li><a href=\"#tab-payment\" data-toggle=\"tab\"><i data-toggle=\"tooltip\" title=\"";
        // line 49
        echo ($context["tab_payment"] ?? null);
        echo "\" class=\"fa fa-credit-card\"></i></a></li>
\t\t\t<li><a href=\"#tab-shipping\" data-toggle=\"tab\"><i data-toggle=\"tooltip\" title=\"";
        // line 50
        echo ($context["tab_shipping"] ?? null);
        echo "\" class=\"fa fa-ship\"></i></a></li>
\t\t\t<li><a href=\"#tab-survey\" data-toggle=\"tab\"><i data-toggle=\"tooltip\" title=\"";
        // line 51
        echo ($context["tab_survey"] ?? null);
        echo "\" class=\"fa fa-edit\"></i></a></li>
\t\t\t<li><a href=\"#tab-delivery\" data-toggle=\"tab\"><i data-toggle=\"tooltip\" title=\"";
        // line 52
        echo ($context["tab_delivery"] ?? null);
        echo "\" class=\"fa fa-truck\"></i></a></li>
\t\t\t<li><a href=\"#tab-countdown\" data-toggle=\"tab\"><i data-toggle=\"tooltip\" title=\"";
        // line 53
        echo ($context["tab_countdown"] ?? null);
        echo "\" class=\"fa fa-clock-o\"></i></a></li>
\t\t\t<!--<li><a href=\"#tab-analytics\" data-toggle=\"tab\"><i data-toggle=\"tooltip\" title=\"";
        // line 54
        echo ($context["tab_analytics"] ?? null);
        echo "\" class=\"fa fa-line-chart\"></i></a></li>-->
\t\t  </ul>
\t\t  <div class=\"tab-content\">
\t\t\t<div class=\"tab-pane active\" id=\"tab-home\">
\t\t\t  <div class=\"row\">
\t\t\t\t<div class=\"col-xs-4 text-center\">
\t\t\t\t  <a href=\"#tab-general\" data-toggle=\"tab\" onclick=\"show('#tab-general');\"><i title=\"";
        // line 60
        echo ($context["text_general"] ?? null);
        echo "\" data-toggle=\"tooltip\" class=\"fa fa-gear fa-5x fa-fw\"></i></a>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-xs-4 text-center\">
\t\t\t\t  <a href=\"#tab-design\" data-toggle=\"tab\" onclick=\"show('#tab-design');\"><i title=\"";
        // line 63
        echo ($context["text_design"] ?? null);
        echo "\" data-toggle=\"tooltip\" class=\"fa fa-television fa-5x fa-fw\"></i></a>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-xs-4 text-center\">
\t\t\t\t  <a href=\"#tab-field\" data-toggle=\"tab\" onclick=\"show('#tab-field')\"><i title=\"";
        // line 66
        echo ($context["text_field"] ?? null);
        echo "\" data-toggle=\"tooltip\" class=\"fa fa-bars fa-5x fa-fw\"></i></a>
\t\t\t\t</div>
\t\t\t  </div><br />
\t\t\t  <div class=\"row\">
\t\t\t\t<div class=\"col-xs-4 text-center\">
\t\t\t\t  <a href=\"#tab-module\" data-toggle=\"tab\" onclick=\"show('#tab-module')\"><i title=\"";
        // line 71
        echo ($context["text_module_home"] ?? null);
        echo "\" data-toggle=\"tooltip\" class=\"fa fa-puzzle-piece fa-5x fa-fw\"></i></a>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-xs-4 text-center\">
\t\t\t\t  <a href=\"#tab-payment\" data-toggle=\"tab\" onclick=\"show('#tab-payment')\"><i title=\"";
        // line 74
        echo ($context["text_payment"] ?? null);
        echo "\" data-toggle=\"tooltip\" class=\"fa fa-credit-card fa-5x fa-fw\"></i></a>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-xs-4 text-center\">
\t\t\t\t  <a href=\"#tab-shipping\" data-toggle=\"tab\" onclick=\"show('#tab-shipping')\"><i title=\"";
        // line 77
        echo ($context["text_shipping"] ?? null);
        echo "\" data-toggle=\"tooltip\" class=\"fa fa-ship fa-5x fa-fw\"></i></a>
\t\t\t\t</div>
\t\t\t  </div><br />
\t\t\t  <div class=\"row\">
\t\t\t    <div class=\"col-xs-4 text-center\">
\t\t\t\t  <a href=\"#tab-survey\" data-toggle=\"tab\" onclick=\"show('#tab-survey')\"><i title=\"";
        // line 82
        echo ($context["text_survey"] ?? null);
        echo "\" data-toggle=\"tooltip\" class=\"fa fa-edit fa-5x fa-fw\"></i></a>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-xs-4 text-center\">
\t\t\t\t  <a href=\"#tab-delivery\" data-toggle=\"tab\" onclick=\"show('#tab-delivery')\"><i title=\"";
        // line 85
        echo ($context["text_delivery"] ?? null);
        echo "\" data-toggle=\"tooltip\" class=\"fa fa-truck fa-5x fa-fw\"></i></a>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-xs-4 text-center\">
\t\t\t\t  <a href=\"#tab-countdown\" data-toggle=\"tab\" onclick=\"show('#tab-countdown')\"><i title=\"";
        // line 88
        echo ($context["text_countdown"] ?? null);
        echo "\" data-toggle=\"tooltip\" class=\"fa fa-clock-o fa-5x fa-fw\"></i></a>
\t\t\t\t</div>
\t\t\t  </div><br />
\t\t\t  <div class=\"row\">
\t\t\t    <div class=\"col-xs-4 text-center\">
\t\t\t\t  <a href=\"#tab-analytics\" data-toggle=\"tab\" onclick=\"show('#tab-analytics')\"><i title=\"";
        // line 93
        echo ($context["text_analytics"] ?? null);
        echo "\" data-toggle=\"tooltip\" class=\"fa fa-line-chart fa-5x fa-fw\"></i></a>
\t\t\t\t</div>
\t\t\t    <div class=\"col-xs-4 text-center\">
\t\t\t\t  <a href=\"#tab-about\" data-toggle=\"tab\" onclick=\"show('#tab-about')\"><i title=\"About Quick Checkout\" data-toggle=\"tooltip\" class=\"fa fa-question-circle fa-5x fa-fw\"></i></a>
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t</div>
            <div class=\"tab-pane\" id=\"tab-general\">
\t\t\t  <div class=\"row\">
\t\t\t    <div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-status\"><span title=\"";
        // line 103
        echo ($context["help_status"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_status"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<select name=\"quickcheckout_status\" id=\"input-status\" class=\"form-control\">
\t\t\t\t\t  <option value=\"1\"";
        // line 106
        echo ((($context["quickcheckout_status"] ?? null)) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_enabled"] ?? null);
        echo "</option>
\t\t\t\t\t  <option value=\"0\"";
        // line 107
        echo ((($context["quickcheckout_status"] ?? null)) ? ("") : (" selected=\"selected\""));
        echo ">";
        echo ($context["text_disabled"] ?? null);
        echo "</option>
\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-minimum-order\"><span title=\"";
        // line 112
        echo ($context["help_minimum_order"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_minimum_order"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<input type=\"text\" name=\"quickcheckout_minimum_order\" value=\"";
        // line 114
        echo ($context["quickcheckout_minimum_order"] ?? null);
        echo "\" class=\"form-control\" />
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t  <div class=\"row\">
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-debug\"><span title=\"";
        // line 120
        echo ($context["help_debug"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_debug"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<select name=\"quickcheckout_debug\" id=\"input-debug\" class=\"form-control\">
\t\t\t\t\t  <option value=\"1\"";
        // line 123
        echo ((($context["quickcheckout_debug"] ?? null)) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_enabled"] ?? null);
        echo "</option>
\t\t\t\t\t  <option value=\"0\"";
        // line 124
        echo ((($context["quickcheckout_debug"] ?? null)) ? ("") : (" selected=\"selected\""));
        echo ">";
        echo ($context["text_disabled"] ?? null);
        echo "</option>
\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-confirmation-page\"><span title=\"";
        // line 129
        echo ($context["help_confirmation_page"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_confirmation_page"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<select name=\"quickcheckout_confirmation_page\" id=\"input-confirmation-page\" class=\"form-control\">
\t\t\t\t\t  <option value=\"1\"";
        // line 132
        echo ((($context["quickcheckout_confirmation_page"] ?? null)) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_enabled"] ?? null);
        echo "</option>
\t\t\t\t\t  <option value=\"0\"";
        // line 133
        echo ((($context["quickcheckout_confirmation_page"] ?? null)) ? ("") : (" selected=\"selected\""));
        echo ">";
        echo ($context["text_disabled"] ?? null);
        echo "</option>
\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t  <div class=\"row\">
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-save-data\"><span title=\"";
        // line 140
        echo ($context["help_save_data"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_save_data"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<select name=\"quickcheckout_save_data\" id=\"input-save-data\" class=\"form-control\">
\t\t\t\t\t  <option value=\"1\"";
        // line 143
        echo ((($context["quickcheckout_save_data"] ?? null)) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_enabled"] ?? null);
        echo "</option>
\t\t\t\t\t  <option value=\"0\"";
        // line 144
        echo ((($context["quickcheckout_save_data"] ?? null)) ? ("") : (" selected=\"selected\""));
        echo ">";
        echo ($context["text_disabled"] ?? null);
        echo "</option>
\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-edit-cart\"><span title=\"";
        // line 149
        echo ($context["help_edit_cart"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_edit_cart"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<select name=\"quickcheckout_edit_cart\" id=\"input-edit-cart\" class=\"form-control\">
\t\t\t\t\t  <option value=\"1\"";
        // line 152
        echo ((($context["quickcheckout_edit_cart"] ?? null)) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_enabled"] ?? null);
        echo "</option>
\t\t\t\t\t  <option value=\"0\"";
        // line 153
        echo ((($context["quickcheckout_edit_cart"] ?? null)) ? ("") : (" selected=\"selected\""));
        echo ">";
        echo ($context["text_disabled"] ?? null);
        echo "</option>
\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t  <div class=\"row\">
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-highlight-error\"><span title=\"";
        // line 160
        echo ($context["help_highlight_error"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_highlight_error"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<select name=\"quickcheckout_highlight_error\" id=\"input-highlight-error\" class=\"form-control\">
\t\t\t\t\t  <option value=\"1\"";
        // line 163
        echo ((($context["quickcheckout_highlight_error"] ?? null)) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_enabled"] ?? null);
        echo "</option>
\t\t\t\t\t  <option value=\"0\"";
        // line 164
        echo ((($context["quickcheckout_highlight_error"] ?? null)) ? ("") : (" selected=\"selected\""));
        echo ">";
        echo ($context["text_disabled"] ?? null);
        echo "</option>
\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-text-error\"><span title=\"";
        // line 169
        echo ($context["help_text_error"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_text_error"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<select name=\"quickcheckout_text_error\" id=\"input-text-error\" class=\"form-control\">
\t\t\t\t\t  <option value=\"1\"";
        // line 172
        echo ((($context["quickcheckout_text_error"] ?? null)) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_enabled"] ?? null);
        echo "</option>
\t\t\t\t\t  <option value=\"0\"";
        // line 173
        echo ((($context["quickcheckout_text_error"] ?? null)) ? ("") : (" selected=\"selected\""));
        echo ">";
        echo ($context["text_disabled"] ?? null);
        echo "</option>
\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t  <div class=\"row\">
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-auto-submit\"><span title=\"";
        // line 180
        echo ($context["help_auto_submit"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_auto_submit"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<select name=\"quickcheckout_auto_submit\" id=\"input-auto-submit\" class=\"form-control\">
\t\t\t\t\t  <option value=\"1\"";
        // line 183
        echo ((($context["quickcheckout_auto_submit"] ?? null)) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_enabled"] ?? null);
        echo "</option>
\t\t\t\t\t  <option value=\"0\"";
        // line 184
        echo ((($context["quickcheckout_auto_submit"] ?? null)) ? ("") : (" selected=\"selected\""));
        echo ">";
        echo ($context["text_disabled"] ?? null);
        echo "</option>
\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-payment-target\"><span title=\"";
        // line 189
        echo ($context["help_payment_target"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_payment_target"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t    <input type=\"text\" name=\"quickcheckout_payment_target\" id=\"input-payment-target\" class=\"form-control\" value=\"";
        // line 191
        echo ($context["quickcheckout_payment_target"] ?? null);
        echo "\" />
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t  <div class=\"row\">
\t\t\t    <div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-proceed-button-text\"><span title=\"";
        // line 197
        echo ($context["help_proceed_button_text"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_proceed_button_text"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t";
        // line 199
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo " 
\t\t\t\t\t  <div class=\"input-group\">
\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 201
            echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 201);
            echo "/";
            echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 201);
            echo ".png\" title=\"";
            echo twig_get_attribute($this->env, $this->source, $context["language"], "name", [], "any", false, false, false, 201);
            echo "\" /></span>
\t\t\t\t\t\t<input type=\"text\" name=\"quickcheckout_proceed_button_text[";
            // line 202
            echo twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 202);
            echo "]\" value=\"";
            echo (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["quickcheckout_proceed_button_text"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4[twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 202)] ?? null) : null);
            echo "\" class=\"form-control\" />
\t\t\t\t\t  </div>
\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 204
        echo " 
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t</div>
\t\t\t<div class=\"tab-pane\" id=\"tab-design\">
\t\t\t  <div class=\"row\">
\t\t\t    <div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-load-screen\"><span title=\"";
        // line 212
        echo ($context["help_load_screen"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_load_screen"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<select name=\"quickcheckout_load_screen\" id=\"input-load-screen\" class=\"form-control\">
\t\t\t\t\t  <option value=\"1\"";
        // line 215
        echo ((($context["quickcheckout_load_screen"] ?? null)) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_enabled"] ?? null);
        echo "</option>
\t\t\t\t\t  <option value=\"0\"";
        // line 216
        echo ((($context["quickcheckout_load_screen"] ?? null)) ? ("") : (" selected=\"selected\""));
        echo ">";
        echo ($context["text_disabled"] ?? null);
        echo "</option>
\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-loading-display\"><span title=\"";
        // line 221
        echo ($context["help_loading_display"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_loading_display"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<select name=\"quickcheckout_loading_display\" id=\"input-loading-display\" class=\"form-control\">
\t\t\t\t\t  <option value=\"1\"";
        // line 224
        echo ((($context["quickcheckout_loading_display"] ?? null)) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_spinner"] ?? null);
        echo "</option>
\t\t\t\t\t  <option value=\"0\"";
        // line 225
        echo ((($context["quickcheckout_loading_display"] ?? null)) ? ("") : (" selected=\"selected\""));
        echo ">";
        echo ($context["text_overlay"] ?? null);
        echo "</option>
\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t  <div class=\"row\">
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-layout\"><span title=\"";
        // line 232
        echo ($context["help_layout"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_layout"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<select name=\"quickcheckout_layout\" id=\"input-layout\" class=\"form-control\">
\t\t\t\t\t  <option value=\"1\"";
        // line 235
        echo (((($context["quickcheckout_layout"] ?? null) == "1")) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_one_column"] ?? null);
        echo "</option>
\t\t\t\t\t  <option value=\"2\"";
        // line 236
        echo (((($context["quickcheckout_layout"] ?? null) == "2")) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_two_column"] ?? null);
        echo "</option>
\t\t\t\t\t  <option value=\"3\"";
        // line 237
        echo (((($context["quickcheckout_layout"] ?? null) == "3")) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_three_column"] ?? null);
        echo "</option>
\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-responsive\"><span title=\"";
        // line 242
        echo ($context["help_responsive"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_responsive"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<select name=\"quickcheckout_responsive\" id=\"input-responsive\" class=\"form-control\">
\t\t\t\t\t  <option value=\"1\"";
        // line 245
        echo ((($context["quickcheckout_responsive"] ?? null)) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_enabled"] ?? null);
        echo "</option>
\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t  <div class=\"row\">
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-slide-effect\"><span title=\"";
        // line 252
        echo ($context["help_slide_effect"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_slide_effect"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<select name=\"quickcheckout_slide_effect\" id=\"input-slide-effect\" class=\"form-control\">
\t\t\t\t\t  <option value=\"1\"";
        // line 255
        echo ((($context["quickcheckout_slide_effect"] ?? null)) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_enabled"] ?? null);
        echo "</option>
\t\t\t\t\t  <option value=\"0\"";
        // line 256
        echo ((($context["quickcheckout_slide_effect"] ?? null)) ? ("") : (" selected=\"selected\""));
        echo ">";
        echo ($context["text_disabled"] ?? null);
        echo "</option>
\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-custom-css\">";
        // line 261
        echo ($context["entry_custom_css"] ?? null);
        echo "</label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t    <textarea name=\"quickcheckout_custom_css\" id=\"input-custom-css\" class=\"form-control\" rows=\"5\">";
        // line 263
        echo ($context["quickcheckout_custom_css"] ?? null);
        echo "</textarea>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t</div>
\t\t\t<div class=\"tab-pane table-responsive\" id=\"tab-field\">
\t\t\t  <table class=\"table table-bordered table-hover table-striped\">
\t\t\t\t<tr>
\t\t\t\t  <td></td>
\t\t\t\t  <td class=\"text-center\">";
        // line 272
        echo ($context["text_display"] ?? null);
        echo "</td>
\t\t\t\t  <td class=\"text-center\">";
        // line 273
        echo ($context["text_required"] ?? null);
        echo "</td>
\t\t\t\t  <td>";
        // line 274
        echo ($context["text_default"] ?? null);
        echo "</td>
\t\t\t\t  <td>";
        // line 275
        echo ($context["text_placeholder"] ?? null);
        echo "</td>
\t\t\t\t  <td>";
        // line 276
        echo ($context["text_sort_order"] ?? null);
        echo "</td>
\t\t\t\t</tr>
\t\t\t\t";
        // line 278
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["fields"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
            echo " 
\t\t\t\t  ";
            // line 279
            if (($context["field"] == "country")) {
                echo " 
\t\t\t\t  <tr>
\t\t\t\t\t<td>";
                // line 281
                echo twig_get_attribute($this->env, $this->source, $context, ("entry_field_" . $context["field"]), [], "any", false, false, false, 281);
                echo "</td>
\t\t\t\t\t<td class=\"text-center\"><input type=\"checkbox\" name=\"quickcheckout_field_";
                // line 282
                echo $context["field"];
                echo "[display]\"";
                echo ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context, ("quickcheckout_field_" . $context["field"]), [], "any", false, false, false, 282), "display", [], "any", false, false, false, 282)) ? (" checked") : (""));
                echo " /></td>
\t\t\t\t\t<td class=\"text-center\"><input type=\"checkbox\" name=\"quickcheckout_field_";
                // line 283
                echo $context["field"];
                echo "[required]\"";
                echo ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context, ("quickcheckout_field_" . $context["field"]), [], "any", false, false, false, 283), "required", [], "any", false, false, false, 283)) ? (" checked") : (""));
                echo " /></td>
\t\t\t\t\t<td><select name=\"quickcheckout_field_";
                // line 284
                echo $context["field"];
                echo "[default]\" class=\"form-control\">
\t\t\t\t\t\t<option value=\"\">";
                // line 285
                echo ($context["text_select"] ?? null);
                echo "</option>
\t\t\t\t\t\t";
                // line 286
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["countries"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["country"]) {
                    echo " 
\t\t\t\t\t\t  ";
                    // line 287
                    if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context, ("quickcheckout_field_" . $context["field"]), [], "any", false, false, false, 287), "default", [], "any", false, false, false, 287) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context, ("quickcheckout_field_" . $context["field"]), [], "any", false, false, false, 287), "default", [], "any", false, false, false, 287) == twig_get_attribute($this->env, $this->source, $context["country"], "country_id", [], "any", false, false, false, 287)))) {
                        // line 288
                        echo "\t\t\t\t\t\t  <option value=\"";
                        echo twig_get_attribute($this->env, $this->source, $context["country"], "country_id", [], "any", false, false, false, 288);
                        echo "\" selected=\"selected\">";
                        echo twig_get_attribute($this->env, $this->source, $context["country"], "name", [], "any", false, false, false, 288);
                        echo "</option>
\t\t\t\t\t\t  ";
                    } else {
                        // line 289
                        echo "   
\t\t\t\t\t\t  <option value=\"";
                        // line 290
                        echo twig_get_attribute($this->env, $this->source, $context["country"], "country_id", [], "any", false, false, false, 290);
                        echo "\">";
                        echo twig_get_attribute($this->env, $this->source, $context["country"], "name", [], "any", false, false, false, 290);
                        echo "</option>
\t\t\t\t\t\t  ";
                    }
                    // line 292
                    echo "\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['country'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 293
                echo "\t\t\t\t\t  </select></td>
\t\t\t\t\t<td class=\"text-center\">NA</td>
\t\t\t\t\t<td><input type=\"text\" name=\"quickcheckout_field_";
                // line 295
                echo $context["field"];
                echo "[sort_order]\" value=\"";
                echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context, ("quickcheckout_field_" . $context["field"]), [], "any", false, false, false, 295), "sort_order", [], "any", false, false, false, 295);
                echo "\" class=\"form-control\" /></td>
\t\t\t\t  </tr>
\t\t\t\t  ";
            } elseif ((            // line 297
$context["field"] == "zone")) {
                echo " 
\t\t\t\t  <tr>
\t\t\t\t\t<td>";
                // line 299
                echo twig_get_attribute($this->env, $this->source, $context, ("entry_field_" . $context["field"]), [], "any", false, false, false, 299);
                echo "</td>
\t\t\t\t\t<td class=\"text-center\"><input type=\"checkbox\" name=\"quickcheckout_field_";
                // line 300
                echo $context["field"];
                echo "[display]\"";
                echo ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context, ("quickcheckout_field_" . $context["field"]), [], "any", false, false, false, 300), "display", [], "any", false, false, false, 300)) ? (" checked") : (""));
                echo " /></td>
\t\t\t\t\t<td class=\"text-center\"><input type=\"checkbox\" name=\"quickcheckout_field_";
                // line 301
                echo $context["field"];
                echo "[required]\"";
                echo ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context, ("quickcheckout_field_" . $context["field"]), [], "any", false, false, false, 301), "required", [], "any", false, false, false, 301)) ? (" checked") : (""));
                echo " /></td>
\t\t\t\t\t<td><select name=\"quickcheckout_field_";
                // line 302
                echo $context["field"];
                echo "[default]\" class=\"form-control\"></select></td>
\t\t\t\t\t<td class=\"text-center\">NA</td>
\t\t\t\t\t<td><input type=\"text\" name=\"quickcheckout_field_";
                // line 304
                echo $context["field"];
                echo "[sort_order]\" value=\"";
                echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context, ("quickcheckout_field_" . $context["field"]), [], "any", false, false, false, 304), "sort_order", [], "any", false, false, false, 304);
                echo "\" class=\"form-control\" /></td>
\t\t\t\t  </tr>
\t\t\t\t  ";
            } elseif ((            // line 306
$context["field"] == "customer_group")) {
                // line 307
                echo "\t\t\t\t  <tr>
\t\t\t\t\t<td>";
                // line 308
                echo twig_get_attribute($this->env, $this->source, $context, ("entry_field_" . $context["field"]), [], "any", false, false, false, 308);
                echo "</td>
\t\t\t\t\t<td class=\"text-center\"><input type=\"checkbox\" name=\"quickcheckout_field_";
                // line 309
                echo $context["field"];
                echo "[display]\"";
                echo ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context, ("quickcheckout_field_" . $context["field"]), [], "any", false, false, false, 309), "display", [], "any", false, false, false, 309)) ? (" checked") : (""));
                echo " /></td>
\t\t\t\t\t<td class=\"text-center\">NA</td>
\t\t\t\t\t<td class=\"text-center\">NA</td>
\t\t\t\t\t<td class=\"text-center\">NA</td>
\t\t\t\t\t<td><input type=\"text\" name=\"quickcheckout_field_";
                // line 313
                echo $context["field"];
                echo "[sort_order]\" value=\"";
                echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context, ("quickcheckout_field_" . $context["field"]), [], "any", false, false, false, 313), "sort_order", [], "any", false, false, false, 313);
                echo "\" class=\"form-control\" /></td>
\t\t\t\t  </tr>
\t\t\t\t   ";
            } elseif (((            // line 315
$context["field"] == "register") || ($context["field"] == "newsletter"))) {
                echo " 
\t\t\t\t  <tr>
\t\t\t\t\t<td>";
                // line 317
                echo twig_get_attribute($this->env, $this->source, $context, ("entry_field_" . $context["field"]), [], "any", false, false, false, 317);
                echo "</td>
\t\t\t\t\t<td class=\"text-center\"><input type=\"checkbox\" name=\"quickcheckout_field_";
                // line 318
                echo $context["field"];
                echo "[display]\"";
                echo ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context, ("quickcheckout_field_" . $context["field"]), [], "any", false, false, false, 318), "display", [], "any", false, false, false, 318)) ? (" checked") : (""));
                echo " /></td>
\t\t\t\t\t<td class=\"text-center\"><input type=\"checkbox\" name=\"quickcheckout_field_";
                // line 319
                echo $context["field"];
                echo "[required]\"";
                echo ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context, ("quickcheckout_field_" . $context["field"]), [], "any", false, false, false, 319), "required", [], "any", false, false, false, 319)) ? (" checked") : (""));
                echo " /></td>
\t\t\t\t\t<td class=\"text-center\"><input type=\"checkbox\" name=\"quickcheckout_field_";
                // line 320
                echo $context["field"];
                echo "[default]\"";
                echo ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context, ("quickcheckout_field_" . $context["field"]), [], "any", false, false, false, 320), "default", [], "any", false, false, false, 320)) ? (" checked") : (""));
                echo " /></td>
\t\t\t\t\t<td class=\"text-center\">NA</td>
\t\t\t\t\t<td><input type=\"text\" name=\"quickcheckout_field_";
                // line 322
                echo $context["field"];
                echo "[sort_order]\" value=\"";
                echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context, ("quickcheckout_field_" . $context["field"]), [], "any", false, false, false, 322), "sort_order", [], "any", false, false, false, 322);
                echo "\" class=\"form-control\" /></td>
\t\t\t\t  </tr>
\t\t\t\t  ";
            } else {
                // line 324
                echo "   
\t\t\t\t  <tr>
\t\t\t\t\t<td>";
                // line 326
                echo twig_get_attribute($this->env, $this->source, $context, ("entry_field_" . $context["field"]), [], "any", false, false, false, 326);
                echo "</td>
\t\t\t\t\t<td class=\"text-center\"><input type=\"checkbox\" name=\"quickcheckout_field_";
                // line 327
                echo $context["field"];
                echo "[display]\"";
                echo ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context, ("quickcheckout_field_" . $context["field"]), [], "any", false, false, false, 327), "display", [], "any", false, false, false, 327)) ? (" checked") : (""));
                echo " /></td>
\t\t\t\t\t";
                // line 328
                if (($context["field"] == "postcode")) {
                    echo " 
\t\t\t\t\t<td class=\"text-center\">NA</td>
\t\t\t\t\t";
                } else {
                    // line 330
                    echo "   
\t\t\t\t\t<td class=\"text-center\"><input type=\"checkbox\" name=\"quickcheckout_field_";
                    // line 331
                    echo $context["field"];
                    echo "[required]\"";
                    echo ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context, ("quickcheckout_field_" . $context["field"]), [], "any", false, false, false, 331), "required", [], "any", false, false, false, 331)) ? (" checked") : (""));
                    echo " /></td>
\t\t\t\t\t";
                }
                // line 333
                echo "\t\t\t\t\t<td>
\t\t\t\t\t  ";
                // line 334
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                    echo " 
\t\t\t\t\t  <div class=\"input-group\">
\t\t\t\t\t    <span class=\"input-group-addon\"><img src=\"language/";
                    // line 336
                    echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 336);
                    echo "/";
                    echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 336);
                    echo ".png\" title=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["language"], "name", [], "any", false, false, false, 336);
                    echo "\" /></span>
\t\t\t\t\t    <input type=\"text\" name=\"quickcheckout_field_";
                    // line 337
                    echo $context["field"];
                    echo "[default][";
                    echo twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 337);
                    echo "]\" value=\"";
                    echo (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context, ("quickcheckout_field_" . $context["field"]), [], "any", false, false, false, 337), "default", [], "any", false, false, false, 337)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144[twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 337)] ?? null) : null);
                    echo "\" class=\"form-control\" />
\t\t\t\t\t  </div>
\t\t\t\t\t  ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 340
                echo "\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t  ";
                // line 342
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                    echo " 
\t\t\t\t\t  <div class=\"input-group\">
\t\t\t\t\t    <span class=\"input-group-addon\"><img src=\"language/";
                    // line 344
                    echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 344);
                    echo "/";
                    echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 344);
                    echo ".png\" title=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["language"], "name", [], "any", false, false, false, 344);
                    echo "\" /></span>
\t\t\t\t\t    <input type=\"text\" name=\"quickcheckout_field_";
                    // line 345
                    echo $context["field"];
                    echo "[placeholder][";
                    echo twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 345);
                    echo "]\" value=\"";
                    echo (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context, ("quickcheckout_field_" . $context["field"]), [], "any", false, false, false, 345), "placeholder", [], "any", false, false, false, 345)) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b[twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 345)] ?? null) : null);
                    echo "\" class=\"form-control\" />
\t\t\t\t\t  </div>
\t\t\t\t\t  ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 348
                echo "\t\t\t\t\t</td>
\t\t\t\t\t";
                // line 349
                if (($context["field"] == "comment")) {
                    echo " 
\t\t\t\t\t<td class=\"text-center\">NA</td>
\t\t\t\t\t";
                } else {
                    // line 351
                    echo "   
\t\t\t\t\t<td><input type=\"text\" name=\"quickcheckout_field_";
                    // line 352
                    echo $context["field"];
                    echo "[sort_order]\" value=\"";
                    echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context, ("quickcheckout_field_" . $context["field"]), [], "any", false, false, false, 352), "sort_order", [], "any", false, false, false, 352);
                    echo "\" class=\"form-control\" /></td>
\t\t\t\t    ";
                }
                // line 354
                echo "\t\t\t\t  </tr>
\t\t\t\t  ";
            }
            // line 356
            echo "\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 357
        echo "\t\t\t  </table>
\t\t\t</div>
\t\t\t<div class=\"tab-pane\" id=\"tab-module\">
\t\t\t  <div class=\"row\">
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-coupon\"><span title=\"";
        // line 362
        echo ($context["help_coupon"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_coupon"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<select name=\"quickcheckout_coupon\" id=\"input-coupon\" class=\"form-control\">
\t\t\t\t\t  <option value=\"1\"";
        // line 365
        echo ((($context["quickcheckout_coupon"] ?? null)) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_enabled"] ?? null);
        echo "</option>
\t\t\t\t\t  <option value=\"0\"";
        // line 366
        echo ((($context["quickcheckout_coupon"] ?? null)) ? ("") : (" selected=\"selected\""));
        echo ">";
        echo ($context["text_disabled"] ?? null);
        echo "</option>
\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-voucher\"><span title=\"";
        // line 371
        echo ($context["help_voucher"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_voucher"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<select name=\"quickcheckout_voucher\" id=\"input-voucher\" class=\"form-control\">
\t\t\t\t\t  <option value=\"1\"";
        // line 374
        echo ((($context["quickcheckout_voucher"] ?? null)) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_enabled"] ?? null);
        echo "</option>
\t\t\t\t\t  <option value=\"0\"";
        // line 375
        echo ((($context["quickcheckout_voucher"] ?? null)) ? ("") : (" selected=\"selected\""));
        echo ">";
        echo ($context["text_disabled"] ?? null);
        echo "</option>
\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t  <div class=\"row\">
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-reward\"><span title=\"";
        // line 382
        echo ($context["help_reward"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_reward"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<select name=\"quickcheckout_reward\" id=\"input-reward\" class=\"form-control\">
\t\t\t\t\t  <option value=\"1\"";
        // line 385
        echo ((($context["quickcheckout_reward"] ?? null)) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_enabled"] ?? null);
        echo "</option>
\t\t\t\t\t  <option value=\"0\"";
        // line 386
        echo ((($context["quickcheckout_reward"] ?? null)) ? ("") : (" selected=\"selected\""));
        echo ">";
        echo ($context["text_disabled"] ?? null);
        echo "</option>
\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-cart\"><span title=\"";
        // line 391
        echo ($context["help_cart"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_cart"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<select name=\"quickcheckout_cart\" id=\"input-cart\" class=\"form-control\">
\t\t\t\t\t  <option value=\"1\"";
        // line 394
        echo ((($context["quickcheckout_cart"] ?? null)) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_enabled"] ?? null);
        echo "</option>
\t\t\t\t\t  <option value=\"0\"";
        // line 395
        echo ((($context["quickcheckout_cart"] ?? null)) ? ("") : (" selected=\"selected\""));
        echo ">";
        echo ($context["text_disabled"] ?? null);
        echo "</option>
\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t  <div class=\"row\">
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-login-module\"><span title=\"";
        // line 402
        echo ($context["help_login_module"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_login_module"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<select name=\"quickcheckout_login_module\" id=\"input-login-module\" class=\"form-control\">
\t\t\t\t\t  <option value=\"1\"";
        // line 405
        echo ((($context["quickcheckout_login_module"] ?? null)) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_enabled"] ?? null);
        echo "</option>
\t\t\t\t\t  <option value=\"0\"";
        // line 406
        echo ((($context["quickcheckout_login_module"] ?? null)) ? ("") : (" selected=\"selected\""));
        echo ">";
        echo ($context["text_disabled"] ?? null);
        echo "</option>
\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t  <div class=\"row\">
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-html-header\"><span title=\"";
        // line 413
        echo ($context["help_html_header"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_html_header"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t";
        // line 415
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo " 
\t\t\t\t\t  <div class=\"input-group\">
\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 417
            echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 417);
            echo "/";
            echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 417);
            echo ".png\" title=\"";
            echo twig_get_attribute($this->env, $this->source, $context["language"], "name", [], "any", false, false, false, 417);
            echo "\" /></span>
\t\t\t\t\t\t<textarea name=\"quickcheckout_html_header[";
            // line 418
            echo twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 418);
            echo "]\" rows=\"7\" cols=\"30\" class=\"form-control\">";
            echo (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = ($context["quickcheckout_html_header"] ?? null)) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002[twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 418)] ?? null) : null);
            echo "</textarea>
\t\t\t\t\t  </div>
\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 420
        echo " 
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-html-footer\"><span title=\"";
        // line 424
        echo ($context["help_html_footer"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_html_footer"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t";
        // line 426
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo " 
\t\t\t\t\t  <div class=\"input-group\">
\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 428
            echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 428);
            echo "/";
            echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 428);
            echo ".png\" title=\"";
            echo twig_get_attribute($this->env, $this->source, $context["language"], "name", [], "any", false, false, false, 428);
            echo "\" /></span>
\t\t\t\t\t\t<textarea name=\"quickcheckout_html_footer[";
            // line 429
            echo twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 429);
            echo "]\" rows=\"7\" cols=\"30\" class=\"form-control\">";
            echo (($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = ($context["quickcheckout_html_footer"] ?? null)) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4[twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 429)] ?? null) : null);
            echo "</textarea>
\t\t\t\t\t  </div>
\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 431
        echo " 
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t</div>
\t\t\t<div class=\"tab-pane\" id=\"tab-payment\">
\t\t\t  <div class=\"row\">
\t\t\t    <div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-payment-module\"><span title=\"";
        // line 439
        echo ($context["help_payment_module"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_payment_module"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<select name=\"quickcheckout_payment_module\" id=\"input-payment-module\" class=\"form-control\">
\t\t\t\t\t  <option value=\"1\"";
        // line 442
        echo ((($context["quickcheckout_payment_module"] ?? null)) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_enabled"] ?? null);
        echo "</option>
\t\t\t\t\t  <option value=\"0\"";
        // line 443
        echo ((($context["quickcheckout_payment_module"] ?? null)) ? ("") : (" selected=\"selected\""));
        echo ">";
        echo ($context["text_disabled"] ?? null);
        echo "</option>
\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-payment-reload\"><span title=\"";
        // line 448
        echo ($context["help_payment_reload"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_payment_reload"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<select name=\"quickcheckout_payment_reload\" id=\"input-payment-reload\" class=\"form-control\">
\t\t\t\t\t  <option value=\"1\"";
        // line 451
        echo ((($context["quickcheckout_payment_reload"] ?? null)) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_enabled"] ?? null);
        echo "</option>
\t\t\t\t\t  <option value=\"0\"";
        // line 452
        echo ((($context["quickcheckout_payment_reload"] ?? null)) ? ("") : (" selected=\"selected\""));
        echo ">";
        echo ($context["text_disabled"] ?? null);
        echo "</option>
\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t  <div class=\"row\">
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-payment\"><span title=\"";
        // line 459
        echo ($context["help_payment"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_payment"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<select name=\"quickcheckout_payment\" id=\"input-payment\" class=\"form-control\">
\t\t\t\t\t  <option value=\"1\"";
        // line 462
        echo ((($context["quickcheckout_payment"] ?? null)) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_radio_type"] ?? null);
        echo "</option>
\t\t\t\t\t  <option value=\"0\"";
        // line 463
        echo ((($context["quickcheckout_payment"] ?? null)) ? ("") : (" selected=\"selected\""));
        echo ">";
        echo ($context["text_select_type"] ?? null);
        echo "</option>
\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-payment-default\"><span title=\"";
        // line 468
        echo ($context["help_payment_default"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_payment_default"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<select name=\"quickcheckout_payment_default\" id=\"input-payment-default\" class=\"form-control\">
\t\t\t\t\t  ";
        // line 471
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["payment_modules"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["payment_module"]) {
            echo " 
\t\t\t\t\t  <option value=\"";
            // line 472
            echo twig_get_attribute($this->env, $this->source, $context["payment_module"], "code", [], "any", false, false, false, 472);
            echo "\"";
            echo (((($context["quickcheckout_payment_default"] ?? null) == twig_get_attribute($this->env, $this->source, $context["payment_module"], "code", [], "any", false, false, false, 472))) ? (" selected=\"selected\"") : (""));
            echo ">";
            echo twig_get_attribute($this->env, $this->source, $context["payment_module"], "name", [], "any", false, false, false, 472);
            echo "</option>
\t\t\t\t\t  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['payment_module'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 473
        echo " 
\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t  <table class=\"table table-bordered table-striped table-hover\">
\t\t\t    <tr>
\t\t\t\t  <th></th>
\t\t\t\t  <th><span title=\"";
        // line 481
        echo ($context["help_payment_logo"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_payment_logo"] ?? null);
        echo " <i class=\"fa fa-question-circle\"></i></span></th>
\t\t\t\t</tr>
\t\t\t\t";
        // line 483
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["payment_modules"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["payment_module"]) {
            // line 484
            echo "\t\t\t\t<tr>
\t\t\t\t  <td>";
            // line 485
            echo twig_get_attribute($this->env, $this->source, $context["payment_module"], "name", [], "any", false, false, false, 485);
            echo "</td>
\t\t\t\t  <td><input type=\"text\" name=\"quickcheckout_payment_logo[";
            // line 486
            echo twig_get_attribute($this->env, $this->source, $context["payment_module"], "code", [], "any", false, false, false, 486);
            echo "]\" value=\"";
            echo (($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = ($context["quickcheckout_payment_logo"] ?? null)) && is_array($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) || $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 instanceof ArrayAccess ? ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666[twig_get_attribute($this->env, $this->source, $context["payment_module"], "code", [], "any", false, false, false, 486)] ?? null) : null);
            echo "\" class=\"form-control\" /></td>
\t\t\t\t</tr>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['payment_module'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 488
        echo " 
\t\t\t\t<tr>
\t\t\t\t  <td colspan=\"2\" class=\"text-center\">";
        // line 490
        echo ($context["help_display_more"] ?? null);
        echo "</td>
\t\t\t\t</tr>
\t\t\t  </table>
\t\t\t</div>
\t\t\t<div class=\"tab-pane\" id=\"tab-shipping\">
\t\t\t  <div class=\"row\">
\t\t\t    <div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-shipping-module\"><span title=\"";
        // line 497
        echo ($context["help_shipping_module"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_shipping_module"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<select name=\"quickcheckout_shipping_module\" id=\"input-shipping-module\" class=\"form-control\">
\t\t\t\t\t  <option value=\"1\"";
        // line 500
        echo ((($context["quickcheckout_shipping_module"] ?? null)) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_enabled"] ?? null);
        echo "</option>
\t\t\t\t\t  <option value=\"0\"";
        // line 501
        echo ((($context["quickcheckout_shipping_module"] ?? null)) ? ("") : (" selected=\"selected\""));
        echo ">";
        echo ($context["text_disabled"] ?? null);
        echo "</option>
\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-shipping-reload\"><span title=\"";
        // line 506
        echo ($context["help_shipping_reload"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_shipping_reload"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<select name=\"quickcheckout_shipping_reload\" id=\"input-shipping-reload\" class=\"form-control\">
\t\t\t\t\t  <option value=\"1\"";
        // line 509
        echo ((($context["quickcheckout_shipping_reload"] ?? null)) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_enabled"] ?? null);
        echo "</option>
\t\t\t\t\t  <option value=\"0\"";
        // line 510
        echo ((($context["quickcheckout_shipping_reload"] ?? null)) ? ("") : (" selected=\"selected\""));
        echo ">";
        echo ($context["text_disabled"] ?? null);
        echo "</option>
\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t  <div class=\"row\">
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-shipping\"><span title=\"";
        // line 517
        echo ($context["help_shipping"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_shipping"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<select name=\"quickcheckout_shipping\" id=\"input-shipping\" class=\"form-control\">
\t\t\t\t\t  <option value=\"1\"";
        // line 520
        echo ((($context["quickcheckout_shipping"] ?? null)) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_radio_type"] ?? null);
        echo "</option>
\t\t\t\t\t  <option value=\"0\"";
        // line 521
        echo ((($context["quickcheckout_shipping"] ?? null)) ? ("") : (" selected=\"selected\""));
        echo ">";
        echo ($context["text_select_type"] ?? null);
        echo "</option>
\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-shipping-default\"><span title=\"";
        // line 526
        echo ($context["help_shipping_default"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_shipping_default"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<select name=\"quickcheckout_shipping_default\" id=\"input-shipping-default\" class=\"form-control\">
\t\t\t\t\t  ";
        // line 529
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["shipping_modules"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["shipping_module"]) {
            echo " 
\t\t\t\t\t  <option value=\"";
            // line 530
            echo twig_get_attribute($this->env, $this->source, $context["shipping_module"], "code", [], "any", false, false, false, 530);
            echo "\"";
            echo (((($context["quickcheckout_shipping_default"] ?? null) == twig_get_attribute($this->env, $this->source, $context["shipping_module"], "code", [], "any", false, false, false, 530))) ? (" selected=\"selected\"") : (""));
            echo ">";
            echo twig_get_attribute($this->env, $this->source, $context["shipping_module"], "name", [], "any", false, false, false, 530);
            echo "</option>
\t\t\t\t\t  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['shipping_module'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 531
        echo " 
\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t  <table class=\"table table-bordered table-striped table-hover\">
\t\t\t    <tr>
\t\t\t\t  <th></th>
\t\t\t\t  <th><span title=\"";
        // line 539
        echo ($context["help_shipping_logo"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_shipping_logo"] ?? null);
        echo " <i class=\"fa fa-question-circle\"></i></span></th>
\t\t\t\t</tr>
\t\t\t\t";
        // line 541
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["shipping_modules"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["shipping_module"]) {
            echo " 
\t\t\t\t<tr>
\t\t\t\t  <td>";
            // line 543
            echo twig_get_attribute($this->env, $this->source, $context["shipping_module"], "name", [], "any", false, false, false, 543);
            echo "</td>
\t\t\t\t  <td><input type=\"text\" name=\"quickcheckout_shipping_logo[";
            // line 544
            echo twig_get_attribute($this->env, $this->source, $context["shipping_module"], "code", [], "any", false, false, false, 544);
            echo "]\" value=\"";
            echo (($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = ($context["quickcheckout_shipping_logo"] ?? null)) && is_array($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e) || $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e instanceof ArrayAccess ? ($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e[twig_get_attribute($this->env, $this->source, $context["shipping_module"], "code", [], "any", false, false, false, 544)] ?? null) : null);
            echo "\" class=\"form-control\" /></td>
\t\t\t\t</tr>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['shipping_module'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 546
        echo " 
\t\t\t\t<tr>
\t\t\t\t  <td colspan=\"2\" class=\"text-center\">";
        // line 548
        echo ($context["help_display_more"] ?? null);
        echo "</td>
\t\t\t\t</tr>
\t\t\t  </table>
\t\t\t</div>
\t\t\t<div class=\"tab-pane\" id=\"tab-survey\">
\t\t\t  <div class=\"row\">
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-survey\">";
        // line 555
        echo ($context["entry_survey"] ?? null);
        echo "</label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<select name=\"quickcheckout_survey\" id=\"input-survey\" class=\"form-control\">
\t\t\t\t\t  <option value=\"1\"";
        // line 558
        echo ((($context["quickcheckout_survey"] ?? null)) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_enabled"] ?? null);
        echo "</option>
\t\t\t\t\t  <option value=\"0\"";
        // line 559
        echo ((($context["quickcheckout_survey"] ?? null)) ? ("") : (" selected=\"selected\""));
        echo ">";
        echo ($context["text_disabled"] ?? null);
        echo "</option>
\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-survey-required\"><span title=\"";
        // line 564
        echo ($context["help_survey_required"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_survey_required"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<select name=\"quickcheckout_survey_required\" id=\"input-survey-required\" class=\"form-control\">
\t\t\t\t\t  <option value=\"1\"";
        // line 567
        echo ((($context["quickcheckout_survey_required"] ?? null)) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_enabled"] ?? null);
        echo "</option>
\t\t\t\t\t  <option value=\"0\"";
        // line 568
        echo ((($context["quickcheckout_survey_required"] ?? null)) ? ("") : (" selected=\"selected\""));
        echo ">";
        echo ($context["text_disabled"] ?? null);
        echo "</option>
\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t  <div class=\"row\">
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-survey-text\"><span title=\"";
        // line 575
        echo ($context["help_survey_text"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_survey_text"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t";
        // line 577
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo " 
\t\t\t\t\t  <div class=\"input-group\">
\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 579
            echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 579);
            echo "/";
            echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 579);
            echo ".png\" title=\"";
            echo twig_get_attribute($this->env, $this->source, $context["language"], "name", [], "any", false, false, false, 579);
            echo "\" /></span>
\t\t\t\t\t\t<input type=\"text\" name=\"quickcheckout_survey_text[";
            // line 580
            echo twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 580);
            echo "]\" value=\"";
            echo (($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 = ($context["quickcheckout_survey_text"] ?? null)) && is_array($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52) || $__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52 instanceof ArrayAccess ? ($__internal_63ad1f9a2bf4db4af64b010785e9665558fdcac0e8db8b5b413ed986c62dbb52[twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 580)] ?? null) : null);
            echo "\" class=\"form-control\" />
\t\t\t\t\t  </div>
\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 582
        echo " 
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-survey-type\"><span title=\"";
        // line 586
        echo ($context["help_survey_type"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_survey_type"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<select name=\"quickcheckout_survey_type\" id=\"input-survey-type\" class=\"form-control\">
\t\t\t\t\t  <option value=\"1\"";
        // line 589
        echo ((($context["quickcheckout_survey_type"] ?? null)) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_select_type"] ?? null);
        echo "</option>
\t\t\t\t\t  <option value=\"0\"";
        // line 590
        echo ((($context["quickcheckout_survey_type"] ?? null)) ? ("") : (" selected=\"selected\""));
        echo ">";
        echo ($context["text_text_type"] ?? null);
        echo "</option>
\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t  <div class=\"table-responsive\">
\t\t\t\t<table id=\"survey-answer\" class=\"table table-hover table-bordered\">
\t\t\t\t  <thead>
\t\t\t\t  <tr>
\t\t\t\t\t<td class=\"text-left\" colspan=\"2\">";
        // line 599
        echo ($context["entry_survey_answer"] ?? null);
        echo "</td>
\t\t\t\t  </tr>
\t\t\t\t  </thead>
\t\t\t\t  <tbody>
\t\t\t\t  ";
        // line 603
        $context["survey_answer_row"] = 0;
        // line 604
        echo "\t\t\t\t  ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["quickcheckout_survey_answers"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["survey_answer"]) {
            echo " 
\t\t\t\t  <tr id=\"survey-answer-";
            // line 605
            echo ($context["survey_answer_row"] ?? null);
            echo "\">
\t\t\t\t\t<td class=\"text-left\">";
            // line 606
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                // line 607
                echo "\t\t\t\t\t  <div class=\"input-group\">
\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
                // line 608
                echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 608);
                echo "/";
                echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 608);
                echo ".png\" title=\"";
                echo twig_get_attribute($this->env, $this->source, $context["language"], "name", [], "any", false, false, false, 608);
                echo "\" /></span>
\t\t\t\t\t\t<input type=\"text-text\" name=\"quickcheckout_survey_answers[";
                // line 609
                echo ($context["survey_answer_row"] ?? null);
                echo "][";
                echo twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 609);
                echo "]\" value=\"";
                echo (($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 = $context["survey_answer"]) && is_array($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136) || $__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136 instanceof ArrayAccess ? ($__internal_f10a4cc339617934220127f034125576ed229e948660ebac906a15846d52f136[twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 609)] ?? null) : null);
                echo "\" class=\"form-control\" />
\t\t\t\t\t  </div>
\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 611
            echo "</td>
\t\t\t\t\t<td class=\"text-right\"><a class=\"btn btn-danger\" onClick=\"\$('#survey-answer-";
            // line 612
            echo ($context["survey_answer_row"] ?? null);
            echo "').remove();\">";
            echo ($context["button_remove"] ?? null);
            echo "</a></td>
\t\t\t\t\t";
            // line 613
            $context["survey_answer_row"] = (($context["survey_answer_row"] ?? null) + 1);
            echo " 
\t\t\t\t  </tr>
\t\t\t\t  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['survey_answer'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 615
        echo " 
\t\t\t\t  </tbody>
\t\t\t\t  <tfoot>
\t\t\t\t  <tr>
\t\t\t\t\t<td class=\"text-right\" colspan=\"2\"><a class=\"btn btn-success\" onClick=\"addAnswer();\">";
        // line 619
        echo ($context["button_add"] ?? null);
        echo "</a></td>
\t\t\t\t  </tr>
\t\t\t\t  </tfoot>
\t\t\t\t</table>
\t\t\t  </div>
\t\t\t</div>
\t\t\t<div class=\"tab-pane\" id=\"tab-delivery\">
\t\t\t  <div class=\"row\">
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-delivery\"><span title=\"";
        // line 628
        echo ($context["help_delivery"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_delivery"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<select name=\"quickcheckout_delivery\" id=\"input-delivery\" class=\"form-control\">
\t\t\t\t\t  <option value=\"1\"";
        // line 631
        echo ((($context["quickcheckout_delivery"] ?? null)) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_enabled"] ?? null);
        echo "</option>
\t\t\t\t\t  <option value=\"0\"";
        // line 632
        echo ((($context["quickcheckout_delivery"] ?? null)) ? ("") : (" selected=\"selected\""));
        echo ">";
        echo ($context["text_disabled"] ?? null);
        echo "</option>
\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-delivery-time\"><span title=\"";
        // line 637
        echo ($context["help_delivery_time"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_delivery_time"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<select name=\"quickcheckout_delivery_time\" id=\"input-delivery-time\" class=\"form-control\">
\t\t\t\t\t  <option value=\"1\"";
        // line 640
        echo (((($context["quickcheckout_delivery_time"] ?? null) == 1)) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_choose"] ?? null);
        echo "</option>
\t\t\t\t\t  <option value=\"2\"";
        // line 641
        echo (((($context["quickcheckout_delivery_time"] ?? null) == 2)) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_estimate"] ?? null);
        echo "</option>
\t\t\t\t\t  <option value=\"3\"";
        // line 642
        echo (((($context["quickcheckout_delivery_time"] ?? null) == 3)) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_select_type"] ?? null);
        echo "</option>
\t\t\t\t\t  <option value=\"0\"";
        // line 643
        echo (((($context["quickcheckout_delivery_time"] ?? null) == 0)) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_disabled"] ?? null);
        echo "</option>
\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t  <div class=\"row\">
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-delivery-required\"><span title=\"";
        // line 650
        echo ($context["help_delivery_required"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_delivery_required"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<select name=\"quickcheckout_delivery_required\" id=\"input-delivery-required\" class=\"form-control\">
\t\t\t\t\t  <option value=\"1\"";
        // line 653
        echo ((($context["quickcheckout_delivery_required"] ?? null)) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_enabled"] ?? null);
        echo "</option>
\t\t\t\t\t  <option value=\"0\"";
        // line 654
        echo ((($context["quickcheckout_delivery_required"] ?? null)) ? ("") : (" selected=\"selected\""));
        echo ">";
        echo ($context["text_disabled"] ?? null);
        echo "</option>
\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-delivery-unavailable\"><span title=\"";
        // line 659
        echo ($context["help_delivery_unavailable"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_delivery_unavailable"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<textarea name=\"quickcheckout_delivery_unavailable\" rows=\"5\" class=\"form-control\">";
        // line 661
        echo ($context["quickcheckout_delivery_unavailable"] ?? null);
        echo "</textarea>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t  <div class=\"row\">
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-delivery-min\"><span title=\"";
        // line 667
        echo ($context["help_delivery_min"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_delivery_min"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<input type=\"text\" name=\"quickcheckout_delivery_min\" value=\"";
        // line 669
        echo ($context["quickcheckout_delivery_min"] ?? null);
        echo "\" class=\"form-control\" />
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-delivery-max\"><span title=\"";
        // line 673
        echo ($context["help_delivery_max"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_delivery_max"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<input type=\"text\" name=\"quickcheckout_delivery_max\" value=\"";
        // line 675
        echo ($context["quickcheckout_delivery_max"] ?? null);
        echo "\" class=\"form-control\" />
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t  <div class=\"row\">
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-delivery-min-hour\"><span title=\"";
        // line 681
        echo ($context["help_delivery_min_hour"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_delivery_min_hour"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<input type=\"text\" name=\"quickcheckout_delivery_min_hour\" value=\"";
        // line 683
        echo ($context["quickcheckout_delivery_min_hour"] ?? null);
        echo "\" class=\"form-control\" />
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-delivery-max-hour\"><span title=\"";
        // line 687
        echo ($context["help_delivery_max_hour"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_delivery_max_hour"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<input type=\"text\" name=\"quickcheckout_delivery_max_hour\" value=\"";
        // line 689
        echo ($context["quickcheckout_delivery_max_hour"] ?? null);
        echo "\" class=\"form-control\" />
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t  <div class=\"row\">
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-delivery-days-of-week\"><span title=\"";
        // line 695
        echo ($context["help_delivery_days_of_week"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_delivery_days_of_week"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<input type=\"text\" name=\"quickcheckout_delivery_days_of_week\" value=\"";
        // line 697
        echo ($context["quickcheckout_delivery_days_of_week"] ?? null);
        echo "\" class=\"form-control\" />
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t  <div class=\"table-responsive\">
\t\t\t\t<table id=\"delivery-time\" class=\"table table-bordered table-hover\">
\t\t\t\t  <thead>
\t\t\t\t  <tr>
\t\t\t\t\t<td class=\"text-left\" colspan=\"2\">";
        // line 705
        echo ($context["entry_delivery_times"] ?? null);
        echo "</td>
\t\t\t\t  </tr>
\t\t\t\t  </thead>
\t\t\t\t  <tbody>
\t\t\t\t  ";
        // line 709
        $context["delivery_time_row"] = 0;
        // line 710
        echo "\t\t\t\t  ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["quickcheckout_delivery_times"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["delivery_time"]) {
            echo " 
\t\t\t\t  <tr id=\"delivery-time-";
            // line 711
            echo ($context["delivery_time_row"] ?? null);
            echo "\">
\t\t\t\t    <td class=\"text-left\">";
            // line 712
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                echo " 
\t\t\t\t\t  <div class=\"input-group\">
\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
                // line 714
                echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 714);
                echo "/";
                echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 714);
                echo ".png\" title=\"";
                echo twig_get_attribute($this->env, $this->source, $context["language"], "name", [], "any", false, false, false, 714);
                echo "\" /></span>
\t\t\t\t\t\t<input type=\"text\" name=\"quickcheckout_delivery_times[";
                // line 715
                echo ($context["delivery_time_row"] ?? null);
                echo "][";
                echo twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 715);
                echo "]\" value=\"";
                echo (($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 = $context["delivery_time"]) && is_array($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386) || $__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386 instanceof ArrayAccess ? ($__internal_887a873a4dc3cf8bd4f99c487b4c7727999c350cc3a772414714e49a195e4386[twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 715)] ?? null) : null);
                echo "\" class=\"form-control\" />
\t\t\t\t\t  </div>
\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 717
            echo "</td>
\t\t\t\t\t<td class=\"text-right\"><a class=\"btn btn-danger\" onClick=\"\$('#delivery-time-";
            // line 718
            echo ($context["delivery_time_row"] ?? null);
            echo "').remove();\">";
            echo ($context["button_remove"] ?? null);
            echo "</a></td>
\t\t\t\t\t";
            // line 719
            $context["delivery_time_row"] = (($context["delivery_time_row"] ?? null) + 1);
            echo " 
\t\t\t\t  </tr>
\t\t\t\t  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['delivery_time'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 721
        echo " 
\t\t\t\t  </tbody>
\t\t\t\t  <tfoot>
\t\t\t\t  <tr>
\t\t\t\t    <td class=\"text-right\" colspan=\"2\"><a class=\"btn btn-success\" onClick=\"addTime();\">";
        // line 725
        echo ($context["button_add"] ?? null);
        echo "</a></td>
\t\t\t\t  </tr>
\t\t\t\t  </tfoot>
\t\t\t\t</table>
\t\t\t  </div>
\t\t\t</div>
\t\t\t<div class=\"tab-pane\" id=\"tab-countdown\">
\t\t\t  <div class=\"row\">
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-countdown\"><span title=\"";
        // line 734
        echo ($context["help_countdown"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_countdown"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<select name=\"quickcheckout_countdown\" id=\"input-countdown\" class=\"form-control\">
\t\t\t\t\t  <option value=\"1\"";
        // line 737
        echo ((($context["quickcheckout_countdown"] ?? null)) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_enabled"] ?? null);
        echo "</option>
\t\t\t\t\t  <option value=\"0\"";
        // line 738
        echo ((($context["quickcheckout_countdown"] ?? null)) ? ("") : (" selected=\"selected\""));
        echo ">";
        echo ($context["text_disabled"] ?? null);
        echo "</option>
\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-countdown-start\"><span title=\"";
        // line 743
        echo ($context["help_countdown_start"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_countdown_start"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<select name=\"quickcheckout_countdown_start\" id=\"input-countdown-start\" class=\"form-control\">
\t\t\t\t\t  <option value=\"1\"";
        // line 746
        echo ((($context["quickcheckout_countdown_start"] ?? null)) ? (" selected=\"selected\"") : (""));
        echo ">";
        echo ($context["text_day"] ?? null);
        echo "</option>
\t\t\t\t\t  <option value=\"0\"";
        // line 747
        echo ((($context["quickcheckout_countdown_start"] ?? null)) ? ("") : (" selected=\"selected\""));
        echo ">";
        echo ($context["text_specific"] ?? null);
        echo "</option>
\t\t\t\t\t</select>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t  <div class=\"row\" id=\"countdown-date\">
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-countdown-date-start\"><span title=\"";
        // line 754
        echo ($context["help_countdown_date_start"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_countdown_date_start"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<input type=\"text\" name=\"quickcheckout_countdown_date_start\" value=\"";
        // line 756
        echo ($context["quickcheckout_countdown_date_start"] ?? null);
        echo "\" class=\"date form-control\" data-date-format=\"YYYY-MM-DD HH:mm:ss\" />
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group col-sm-6\">
\t\t\t\t  <label class=\"col-sm-4 control-label\" for=\"input-countdown-date-end\"><span title=\"";
        // line 760
        echo ($context["help_countdown_date_end"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_countdown_date_end"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-8\">
\t\t\t\t\t<input type=\"text\" name=\"quickcheckout_countdown_date_end\" value=\"";
        // line 762
        echo ($context["quickcheckout_countdown_date_end"] ?? null);
        echo "\" class=\"date form-control\" data-date-format=\"YYYY-MM-DD HH:mm:ss\" />
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t  <div class=\"row\" id=\"countdown-time\">
\t\t\t\t<div class=\"form-group col-sm-12\">
\t\t\t\t  <label class=\"col-sm-2 control-label\" for=\"input-countdown-time\"><span title=\"";
        // line 768
        echo ($context["help_countdown_time"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_countdown_time"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-10\">
\t\t\t\t\t<input type=\"text\" name=\"quickcheckout_countdown_time\" value=\"";
        // line 770
        echo ($context["quickcheckout_countdown_time"] ?? null);
        echo "\" class=\"form-control\" />
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t  <div class=\"row\">
\t\t\t\t<div class=\"form-group col-sm-12\">
\t\t\t\t  <label class=\"col-sm-2 control-label\" for=\"input-countdown-text\"><span title=\"";
        // line 776
        echo ($context["help_countdown_text"] ?? null);
        echo "\" data-toggle=\"tooltip\">";
        echo ($context["entry_countdown_text"] ?? null);
        echo "</span></label>
\t\t\t\t  <div class=\"col-sm-10\">
\t\t\t\t\t";
        // line 778
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo " 
\t\t\t\t\t  <div class=\"input-group\">
\t\t\t\t\t    <span class=\"input-group-addon\"><img src=\"language/";
            // line 780
            echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 780);
            echo "/";
            echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 780);
            echo ".png\" title=\"";
            echo twig_get_attribute($this->env, $this->source, $context["language"], "name", [], "any", false, false, false, 780);
            echo "\" /></span>
\t\t\t\t\t\t<textarea name=\"quickcheckout_countdown_text[";
            // line 781
            echo twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 781);
            echo "]\" class=\"form-control\" rows=\"5\">";
            echo (($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 = ($context["quickcheckout_countdown_text"] ?? null)) && is_array($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9) || $__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9 instanceof ArrayAccess ? ($__internal_d527c24a729d38501d770b40a0d25e1ce8a7f0bff897cc4f8f449ba71fcff3d9[twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 781)] ?? null) : null);
            echo "</textarea>
\t\t\t\t\t  </div>
\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 783
        echo " 
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t</div>
\t\t\t<div class=\"tab-pane text-center\" id=\"tab-analytics\">
\t\t\t  ";
        // line 789
        if (($context["analytics"] ?? null)) {
            echo " 
\t\t\t  <a href=\"";
            // line 790
            echo ($context["analytics"] ?? null);
            echo "\" class=\"btn btn-lg btn-success\">Recover Abandoned Cart</a>
\t\t\t  ";
        } else {
            // line 791
            echo "   
\t\t\t  <p>";
            // line 792
            echo ($context["text_purchase_analytics"] ?? null);
            echo "</p>
\t\t\t  <a href=\"https://www.marketinsg.com/recover-abandoned-carts\" class=\"btn btn-lg btn-warning\" target=\"_blank\">More Information</a>
\t\t\t  ";
        }
        // line 795
        echo "\t\t\t</div>
\t\t  </div>
\t\t</form>
      </div>
    </div>
  </div>
</div>
<style type=\"text/css\">
.form-group + .form-group {
\tborder: none;
}
</style>
<script type=\"text/javascript\"><!--
function show(element) {
\t\$(element).tab('show');
\t
\t\$('a[href=\\'' + element + '\\']').parent('li').siblings().removeClass('active');
\t
\t\$('a[href=\\'' + element + '\\']').parent('li').addClass('active');
\t
\treturn false;
}

\$(document).ready(function() {
\t\$('.date').datetimepicker();
});

\$('select[name=\\'quickcheckout_field_country[default]\\']').on('change', function() {
\t\$.ajax({
\t\turl: 'index.php?route=extension/module/quickcheckout/country";
        // line 824
        echo ($context["equotix_token"] ?? null);
        echo "&country_id=' + this.value,
\t\tdataType: 'json',\t\t
\t\tsuccess: function(json) {
\t\t\thtml = '<option value=\"\">";
        // line 827
        echo ($context["text_select"] ?? null);
        echo "</option>';
\t\t\t
\t\t\tif (json['zone'] != '') {
\t\t\t\tfor (i = 0; i < json['zone'].length; i++) {
        \t\t\thtml += '<option value=\"' + json['zone'][i]['zone_id'] + '\"';
\t    \t\t\t
\t\t\t\t\tif (json['zone'][i]['zone_id'] == '";
        // line 833
        echo twig_get_attribute($this->env, $this->source, ($context["quickcheckout_field_zone"] ?? null), "default", [], "any", false, false, false, 833);
        echo "') {
\t      \t\t\t\thtml += ' selected=\"selected\"';
\t    \t\t\t}
\t
\t    \t\t\thtml += '>' + json['zone'][i]['name'] + '</option>';
\t\t\t\t}
\t\t\t} else {
\t\t\t\thtml += '<option value=\"0\" selected=\"selected\">";
        // line 840
        echo ($context["text_none"] ?? null);
        echo "</option>';
\t\t\t}
\t\t\t
\t\t\t\$('select[name=\\'quickcheckout_field_zone[default]\\']').html(html);
\t\t}
\t});
});

\$('select[name=\\'quickcheckout_field_country[default]\\']').trigger('change');

\$('select[name=\\'quickcheckout_countdown_start\\']').change(function() {
\tif (\$('select[name=\\'quickcheckout_countdown_start\\']').val() == '1') {
\t\t\$('#countdown-time').fadeIn();
\t\t\$('#countdown-date').fadeOut();
\t} else {
\t\t\$('#countdown-date').fadeIn();
\t\t\$('#countdown-time').fadeOut();
\t}
});

\$('select[name=\\'quickcheckout_countdown_start\\']').trigger('change');

\$('select[name=\\'quickcheckout_survey_type\\']').change(function() {
\tif (\$('select[name=\\'quickcheckout_survey_type\\']').val() == '1') {
\t\t\$('#survey-answer').fadeIn();
\t} else {
\t\t\$('#survey-answer').fadeOut();
\t}
});

\$('select[name=\\'quickcheckout_survey_type\\']').trigger('change');

var survey_answer_row = ";
        // line 872
        echo ($context["survey_answer_row"] ?? null);
        echo " ;

function addAnswer() {
\thtml  = '<tr id=\"survey-answer-' + survey_answer_row + '\">';
\thtml += '  <td class=\"left\">';
\t";
        // line 877
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo " 
\thtml += '<div class=\"input-group\"><span class=\"input-group-addon\"><img src=\"language/";
            // line 878
            echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 878);
            echo "/";
            echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 878);
            echo ".png\" title=\"";
            echo twig_get_attribute($this->env, $this->source, $context["language"], "name", [], "any", false, false, false, 878);
            echo "\" /></span>';
\thtml += '<input type=\"text-text\" name=\"quickcheckout_survey_answers[' + survey_answer_row + '][";
            // line 879
            echo twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 879);
            echo "]\" value=\"\" class=\"form-control\" />';
\thtml += '</div>';
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 881
        echo " 
\thtml += '  </td>';
\thtml += '  <td class=\"text-right\"><a class=\"btn btn-danger\" onClick=\"\$(\\'#survey-answer-' + survey_answer_row + '\\').remove();\">";
        // line 883
        echo ($context["button_remove"] ?? null);
        echo "</a></td>';
\thtml += '</tr>';
\t
\t\$('#survey-answer tbody').append(html);
\t
\tsurvey_answer_row++;
}

\$('select[name=\\'quickcheckout_delivery_time\\']').change(function() {
\tif (\$('select[name=\\'quickcheckout_delivery_time\\']').val() == '3') {
\t\t\$('#delivery-time').fadeIn();
\t} else {
\t\t\$('#delivery-time').fadeOut();
\t}
});

\$('select[name=\\'quickcheckout_delivery_time\\']').trigger('change');

var delivery_time_row = ";
        // line 901
        echo ($context["delivery_time_row"] ?? null);
        echo " ;

function addTime() {
\thtml  = '<tr id=\"delivery-time-' + delivery_time_row + '\">';
\thtml += '  <td class=\"left\">';
\t";
        // line 906
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo " 
\thtml += '<div class=\"input-group\"><span class=\"input-group-addon\"><img src=\"language/";
            // line 907
            echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 907);
            echo "/";
            echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 907);
            echo ".png\" title=\"";
            echo twig_get_attribute($this->env, $this->source, $context["language"], "name", [], "any", false, false, false, 907);
            echo "\" /></span>';
\thtml += '<input type=\"text\" name=\"quickcheckout_delivery_times[' + delivery_time_row + '][";
            // line 908
            echo twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 908);
            echo "]\" value=\"\" class=\"form-control\" />';
\thtml += '</div>';
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 910
        echo " 
\thtml += '  </td>';
\thtml += '  <td class=\"text-right\"><a class=\"btn btn-danger\" onClick=\"\$(\\'#delivery-time-' + delivery_time_row + '\\').remove();\">";
        // line 912
        echo ($context["button_remove"] ?? null);
        echo "</a></td>';
\thtml += '</tr>';
\t
\t\$('#delivery-time tbody').append(html);
\t
\tdelivery_time_row++;
}

function store() {
\tlocation = 'index.php?route=extension/module/quickcheckout";
        // line 921
        echo ($context["equotix_token"] ?? null);
        echo "&store_id=' + \$('select[name=\\'store_id\\']').val();
}
//--></script>
";
        // line 924
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "extension/module/quickcheckout.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  2357 => 924,  2351 => 921,  2339 => 912,  2335 => 910,  2326 => 908,  2318 => 907,  2312 => 906,  2304 => 901,  2283 => 883,  2279 => 881,  2270 => 879,  2262 => 878,  2256 => 877,  2248 => 872,  2213 => 840,  2203 => 833,  2194 => 827,  2188 => 824,  2157 => 795,  2151 => 792,  2148 => 791,  2143 => 790,  2139 => 789,  2131 => 783,  2120 => 781,  2112 => 780,  2105 => 778,  2098 => 776,  2089 => 770,  2082 => 768,  2073 => 762,  2066 => 760,  2059 => 756,  2052 => 754,  2040 => 747,  2034 => 746,  2026 => 743,  2016 => 738,  2010 => 737,  2002 => 734,  1990 => 725,  1984 => 721,  1975 => 719,  1969 => 718,  1966 => 717,  1953 => 715,  1945 => 714,  1938 => 712,  1934 => 711,  1927 => 710,  1925 => 709,  1918 => 705,  1907 => 697,  1900 => 695,  1891 => 689,  1884 => 687,  1877 => 683,  1870 => 681,  1861 => 675,  1854 => 673,  1847 => 669,  1840 => 667,  1831 => 661,  1824 => 659,  1814 => 654,  1808 => 653,  1800 => 650,  1788 => 643,  1782 => 642,  1776 => 641,  1770 => 640,  1762 => 637,  1752 => 632,  1746 => 631,  1738 => 628,  1726 => 619,  1720 => 615,  1711 => 613,  1705 => 612,  1702 => 611,  1689 => 609,  1681 => 608,  1678 => 607,  1674 => 606,  1670 => 605,  1663 => 604,  1661 => 603,  1654 => 599,  1640 => 590,  1634 => 589,  1626 => 586,  1620 => 582,  1609 => 580,  1601 => 579,  1594 => 577,  1587 => 575,  1575 => 568,  1569 => 567,  1561 => 564,  1551 => 559,  1545 => 558,  1539 => 555,  1529 => 548,  1525 => 546,  1514 => 544,  1510 => 543,  1503 => 541,  1496 => 539,  1486 => 531,  1474 => 530,  1468 => 529,  1460 => 526,  1450 => 521,  1444 => 520,  1436 => 517,  1424 => 510,  1418 => 509,  1410 => 506,  1400 => 501,  1394 => 500,  1386 => 497,  1376 => 490,  1372 => 488,  1361 => 486,  1357 => 485,  1354 => 484,  1350 => 483,  1343 => 481,  1333 => 473,  1321 => 472,  1315 => 471,  1307 => 468,  1297 => 463,  1291 => 462,  1283 => 459,  1271 => 452,  1265 => 451,  1257 => 448,  1247 => 443,  1241 => 442,  1233 => 439,  1223 => 431,  1212 => 429,  1204 => 428,  1197 => 426,  1190 => 424,  1184 => 420,  1173 => 418,  1165 => 417,  1158 => 415,  1151 => 413,  1139 => 406,  1133 => 405,  1125 => 402,  1113 => 395,  1107 => 394,  1099 => 391,  1089 => 386,  1083 => 385,  1075 => 382,  1063 => 375,  1057 => 374,  1049 => 371,  1039 => 366,  1033 => 365,  1025 => 362,  1018 => 357,  1012 => 356,  1008 => 354,  1001 => 352,  998 => 351,  992 => 349,  989 => 348,  976 => 345,  968 => 344,  961 => 342,  957 => 340,  944 => 337,  936 => 336,  929 => 334,  926 => 333,  919 => 331,  916 => 330,  910 => 328,  904 => 327,  900 => 326,  896 => 324,  888 => 322,  881 => 320,  875 => 319,  869 => 318,  865 => 317,  860 => 315,  853 => 313,  844 => 309,  840 => 308,  837 => 307,  835 => 306,  828 => 304,  823 => 302,  817 => 301,  811 => 300,  807 => 299,  802 => 297,  795 => 295,  791 => 293,  785 => 292,  778 => 290,  775 => 289,  767 => 288,  765 => 287,  759 => 286,  755 => 285,  751 => 284,  745 => 283,  739 => 282,  735 => 281,  730 => 279,  724 => 278,  719 => 276,  715 => 275,  711 => 274,  707 => 273,  703 => 272,  691 => 263,  686 => 261,  676 => 256,  670 => 255,  662 => 252,  650 => 245,  642 => 242,  632 => 237,  626 => 236,  620 => 235,  612 => 232,  600 => 225,  594 => 224,  586 => 221,  576 => 216,  570 => 215,  562 => 212,  552 => 204,  541 => 202,  533 => 201,  526 => 199,  519 => 197,  510 => 191,  503 => 189,  493 => 184,  487 => 183,  479 => 180,  467 => 173,  461 => 172,  453 => 169,  443 => 164,  437 => 163,  429 => 160,  417 => 153,  411 => 152,  403 => 149,  393 => 144,  387 => 143,  379 => 140,  367 => 133,  361 => 132,  353 => 129,  343 => 124,  337 => 123,  329 => 120,  320 => 114,  313 => 112,  303 => 107,  297 => 106,  289 => 103,  276 => 93,  268 => 88,  262 => 85,  256 => 82,  248 => 77,  242 => 74,  236 => 71,  228 => 66,  222 => 63,  216 => 60,  207 => 54,  203 => 53,  199 => 52,  195 => 51,  191 => 50,  187 => 49,  183 => 48,  179 => 47,  175 => 46,  171 => 45,  167 => 44,  162 => 42,  156 => 39,  149 => 34,  137 => 33,  131 => 32,  125 => 31,  120 => 29,  116 => 27,  109 => 24,  105 => 23,  102 => 22,  95 => 19,  91 => 18,  84 => 13,  74 => 12,  68 => 11,  63 => 9,  57 => 8,  51 => 7,  47 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "extension/module/quickcheckout.twig", "");
    }
}
