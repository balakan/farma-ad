<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/payment/wspay.twig */
class __TwigTemplate_9071f34954781a0c8ccfae85956558bc1277d00dd91c6d0be236cfa0cd49aba1 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
 <ul class=\"breadcrumb\">
       ";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 5
            echo "    <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 5);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 5);
            echo "</a></li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 7
        echo "  </ul>
    <div class=\"page-header\">
        <div class=\"container-fluid\">
             <div class=\"pull-right\">
        <button type=\"submit\" form=\"form-payment\" data-toggle=\"tooltip\" title=\"";
        // line 11
        echo ($context["button_save"] ?? null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
        <a href=\"";
        // line 12
        echo ($context["cancel"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_cancel"] ?? null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
          <h1><i class=\"fa fa-credit-card\"></i> ";
        // line 13
        echo ($context["heading_title"] ?? null);
        echo "</h1>
          
        </div>
    </div>


    <div class=\"container-fluid\">
            <div class=\"panel-body\">
                <form action=\"";
        // line 21
        echo ($context["action"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-payment\" class=\"form-horizontal\">
                    <div class=\"form-group required\">
                        <label class=\"col-sm-2 control-label\" for=\"input-merchant\">";
        // line 23
        echo ($context["entry_merchant"] ?? null);
        echo "</label>
                        <div class=\"col-sm-10\">
                            <input type=\"text\" name=\"payment_wspay_merchant\" value=\"";
        // line 25
        echo ($context["payment_wspay_merchant"] ?? null);
        echo " \" placeholder=\"";
        echo ($context["entry_merchant"] ?? null);
        echo "\" id=\"input-merchant\" class=\"form-control\" />
                           ";
        // line 26
        if (($context["error_merchant"] ?? null)) {
            // line 27
            echo "                            <span class=\"error\">";
            echo ($context["error_merchant"] ?? null);
            echo "</span>
                            ";
        }
        // line 28
        echo " 
                        </div>
                    </div>
                    <div class=\"form-group required\">
                        <label class=\"col-sm-2 control-label\" for=\"input-password\">";
        // line 32
        echo ($context["entry_password"] ?? null);
        echo "</label>
                        <div class=\"col-sm-10\">
                            <input type=\"text\" name=\"payment_wspay_password\" value=\"";
        // line 34
        echo ($context["payment_wspay_password"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_password"] ?? null);
        echo "\" id=\"input-password\" class=\"form-control\" />
                             ";
        // line 35
        if (($context["error_password"] ?? null)) {
            // line 36
            echo "                            <span class=\"error\"> ";
            echo ($context["error_password"] ?? null);
            echo "</span>
                          ";
        }
        // line 37
        echo " 
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <label class=\"col-sm-2 control-label\" for=\"input-authorisationtype\">";
        // line 41
        echo ($context["entry_authorisationtype"] ?? null);
        echo "</label>
                        <div class=\"col-sm-10\">
                            <select name=\"payment_wspay_authorisationtype\" id=\"input-authorisationtype\" class=\"form-control\">
                                ";
        // line 44
        if (($context["payment_wspay_authorisationtype"] ?? null)) {
            // line 45
            echo "                                <option value=\"1\" selected=\"selected\">";
            echo ($context["entry_authorisationtype1"] ?? null);
            echo "</option>
                                <option value=\"0\">";
            // line 46
            echo ($context["entry_authorisationtype0"] ?? null);
            echo "</option>
                                 ";
        } else {
            // line 48
            echo "                                <option value=\"1\">";
            echo ($context["entry_authorisationtype1"] ?? null);
            echo "</option>
                                <option value=\"0\" selected=\"selected\">";
            // line 49
            echo ($context["entry_authorisationtype0"] ?? null);
            echo "</option>
                                ";
        }
        // line 50
        echo " 
                            </select>
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <label class=\"col-sm-2 control-label\" for=\"input-callback\"><span data-toggle=\"tooltip\" title=\"";
        // line 55
        echo ($context["help_entry_callback"] ?? null);
        echo "\">";
        echo ($context["entry_callback"] ?? null);
        echo "</span></label>
                        <div class=\"col-sm-10\">
                            <input type=\"text\" name=\"callback\" value=\"";
        // line 57
        echo ($context["callback"] ?? null);
        echo "\" id=\"input-callback\" class=\"form-control\" />
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <label class=\"col-sm-2 control-label\" for=\"input-test\">";
        // line 61
        echo ($context["entry_test"] ?? null);
        echo "</label>
                        <div class=\"col-sm-10\">
                            <select name=\"payment_wspay_test\" id=\"input-test\" class=\"form-control\">
                                ";
        // line 64
        if ((($context["payment_wspay_test"] ?? null) == "0")) {
            // line 65
            echo "                                <option value=\"0\" selected=\"selected\">";
            echo ($context["text_off"] ?? null);
            echo "</option>
                               ";
        } else {
            // line 67
            echo "                                <option value=\"0\">";
            echo ($context["text_off"] ?? null);
            echo "</option>
                                ";
        }
        // line 68
        echo " 
                                ";
        // line 69
        if ((($context["payment_wspay_test"] ?? null) == "100")) {
            // line 70
            echo "                                <option value=\"100\" selected=\"selected\">";
            echo ($context["text_successful"] ?? null);
            echo "</option>
                               ";
        } else {
            // line 72
            echo "                                <option value=\"100\">";
            echo ($context["text_successful"] ?? null);
            echo "</option>
                                 ";
        }
        // line 73
        echo " 
                            </select>
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <label class=\"col-sm-2 control-label\" for=\"input-total\"><span data-toggle=\"tooltip\" title=\"";
        // line 78
        echo ($context["help_entry_total"] ?? null);
        echo "\">";
        echo ($context["entry_total"] ?? null);
        echo "</span></label>
                        <div class=\"col-sm-10\">
                            <input type=\"text\" name=\"payment_wspay_total\" value=\"";
        // line 80
        echo ($context["payment_wspay_total"] ?? null);
        echo "\" id=\"input-total\" class=\"form-control\" />
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <label class=\"col-sm-2 control-label\" for=\"input-order-status\">";
        // line 84
        echo ($context["entry_order_status"] ?? null);
        echo "</label>
                        <div class=\"col-sm-10\">
                            <select name=\"payment_wspay_order_status_id\" id=\"input-order-status\" class=\"form-control\">
                                ";
        // line 87
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["order_statuses"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["order_status"]) {
            // line 88
            echo "                                  ";
            if ((twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 88) == ($context["payment_wspay_order_status_id"] ?? null))) {
                // line 89
                echo "                                <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 89);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 89);
                echo "</option>
                              ";
            } else {
                // line 91
                echo "                                <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 91);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 91);
                echo "</option>
                            ";
            }
            // line 93
            echo "                             ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order_status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 94
        echo "                            </select>
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <label class=\"col-sm-2 control-label\" for=\"input-geo-zone\">";
        // line 98
        echo ($context["entry_geo_zone"] ?? null);
        echo "</label>
                        <div class=\"col-sm-10\">
                            <select name=\"payment_wspay_geo_zone_id\" id=\"input-geo-zone\" class=\"form-control\">
                                <option value=\"0\">";
        // line 101
        echo ($context["text_all_zones"] ?? null);
        echo "</option>
                                 ";
        // line 102
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["geo_zones"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["geo_zone"]) {
            // line 103
            echo "                                  ";
            if ((twig_get_attribute($this->env, $this->source, $context["geo_zone"], "geo_zone_id", [], "any", false, false, false, 103) == ($context["payment_wspay_geo_zone_id"] ?? null))) {
                // line 104
                echo "                                <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["geo_zone"], "geo_zone_id", [], "any", false, false, false, 104);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["geo_zone"], "name", [], "any", false, false, false, 104);
                echo "</option>
                                  ";
            } else {
                // line 106
                echo "                                <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["geo_zone"], "geo_zone_id", [], "any", false, false, false, 106);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["geo_zone"], "name", [], "any", false, false, false, 106);
                echo "</option>
                              ";
            }
            // line 108
            echo "                             ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['geo_zone'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 109
        echo "                            </select>
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <label class=\"col-sm-2 control-label\" for=\"input-status\">";
        // line 113
        echo ($context["entry_status"] ?? null);
        echo "</label>
                        <div class=\"col-sm-10\">
                            <select name=\"payment_wspay_status\" id=\"input-status\" class=\"form-control\">
                                 ";
        // line 116
        if (($context["payment_wspay_status"] ?? null)) {
            // line 117
            echo "                                <option value=\"1\" selected=\"selected\">";
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                                <option value=\"0\">";
            // line 118
            echo ($context["text_disabled"] ?? null);
            echo "</option>
                                ";
        } else {
            // line 120
            echo "                                <option value=\"1\">";
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                                <option value=\"0\" selected=\"selected\">";
            // line 121
            echo ($context["text_disabled"] ?? null);
            echo "</option>
                                ";
        }
        // line 123
        echo "                            </select>
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <label class=\"col-sm-2 control-label\" for=\"input-sort\">";
        // line 127
        echo ($context["entry_sort_order"] ?? null);
        echo "</label>
                        <div class=\"col-sm-10\">
                            <input type=\"text\" name=\"payment_wspay_sort_order\" value=\"";
        // line 129
        echo ($context["payment_wspay_sort_order"] ?? null);
        echo "\" id=\"input-sort\" class=\"form-control\" />
                        </div>
                    </div>
                </form>
            </div>
     
    </div>
</div>
";
        // line 137
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "extension/payment/wspay.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  384 => 137,  373 => 129,  368 => 127,  362 => 123,  357 => 121,  352 => 120,  347 => 118,  342 => 117,  340 => 116,  334 => 113,  328 => 109,  322 => 108,  314 => 106,  306 => 104,  303 => 103,  299 => 102,  295 => 101,  289 => 98,  283 => 94,  277 => 93,  269 => 91,  261 => 89,  258 => 88,  254 => 87,  248 => 84,  241 => 80,  234 => 78,  227 => 73,  221 => 72,  215 => 70,  213 => 69,  210 => 68,  204 => 67,  198 => 65,  196 => 64,  190 => 61,  183 => 57,  176 => 55,  169 => 50,  164 => 49,  159 => 48,  154 => 46,  149 => 45,  147 => 44,  141 => 41,  135 => 37,  129 => 36,  127 => 35,  121 => 34,  116 => 32,  110 => 28,  104 => 27,  102 => 26,  96 => 25,  91 => 23,  86 => 21,  75 => 13,  69 => 12,  65 => 11,  59 => 7,  48 => 5,  44 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "extension/payment/wspay.twig", "");
    }
}
