<?php
// HTTP
define('HTTP_SERVER', 'http://kaonekad.agm/admin/');
define('HTTP_CATALOG', 'http://kaonekad.agm/');

// HTTPS
define('HTTPS_SERVER', 'http://kaonekad.agm/admin/');
define('HTTPS_CATALOG', 'http://kaonekad.agm/');

// DIR
define('DIR_APPLICATION', '/Users/alive/Sites/Agmedia/Live/kaonekad/upload/admin/');
define('DIR_SYSTEM', '/Users/alive/Sites/Agmedia/Live/kaonekad/upload/system/');
define('DIR_IMAGE', '/Users/alive/Sites/Agmedia/Live/kaonekad/upload/image/');
define('DIR_STORAGE', '/Users/alive/Sites/Agmedia/Live/kaonekad/storage/');
define('DIR_CATALOG', '/Users/alive/Sites/Agmedia/Live/kaonekad/upload/catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'Filip1314#');
define('DB_DATABASE', 'kaonekad');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');
